import {
  Event,
  Livestream,
  Shadow,
  Webrtc,
  Rule
} from "../index"

describe("protobuf", () => {
  it("index", () => {
    expect(Event).toMatchSnapshot();
    expect(Livestream).toMatchSnapshot();
    expect(Shadow).toMatchSnapshot();
    expect(Webrtc).toMatchSnapshot();
    expect(Rule).toMatchSnapshot();
  });
});
