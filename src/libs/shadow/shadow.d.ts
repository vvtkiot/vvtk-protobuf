import * as $protobuf from "protobufjs";
/** Namespace Shadow. */
export namespace Shadow {

    /** Properties of a Msg. */
    interface IMsg {

        /** Msg state */
        state?: (Shadow.IstateMsg|null);
    }

    /** Represents a Msg. */
    class Msg implements IMsg {

        /**
         * Constructs a new Msg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IMsg);

        /** Msg state. */
        public state?: (Shadow.IstateMsg|null);

        /**
         * Creates a new Msg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Msg instance
         */
        public static create(properties?: Shadow.IMsg): Shadow.Msg;

        /**
         * Encodes the specified Msg message. Does not implicitly {@link Shadow.Msg.verify|verify} messages.
         * @param message Msg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Msg message, length delimited. Does not implicitly {@link Shadow.Msg.verify|verify} messages.
         * @param message Msg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Msg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.Msg;

        /**
         * Decodes a Msg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.Msg;

        /**
         * Verifies a Msg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Msg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Msg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.Msg;

        /**
         * Creates a plain object from a Msg message. Also converts values to other types if specified.
         * @param message Msg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.Msg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Msg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a stateMsg. */
    interface IstateMsg {

        /** stateMsg id */
        id?: (string|null);

        /** stateMsg desired */
        desired?: (Shadow.IdesiredMsg|null);

        /** stateMsg reported */
        reported?: (Shadow.IreportedMsg|null);
    }

    /** Represents a stateMsg. */
    class stateMsg implements IstateMsg {

        /**
         * Constructs a new stateMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IstateMsg);

        /** stateMsg id. */
        public id: string;

        /** stateMsg desired. */
        public desired?: (Shadow.IdesiredMsg|null);

        /** stateMsg reported. */
        public reported?: (Shadow.IreportedMsg|null);

        /**
         * Creates a new stateMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns stateMsg instance
         */
        public static create(properties?: Shadow.IstateMsg): Shadow.stateMsg;

        /**
         * Encodes the specified stateMsg message. Does not implicitly {@link Shadow.stateMsg.verify|verify} messages.
         * @param message stateMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IstateMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified stateMsg message, length delimited. Does not implicitly {@link Shadow.stateMsg.verify|verify} messages.
         * @param message stateMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IstateMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a stateMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns stateMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.stateMsg;

        /**
         * Decodes a stateMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns stateMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.stateMsg;

        /**
         * Verifies a stateMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a stateMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns stateMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.stateMsg;

        /**
         * Creates a plain object from a stateMsg message. Also converts values to other types if specified.
         * @param message stateMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.stateMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this stateMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a desiredMsg. */
    interface IdesiredMsg {

        /** desiredMsg videoSources */
        videoSources?: (Shadow.IvideoSource[]|null);

        /** desiredMsg versionInfo */
        versionInfo?: (Shadow.IversionMsg|null);

        /** desiredMsg avs */
        avs?: (Shadow.IavsMsg|null);

        /** desiredMsg autoUpgrade */
        autoUpgrade?: (boolean|null);

        /** desiredMsg calibration */
        calibration?: (Shadow.IcalibrationMsg|null);

        /** desiredMsg stitchParam */
        stitchParam?: (Shadow.IstitchParamMsg|null);

        /** desiredMsg chripParam */
        chripParam?: (Shadow.IchripParamMsg|null);
    }

    /** Represents a desiredMsg. */
    class desiredMsg implements IdesiredMsg {

        /**
         * Constructs a new desiredMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IdesiredMsg);

        /** desiredMsg videoSources. */
        public videoSources: Shadow.IvideoSource[];

        /** desiredMsg versionInfo. */
        public versionInfo?: (Shadow.IversionMsg|null);

        /** desiredMsg avs. */
        public avs?: (Shadow.IavsMsg|null);

        /** desiredMsg autoUpgrade. */
        public autoUpgrade: boolean;

        /** desiredMsg calibration. */
        public calibration?: (Shadow.IcalibrationMsg|null);

        /** desiredMsg stitchParam. */
        public stitchParam?: (Shadow.IstitchParamMsg|null);

        /** desiredMsg chripParam. */
        public chripParam?: (Shadow.IchripParamMsg|null);

        /**
         * Creates a new desiredMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns desiredMsg instance
         */
        public static create(properties?: Shadow.IdesiredMsg): Shadow.desiredMsg;

        /**
         * Encodes the specified desiredMsg message. Does not implicitly {@link Shadow.desiredMsg.verify|verify} messages.
         * @param message desiredMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IdesiredMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified desiredMsg message, length delimited. Does not implicitly {@link Shadow.desiredMsg.verify|verify} messages.
         * @param message desiredMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IdesiredMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a desiredMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns desiredMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.desiredMsg;

        /**
         * Decodes a desiredMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns desiredMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.desiredMsg;

        /**
         * Verifies a desiredMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a desiredMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns desiredMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.desiredMsg;

        /**
         * Creates a plain object from a desiredMsg message. Also converts values to other types if specified.
         * @param message desiredMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.desiredMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this desiredMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a videoSource. */
    interface IvideoSource {

        /** videoSource id */
        id?: (string|null);

        /** videoSource password */
        password?: (string|null);
    }

    /** Represents a videoSource. */
    class videoSource implements IvideoSource {

        /**
         * Constructs a new videoSource.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IvideoSource);

        /** videoSource id. */
        public id: string;

        /** videoSource password. */
        public password: string;

        /**
         * Creates a new videoSource instance using the specified properties.
         * @param [properties] Properties to set
         * @returns videoSource instance
         */
        public static create(properties?: Shadow.IvideoSource): Shadow.videoSource;

        /**
         * Encodes the specified videoSource message. Does not implicitly {@link Shadow.videoSource.verify|verify} messages.
         * @param message videoSource message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IvideoSource, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified videoSource message, length delimited. Does not implicitly {@link Shadow.videoSource.verify|verify} messages.
         * @param message videoSource message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IvideoSource, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a videoSource message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns videoSource
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.videoSource;

        /**
         * Decodes a videoSource message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns videoSource
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.videoSource;

        /**
         * Verifies a videoSource message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a videoSource message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns videoSource
         */
        public static fromObject(object: { [k: string]: any }): Shadow.videoSource;

        /**
         * Creates a plain object from a videoSource message. Also converts values to other types if specified.
         * @param message videoSource
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.videoSource, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this videoSource to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a reportedMsg. */
    interface IreportedMsg {

        /** reportedMsg videoSources */
        videoSources?: (Shadow.IvideoSource[]|null);

        /** reportedMsg discoveredVideoSources */
        discoveredVideoSources?: (string[]|null);

        /** reportedMsg connected */
        connected?: (boolean|null);

        /** reportedMsg status */
        status?: (string|null);

        /** reportedMsg ip */
        ip?: (string|null);

        /** reportedMsg versionInfo */
        versionInfo?: (Shadow.IversionMsg|null);

        /** reportedMsg lastVersionInfo */
        lastVersionInfo?: (Shadow.IversionMsg|null);

        /** reportedMsg health */
        health?: (Shadow.IhealthMsg|null);

        /** reportedMsg environment */
        environment?: (Shadow.IenvironmentMsg|null);
    }

    /** Represents a reportedMsg. */
    class reportedMsg implements IreportedMsg {

        /**
         * Constructs a new reportedMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IreportedMsg);

        /** reportedMsg videoSources. */
        public videoSources: Shadow.IvideoSource[];

        /** reportedMsg discoveredVideoSources. */
        public discoveredVideoSources: string[];

        /** reportedMsg connected. */
        public connected: boolean;

        /** reportedMsg status. */
        public status: string;

        /** reportedMsg ip. */
        public ip: string;

        /** reportedMsg versionInfo. */
        public versionInfo?: (Shadow.IversionMsg|null);

        /** reportedMsg lastVersionInfo. */
        public lastVersionInfo?: (Shadow.IversionMsg|null);

        /** reportedMsg health. */
        public health?: (Shadow.IhealthMsg|null);

        /** reportedMsg environment. */
        public environment?: (Shadow.IenvironmentMsg|null);

        /**
         * Creates a new reportedMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns reportedMsg instance
         */
        public static create(properties?: Shadow.IreportedMsg): Shadow.reportedMsg;

        /**
         * Encodes the specified reportedMsg message. Does not implicitly {@link Shadow.reportedMsg.verify|verify} messages.
         * @param message reportedMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IreportedMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified reportedMsg message, length delimited. Does not implicitly {@link Shadow.reportedMsg.verify|verify} messages.
         * @param message reportedMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IreportedMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a reportedMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns reportedMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.reportedMsg;

        /**
         * Decodes a reportedMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns reportedMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.reportedMsg;

        /**
         * Verifies a reportedMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a reportedMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns reportedMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.reportedMsg;

        /**
         * Creates a plain object from a reportedMsg message. Also converts values to other types if specified.
         * @param message reportedMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.reportedMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this reportedMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a versionMsg. */
    interface IversionMsg {

        /** versionMsg branch */
        branch?: (string|null);

        /** versionMsg chripVersion */
        chripVersion?: (string|null);

        /** versionMsg mmwaveVersion */
        mmwaveVersion?: (string|null);

        /** versionMsg esp32Version */
        esp32Version?: (string|null);

        /** versionMsg portalVersion */
        portalVersion?: (string|null);
    }

    /** Represents a versionMsg. */
    class versionMsg implements IversionMsg {

        /**
         * Constructs a new versionMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IversionMsg);

        /** versionMsg branch. */
        public branch: string;

        /** versionMsg chripVersion. */
        public chripVersion: string;

        /** versionMsg mmwaveVersion. */
        public mmwaveVersion: string;

        /** versionMsg esp32Version. */
        public esp32Version: string;

        /** versionMsg portalVersion. */
        public portalVersion: string;

        /**
         * Creates a new versionMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns versionMsg instance
         */
        public static create(properties?: Shadow.IversionMsg): Shadow.versionMsg;

        /**
         * Encodes the specified versionMsg message. Does not implicitly {@link Shadow.versionMsg.verify|verify} messages.
         * @param message versionMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IversionMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified versionMsg message, length delimited. Does not implicitly {@link Shadow.versionMsg.verify|verify} messages.
         * @param message versionMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IversionMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a versionMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns versionMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.versionMsg;

        /**
         * Decodes a versionMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns versionMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.versionMsg;

        /**
         * Verifies a versionMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a versionMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns versionMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.versionMsg;

        /**
         * Creates a plain object from a versionMsg message. Also converts values to other types if specified.
         * @param message versionMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.versionMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this versionMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an avsMsg. */
    interface IavsMsg {

        /** avsMsg clientId */
        clientId?: (string|null);

        /** avsMsg redirectUrl */
        redirectUrl?: (string|null);

        /** avsMsg authCode */
        authCode?: (string|null);

        /** avsMsg codeVerifier */
        codeVerifier?: (string|null);
    }

    /** Represents an avsMsg. */
    class avsMsg implements IavsMsg {

        /**
         * Constructs a new avsMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IavsMsg);

        /** avsMsg clientId. */
        public clientId: string;

        /** avsMsg redirectUrl. */
        public redirectUrl: string;

        /** avsMsg authCode. */
        public authCode: string;

        /** avsMsg codeVerifier. */
        public codeVerifier: string;

        /**
         * Creates a new avsMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns avsMsg instance
         */
        public static create(properties?: Shadow.IavsMsg): Shadow.avsMsg;

        /**
         * Encodes the specified avsMsg message. Does not implicitly {@link Shadow.avsMsg.verify|verify} messages.
         * @param message avsMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IavsMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified avsMsg message, length delimited. Does not implicitly {@link Shadow.avsMsg.verify|verify} messages.
         * @param message avsMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IavsMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an avsMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns avsMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.avsMsg;

        /**
         * Decodes an avsMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns avsMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.avsMsg;

        /**
         * Verifies an avsMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an avsMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns avsMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.avsMsg;

        /**
         * Creates a plain object from an avsMsg message. Also converts values to other types if specified.
         * @param message avsMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.avsMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this avsMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a calibrationMsg. */
    interface IcalibrationMsg {

        /** calibrationMsg startTime */
        startTime?: (number|null);

        /** calibrationMsg stopTime */
        stopTime?: (number|null);

        /** calibrationMsg auto */
        auto?: (boolean|null);

        /** calibrationMsg calibrationState */
        calibrationState?: (Shadow.calibrationMsg.State|null);

        /** calibrationMsg environment */
        environment?: (Shadow.IenvironmentMsg|null);
    }

    /** Represents a calibrationMsg. */
    class calibrationMsg implements IcalibrationMsg {

        /**
         * Constructs a new calibrationMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IcalibrationMsg);

        /** calibrationMsg startTime. */
        public startTime: number;

        /** calibrationMsg stopTime. */
        public stopTime: number;

        /** calibrationMsg auto. */
        public auto: boolean;

        /** calibrationMsg calibrationState. */
        public calibrationState: Shadow.calibrationMsg.State;

        /** calibrationMsg environment. */
        public environment?: (Shadow.IenvironmentMsg|null);

        /**
         * Creates a new calibrationMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns calibrationMsg instance
         */
        public static create(properties?: Shadow.IcalibrationMsg): Shadow.calibrationMsg;

        /**
         * Encodes the specified calibrationMsg message. Does not implicitly {@link Shadow.calibrationMsg.verify|verify} messages.
         * @param message calibrationMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IcalibrationMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified calibrationMsg message, length delimited. Does not implicitly {@link Shadow.calibrationMsg.verify|verify} messages.
         * @param message calibrationMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IcalibrationMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a calibrationMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns calibrationMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.calibrationMsg;

        /**
         * Decodes a calibrationMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns calibrationMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.calibrationMsg;

        /**
         * Verifies a calibrationMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a calibrationMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns calibrationMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.calibrationMsg;

        /**
         * Creates a plain object from a calibrationMsg message. Also converts values to other types if specified.
         * @param message calibrationMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.calibrationMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this calibrationMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace calibrationMsg {

        /** State enum. */
        enum State {
            None = 0,
            Calibrating = 1,
            Fail = 2,
            Success = 3,
            Collecting = 4
        }
    }

    /** Properties of a chripParamMsg. */
    interface IchripParamMsg {

        /** chripParamMsg occupancyFlagsCfg */
        occupancyFlagsCfg?: (string|null);

        /** chripParamMsg occupancyParamCfg */
        occupancyParamCfg?: (string|null);

        /** chripParamMsg correctionParam */
        correctionParam?: (string|null);

        /** chripParamMsg occuStanceParam */
        occuStanceParam?: (string|null);

        /** chripParamMsg sceenRoiParam */
        sceenRoiParam?: (string|null);

        /** chripParamMsg profileCfg */
        profileCfg?: (string|null);
    }

    /** Represents a chripParamMsg. */
    class chripParamMsg implements IchripParamMsg {

        /**
         * Constructs a new chripParamMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IchripParamMsg);

        /** chripParamMsg occupancyFlagsCfg. */
        public occupancyFlagsCfg: string;

        /** chripParamMsg occupancyParamCfg. */
        public occupancyParamCfg: string;

        /** chripParamMsg correctionParam. */
        public correctionParam: string;

        /** chripParamMsg occuStanceParam. */
        public occuStanceParam: string;

        /** chripParamMsg sceenRoiParam. */
        public sceenRoiParam: string;

        /** chripParamMsg profileCfg. */
        public profileCfg: string;

        /**
         * Creates a new chripParamMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns chripParamMsg instance
         */
        public static create(properties?: Shadow.IchripParamMsg): Shadow.chripParamMsg;

        /**
         * Encodes the specified chripParamMsg message. Does not implicitly {@link Shadow.chripParamMsg.verify|verify} messages.
         * @param message chripParamMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IchripParamMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified chripParamMsg message, length delimited. Does not implicitly {@link Shadow.chripParamMsg.verify|verify} messages.
         * @param message chripParamMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IchripParamMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a chripParamMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns chripParamMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.chripParamMsg;

        /**
         * Decodes a chripParamMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns chripParamMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.chripParamMsg;

        /**
         * Verifies a chripParamMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a chripParamMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns chripParamMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.chripParamMsg;

        /**
         * Creates a plain object from a chripParamMsg message. Also converts values to other types if specified.
         * @param message chripParamMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.chripParamMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this chripParamMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an environmentMsg. */
    interface IenvironmentMsg {

        /** environmentMsg rotation */
        rotation?: (number|null);

        /** environmentMsg upDis */
        upDis?: (number|null);

        /** environmentMsg downDis */
        downDis?: (number|null);

        /** environmentMsg leftDis */
        leftDis?: (number|null);

        /** environmentMsg rightDis */
        rightDis?: (number|null);

        /** environmentMsg height */
        height?: (number|null);
    }

    /** Represents an environmentMsg. */
    class environmentMsg implements IenvironmentMsg {

        /**
         * Constructs a new environmentMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IenvironmentMsg);

        /** environmentMsg rotation. */
        public rotation: number;

        /** environmentMsg upDis. */
        public upDis: number;

        /** environmentMsg downDis. */
        public downDis: number;

        /** environmentMsg leftDis. */
        public leftDis: number;

        /** environmentMsg rightDis. */
        public rightDis: number;

        /** environmentMsg height. */
        public height: number;

        /**
         * Creates a new environmentMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns environmentMsg instance
         */
        public static create(properties?: Shadow.IenvironmentMsg): Shadow.environmentMsg;

        /**
         * Encodes the specified environmentMsg message. Does not implicitly {@link Shadow.environmentMsg.verify|verify} messages.
         * @param message environmentMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IenvironmentMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified environmentMsg message, length delimited. Does not implicitly {@link Shadow.environmentMsg.verify|verify} messages.
         * @param message environmentMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IenvironmentMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an environmentMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns environmentMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.environmentMsg;

        /**
         * Decodes an environmentMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns environmentMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.environmentMsg;

        /**
         * Verifies an environmentMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an environmentMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns environmentMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.environmentMsg;

        /**
         * Creates a plain object from an environmentMsg message. Also converts values to other types if specified.
         * @param message environmentMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.environmentMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this environmentMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a stitchParamMsg. */
    interface IstitchParamMsg {

        /** stitchParamMsg height */
        height?: (number|null);

        /** stitchParamMsg theta */
        theta?: (number|null);

        /** stitchParamMsg shiftX */
        shiftX?: (number|null);

        /** stitchParamMsg shiftZ */
        shiftZ?: (number|null);
    }

    /** Represents a stitchParamMsg. */
    class stitchParamMsg implements IstitchParamMsg {

        /**
         * Constructs a new stitchParamMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IstitchParamMsg);

        /** stitchParamMsg height. */
        public height: number;

        /** stitchParamMsg theta. */
        public theta: number;

        /** stitchParamMsg shiftX. */
        public shiftX: number;

        /** stitchParamMsg shiftZ. */
        public shiftZ: number;

        /**
         * Creates a new stitchParamMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns stitchParamMsg instance
         */
        public static create(properties?: Shadow.IstitchParamMsg): Shadow.stitchParamMsg;

        /**
         * Encodes the specified stitchParamMsg message. Does not implicitly {@link Shadow.stitchParamMsg.verify|verify} messages.
         * @param message stitchParamMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IstitchParamMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified stitchParamMsg message, length delimited. Does not implicitly {@link Shadow.stitchParamMsg.verify|verify} messages.
         * @param message stitchParamMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IstitchParamMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a stitchParamMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns stitchParamMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.stitchParamMsg;

        /**
         * Decodes a stitchParamMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns stitchParamMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.stitchParamMsg;

        /**
         * Verifies a stitchParamMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a stitchParamMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns stitchParamMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.stitchParamMsg;

        /**
         * Creates a plain object from a stitchParamMsg message. Also converts values to other types if specified.
         * @param message stitchParamMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.stitchParamMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this stitchParamMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a healthMsg. */
    interface IhealthMsg {

        /** healthMsg uptime */
        uptime?: (number|null);

        /** healthMsg rebootReason */
        rebootReason?: (string|null);
    }

    /** Represents a healthMsg. */
    class healthMsg implements IhealthMsg {

        /**
         * Constructs a new healthMsg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IhealthMsg);

        /** healthMsg uptime. */
        public uptime: number;

        /** healthMsg rebootReason. */
        public rebootReason: string;

        /**
         * Creates a new healthMsg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns healthMsg instance
         */
        public static create(properties?: Shadow.IhealthMsg): Shadow.healthMsg;

        /**
         * Encodes the specified healthMsg message. Does not implicitly {@link Shadow.healthMsg.verify|verify} messages.
         * @param message healthMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IhealthMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified healthMsg message, length delimited. Does not implicitly {@link Shadow.healthMsg.verify|verify} messages.
         * @param message healthMsg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IhealthMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a healthMsg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns healthMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.healthMsg;

        /**
         * Decodes a healthMsg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns healthMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.healthMsg;

        /**
         * Verifies a healthMsg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a healthMsg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns healthMsg
         */
        public static fromObject(object: { [k: string]: any }): Shadow.healthMsg;

        /**
         * Creates a plain object from a healthMsg message. Also converts values to other types if specified.
         * @param message healthMsg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.healthMsg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this healthMsg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a DeviceReq. */
    interface IDeviceReq {

        /** DeviceReq token */
        token?: (string|null);
    }

    /** Represents a DeviceReq. */
    class DeviceReq implements IDeviceReq {

        /**
         * Constructs a new DeviceReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IDeviceReq);

        /** DeviceReq token. */
        public token: string;

        /**
         * Creates a new DeviceReq instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DeviceReq instance
         */
        public static create(properties?: Shadow.IDeviceReq): Shadow.DeviceReq;

        /**
         * Encodes the specified DeviceReq message. Does not implicitly {@link Shadow.DeviceReq.verify|verify} messages.
         * @param message DeviceReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IDeviceReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified DeviceReq message, length delimited. Does not implicitly {@link Shadow.DeviceReq.verify|verify} messages.
         * @param message DeviceReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IDeviceReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a DeviceReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DeviceReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.DeviceReq;

        /**
         * Decodes a DeviceReq message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DeviceReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.DeviceReq;

        /**
         * Verifies a DeviceReq message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a DeviceReq message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns DeviceReq
         */
        public static fromObject(object: { [k: string]: any }): Shadow.DeviceReq;

        /**
         * Creates a plain object from a DeviceReq message. Also converts values to other types if specified.
         * @param message DeviceReq
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.DeviceReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this DeviceReq to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SetMsgReq. */
    interface ISetMsgReq {

        /** SetMsgReq token */
        token?: (string|null);

        /** SetMsgReq msg */
        msg?: (string|null);
    }

    /** Represents a SetMsgReq. */
    class SetMsgReq implements ISetMsgReq {

        /**
         * Constructs a new SetMsgReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.ISetMsgReq);

        /** SetMsgReq token. */
        public token: string;

        /** SetMsgReq msg. */
        public msg: string;

        /**
         * Creates a new SetMsgReq instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SetMsgReq instance
         */
        public static create(properties?: Shadow.ISetMsgReq): Shadow.SetMsgReq;

        /**
         * Encodes the specified SetMsgReq message. Does not implicitly {@link Shadow.SetMsgReq.verify|verify} messages.
         * @param message SetMsgReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.ISetMsgReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SetMsgReq message, length delimited. Does not implicitly {@link Shadow.SetMsgReq.verify|verify} messages.
         * @param message SetMsgReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.ISetMsgReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SetMsgReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SetMsgReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.SetMsgReq;

        /**
         * Decodes a SetMsgReq message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SetMsgReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.SetMsgReq;

        /**
         * Verifies a SetMsgReq message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SetMsgReq message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SetMsgReq
         */
        public static fromObject(object: { [k: string]: any }): Shadow.SetMsgReq;

        /**
         * Creates a plain object from a SetMsgReq message. Also converts values to other types if specified.
         * @param message SetMsgReq
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.SetMsgReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SetMsgReq to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Res. */
    interface IRes {

        /** Res status */
        status?: (Shadow.Res.Status|null);

        /** Res error */
        error?: (string|null);
    }

    /** Represents a Res. */
    class Res implements IRes {

        /**
         * Constructs a new Res.
         * @param [properties] Properties to set
         */
        constructor(properties?: Shadow.IRes);

        /** Res status. */
        public status: Shadow.Res.Status;

        /** Res error. */
        public error: string;

        /**
         * Creates a new Res instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Res instance
         */
        public static create(properties?: Shadow.IRes): Shadow.Res;

        /**
         * Encodes the specified Res message. Does not implicitly {@link Shadow.Res.verify|verify} messages.
         * @param message Res message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Shadow.IRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Res message, length delimited. Does not implicitly {@link Shadow.Res.verify|verify} messages.
         * @param message Res message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Shadow.IRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Res message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Res
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Shadow.Res;

        /**
         * Decodes a Res message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Res
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Shadow.Res;

        /**
         * Verifies a Res message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Res message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Res
         */
        public static fromObject(object: { [k: string]: any }): Shadow.Res;

        /**
         * Creates a plain object from a Res message. Also converts values to other types if specified.
         * @param message Res
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Shadow.Res, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Res to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace Res {

        /** Status enum. */
        enum Status {
            OK = 0,
            Fail = 1
        }
    }

    /** Represents a Service */
    class Service extends $protobuf.rpc.Service {

        /**
         * Constructs a new Service service.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         */
        constructor(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean);

        /**
         * Creates new Service service using the specified rpc implementation.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         * @returns RPC service. Useful where requests and/or responses are streamed.
         */
        public static create(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean): Service;

        /**
         * Calls GetMsg.
         * @param request DeviceReq message or plain object
         * @param callback Node-style callback called with the error, if any, and Msg
         */
        public getMsg(request: Shadow.IDeviceReq, callback: Shadow.Service.GetMsgCallback): void;

        /**
         * Calls GetMsg.
         * @param request DeviceReq message or plain object
         * @returns Promise
         */
        public getMsg(request: Shadow.IDeviceReq): Promise<Shadow.Msg>;

        /**
         * Calls SetMsg.
         * @param request SetMsgReq message or plain object
         * @param callback Node-style callback called with the error, if any, and Res
         */
        public setMsg(request: Shadow.ISetMsgReq, callback: Shadow.Service.SetMsgCallback): void;

        /**
         * Calls SetMsg.
         * @param request SetMsgReq message or plain object
         * @returns Promise
         */
        public setMsg(request: Shadow.ISetMsgReq): Promise<Shadow.Res>;
    }

    namespace Service {

        /**
         * Callback as used by {@link Shadow.Service#getMsg}.
         * @param error Error, if any
         * @param [response] Msg
         */
        type GetMsgCallback = (error: (Error|null), response?: Shadow.Msg) => void;

        /**
         * Callback as used by {@link Shadow.Service#setMsg}.
         * @param error Error, if any
         * @param [response] Res
         */
        type SetMsgCallback = (error: (Error|null), response?: Shadow.Res) => void;
    }
}
