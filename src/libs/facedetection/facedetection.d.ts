import * as $protobuf from "protobufjs";
/** Namespace Facedetection. */
export namespace Facedetection {

    /** Properties of a Detection. */
    interface IDetection {

        /** Detection timestamp */
        timestamp?: (number|Long|null);

        /** Detection imageWidth */
        imageWidth?: (number|Long|null);

        /** Detection imageHeight */
        imageHeight?: (number|Long|null);

        /** Detection image */
        image?: (Uint8Array|null);

        /** Detection rects */
        rects?: (Facedetection.IRectangle[]|null);
    }

    /** Represents a Detection. */
    class Detection implements IDetection {

        /**
         * Constructs a new Detection.
         * @param [properties] Properties to set
         */
        constructor(properties?: Facedetection.IDetection);

        /** Detection timestamp. */
        public timestamp: (number|Long);

        /** Detection imageWidth. */
        public imageWidth: (number|Long);

        /** Detection imageHeight. */
        public imageHeight: (number|Long);

        /** Detection image. */
        public image: Uint8Array;

        /** Detection rects. */
        public rects: Facedetection.IRectangle[];

        /**
         * Creates a new Detection instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Detection instance
         */
        public static create(properties?: Facedetection.IDetection): Facedetection.Detection;

        /**
         * Encodes the specified Detection message. Does not implicitly {@link Facedetection.Detection.verify|verify} messages.
         * @param message Detection message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Facedetection.IDetection, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Detection message, length delimited. Does not implicitly {@link Facedetection.Detection.verify|verify} messages.
         * @param message Detection message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Facedetection.IDetection, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Detection message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Detection
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Facedetection.Detection;

        /**
         * Decodes a Detection message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Detection
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Facedetection.Detection;

        /**
         * Verifies a Detection message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Detection message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Detection
         */
        public static fromObject(object: { [k: string]: any }): Facedetection.Detection;

        /**
         * Creates a plain object from a Detection message. Also converts values to other types if specified.
         * @param message Detection
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Facedetection.Detection, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Detection to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Rectangle. */
    interface IRectangle {

        /** Rectangle x */
        x?: (number|null);

        /** Rectangle y */
        y?: (number|null);

        /** Rectangle w */
        w?: (number|null);

        /** Rectangle h */
        h?: (number|null);
    }

    /** Represents a Rectangle. */
    class Rectangle implements IRectangle {

        /**
         * Constructs a new Rectangle.
         * @param [properties] Properties to set
         */
        constructor(properties?: Facedetection.IRectangle);

        /** Rectangle x. */
        public x: number;

        /** Rectangle y. */
        public y: number;

        /** Rectangle w. */
        public w: number;

        /** Rectangle h. */
        public h: number;

        /**
         * Creates a new Rectangle instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Rectangle instance
         */
        public static create(properties?: Facedetection.IRectangle): Facedetection.Rectangle;

        /**
         * Encodes the specified Rectangle message. Does not implicitly {@link Facedetection.Rectangle.verify|verify} messages.
         * @param message Rectangle message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Facedetection.IRectangle, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Rectangle message, length delimited. Does not implicitly {@link Facedetection.Rectangle.verify|verify} messages.
         * @param message Rectangle message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Facedetection.IRectangle, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Rectangle message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Rectangle
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Facedetection.Rectangle;

        /**
         * Decodes a Rectangle message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Rectangle
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Facedetection.Rectangle;

        /**
         * Verifies a Rectangle message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Rectangle message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Rectangle
         */
        public static fromObject(object: { [k: string]: any }): Facedetection.Rectangle;

        /**
         * Creates a plain object from a Rectangle message. Also converts values to other types if specified.
         * @param message Rectangle
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Facedetection.Rectangle, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Rectangle to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }
}
