/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Facedetection = $root.Facedetection = (() => {

    /**
     * Namespace Facedetection.
     * @exports Facedetection
     * @namespace
     */
    const Facedetection = {};

    Facedetection.Detection = (function() {

        /**
         * Properties of a Detection.
         * @memberof Facedetection
         * @interface IDetection
         * @property {number|Long|null} [timestamp] Detection timestamp
         * @property {number|Long|null} [imageWidth] Detection imageWidth
         * @property {number|Long|null} [imageHeight] Detection imageHeight
         * @property {Uint8Array|null} [image] Detection image
         * @property {Array.<Facedetection.IRectangle>|null} [rects] Detection rects
         */

        /**
         * Constructs a new Detection.
         * @memberof Facedetection
         * @classdesc Represents a Detection.
         * @implements IDetection
         * @constructor
         * @param {Facedetection.IDetection=} [properties] Properties to set
         */
        function Detection(properties) {
            this.rects = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Detection timestamp.
         * @member {number|Long} timestamp
         * @memberof Facedetection.Detection
         * @instance
         */
        Detection.prototype.timestamp = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Detection imageWidth.
         * @member {number|Long} imageWidth
         * @memberof Facedetection.Detection
         * @instance
         */
        Detection.prototype.imageWidth = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Detection imageHeight.
         * @member {number|Long} imageHeight
         * @memberof Facedetection.Detection
         * @instance
         */
        Detection.prototype.imageHeight = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Detection image.
         * @member {Uint8Array} image
         * @memberof Facedetection.Detection
         * @instance
         */
        Detection.prototype.image = $util.newBuffer([]);

        /**
         * Detection rects.
         * @member {Array.<Facedetection.IRectangle>} rects
         * @memberof Facedetection.Detection
         * @instance
         */
        Detection.prototype.rects = $util.emptyArray;

        /**
         * Creates a new Detection instance using the specified properties.
         * @function create
         * @memberof Facedetection.Detection
         * @static
         * @param {Facedetection.IDetection=} [properties] Properties to set
         * @returns {Facedetection.Detection} Detection instance
         */
        Detection.create = function create(properties) {
            return new Detection(properties);
        };

        /**
         * Encodes the specified Detection message. Does not implicitly {@link Facedetection.Detection.verify|verify} messages.
         * @function encode
         * @memberof Facedetection.Detection
         * @static
         * @param {Facedetection.IDetection} message Detection message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Detection.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp"))
                writer.uint32(/* id 1, wireType 0 =*/8).int64(message.timestamp);
            if (message.imageWidth != null && Object.hasOwnProperty.call(message, "imageWidth"))
                writer.uint32(/* id 2, wireType 0 =*/16).int64(message.imageWidth);
            if (message.imageHeight != null && Object.hasOwnProperty.call(message, "imageHeight"))
                writer.uint32(/* id 3, wireType 0 =*/24).int64(message.imageHeight);
            if (message.image != null && Object.hasOwnProperty.call(message, "image"))
                writer.uint32(/* id 4, wireType 2 =*/34).bytes(message.image);
            if (message.rects != null && message.rects.length)
                for (let i = 0; i < message.rects.length; ++i)
                    $root.Facedetection.Rectangle.encode(message.rects[i], writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Detection message, length delimited. Does not implicitly {@link Facedetection.Detection.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Facedetection.Detection
         * @static
         * @param {Facedetection.IDetection} message Detection message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Detection.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Detection message from the specified reader or buffer.
         * @function decode
         * @memberof Facedetection.Detection
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Facedetection.Detection} Detection
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Detection.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Facedetection.Detection();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.timestamp = reader.int64();
                    break;
                case 2:
                    message.imageWidth = reader.int64();
                    break;
                case 3:
                    message.imageHeight = reader.int64();
                    break;
                case 4:
                    message.image = reader.bytes();
                    break;
                case 5:
                    if (!(message.rects && message.rects.length))
                        message.rects = [];
                    message.rects.push($root.Facedetection.Rectangle.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Detection message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Facedetection.Detection
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Facedetection.Detection} Detection
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Detection.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Detection message.
         * @function verify
         * @memberof Facedetection.Detection
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Detection.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (!$util.isInteger(message.timestamp) && !(message.timestamp && $util.isInteger(message.timestamp.low) && $util.isInteger(message.timestamp.high)))
                    return "timestamp: integer|Long expected";
            if (message.imageWidth != null && message.hasOwnProperty("imageWidth"))
                if (!$util.isInteger(message.imageWidth) && !(message.imageWidth && $util.isInteger(message.imageWidth.low) && $util.isInteger(message.imageWidth.high)))
                    return "imageWidth: integer|Long expected";
            if (message.imageHeight != null && message.hasOwnProperty("imageHeight"))
                if (!$util.isInteger(message.imageHeight) && !(message.imageHeight && $util.isInteger(message.imageHeight.low) && $util.isInteger(message.imageHeight.high)))
                    return "imageHeight: integer|Long expected";
            if (message.image != null && message.hasOwnProperty("image"))
                if (!(message.image && typeof message.image.length === "number" || $util.isString(message.image)))
                    return "image: buffer expected";
            if (message.rects != null && message.hasOwnProperty("rects")) {
                if (!Array.isArray(message.rects))
                    return "rects: array expected";
                for (let i = 0; i < message.rects.length; ++i) {
                    let error = $root.Facedetection.Rectangle.verify(message.rects[i]);
                    if (error)
                        return "rects." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Detection message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Facedetection.Detection
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Facedetection.Detection} Detection
         */
        Detection.fromObject = function fromObject(object) {
            if (object instanceof $root.Facedetection.Detection)
                return object;
            let message = new $root.Facedetection.Detection();
            if (object.timestamp != null)
                if ($util.Long)
                    (message.timestamp = $util.Long.fromValue(object.timestamp)).unsigned = false;
                else if (typeof object.timestamp === "string")
                    message.timestamp = parseInt(object.timestamp, 10);
                else if (typeof object.timestamp === "number")
                    message.timestamp = object.timestamp;
                else if (typeof object.timestamp === "object")
                    message.timestamp = new $util.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber();
            if (object.imageWidth != null)
                if ($util.Long)
                    (message.imageWidth = $util.Long.fromValue(object.imageWidth)).unsigned = false;
                else if (typeof object.imageWidth === "string")
                    message.imageWidth = parseInt(object.imageWidth, 10);
                else if (typeof object.imageWidth === "number")
                    message.imageWidth = object.imageWidth;
                else if (typeof object.imageWidth === "object")
                    message.imageWidth = new $util.LongBits(object.imageWidth.low >>> 0, object.imageWidth.high >>> 0).toNumber();
            if (object.imageHeight != null)
                if ($util.Long)
                    (message.imageHeight = $util.Long.fromValue(object.imageHeight)).unsigned = false;
                else if (typeof object.imageHeight === "string")
                    message.imageHeight = parseInt(object.imageHeight, 10);
                else if (typeof object.imageHeight === "number")
                    message.imageHeight = object.imageHeight;
                else if (typeof object.imageHeight === "object")
                    message.imageHeight = new $util.LongBits(object.imageHeight.low >>> 0, object.imageHeight.high >>> 0).toNumber();
            if (object.image != null)
                if (typeof object.image === "string")
                    $util.base64.decode(object.image, message.image = $util.newBuffer($util.base64.length(object.image)), 0);
                else if (object.image.length)
                    message.image = object.image;
            if (object.rects) {
                if (!Array.isArray(object.rects))
                    throw TypeError(".Facedetection.Detection.rects: array expected");
                message.rects = [];
                for (let i = 0; i < object.rects.length; ++i) {
                    if (typeof object.rects[i] !== "object")
                        throw TypeError(".Facedetection.Detection.rects: object expected");
                    message.rects[i] = $root.Facedetection.Rectangle.fromObject(object.rects[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a Detection message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Facedetection.Detection
         * @static
         * @param {Facedetection.Detection} message Detection
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Detection.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults)
                object.rects = [];
            if (options.defaults) {
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.timestamp = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.imageWidth = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.imageWidth = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.imageHeight = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.imageHeight = options.longs === String ? "0" : 0;
                if (options.bytes === String)
                    object.image = "";
                else {
                    object.image = [];
                    if (options.bytes !== Array)
                        object.image = $util.newBuffer(object.image);
                }
            }
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (typeof message.timestamp === "number")
                    object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;
                else
                    object.timestamp = options.longs === String ? $util.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber() : message.timestamp;
            if (message.imageWidth != null && message.hasOwnProperty("imageWidth"))
                if (typeof message.imageWidth === "number")
                    object.imageWidth = options.longs === String ? String(message.imageWidth) : message.imageWidth;
                else
                    object.imageWidth = options.longs === String ? $util.Long.prototype.toString.call(message.imageWidth) : options.longs === Number ? new $util.LongBits(message.imageWidth.low >>> 0, message.imageWidth.high >>> 0).toNumber() : message.imageWidth;
            if (message.imageHeight != null && message.hasOwnProperty("imageHeight"))
                if (typeof message.imageHeight === "number")
                    object.imageHeight = options.longs === String ? String(message.imageHeight) : message.imageHeight;
                else
                    object.imageHeight = options.longs === String ? $util.Long.prototype.toString.call(message.imageHeight) : options.longs === Number ? new $util.LongBits(message.imageHeight.low >>> 0, message.imageHeight.high >>> 0).toNumber() : message.imageHeight;
            if (message.image != null && message.hasOwnProperty("image"))
                object.image = options.bytes === String ? $util.base64.encode(message.image, 0, message.image.length) : options.bytes === Array ? Array.prototype.slice.call(message.image) : message.image;
            if (message.rects && message.rects.length) {
                object.rects = [];
                for (let j = 0; j < message.rects.length; ++j)
                    object.rects[j] = $root.Facedetection.Rectangle.toObject(message.rects[j], options);
            }
            return object;
        };

        /**
         * Converts this Detection to JSON.
         * @function toJSON
         * @memberof Facedetection.Detection
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Detection.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Detection;
    })();

    Facedetection.Rectangle = (function() {

        /**
         * Properties of a Rectangle.
         * @memberof Facedetection
         * @interface IRectangle
         * @property {number|null} [x] Rectangle x
         * @property {number|null} [y] Rectangle y
         * @property {number|null} [w] Rectangle w
         * @property {number|null} [h] Rectangle h
         */

        /**
         * Constructs a new Rectangle.
         * @memberof Facedetection
         * @classdesc Represents a Rectangle.
         * @implements IRectangle
         * @constructor
         * @param {Facedetection.IRectangle=} [properties] Properties to set
         */
        function Rectangle(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Rectangle x.
         * @member {number} x
         * @memberof Facedetection.Rectangle
         * @instance
         */
        Rectangle.prototype.x = 0;

        /**
         * Rectangle y.
         * @member {number} y
         * @memberof Facedetection.Rectangle
         * @instance
         */
        Rectangle.prototype.y = 0;

        /**
         * Rectangle w.
         * @member {number} w
         * @memberof Facedetection.Rectangle
         * @instance
         */
        Rectangle.prototype.w = 0;

        /**
         * Rectangle h.
         * @member {number} h
         * @memberof Facedetection.Rectangle
         * @instance
         */
        Rectangle.prototype.h = 0;

        /**
         * Creates a new Rectangle instance using the specified properties.
         * @function create
         * @memberof Facedetection.Rectangle
         * @static
         * @param {Facedetection.IRectangle=} [properties] Properties to set
         * @returns {Facedetection.Rectangle} Rectangle instance
         */
        Rectangle.create = function create(properties) {
            return new Rectangle(properties);
        };

        /**
         * Encodes the specified Rectangle message. Does not implicitly {@link Facedetection.Rectangle.verify|verify} messages.
         * @function encode
         * @memberof Facedetection.Rectangle
         * @static
         * @param {Facedetection.IRectangle} message Rectangle message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Rectangle.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.x != null && Object.hasOwnProperty.call(message, "x"))
                writer.uint32(/* id 1, wireType 1 =*/9).double(message.x);
            if (message.y != null && Object.hasOwnProperty.call(message, "y"))
                writer.uint32(/* id 2, wireType 1 =*/17).double(message.y);
            if (message.w != null && Object.hasOwnProperty.call(message, "w"))
                writer.uint32(/* id 3, wireType 1 =*/25).double(message.w);
            if (message.h != null && Object.hasOwnProperty.call(message, "h"))
                writer.uint32(/* id 4, wireType 1 =*/33).double(message.h);
            return writer;
        };

        /**
         * Encodes the specified Rectangle message, length delimited. Does not implicitly {@link Facedetection.Rectangle.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Facedetection.Rectangle
         * @static
         * @param {Facedetection.IRectangle} message Rectangle message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Rectangle.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Rectangle message from the specified reader or buffer.
         * @function decode
         * @memberof Facedetection.Rectangle
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Facedetection.Rectangle} Rectangle
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Rectangle.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Facedetection.Rectangle();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.x = reader.double();
                    break;
                case 2:
                    message.y = reader.double();
                    break;
                case 3:
                    message.w = reader.double();
                    break;
                case 4:
                    message.h = reader.double();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Rectangle message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Facedetection.Rectangle
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Facedetection.Rectangle} Rectangle
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Rectangle.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Rectangle message.
         * @function verify
         * @memberof Facedetection.Rectangle
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Rectangle.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.x != null && message.hasOwnProperty("x"))
                if (typeof message.x !== "number")
                    return "x: number expected";
            if (message.y != null && message.hasOwnProperty("y"))
                if (typeof message.y !== "number")
                    return "y: number expected";
            if (message.w != null && message.hasOwnProperty("w"))
                if (typeof message.w !== "number")
                    return "w: number expected";
            if (message.h != null && message.hasOwnProperty("h"))
                if (typeof message.h !== "number")
                    return "h: number expected";
            return null;
        };

        /**
         * Creates a Rectangle message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Facedetection.Rectangle
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Facedetection.Rectangle} Rectangle
         */
        Rectangle.fromObject = function fromObject(object) {
            if (object instanceof $root.Facedetection.Rectangle)
                return object;
            let message = new $root.Facedetection.Rectangle();
            if (object.x != null)
                message.x = Number(object.x);
            if (object.y != null)
                message.y = Number(object.y);
            if (object.w != null)
                message.w = Number(object.w);
            if (object.h != null)
                message.h = Number(object.h);
            return message;
        };

        /**
         * Creates a plain object from a Rectangle message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Facedetection.Rectangle
         * @static
         * @param {Facedetection.Rectangle} message Rectangle
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Rectangle.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.x = 0;
                object.y = 0;
                object.w = 0;
                object.h = 0;
            }
            if (message.x != null && message.hasOwnProperty("x"))
                object.x = options.json && !isFinite(message.x) ? String(message.x) : message.x;
            if (message.y != null && message.hasOwnProperty("y"))
                object.y = options.json && !isFinite(message.y) ? String(message.y) : message.y;
            if (message.w != null && message.hasOwnProperty("w"))
                object.w = options.json && !isFinite(message.w) ? String(message.w) : message.w;
            if (message.h != null && message.hasOwnProperty("h"))
                object.h = options.json && !isFinite(message.h) ? String(message.h) : message.h;
            return object;
        };

        /**
         * Converts this Rectangle to JSON.
         * @function toJSON
         * @memberof Facedetection.Rectangle
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Rectangle.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Rectangle;
    })();

    return Facedetection;
})();

export { $root as default };
