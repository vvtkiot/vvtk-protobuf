import * as $protobuf from "protobufjs";
/** Namespace Livestream. */
export namespace Livestream {

    /** Properties of a Msg. */
    interface IMsg {

        /** Msg timestamp */
        timestamp?: (number|null);

        /** Msg type */
        type?: (Livestream.Msg.Type|null);

        /** Msg streamid */
        streamid?: (string|null);

        /** Msg ip */
        ip?: (string|null);

        /** Msg audioPort */
        audioPort?: (string|null);

        /** Msg videoPort */
        videoPort?: (string|null);

        /** Msg videoSource */
        videoSource?: (string|null);
    }

    /** Represents a Msg. */
    class Msg implements IMsg {

        /**
         * Constructs a new Msg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Livestream.IMsg);

        /** Msg timestamp. */
        public timestamp: number;

        /** Msg type. */
        public type: Livestream.Msg.Type;

        /** Msg streamid. */
        public streamid: string;

        /** Msg ip. */
        public ip: string;

        /** Msg audioPort. */
        public audioPort: string;

        /** Msg videoPort. */
        public videoPort: string;

        /** Msg videoSource. */
        public videoSource: string;

        /**
         * Creates a new Msg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Msg instance
         */
        public static create(properties?: Livestream.IMsg): Livestream.Msg;

        /**
         * Encodes the specified Msg message. Does not implicitly {@link Livestream.Msg.verify|verify} messages.
         * @param message Msg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Livestream.IMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Msg message, length delimited. Does not implicitly {@link Livestream.Msg.verify|verify} messages.
         * @param message Msg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Livestream.IMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Msg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Livestream.Msg;

        /**
         * Decodes a Msg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Livestream.Msg;

        /**
         * Verifies a Msg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Msg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Msg
         */
        public static fromObject(object: { [k: string]: any }): Livestream.Msg;

        /**
         * Creates a plain object from a Msg message. Also converts values to other types if specified.
         * @param message Msg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Livestream.Msg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Msg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace Msg {

        /** Type enum. */
        enum Type {
            PLAY = 0,
            STOP = 1,
            KEEPALIVE = 2
        }
    }
}
