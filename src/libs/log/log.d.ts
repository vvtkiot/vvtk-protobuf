import * as $protobuf from "protobufjs";
/** Namespace Log. */
export namespace Log {

    /** Properties of a Data. */
    interface IData {

        /** Data source */
        source?: (string|null);

        /** Data level */
        level?: (Log.Data.Level|null);

        /** Data user */
        user?: (string|null);

        /** Data caller */
        caller?: (string|null);

        /** Data time */
        time?: (string|null);

        /** Data msg */
        msg?: (string|null);

        /** Data deviceToken */
        deviceToken?: (string|null);

        /** Data websocketID */
        websocketID?: (string|null);

        /** Data webrtcID */
        webrtcID?: (string|null);

        /** Data count */
        count?: (number|null);

        /** Data hostname */
        hostname?: (string|null);
    }

    /** Represents a Data. */
    class Data implements IData {

        /**
         * Constructs a new Data.
         * @param [properties] Properties to set
         */
        constructor(properties?: Log.IData);

        /** Data source. */
        public source: string;

        /** Data level. */
        public level: Log.Data.Level;

        /** Data user. */
        public user: string;

        /** Data caller. */
        public caller: string;

        /** Data time. */
        public time: string;

        /** Data msg. */
        public msg: string;

        /** Data deviceToken. */
        public deviceToken: string;

        /** Data websocketID. */
        public websocketID: string;

        /** Data webrtcID. */
        public webrtcID: string;

        /** Data count. */
        public count: number;

        /** Data hostname. */
        public hostname: string;

        /**
         * Creates a new Data instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Data instance
         */
        public static create(properties?: Log.IData): Log.Data;

        /**
         * Encodes the specified Data message. Does not implicitly {@link Log.Data.verify|verify} messages.
         * @param message Data message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Log.IData, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Data message, length delimited. Does not implicitly {@link Log.Data.verify|verify} messages.
         * @param message Data message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Log.IData, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Data message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Data
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Log.Data;

        /**
         * Decodes a Data message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Data
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Log.Data;

        /**
         * Verifies a Data message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Data message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Data
         */
        public static fromObject(object: { [k: string]: any }): Log.Data;

        /**
         * Creates a plain object from a Data message. Also converts values to other types if specified.
         * @param message Data
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Log.Data, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Data to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace Data {

        /** Level enum. */
        enum Level {
            Debug = 0,
            Info = 1,
            Warn = 2,
            Error = 3,
            Fatal = 4
        }
    }
}
