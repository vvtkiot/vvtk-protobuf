/* Generated by the protocol buffer compiler.  DO NOT EDIT! */
/* Generated from: peopletrack.proto */

#ifndef PROTOBUF_C_peopletrack_2eproto__INCLUDED
#define PROTOBUF_C_peopletrack_2eproto__INCLUDED

#include <protobuf-c/protobuf-c.h>

PROTOBUF_C__BEGIN_DECLS

#if PROTOBUF_C_VERSION_NUMBER < 1003000
# error This file was generated by a newer version of protoc-c which is incompatible with your libprotobuf-c headers. Please update your headers.
#elif 1003003 < PROTOBUF_C_MIN_COMPILER_VERSION
# error This file was generated by an older version of protoc-c which is incompatible with your libprotobuf-c headers. Please regenerate this file with a newer version of protoc-c.
#endif


typedef struct _Peopletrack__People Peopletrack__People;
typedef struct _Peopletrack__Msg Peopletrack__Msg;
typedef struct _Peopletrack__StitchParam Peopletrack__StitchParam;
typedef struct _Peopletrack__SetupInput Peopletrack__SetupInput;
typedef struct _Peopletrack__SetupOutput Peopletrack__SetupOutput;
typedef struct _Peopletrack__StitchInput Peopletrack__StitchInput;
typedef struct _Peopletrack__StitchOutput Peopletrack__StitchOutput;
typedef struct _Peopletrack__Device Peopletrack__Device;
typedef struct _Peopletrack__Devices Peopletrack__Devices;
typedef struct _Peopletrack__DeviceReq Peopletrack__DeviceReq;


/* --- enums --- */

typedef enum _Peopletrack__People__State {
  PEOPLETRACK__PEOPLE__STATE__NONE = 0,
  PEOPLETRACK__PEOPLE__STATE__ACTIVE = 1,
  PEOPLETRACK__PEOPLE__STATE__STATIC = 2
    PROTOBUF_C__FORCE_ENUM_TO_BE_INT_SIZE(PEOPLETRACK__PEOPLE__STATE)
} Peopletrack__People__State;
typedef enum _Peopletrack__People__Stance {
  PEOPLETRACK__PEOPLE__STANCE__UNKNOWN = 0,
  PEOPLETRACK__PEOPLE__STANCE__STAND = 1,
  PEOPLETRACK__PEOPLE__STANCE__SIT = 2,
  PEOPLETRACK__PEOPLE__STANCE__LAY = 3,
  PEOPLETRACK__PEOPLE__STANCE__SUSFALL = 4,
  PEOPLETRACK__PEOPLE__STANCE__COUNTDOWN = 5,
  PEOPLETRACK__PEOPLE__STANCE__DETFALL = 6
    PROTOBUF_C__FORCE_ENUM_TO_BE_INT_SIZE(PEOPLETRACK__PEOPLE__STANCE)
} Peopletrack__People__Stance;

/* --- messages --- */

struct  _Peopletrack__People
{
  ProtobufCMessage base;
  uint32_t tid;
  float posx;
  float posy;
  float posz;
  Peopletrack__People__State state;
  Peopletrack__People__Stance stance;
};
#define PEOPLETRACK__PEOPLE__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__people__descriptor) \
    , 0, 0, 0, 0, PEOPLETRACK__PEOPLE__STATE__NONE, PEOPLETRACK__PEOPLE__STANCE__UNKNOWN }


struct  _Peopletrack__Msg
{
  ProtobufCMessage base;
  uint32_t framenumber;
  uint64_t timestamp;
  size_t n_people;
  Peopletrack__People **people;
  ProtobufCBinaryData cloudpoint;
};
#define PEOPLETRACK__MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__msg__descriptor) \
    , 0, 0, 0,NULL, {0,NULL} }


struct  _Peopletrack__StitchParam
{
  ProtobufCMessage base;
  float height;
  float theta;
  float shiftx;
  float shiftz;
};
#define PEOPLETRACK__STITCH_PARAM__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__stitch_param__descriptor) \
    , 0, 0, 0, 0 }


struct  _Peopletrack__SetupInput
{
  ProtobufCMessage base;
  uint32_t stitchid;
  uint32_t framenumber;
  uint64_t timestamp;
  ProtobufCBinaryData cloudpoint;
};
#define PEOPLETRACK__SETUP_INPUT__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__setup_input__descriptor) \
    , 0, 0, 0, {0,NULL} }


struct  _Peopletrack__SetupOutput
{
  ProtobufCMessage base;
  uint32_t framenumber;
  uint64_t timestamp;
  size_t n_people;
  Peopletrack__People **people;
  ProtobufCBinaryData backgroundchart;
  Peopletrack__StitchParam *stitchparam;
};
#define PEOPLETRACK__SETUP_OUTPUT__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__setup_output__descriptor) \
    , 0, 0, 0,NULL, {0,NULL}, NULL }


struct  _Peopletrack__StitchInput
{
  ProtobufCMessage base;
  uint32_t stitchid;
  uint32_t framenumber;
  uint64_t timestamp;
  size_t n_people;
  Peopletrack__People **people;
  Peopletrack__StitchParam *stitchparam;
};
#define PEOPLETRACK__STITCH_INPUT__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__stitch_input__descriptor) \
    , 0, 0, 0, 0,NULL, NULL }


struct  _Peopletrack__StitchOutput
{
  ProtobufCMessage base;
  uint32_t framenumber;
  uint64_t timestamp;
  size_t n_people;
  Peopletrack__People **people;
  ProtobufCBinaryData backgroundchart;
  ProtobufCBinaryData parameters;
};
#define PEOPLETRACK__STITCH_OUTPUT__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__stitch_output__descriptor) \
    , 0, 0, 0,NULL, {0,NULL}, {0,NULL} }


struct  _Peopletrack__Device
{
  ProtobufCMessage base;
  uint32_t stitchid;
  char *token;
};
#define PEOPLETRACK__DEVICE__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__device__descriptor) \
    , 0, (char *)protobuf_c_empty_string }


struct  _Peopletrack__Devices
{
  ProtobufCMessage base;
  size_t n_device;
  Peopletrack__Device **device;
};
#define PEOPLETRACK__DEVICES__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__devices__descriptor) \
    , 0,NULL }


struct  _Peopletrack__DeviceReq
{
  ProtobufCMessage base;
  char *token;
};
#define PEOPLETRACK__DEVICE_REQ__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&peopletrack__device_req__descriptor) \
    , (char *)protobuf_c_empty_string }


/* Peopletrack__People methods */
void   peopletrack__people__init
                     (Peopletrack__People         *message);
size_t peopletrack__people__get_packed_size
                     (const Peopletrack__People   *message);
size_t peopletrack__people__pack
                     (const Peopletrack__People   *message,
                      uint8_t             *out);
size_t peopletrack__people__pack_to_buffer
                     (const Peopletrack__People   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__People *
       peopletrack__people__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__people__free_unpacked
                     (Peopletrack__People *message,
                      ProtobufCAllocator *allocator);
/* Peopletrack__Msg methods */
void   peopletrack__msg__init
                     (Peopletrack__Msg         *message);
size_t peopletrack__msg__get_packed_size
                     (const Peopletrack__Msg   *message);
size_t peopletrack__msg__pack
                     (const Peopletrack__Msg   *message,
                      uint8_t             *out);
size_t peopletrack__msg__pack_to_buffer
                     (const Peopletrack__Msg   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__Msg *
       peopletrack__msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__msg__free_unpacked
                     (Peopletrack__Msg *message,
                      ProtobufCAllocator *allocator);
/* Peopletrack__StitchParam methods */
void   peopletrack__stitch_param__init
                     (Peopletrack__StitchParam         *message);
size_t peopletrack__stitch_param__get_packed_size
                     (const Peopletrack__StitchParam   *message);
size_t peopletrack__stitch_param__pack
                     (const Peopletrack__StitchParam   *message,
                      uint8_t             *out);
size_t peopletrack__stitch_param__pack_to_buffer
                     (const Peopletrack__StitchParam   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__StitchParam *
       peopletrack__stitch_param__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__stitch_param__free_unpacked
                     (Peopletrack__StitchParam *message,
                      ProtobufCAllocator *allocator);
/* Peopletrack__SetupInput methods */
void   peopletrack__setup_input__init
                     (Peopletrack__SetupInput         *message);
size_t peopletrack__setup_input__get_packed_size
                     (const Peopletrack__SetupInput   *message);
size_t peopletrack__setup_input__pack
                     (const Peopletrack__SetupInput   *message,
                      uint8_t             *out);
size_t peopletrack__setup_input__pack_to_buffer
                     (const Peopletrack__SetupInput   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__SetupInput *
       peopletrack__setup_input__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__setup_input__free_unpacked
                     (Peopletrack__SetupInput *message,
                      ProtobufCAllocator *allocator);
/* Peopletrack__SetupOutput methods */
void   peopletrack__setup_output__init
                     (Peopletrack__SetupOutput         *message);
size_t peopletrack__setup_output__get_packed_size
                     (const Peopletrack__SetupOutput   *message);
size_t peopletrack__setup_output__pack
                     (const Peopletrack__SetupOutput   *message,
                      uint8_t             *out);
size_t peopletrack__setup_output__pack_to_buffer
                     (const Peopletrack__SetupOutput   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__SetupOutput *
       peopletrack__setup_output__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__setup_output__free_unpacked
                     (Peopletrack__SetupOutput *message,
                      ProtobufCAllocator *allocator);
/* Peopletrack__StitchInput methods */
void   peopletrack__stitch_input__init
                     (Peopletrack__StitchInput         *message);
size_t peopletrack__stitch_input__get_packed_size
                     (const Peopletrack__StitchInput   *message);
size_t peopletrack__stitch_input__pack
                     (const Peopletrack__StitchInput   *message,
                      uint8_t             *out);
size_t peopletrack__stitch_input__pack_to_buffer
                     (const Peopletrack__StitchInput   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__StitchInput *
       peopletrack__stitch_input__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__stitch_input__free_unpacked
                     (Peopletrack__StitchInput *message,
                      ProtobufCAllocator *allocator);
/* Peopletrack__StitchOutput methods */
void   peopletrack__stitch_output__init
                     (Peopletrack__StitchOutput         *message);
size_t peopletrack__stitch_output__get_packed_size
                     (const Peopletrack__StitchOutput   *message);
size_t peopletrack__stitch_output__pack
                     (const Peopletrack__StitchOutput   *message,
                      uint8_t             *out);
size_t peopletrack__stitch_output__pack_to_buffer
                     (const Peopletrack__StitchOutput   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__StitchOutput *
       peopletrack__stitch_output__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__stitch_output__free_unpacked
                     (Peopletrack__StitchOutput *message,
                      ProtobufCAllocator *allocator);
/* Peopletrack__Device methods */
void   peopletrack__device__init
                     (Peopletrack__Device         *message);
size_t peopletrack__device__get_packed_size
                     (const Peopletrack__Device   *message);
size_t peopletrack__device__pack
                     (const Peopletrack__Device   *message,
                      uint8_t             *out);
size_t peopletrack__device__pack_to_buffer
                     (const Peopletrack__Device   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__Device *
       peopletrack__device__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__device__free_unpacked
                     (Peopletrack__Device *message,
                      ProtobufCAllocator *allocator);
/* Peopletrack__Devices methods */
void   peopletrack__devices__init
                     (Peopletrack__Devices         *message);
size_t peopletrack__devices__get_packed_size
                     (const Peopletrack__Devices   *message);
size_t peopletrack__devices__pack
                     (const Peopletrack__Devices   *message,
                      uint8_t             *out);
size_t peopletrack__devices__pack_to_buffer
                     (const Peopletrack__Devices   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__Devices *
       peopletrack__devices__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__devices__free_unpacked
                     (Peopletrack__Devices *message,
                      ProtobufCAllocator *allocator);
/* Peopletrack__DeviceReq methods */
void   peopletrack__device_req__init
                     (Peopletrack__DeviceReq         *message);
size_t peopletrack__device_req__get_packed_size
                     (const Peopletrack__DeviceReq   *message);
size_t peopletrack__device_req__pack
                     (const Peopletrack__DeviceReq   *message,
                      uint8_t             *out);
size_t peopletrack__device_req__pack_to_buffer
                     (const Peopletrack__DeviceReq   *message,
                      ProtobufCBuffer     *buffer);
Peopletrack__DeviceReq *
       peopletrack__device_req__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   peopletrack__device_req__free_unpacked
                     (Peopletrack__DeviceReq *message,
                      ProtobufCAllocator *allocator);
/* --- per-message closures --- */

typedef void (*Peopletrack__People_Closure)
                 (const Peopletrack__People *message,
                  void *closure_data);
typedef void (*Peopletrack__Msg_Closure)
                 (const Peopletrack__Msg *message,
                  void *closure_data);
typedef void (*Peopletrack__StitchParam_Closure)
                 (const Peopletrack__StitchParam *message,
                  void *closure_data);
typedef void (*Peopletrack__SetupInput_Closure)
                 (const Peopletrack__SetupInput *message,
                  void *closure_data);
typedef void (*Peopletrack__SetupOutput_Closure)
                 (const Peopletrack__SetupOutput *message,
                  void *closure_data);
typedef void (*Peopletrack__StitchInput_Closure)
                 (const Peopletrack__StitchInput *message,
                  void *closure_data);
typedef void (*Peopletrack__StitchOutput_Closure)
                 (const Peopletrack__StitchOutput *message,
                  void *closure_data);
typedef void (*Peopletrack__Device_Closure)
                 (const Peopletrack__Device *message,
                  void *closure_data);
typedef void (*Peopletrack__Devices_Closure)
                 (const Peopletrack__Devices *message,
                  void *closure_data);
typedef void (*Peopletrack__DeviceReq_Closure)
                 (const Peopletrack__DeviceReq *message,
                  void *closure_data);

/* --- services --- */

typedef struct _Peopletrack__AI_Service Peopletrack__AI_Service;
struct _Peopletrack__AI_Service
{
  ProtobufCService base;
  void (*echo)(Peopletrack__AI_Service *service,
               const Peopletrack__Msg *input,
               Peopletrack__Msg_Closure closure,
               void *closure_data);
  void (*setup)(Peopletrack__AI_Service *service,
                const Peopletrack__SetupInput *input,
                Peopletrack__SetupOutput_Closure closure,
                void *closure_data);
  void (*stitch)(Peopletrack__AI_Service *service,
                 const Peopletrack__StitchInput *input,
                 Peopletrack__StitchOutput_Closure closure,
                 void *closure_data);
  void (*stitch_app)(Peopletrack__AI_Service *service,
                     const Peopletrack__Devices *input,
                     Peopletrack__StitchOutput_Closure closure,
                     void *closure_data);
  void (*get_msg)(Peopletrack__AI_Service *service,
                  const Peopletrack__DeviceReq *input,
                  Peopletrack__Msg_Closure closure,
                  void *closure_data);
};
typedef void (*Peopletrack__AI_ServiceDestroy)(Peopletrack__AI_Service *);
void peopletrack__ai__init (Peopletrack__AI_Service *service,
                            Peopletrack__AI_ServiceDestroy destroy);
#define PEOPLETRACK__AI__BASE_INIT \
    { &peopletrack__ai__descriptor, protobuf_c_service_invoke_internal, NULL }
#define PEOPLETRACK__AI__INIT(function_prefix__) \
    { PEOPLETRACK__AI__BASE_INIT,\
      function_prefix__ ## echo,\
      function_prefix__ ## setup,\
      function_prefix__ ## stitch,\
      function_prefix__ ## stitch_app,\
      function_prefix__ ## get_msg  }
void peopletrack__ai__echo(ProtobufCService *service,
                           const Peopletrack__Msg *input,
                           Peopletrack__Msg_Closure closure,
                           void *closure_data);
void peopletrack__ai__setup(ProtobufCService *service,
                            const Peopletrack__SetupInput *input,
                            Peopletrack__SetupOutput_Closure closure,
                            void *closure_data);
void peopletrack__ai__stitch(ProtobufCService *service,
                             const Peopletrack__StitchInput *input,
                             Peopletrack__StitchOutput_Closure closure,
                             void *closure_data);
void peopletrack__ai__stitch_app(ProtobufCService *service,
                                 const Peopletrack__Devices *input,
                                 Peopletrack__StitchOutput_Closure closure,
                                 void *closure_data);
void peopletrack__ai__get_msg(ProtobufCService *service,
                              const Peopletrack__DeviceReq *input,
                              Peopletrack__Msg_Closure closure,
                              void *closure_data);

/* --- descriptors --- */

extern const ProtobufCMessageDescriptor peopletrack__people__descriptor;
extern const ProtobufCEnumDescriptor    peopletrack__people__state__descriptor;
extern const ProtobufCEnumDescriptor    peopletrack__people__stance__descriptor;
extern const ProtobufCMessageDescriptor peopletrack__msg__descriptor;
extern const ProtobufCMessageDescriptor peopletrack__stitch_param__descriptor;
extern const ProtobufCMessageDescriptor peopletrack__setup_input__descriptor;
extern const ProtobufCMessageDescriptor peopletrack__setup_output__descriptor;
extern const ProtobufCMessageDescriptor peopletrack__stitch_input__descriptor;
extern const ProtobufCMessageDescriptor peopletrack__stitch_output__descriptor;
extern const ProtobufCMessageDescriptor peopletrack__device__descriptor;
extern const ProtobufCMessageDescriptor peopletrack__devices__descriptor;
extern const ProtobufCMessageDescriptor peopletrack__device_req__descriptor;
extern const ProtobufCServiceDescriptor peopletrack__ai__descriptor;

PROTOBUF_C__END_DECLS


#endif  /* PROTOBUF_C_peopletrack_2eproto__INCLUDED */
