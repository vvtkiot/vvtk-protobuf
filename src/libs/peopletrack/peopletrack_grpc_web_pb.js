/**
 * @fileoverview gRPC-Web generated client stub for Peopletrack
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.Peopletrack = require('./peopletrack_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.Peopletrack.AIClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.Peopletrack.AIPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.Peopletrack.Msg,
 *   !proto.Peopletrack.Msg>}
 */
const methodDescriptor_AI_Echo = new grpc.web.MethodDescriptor(
  '/Peopletrack.AI/Echo',
  grpc.web.MethodType.UNARY,
  proto.Peopletrack.Msg,
  proto.Peopletrack.Msg,
  /**
   * @param {!proto.Peopletrack.Msg} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Peopletrack.Msg.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.Peopletrack.Msg,
 *   !proto.Peopletrack.Msg>}
 */
const methodInfo_AI_Echo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.Peopletrack.Msg,
  /**
   * @param {!proto.Peopletrack.Msg} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Peopletrack.Msg.deserializeBinary
);


/**
 * @param {!proto.Peopletrack.Msg} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.Peopletrack.Msg)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.Peopletrack.Msg>|undefined}
 *     The XHR Node Readable Stream
 */
proto.Peopletrack.AIClient.prototype.echo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/Peopletrack.AI/Echo',
      request,
      metadata || {},
      methodDescriptor_AI_Echo,
      callback);
};


/**
 * @param {!proto.Peopletrack.Msg} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.Peopletrack.Msg>}
 *     Promise that resolves to the response
 */
proto.Peopletrack.AIPromiseClient.prototype.echo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/Peopletrack.AI/Echo',
      request,
      metadata || {},
      methodDescriptor_AI_Echo);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.Peopletrack.Devices,
 *   !proto.Peopletrack.StitchOutput>}
 */
const methodDescriptor_AI_StitchApp = new grpc.web.MethodDescriptor(
  '/Peopletrack.AI/StitchApp',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.Peopletrack.Devices,
  proto.Peopletrack.StitchOutput,
  /**
   * @param {!proto.Peopletrack.Devices} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Peopletrack.StitchOutput.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.Peopletrack.Devices,
 *   !proto.Peopletrack.StitchOutput>}
 */
const methodInfo_AI_StitchApp = new grpc.web.AbstractClientBase.MethodInfo(
  proto.Peopletrack.StitchOutput,
  /**
   * @param {!proto.Peopletrack.Devices} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Peopletrack.StitchOutput.deserializeBinary
);


/**
 * @param {!proto.Peopletrack.Devices} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Peopletrack.StitchOutput>}
 *     The XHR Node Readable Stream
 */
proto.Peopletrack.AIClient.prototype.stitchApp =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Peopletrack.AI/StitchApp',
      request,
      metadata || {},
      methodDescriptor_AI_StitchApp);
};


/**
 * @param {!proto.Peopletrack.Devices} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Peopletrack.StitchOutput>}
 *     The XHR Node Readable Stream
 */
proto.Peopletrack.AIPromiseClient.prototype.stitchApp =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Peopletrack.AI/StitchApp',
      request,
      metadata || {},
      methodDescriptor_AI_StitchApp);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.Peopletrack.DeviceReq,
 *   !proto.Peopletrack.Msg>}
 */
const methodDescriptor_AI_GetMsg = new grpc.web.MethodDescriptor(
  '/Peopletrack.AI/GetMsg',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.Peopletrack.DeviceReq,
  proto.Peopletrack.Msg,
  /**
   * @param {!proto.Peopletrack.DeviceReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Peopletrack.Msg.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.Peopletrack.DeviceReq,
 *   !proto.Peopletrack.Msg>}
 */
const methodInfo_AI_GetMsg = new grpc.web.AbstractClientBase.MethodInfo(
  proto.Peopletrack.Msg,
  /**
   * @param {!proto.Peopletrack.DeviceReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Peopletrack.Msg.deserializeBinary
);


/**
 * @param {!proto.Peopletrack.DeviceReq} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Peopletrack.Msg>}
 *     The XHR Node Readable Stream
 */
proto.Peopletrack.AIClient.prototype.getMsg =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Peopletrack.AI/GetMsg',
      request,
      metadata || {},
      methodDescriptor_AI_GetMsg);
};


/**
 * @param {!proto.Peopletrack.DeviceReq} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Peopletrack.Msg>}
 *     The XHR Node Readable Stream
 */
proto.Peopletrack.AIPromiseClient.prototype.getMsg =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Peopletrack.AI/GetMsg',
      request,
      metadata || {},
      methodDescriptor_AI_GetMsg);
};


module.exports = proto.Peopletrack;

