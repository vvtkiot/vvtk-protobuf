import * as grpcWeb from 'grpc-web';

import * as peopletrack_pb from './peopletrack_pb';


export class AIClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  echo(
    request: peopletrack_pb.Msg,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: peopletrack_pb.Msg) => void
  ): grpcWeb.ClientReadableStream<peopletrack_pb.Msg>;

  stitchApp(
    request: peopletrack_pb.Devices,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<peopletrack_pb.StitchOutput>;

  getMsg(
    request: peopletrack_pb.DeviceReq,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<peopletrack_pb.Msg>;

}

export class AIPromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  echo(
    request: peopletrack_pb.Msg,
    metadata?: grpcWeb.Metadata
  ): Promise<peopletrack_pb.Msg>;

  stitchApp(
    request: peopletrack_pb.Devices,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<peopletrack_pb.StitchOutput>;

  getMsg(
    request: peopletrack_pb.DeviceReq,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<peopletrack_pb.Msg>;

}

