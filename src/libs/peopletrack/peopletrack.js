/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Peopletrack = $root.Peopletrack = (() => {

    /**
     * Namespace Peopletrack.
     * @exports Peopletrack
     * @namespace
     */
    const Peopletrack = {};

    Peopletrack.People = (function() {

        /**
         * Properties of a People.
         * @memberof Peopletrack
         * @interface IPeople
         * @property {number|null} [tid] People tid
         * @property {number|null} [posX] People posX
         * @property {number|null} [posY] People posY
         * @property {number|null} [posZ] People posZ
         * @property {Peopletrack.People.State|null} [state] People state
         * @property {Peopletrack.People.Stance|null} [stance] People stance
         */

        /**
         * Constructs a new People.
         * @memberof Peopletrack
         * @classdesc Represents a People.
         * @implements IPeople
         * @constructor
         * @param {Peopletrack.IPeople=} [properties] Properties to set
         */
        function People(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * People tid.
         * @member {number} tid
         * @memberof Peopletrack.People
         * @instance
         */
        People.prototype.tid = 0;

        /**
         * People posX.
         * @member {number} posX
         * @memberof Peopletrack.People
         * @instance
         */
        People.prototype.posX = 0;

        /**
         * People posY.
         * @member {number} posY
         * @memberof Peopletrack.People
         * @instance
         */
        People.prototype.posY = 0;

        /**
         * People posZ.
         * @member {number} posZ
         * @memberof Peopletrack.People
         * @instance
         */
        People.prototype.posZ = 0;

        /**
         * People state.
         * @member {Peopletrack.People.State} state
         * @memberof Peopletrack.People
         * @instance
         */
        People.prototype.state = 0;

        /**
         * People stance.
         * @member {Peopletrack.People.Stance} stance
         * @memberof Peopletrack.People
         * @instance
         */
        People.prototype.stance = 0;

        /**
         * Creates a new People instance using the specified properties.
         * @function create
         * @memberof Peopletrack.People
         * @static
         * @param {Peopletrack.IPeople=} [properties] Properties to set
         * @returns {Peopletrack.People} People instance
         */
        People.create = function create(properties) {
            return new People(properties);
        };

        /**
         * Encodes the specified People message. Does not implicitly {@link Peopletrack.People.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.People
         * @static
         * @param {Peopletrack.IPeople} message People message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        People.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.tid != null && Object.hasOwnProperty.call(message, "tid"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.tid);
            if (message.posX != null && Object.hasOwnProperty.call(message, "posX"))
                writer.uint32(/* id 2, wireType 5 =*/21).float(message.posX);
            if (message.posY != null && Object.hasOwnProperty.call(message, "posY"))
                writer.uint32(/* id 3, wireType 5 =*/29).float(message.posY);
            if (message.posZ != null && Object.hasOwnProperty.call(message, "posZ"))
                writer.uint32(/* id 4, wireType 5 =*/37).float(message.posZ);
            if (message.state != null && Object.hasOwnProperty.call(message, "state"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.state);
            if (message.stance != null && Object.hasOwnProperty.call(message, "stance"))
                writer.uint32(/* id 6, wireType 0 =*/48).int32(message.stance);
            return writer;
        };

        /**
         * Encodes the specified People message, length delimited. Does not implicitly {@link Peopletrack.People.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.People
         * @static
         * @param {Peopletrack.IPeople} message People message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        People.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a People message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.People
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.People} People
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        People.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.People();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.tid = reader.uint32();
                    break;
                case 2:
                    message.posX = reader.float();
                    break;
                case 3:
                    message.posY = reader.float();
                    break;
                case 4:
                    message.posZ = reader.float();
                    break;
                case 5:
                    message.state = reader.int32();
                    break;
                case 6:
                    message.stance = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a People message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.People
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.People} People
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        People.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a People message.
         * @function verify
         * @memberof Peopletrack.People
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        People.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.tid != null && message.hasOwnProperty("tid"))
                if (!$util.isInteger(message.tid))
                    return "tid: integer expected";
            if (message.posX != null && message.hasOwnProperty("posX"))
                if (typeof message.posX !== "number")
                    return "posX: number expected";
            if (message.posY != null && message.hasOwnProperty("posY"))
                if (typeof message.posY !== "number")
                    return "posY: number expected";
            if (message.posZ != null && message.hasOwnProperty("posZ"))
                if (typeof message.posZ !== "number")
                    return "posZ: number expected";
            if (message.state != null && message.hasOwnProperty("state"))
                switch (message.state) {
                default:
                    return "state: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.stance != null && message.hasOwnProperty("stance"))
                switch (message.stance) {
                default:
                    return "stance: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    break;
                }
            return null;
        };

        /**
         * Creates a People message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.People
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.People} People
         */
        People.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.People)
                return object;
            let message = new $root.Peopletrack.People();
            if (object.tid != null)
                message.tid = object.tid >>> 0;
            if (object.posX != null)
                message.posX = Number(object.posX);
            if (object.posY != null)
                message.posY = Number(object.posY);
            if (object.posZ != null)
                message.posZ = Number(object.posZ);
            switch (object.state) {
            case "NONE":
            case 0:
                message.state = 0;
                break;
            case "ACTIVE":
            case 1:
                message.state = 1;
                break;
            case "STATIC":
            case 2:
                message.state = 2;
                break;
            }
            switch (object.stance) {
            case "UNKNOWN":
            case 0:
                message.stance = 0;
                break;
            case "STAND":
            case 1:
                message.stance = 1;
                break;
            case "SIT":
            case 2:
                message.stance = 2;
                break;
            case "LAY":
            case 3:
                message.stance = 3;
                break;
            case "SUSFALL":
            case 4:
                message.stance = 4;
                break;
            case "COUNTDOWN":
            case 5:
                message.stance = 5;
                break;
            case "DETFALL":
            case 6:
                message.stance = 6;
                break;
            }
            return message;
        };

        /**
         * Creates a plain object from a People message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.People
         * @static
         * @param {Peopletrack.People} message People
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        People.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.tid = 0;
                object.posX = 0;
                object.posY = 0;
                object.posZ = 0;
                object.state = options.enums === String ? "NONE" : 0;
                object.stance = options.enums === String ? "UNKNOWN" : 0;
            }
            if (message.tid != null && message.hasOwnProperty("tid"))
                object.tid = message.tid;
            if (message.posX != null && message.hasOwnProperty("posX"))
                object.posX = options.json && !isFinite(message.posX) ? String(message.posX) : message.posX;
            if (message.posY != null && message.hasOwnProperty("posY"))
                object.posY = options.json && !isFinite(message.posY) ? String(message.posY) : message.posY;
            if (message.posZ != null && message.hasOwnProperty("posZ"))
                object.posZ = options.json && !isFinite(message.posZ) ? String(message.posZ) : message.posZ;
            if (message.state != null && message.hasOwnProperty("state"))
                object.state = options.enums === String ? $root.Peopletrack.People.State[message.state] : message.state;
            if (message.stance != null && message.hasOwnProperty("stance"))
                object.stance = options.enums === String ? $root.Peopletrack.People.Stance[message.stance] : message.stance;
            return object;
        };

        /**
         * Converts this People to JSON.
         * @function toJSON
         * @memberof Peopletrack.People
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        People.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * State enum.
         * @name Peopletrack.People.State
         * @enum {number}
         * @property {number} NONE=0 NONE value
         * @property {number} ACTIVE=1 ACTIVE value
         * @property {number} STATIC=2 STATIC value
         */
        People.State = (function() {
            const valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "NONE"] = 0;
            values[valuesById[1] = "ACTIVE"] = 1;
            values[valuesById[2] = "STATIC"] = 2;
            return values;
        })();

        /**
         * Stance enum.
         * @name Peopletrack.People.Stance
         * @enum {number}
         * @property {number} UNKNOWN=0 UNKNOWN value
         * @property {number} STAND=1 STAND value
         * @property {number} SIT=2 SIT value
         * @property {number} LAY=3 LAY value
         * @property {number} SUSFALL=4 SUSFALL value
         * @property {number} COUNTDOWN=5 COUNTDOWN value
         * @property {number} DETFALL=6 DETFALL value
         */
        People.Stance = (function() {
            const valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "UNKNOWN"] = 0;
            values[valuesById[1] = "STAND"] = 1;
            values[valuesById[2] = "SIT"] = 2;
            values[valuesById[3] = "LAY"] = 3;
            values[valuesById[4] = "SUSFALL"] = 4;
            values[valuesById[5] = "COUNTDOWN"] = 5;
            values[valuesById[6] = "DETFALL"] = 6;
            return values;
        })();

        return People;
    })();

    Peopletrack.Msg = (function() {

        /**
         * Properties of a Msg.
         * @memberof Peopletrack
         * @interface IMsg
         * @property {number|null} [frameNumber] Msg frameNumber
         * @property {number|Long|null} [timestamp] Msg timestamp
         * @property {Array.<Peopletrack.IPeople>|null} [people] Msg people
         * @property {Uint8Array|null} [cloudpoint] Msg cloudpoint
         */

        /**
         * Constructs a new Msg.
         * @memberof Peopletrack
         * @classdesc Represents a Msg.
         * @implements IMsg
         * @constructor
         * @param {Peopletrack.IMsg=} [properties] Properties to set
         */
        function Msg(properties) {
            this.people = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Msg frameNumber.
         * @member {number} frameNumber
         * @memberof Peopletrack.Msg
         * @instance
         */
        Msg.prototype.frameNumber = 0;

        /**
         * Msg timestamp.
         * @member {number|Long} timestamp
         * @memberof Peopletrack.Msg
         * @instance
         */
        Msg.prototype.timestamp = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * Msg people.
         * @member {Array.<Peopletrack.IPeople>} people
         * @memberof Peopletrack.Msg
         * @instance
         */
        Msg.prototype.people = $util.emptyArray;

        /**
         * Msg cloudpoint.
         * @member {Uint8Array} cloudpoint
         * @memberof Peopletrack.Msg
         * @instance
         */
        Msg.prototype.cloudpoint = $util.newBuffer([]);

        /**
         * Creates a new Msg instance using the specified properties.
         * @function create
         * @memberof Peopletrack.Msg
         * @static
         * @param {Peopletrack.IMsg=} [properties] Properties to set
         * @returns {Peopletrack.Msg} Msg instance
         */
        Msg.create = function create(properties) {
            return new Msg(properties);
        };

        /**
         * Encodes the specified Msg message. Does not implicitly {@link Peopletrack.Msg.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.Msg
         * @static
         * @param {Peopletrack.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.frameNumber);
            if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.timestamp);
            if (message.people != null && message.people.length)
                for (let i = 0; i < message.people.length; ++i)
                    $root.Peopletrack.People.encode(message.people[i], writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.cloudpoint != null && Object.hasOwnProperty.call(message, "cloudpoint"))
                writer.uint32(/* id 4, wireType 2 =*/34).bytes(message.cloudpoint);
            return writer;
        };

        /**
         * Encodes the specified Msg message, length delimited. Does not implicitly {@link Peopletrack.Msg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.Msg
         * @static
         * @param {Peopletrack.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Msg message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.Msg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.frameNumber = reader.uint32();
                    break;
                case 2:
                    message.timestamp = reader.uint64();
                    break;
                case 3:
                    if (!(message.people && message.people.length))
                        message.people = [];
                    message.people.push($root.Peopletrack.People.decode(reader, reader.uint32()));
                    break;
                case 4:
                    message.cloudpoint = reader.bytes();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Msg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Msg message.
         * @function verify
         * @memberof Peopletrack.Msg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Msg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                if (!$util.isInteger(message.frameNumber))
                    return "frameNumber: integer expected";
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (!$util.isInteger(message.timestamp) && !(message.timestamp && $util.isInteger(message.timestamp.low) && $util.isInteger(message.timestamp.high)))
                    return "timestamp: integer|Long expected";
            if (message.people != null && message.hasOwnProperty("people")) {
                if (!Array.isArray(message.people))
                    return "people: array expected";
                for (let i = 0; i < message.people.length; ++i) {
                    let error = $root.Peopletrack.People.verify(message.people[i]);
                    if (error)
                        return "people." + error;
                }
            }
            if (message.cloudpoint != null && message.hasOwnProperty("cloudpoint"))
                if (!(message.cloudpoint && typeof message.cloudpoint.length === "number" || $util.isString(message.cloudpoint)))
                    return "cloudpoint: buffer expected";
            return null;
        };

        /**
         * Creates a Msg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.Msg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.Msg} Msg
         */
        Msg.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.Msg)
                return object;
            let message = new $root.Peopletrack.Msg();
            if (object.frameNumber != null)
                message.frameNumber = object.frameNumber >>> 0;
            if (object.timestamp != null)
                if ($util.Long)
                    (message.timestamp = $util.Long.fromValue(object.timestamp)).unsigned = true;
                else if (typeof object.timestamp === "string")
                    message.timestamp = parseInt(object.timestamp, 10);
                else if (typeof object.timestamp === "number")
                    message.timestamp = object.timestamp;
                else if (typeof object.timestamp === "object")
                    message.timestamp = new $util.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);
            if (object.people) {
                if (!Array.isArray(object.people))
                    throw TypeError(".Peopletrack.Msg.people: array expected");
                message.people = [];
                for (let i = 0; i < object.people.length; ++i) {
                    if (typeof object.people[i] !== "object")
                        throw TypeError(".Peopletrack.Msg.people: object expected");
                    message.people[i] = $root.Peopletrack.People.fromObject(object.people[i]);
                }
            }
            if (object.cloudpoint != null)
                if (typeof object.cloudpoint === "string")
                    $util.base64.decode(object.cloudpoint, message.cloudpoint = $util.newBuffer($util.base64.length(object.cloudpoint)), 0);
                else if (object.cloudpoint.length)
                    message.cloudpoint = object.cloudpoint;
            return message;
        };

        /**
         * Creates a plain object from a Msg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.Msg
         * @static
         * @param {Peopletrack.Msg} message Msg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Msg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults)
                object.people = [];
            if (options.defaults) {
                object.frameNumber = 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, true);
                    object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.timestamp = options.longs === String ? "0" : 0;
                if (options.bytes === String)
                    object.cloudpoint = "";
                else {
                    object.cloudpoint = [];
                    if (options.bytes !== Array)
                        object.cloudpoint = $util.newBuffer(object.cloudpoint);
                }
            }
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                object.frameNumber = message.frameNumber;
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (typeof message.timestamp === "number")
                    object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;
                else
                    object.timestamp = options.longs === String ? $util.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;
            if (message.people && message.people.length) {
                object.people = [];
                for (let j = 0; j < message.people.length; ++j)
                    object.people[j] = $root.Peopletrack.People.toObject(message.people[j], options);
            }
            if (message.cloudpoint != null && message.hasOwnProperty("cloudpoint"))
                object.cloudpoint = options.bytes === String ? $util.base64.encode(message.cloudpoint, 0, message.cloudpoint.length) : options.bytes === Array ? Array.prototype.slice.call(message.cloudpoint) : message.cloudpoint;
            return object;
        };

        /**
         * Converts this Msg to JSON.
         * @function toJSON
         * @memberof Peopletrack.Msg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Msg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Msg;
    })();

    Peopletrack.StitchParam = (function() {

        /**
         * Properties of a StitchParam.
         * @memberof Peopletrack
         * @interface IStitchParam
         * @property {number|null} [height] StitchParam height
         * @property {number|null} [theta] StitchParam theta
         * @property {number|null} [shiftX] StitchParam shiftX
         * @property {number|null} [shiftZ] StitchParam shiftZ
         */

        /**
         * Constructs a new StitchParam.
         * @memberof Peopletrack
         * @classdesc Represents a StitchParam.
         * @implements IStitchParam
         * @constructor
         * @param {Peopletrack.IStitchParam=} [properties] Properties to set
         */
        function StitchParam(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * StitchParam height.
         * @member {number} height
         * @memberof Peopletrack.StitchParam
         * @instance
         */
        StitchParam.prototype.height = 0;

        /**
         * StitchParam theta.
         * @member {number} theta
         * @memberof Peopletrack.StitchParam
         * @instance
         */
        StitchParam.prototype.theta = 0;

        /**
         * StitchParam shiftX.
         * @member {number} shiftX
         * @memberof Peopletrack.StitchParam
         * @instance
         */
        StitchParam.prototype.shiftX = 0;

        /**
         * StitchParam shiftZ.
         * @member {number} shiftZ
         * @memberof Peopletrack.StitchParam
         * @instance
         */
        StitchParam.prototype.shiftZ = 0;

        /**
         * Creates a new StitchParam instance using the specified properties.
         * @function create
         * @memberof Peopletrack.StitchParam
         * @static
         * @param {Peopletrack.IStitchParam=} [properties] Properties to set
         * @returns {Peopletrack.StitchParam} StitchParam instance
         */
        StitchParam.create = function create(properties) {
            return new StitchParam(properties);
        };

        /**
         * Encodes the specified StitchParam message. Does not implicitly {@link Peopletrack.StitchParam.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.StitchParam
         * @static
         * @param {Peopletrack.IStitchParam} message StitchParam message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StitchParam.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.height != null && Object.hasOwnProperty.call(message, "height"))
                writer.uint32(/* id 1, wireType 5 =*/13).float(message.height);
            if (message.theta != null && Object.hasOwnProperty.call(message, "theta"))
                writer.uint32(/* id 2, wireType 5 =*/21).float(message.theta);
            if (message.shiftX != null && Object.hasOwnProperty.call(message, "shiftX"))
                writer.uint32(/* id 3, wireType 5 =*/29).float(message.shiftX);
            if (message.shiftZ != null && Object.hasOwnProperty.call(message, "shiftZ"))
                writer.uint32(/* id 4, wireType 5 =*/37).float(message.shiftZ);
            return writer;
        };

        /**
         * Encodes the specified StitchParam message, length delimited. Does not implicitly {@link Peopletrack.StitchParam.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.StitchParam
         * @static
         * @param {Peopletrack.IStitchParam} message StitchParam message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StitchParam.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a StitchParam message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.StitchParam
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.StitchParam} StitchParam
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StitchParam.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.StitchParam();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.height = reader.float();
                    break;
                case 2:
                    message.theta = reader.float();
                    break;
                case 3:
                    message.shiftX = reader.float();
                    break;
                case 4:
                    message.shiftZ = reader.float();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a StitchParam message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.StitchParam
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.StitchParam} StitchParam
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StitchParam.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a StitchParam message.
         * @function verify
         * @memberof Peopletrack.StitchParam
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        StitchParam.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.height != null && message.hasOwnProperty("height"))
                if (typeof message.height !== "number")
                    return "height: number expected";
            if (message.theta != null && message.hasOwnProperty("theta"))
                if (typeof message.theta !== "number")
                    return "theta: number expected";
            if (message.shiftX != null && message.hasOwnProperty("shiftX"))
                if (typeof message.shiftX !== "number")
                    return "shiftX: number expected";
            if (message.shiftZ != null && message.hasOwnProperty("shiftZ"))
                if (typeof message.shiftZ !== "number")
                    return "shiftZ: number expected";
            return null;
        };

        /**
         * Creates a StitchParam message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.StitchParam
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.StitchParam} StitchParam
         */
        StitchParam.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.StitchParam)
                return object;
            let message = new $root.Peopletrack.StitchParam();
            if (object.height != null)
                message.height = Number(object.height);
            if (object.theta != null)
                message.theta = Number(object.theta);
            if (object.shiftX != null)
                message.shiftX = Number(object.shiftX);
            if (object.shiftZ != null)
                message.shiftZ = Number(object.shiftZ);
            return message;
        };

        /**
         * Creates a plain object from a StitchParam message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.StitchParam
         * @static
         * @param {Peopletrack.StitchParam} message StitchParam
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        StitchParam.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.height = 0;
                object.theta = 0;
                object.shiftX = 0;
                object.shiftZ = 0;
            }
            if (message.height != null && message.hasOwnProperty("height"))
                object.height = options.json && !isFinite(message.height) ? String(message.height) : message.height;
            if (message.theta != null && message.hasOwnProperty("theta"))
                object.theta = options.json && !isFinite(message.theta) ? String(message.theta) : message.theta;
            if (message.shiftX != null && message.hasOwnProperty("shiftX"))
                object.shiftX = options.json && !isFinite(message.shiftX) ? String(message.shiftX) : message.shiftX;
            if (message.shiftZ != null && message.hasOwnProperty("shiftZ"))
                object.shiftZ = options.json && !isFinite(message.shiftZ) ? String(message.shiftZ) : message.shiftZ;
            return object;
        };

        /**
         * Converts this StitchParam to JSON.
         * @function toJSON
         * @memberof Peopletrack.StitchParam
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        StitchParam.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return StitchParam;
    })();

    Peopletrack.SetupInput = (function() {

        /**
         * Properties of a SetupInput.
         * @memberof Peopletrack
         * @interface ISetupInput
         * @property {number|null} [stitchID] SetupInput stitchID
         * @property {number|null} [frameNumber] SetupInput frameNumber
         * @property {number|Long|null} [timestamp] SetupInput timestamp
         * @property {Uint8Array|null} [cloudpoint] SetupInput cloudpoint
         */

        /**
         * Constructs a new SetupInput.
         * @memberof Peopletrack
         * @classdesc Represents a SetupInput.
         * @implements ISetupInput
         * @constructor
         * @param {Peopletrack.ISetupInput=} [properties] Properties to set
         */
        function SetupInput(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SetupInput stitchID.
         * @member {number} stitchID
         * @memberof Peopletrack.SetupInput
         * @instance
         */
        SetupInput.prototype.stitchID = 0;

        /**
         * SetupInput frameNumber.
         * @member {number} frameNumber
         * @memberof Peopletrack.SetupInput
         * @instance
         */
        SetupInput.prototype.frameNumber = 0;

        /**
         * SetupInput timestamp.
         * @member {number|Long} timestamp
         * @memberof Peopletrack.SetupInput
         * @instance
         */
        SetupInput.prototype.timestamp = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * SetupInput cloudpoint.
         * @member {Uint8Array} cloudpoint
         * @memberof Peopletrack.SetupInput
         * @instance
         */
        SetupInput.prototype.cloudpoint = $util.newBuffer([]);

        /**
         * Creates a new SetupInput instance using the specified properties.
         * @function create
         * @memberof Peopletrack.SetupInput
         * @static
         * @param {Peopletrack.ISetupInput=} [properties] Properties to set
         * @returns {Peopletrack.SetupInput} SetupInput instance
         */
        SetupInput.create = function create(properties) {
            return new SetupInput(properties);
        };

        /**
         * Encodes the specified SetupInput message. Does not implicitly {@link Peopletrack.SetupInput.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.SetupInput
         * @static
         * @param {Peopletrack.ISetupInput} message SetupInput message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetupInput.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.stitchID != null && Object.hasOwnProperty.call(message, "stitchID"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.stitchID);
            if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.frameNumber);
            if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint64(message.timestamp);
            if (message.cloudpoint != null && Object.hasOwnProperty.call(message, "cloudpoint"))
                writer.uint32(/* id 4, wireType 2 =*/34).bytes(message.cloudpoint);
            return writer;
        };

        /**
         * Encodes the specified SetupInput message, length delimited. Does not implicitly {@link Peopletrack.SetupInput.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.SetupInput
         * @static
         * @param {Peopletrack.ISetupInput} message SetupInput message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetupInput.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SetupInput message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.SetupInput
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.SetupInput} SetupInput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetupInput.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.SetupInput();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.stitchID = reader.uint32();
                    break;
                case 2:
                    message.frameNumber = reader.uint32();
                    break;
                case 3:
                    message.timestamp = reader.uint64();
                    break;
                case 4:
                    message.cloudpoint = reader.bytes();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SetupInput message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.SetupInput
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.SetupInput} SetupInput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetupInput.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SetupInput message.
         * @function verify
         * @memberof Peopletrack.SetupInput
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SetupInput.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.stitchID != null && message.hasOwnProperty("stitchID"))
                if (!$util.isInteger(message.stitchID))
                    return "stitchID: integer expected";
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                if (!$util.isInteger(message.frameNumber))
                    return "frameNumber: integer expected";
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (!$util.isInteger(message.timestamp) && !(message.timestamp && $util.isInteger(message.timestamp.low) && $util.isInteger(message.timestamp.high)))
                    return "timestamp: integer|Long expected";
            if (message.cloudpoint != null && message.hasOwnProperty("cloudpoint"))
                if (!(message.cloudpoint && typeof message.cloudpoint.length === "number" || $util.isString(message.cloudpoint)))
                    return "cloudpoint: buffer expected";
            return null;
        };

        /**
         * Creates a SetupInput message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.SetupInput
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.SetupInput} SetupInput
         */
        SetupInput.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.SetupInput)
                return object;
            let message = new $root.Peopletrack.SetupInput();
            if (object.stitchID != null)
                message.stitchID = object.stitchID >>> 0;
            if (object.frameNumber != null)
                message.frameNumber = object.frameNumber >>> 0;
            if (object.timestamp != null)
                if ($util.Long)
                    (message.timestamp = $util.Long.fromValue(object.timestamp)).unsigned = true;
                else if (typeof object.timestamp === "string")
                    message.timestamp = parseInt(object.timestamp, 10);
                else if (typeof object.timestamp === "number")
                    message.timestamp = object.timestamp;
                else if (typeof object.timestamp === "object")
                    message.timestamp = new $util.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);
            if (object.cloudpoint != null)
                if (typeof object.cloudpoint === "string")
                    $util.base64.decode(object.cloudpoint, message.cloudpoint = $util.newBuffer($util.base64.length(object.cloudpoint)), 0);
                else if (object.cloudpoint.length)
                    message.cloudpoint = object.cloudpoint;
            return message;
        };

        /**
         * Creates a plain object from a SetupInput message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.SetupInput
         * @static
         * @param {Peopletrack.SetupInput} message SetupInput
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SetupInput.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.stitchID = 0;
                object.frameNumber = 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, true);
                    object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.timestamp = options.longs === String ? "0" : 0;
                if (options.bytes === String)
                    object.cloudpoint = "";
                else {
                    object.cloudpoint = [];
                    if (options.bytes !== Array)
                        object.cloudpoint = $util.newBuffer(object.cloudpoint);
                }
            }
            if (message.stitchID != null && message.hasOwnProperty("stitchID"))
                object.stitchID = message.stitchID;
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                object.frameNumber = message.frameNumber;
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (typeof message.timestamp === "number")
                    object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;
                else
                    object.timestamp = options.longs === String ? $util.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;
            if (message.cloudpoint != null && message.hasOwnProperty("cloudpoint"))
                object.cloudpoint = options.bytes === String ? $util.base64.encode(message.cloudpoint, 0, message.cloudpoint.length) : options.bytes === Array ? Array.prototype.slice.call(message.cloudpoint) : message.cloudpoint;
            return object;
        };

        /**
         * Converts this SetupInput to JSON.
         * @function toJSON
         * @memberof Peopletrack.SetupInput
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SetupInput.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return SetupInput;
    })();

    Peopletrack.SetupOutput = (function() {

        /**
         * Properties of a SetupOutput.
         * @memberof Peopletrack
         * @interface ISetupOutput
         * @property {number|null} [frameNumber] SetupOutput frameNumber
         * @property {number|Long|null} [timestamp] SetupOutput timestamp
         * @property {Array.<Peopletrack.IPeople>|null} [people] SetupOutput people
         * @property {Uint8Array|null} [backgroundChart] SetupOutput backgroundChart
         * @property {Peopletrack.IStitchParam|null} [stitchParam] SetupOutput stitchParam
         */

        /**
         * Constructs a new SetupOutput.
         * @memberof Peopletrack
         * @classdesc Represents a SetupOutput.
         * @implements ISetupOutput
         * @constructor
         * @param {Peopletrack.ISetupOutput=} [properties] Properties to set
         */
        function SetupOutput(properties) {
            this.people = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SetupOutput frameNumber.
         * @member {number} frameNumber
         * @memberof Peopletrack.SetupOutput
         * @instance
         */
        SetupOutput.prototype.frameNumber = 0;

        /**
         * SetupOutput timestamp.
         * @member {number|Long} timestamp
         * @memberof Peopletrack.SetupOutput
         * @instance
         */
        SetupOutput.prototype.timestamp = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * SetupOutput people.
         * @member {Array.<Peopletrack.IPeople>} people
         * @memberof Peopletrack.SetupOutput
         * @instance
         */
        SetupOutput.prototype.people = $util.emptyArray;

        /**
         * SetupOutput backgroundChart.
         * @member {Uint8Array} backgroundChart
         * @memberof Peopletrack.SetupOutput
         * @instance
         */
        SetupOutput.prototype.backgroundChart = $util.newBuffer([]);

        /**
         * SetupOutput stitchParam.
         * @member {Peopletrack.IStitchParam|null|undefined} stitchParam
         * @memberof Peopletrack.SetupOutput
         * @instance
         */
        SetupOutput.prototype.stitchParam = null;

        /**
         * Creates a new SetupOutput instance using the specified properties.
         * @function create
         * @memberof Peopletrack.SetupOutput
         * @static
         * @param {Peopletrack.ISetupOutput=} [properties] Properties to set
         * @returns {Peopletrack.SetupOutput} SetupOutput instance
         */
        SetupOutput.create = function create(properties) {
            return new SetupOutput(properties);
        };

        /**
         * Encodes the specified SetupOutput message. Does not implicitly {@link Peopletrack.SetupOutput.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.SetupOutput
         * @static
         * @param {Peopletrack.ISetupOutput} message SetupOutput message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetupOutput.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.frameNumber);
            if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.timestamp);
            if (message.people != null && message.people.length)
                for (let i = 0; i < message.people.length; ++i)
                    $root.Peopletrack.People.encode(message.people[i], writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.backgroundChart != null && Object.hasOwnProperty.call(message, "backgroundChart"))
                writer.uint32(/* id 4, wireType 2 =*/34).bytes(message.backgroundChart);
            if (message.stitchParam != null && Object.hasOwnProperty.call(message, "stitchParam"))
                $root.Peopletrack.StitchParam.encode(message.stitchParam, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified SetupOutput message, length delimited. Does not implicitly {@link Peopletrack.SetupOutput.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.SetupOutput
         * @static
         * @param {Peopletrack.ISetupOutput} message SetupOutput message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetupOutput.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SetupOutput message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.SetupOutput
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.SetupOutput} SetupOutput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetupOutput.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.SetupOutput();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.frameNumber = reader.uint32();
                    break;
                case 2:
                    message.timestamp = reader.uint64();
                    break;
                case 3:
                    if (!(message.people && message.people.length))
                        message.people = [];
                    message.people.push($root.Peopletrack.People.decode(reader, reader.uint32()));
                    break;
                case 4:
                    message.backgroundChart = reader.bytes();
                    break;
                case 5:
                    message.stitchParam = $root.Peopletrack.StitchParam.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SetupOutput message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.SetupOutput
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.SetupOutput} SetupOutput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetupOutput.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SetupOutput message.
         * @function verify
         * @memberof Peopletrack.SetupOutput
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SetupOutput.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                if (!$util.isInteger(message.frameNumber))
                    return "frameNumber: integer expected";
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (!$util.isInteger(message.timestamp) && !(message.timestamp && $util.isInteger(message.timestamp.low) && $util.isInteger(message.timestamp.high)))
                    return "timestamp: integer|Long expected";
            if (message.people != null && message.hasOwnProperty("people")) {
                if (!Array.isArray(message.people))
                    return "people: array expected";
                for (let i = 0; i < message.people.length; ++i) {
                    let error = $root.Peopletrack.People.verify(message.people[i]);
                    if (error)
                        return "people." + error;
                }
            }
            if (message.backgroundChart != null && message.hasOwnProperty("backgroundChart"))
                if (!(message.backgroundChart && typeof message.backgroundChart.length === "number" || $util.isString(message.backgroundChart)))
                    return "backgroundChart: buffer expected";
            if (message.stitchParam != null && message.hasOwnProperty("stitchParam")) {
                let error = $root.Peopletrack.StitchParam.verify(message.stitchParam);
                if (error)
                    return "stitchParam." + error;
            }
            return null;
        };

        /**
         * Creates a SetupOutput message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.SetupOutput
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.SetupOutput} SetupOutput
         */
        SetupOutput.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.SetupOutput)
                return object;
            let message = new $root.Peopletrack.SetupOutput();
            if (object.frameNumber != null)
                message.frameNumber = object.frameNumber >>> 0;
            if (object.timestamp != null)
                if ($util.Long)
                    (message.timestamp = $util.Long.fromValue(object.timestamp)).unsigned = true;
                else if (typeof object.timestamp === "string")
                    message.timestamp = parseInt(object.timestamp, 10);
                else if (typeof object.timestamp === "number")
                    message.timestamp = object.timestamp;
                else if (typeof object.timestamp === "object")
                    message.timestamp = new $util.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);
            if (object.people) {
                if (!Array.isArray(object.people))
                    throw TypeError(".Peopletrack.SetupOutput.people: array expected");
                message.people = [];
                for (let i = 0; i < object.people.length; ++i) {
                    if (typeof object.people[i] !== "object")
                        throw TypeError(".Peopletrack.SetupOutput.people: object expected");
                    message.people[i] = $root.Peopletrack.People.fromObject(object.people[i]);
                }
            }
            if (object.backgroundChart != null)
                if (typeof object.backgroundChart === "string")
                    $util.base64.decode(object.backgroundChart, message.backgroundChart = $util.newBuffer($util.base64.length(object.backgroundChart)), 0);
                else if (object.backgroundChart.length)
                    message.backgroundChart = object.backgroundChart;
            if (object.stitchParam != null) {
                if (typeof object.stitchParam !== "object")
                    throw TypeError(".Peopletrack.SetupOutput.stitchParam: object expected");
                message.stitchParam = $root.Peopletrack.StitchParam.fromObject(object.stitchParam);
            }
            return message;
        };

        /**
         * Creates a plain object from a SetupOutput message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.SetupOutput
         * @static
         * @param {Peopletrack.SetupOutput} message SetupOutput
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SetupOutput.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults)
                object.people = [];
            if (options.defaults) {
                object.frameNumber = 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, true);
                    object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.timestamp = options.longs === String ? "0" : 0;
                if (options.bytes === String)
                    object.backgroundChart = "";
                else {
                    object.backgroundChart = [];
                    if (options.bytes !== Array)
                        object.backgroundChart = $util.newBuffer(object.backgroundChart);
                }
                object.stitchParam = null;
            }
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                object.frameNumber = message.frameNumber;
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (typeof message.timestamp === "number")
                    object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;
                else
                    object.timestamp = options.longs === String ? $util.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;
            if (message.people && message.people.length) {
                object.people = [];
                for (let j = 0; j < message.people.length; ++j)
                    object.people[j] = $root.Peopletrack.People.toObject(message.people[j], options);
            }
            if (message.backgroundChart != null && message.hasOwnProperty("backgroundChart"))
                object.backgroundChart = options.bytes === String ? $util.base64.encode(message.backgroundChart, 0, message.backgroundChart.length) : options.bytes === Array ? Array.prototype.slice.call(message.backgroundChart) : message.backgroundChart;
            if (message.stitchParam != null && message.hasOwnProperty("stitchParam"))
                object.stitchParam = $root.Peopletrack.StitchParam.toObject(message.stitchParam, options);
            return object;
        };

        /**
         * Converts this SetupOutput to JSON.
         * @function toJSON
         * @memberof Peopletrack.SetupOutput
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SetupOutput.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return SetupOutput;
    })();

    Peopletrack.StitchInput = (function() {

        /**
         * Properties of a StitchInput.
         * @memberof Peopletrack
         * @interface IStitchInput
         * @property {number|null} [stitchID] StitchInput stitchID
         * @property {number|null} [frameNumber] StitchInput frameNumber
         * @property {number|Long|null} [timestamp] StitchInput timestamp
         * @property {Array.<Peopletrack.IPeople>|null} [people] StitchInput people
         * @property {Peopletrack.IStitchParam|null} [stitchParam] StitchInput stitchParam
         */

        /**
         * Constructs a new StitchInput.
         * @memberof Peopletrack
         * @classdesc Represents a StitchInput.
         * @implements IStitchInput
         * @constructor
         * @param {Peopletrack.IStitchInput=} [properties] Properties to set
         */
        function StitchInput(properties) {
            this.people = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * StitchInput stitchID.
         * @member {number} stitchID
         * @memberof Peopletrack.StitchInput
         * @instance
         */
        StitchInput.prototype.stitchID = 0;

        /**
         * StitchInput frameNumber.
         * @member {number} frameNumber
         * @memberof Peopletrack.StitchInput
         * @instance
         */
        StitchInput.prototype.frameNumber = 0;

        /**
         * StitchInput timestamp.
         * @member {number|Long} timestamp
         * @memberof Peopletrack.StitchInput
         * @instance
         */
        StitchInput.prototype.timestamp = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * StitchInput people.
         * @member {Array.<Peopletrack.IPeople>} people
         * @memberof Peopletrack.StitchInput
         * @instance
         */
        StitchInput.prototype.people = $util.emptyArray;

        /**
         * StitchInput stitchParam.
         * @member {Peopletrack.IStitchParam|null|undefined} stitchParam
         * @memberof Peopletrack.StitchInput
         * @instance
         */
        StitchInput.prototype.stitchParam = null;

        /**
         * Creates a new StitchInput instance using the specified properties.
         * @function create
         * @memberof Peopletrack.StitchInput
         * @static
         * @param {Peopletrack.IStitchInput=} [properties] Properties to set
         * @returns {Peopletrack.StitchInput} StitchInput instance
         */
        StitchInput.create = function create(properties) {
            return new StitchInput(properties);
        };

        /**
         * Encodes the specified StitchInput message. Does not implicitly {@link Peopletrack.StitchInput.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.StitchInput
         * @static
         * @param {Peopletrack.IStitchInput} message StitchInput message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StitchInput.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.stitchID != null && Object.hasOwnProperty.call(message, "stitchID"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.stitchID);
            if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.frameNumber);
            if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint64(message.timestamp);
            if (message.people != null && message.people.length)
                for (let i = 0; i < message.people.length; ++i)
                    $root.Peopletrack.People.encode(message.people[i], writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.stitchParam != null && Object.hasOwnProperty.call(message, "stitchParam"))
                $root.Peopletrack.StitchParam.encode(message.stitchParam, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified StitchInput message, length delimited. Does not implicitly {@link Peopletrack.StitchInput.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.StitchInput
         * @static
         * @param {Peopletrack.IStitchInput} message StitchInput message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StitchInput.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a StitchInput message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.StitchInput
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.StitchInput} StitchInput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StitchInput.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.StitchInput();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.stitchID = reader.uint32();
                    break;
                case 2:
                    message.frameNumber = reader.uint32();
                    break;
                case 3:
                    message.timestamp = reader.uint64();
                    break;
                case 4:
                    if (!(message.people && message.people.length))
                        message.people = [];
                    message.people.push($root.Peopletrack.People.decode(reader, reader.uint32()));
                    break;
                case 5:
                    message.stitchParam = $root.Peopletrack.StitchParam.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a StitchInput message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.StitchInput
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.StitchInput} StitchInput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StitchInput.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a StitchInput message.
         * @function verify
         * @memberof Peopletrack.StitchInput
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        StitchInput.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.stitchID != null && message.hasOwnProperty("stitchID"))
                if (!$util.isInteger(message.stitchID))
                    return "stitchID: integer expected";
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                if (!$util.isInteger(message.frameNumber))
                    return "frameNumber: integer expected";
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (!$util.isInteger(message.timestamp) && !(message.timestamp && $util.isInteger(message.timestamp.low) && $util.isInteger(message.timestamp.high)))
                    return "timestamp: integer|Long expected";
            if (message.people != null && message.hasOwnProperty("people")) {
                if (!Array.isArray(message.people))
                    return "people: array expected";
                for (let i = 0; i < message.people.length; ++i) {
                    let error = $root.Peopletrack.People.verify(message.people[i]);
                    if (error)
                        return "people." + error;
                }
            }
            if (message.stitchParam != null && message.hasOwnProperty("stitchParam")) {
                let error = $root.Peopletrack.StitchParam.verify(message.stitchParam);
                if (error)
                    return "stitchParam." + error;
            }
            return null;
        };

        /**
         * Creates a StitchInput message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.StitchInput
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.StitchInput} StitchInput
         */
        StitchInput.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.StitchInput)
                return object;
            let message = new $root.Peopletrack.StitchInput();
            if (object.stitchID != null)
                message.stitchID = object.stitchID >>> 0;
            if (object.frameNumber != null)
                message.frameNumber = object.frameNumber >>> 0;
            if (object.timestamp != null)
                if ($util.Long)
                    (message.timestamp = $util.Long.fromValue(object.timestamp)).unsigned = true;
                else if (typeof object.timestamp === "string")
                    message.timestamp = parseInt(object.timestamp, 10);
                else if (typeof object.timestamp === "number")
                    message.timestamp = object.timestamp;
                else if (typeof object.timestamp === "object")
                    message.timestamp = new $util.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);
            if (object.people) {
                if (!Array.isArray(object.people))
                    throw TypeError(".Peopletrack.StitchInput.people: array expected");
                message.people = [];
                for (let i = 0; i < object.people.length; ++i) {
                    if (typeof object.people[i] !== "object")
                        throw TypeError(".Peopletrack.StitchInput.people: object expected");
                    message.people[i] = $root.Peopletrack.People.fromObject(object.people[i]);
                }
            }
            if (object.stitchParam != null) {
                if (typeof object.stitchParam !== "object")
                    throw TypeError(".Peopletrack.StitchInput.stitchParam: object expected");
                message.stitchParam = $root.Peopletrack.StitchParam.fromObject(object.stitchParam);
            }
            return message;
        };

        /**
         * Creates a plain object from a StitchInput message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.StitchInput
         * @static
         * @param {Peopletrack.StitchInput} message StitchInput
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        StitchInput.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults)
                object.people = [];
            if (options.defaults) {
                object.stitchID = 0;
                object.frameNumber = 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, true);
                    object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.timestamp = options.longs === String ? "0" : 0;
                object.stitchParam = null;
            }
            if (message.stitchID != null && message.hasOwnProperty("stitchID"))
                object.stitchID = message.stitchID;
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                object.frameNumber = message.frameNumber;
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (typeof message.timestamp === "number")
                    object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;
                else
                    object.timestamp = options.longs === String ? $util.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;
            if (message.people && message.people.length) {
                object.people = [];
                for (let j = 0; j < message.people.length; ++j)
                    object.people[j] = $root.Peopletrack.People.toObject(message.people[j], options);
            }
            if (message.stitchParam != null && message.hasOwnProperty("stitchParam"))
                object.stitchParam = $root.Peopletrack.StitchParam.toObject(message.stitchParam, options);
            return object;
        };

        /**
         * Converts this StitchInput to JSON.
         * @function toJSON
         * @memberof Peopletrack.StitchInput
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        StitchInput.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return StitchInput;
    })();

    Peopletrack.StitchOutput = (function() {

        /**
         * Properties of a StitchOutput.
         * @memberof Peopletrack
         * @interface IStitchOutput
         * @property {number|null} [frameNumber] StitchOutput frameNumber
         * @property {number|Long|null} [timestamp] StitchOutput timestamp
         * @property {Array.<Peopletrack.IPeople>|null} [people] StitchOutput people
         * @property {Uint8Array|null} [backgroundChart] StitchOutput backgroundChart
         * @property {Uint8Array|null} [parameters] StitchOutput parameters
         */

        /**
         * Constructs a new StitchOutput.
         * @memberof Peopletrack
         * @classdesc Represents a StitchOutput.
         * @implements IStitchOutput
         * @constructor
         * @param {Peopletrack.IStitchOutput=} [properties] Properties to set
         */
        function StitchOutput(properties) {
            this.people = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * StitchOutput frameNumber.
         * @member {number} frameNumber
         * @memberof Peopletrack.StitchOutput
         * @instance
         */
        StitchOutput.prototype.frameNumber = 0;

        /**
         * StitchOutput timestamp.
         * @member {number|Long} timestamp
         * @memberof Peopletrack.StitchOutput
         * @instance
         */
        StitchOutput.prototype.timestamp = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

        /**
         * StitchOutput people.
         * @member {Array.<Peopletrack.IPeople>} people
         * @memberof Peopletrack.StitchOutput
         * @instance
         */
        StitchOutput.prototype.people = $util.emptyArray;

        /**
         * StitchOutput backgroundChart.
         * @member {Uint8Array} backgroundChart
         * @memberof Peopletrack.StitchOutput
         * @instance
         */
        StitchOutput.prototype.backgroundChart = $util.newBuffer([]);

        /**
         * StitchOutput parameters.
         * @member {Uint8Array} parameters
         * @memberof Peopletrack.StitchOutput
         * @instance
         */
        StitchOutput.prototype.parameters = $util.newBuffer([]);

        /**
         * Creates a new StitchOutput instance using the specified properties.
         * @function create
         * @memberof Peopletrack.StitchOutput
         * @static
         * @param {Peopletrack.IStitchOutput=} [properties] Properties to set
         * @returns {Peopletrack.StitchOutput} StitchOutput instance
         */
        StitchOutput.create = function create(properties) {
            return new StitchOutput(properties);
        };

        /**
         * Encodes the specified StitchOutput message. Does not implicitly {@link Peopletrack.StitchOutput.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.StitchOutput
         * @static
         * @param {Peopletrack.IStitchOutput} message StitchOutput message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StitchOutput.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.frameNumber);
            if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.timestamp);
            if (message.people != null && message.people.length)
                for (let i = 0; i < message.people.length; ++i)
                    $root.Peopletrack.People.encode(message.people[i], writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.backgroundChart != null && Object.hasOwnProperty.call(message, "backgroundChart"))
                writer.uint32(/* id 4, wireType 2 =*/34).bytes(message.backgroundChart);
            if (message.parameters != null && Object.hasOwnProperty.call(message, "parameters"))
                writer.uint32(/* id 5, wireType 2 =*/42).bytes(message.parameters);
            return writer;
        };

        /**
         * Encodes the specified StitchOutput message, length delimited. Does not implicitly {@link Peopletrack.StitchOutput.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.StitchOutput
         * @static
         * @param {Peopletrack.IStitchOutput} message StitchOutput message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StitchOutput.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a StitchOutput message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.StitchOutput
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.StitchOutput} StitchOutput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StitchOutput.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.StitchOutput();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.frameNumber = reader.uint32();
                    break;
                case 2:
                    message.timestamp = reader.uint64();
                    break;
                case 3:
                    if (!(message.people && message.people.length))
                        message.people = [];
                    message.people.push($root.Peopletrack.People.decode(reader, reader.uint32()));
                    break;
                case 4:
                    message.backgroundChart = reader.bytes();
                    break;
                case 5:
                    message.parameters = reader.bytes();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a StitchOutput message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.StitchOutput
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.StitchOutput} StitchOutput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StitchOutput.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a StitchOutput message.
         * @function verify
         * @memberof Peopletrack.StitchOutput
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        StitchOutput.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                if (!$util.isInteger(message.frameNumber))
                    return "frameNumber: integer expected";
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (!$util.isInteger(message.timestamp) && !(message.timestamp && $util.isInteger(message.timestamp.low) && $util.isInteger(message.timestamp.high)))
                    return "timestamp: integer|Long expected";
            if (message.people != null && message.hasOwnProperty("people")) {
                if (!Array.isArray(message.people))
                    return "people: array expected";
                for (let i = 0; i < message.people.length; ++i) {
                    let error = $root.Peopletrack.People.verify(message.people[i]);
                    if (error)
                        return "people." + error;
                }
            }
            if (message.backgroundChart != null && message.hasOwnProperty("backgroundChart"))
                if (!(message.backgroundChart && typeof message.backgroundChart.length === "number" || $util.isString(message.backgroundChart)))
                    return "backgroundChart: buffer expected";
            if (message.parameters != null && message.hasOwnProperty("parameters"))
                if (!(message.parameters && typeof message.parameters.length === "number" || $util.isString(message.parameters)))
                    return "parameters: buffer expected";
            return null;
        };

        /**
         * Creates a StitchOutput message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.StitchOutput
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.StitchOutput} StitchOutput
         */
        StitchOutput.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.StitchOutput)
                return object;
            let message = new $root.Peopletrack.StitchOutput();
            if (object.frameNumber != null)
                message.frameNumber = object.frameNumber >>> 0;
            if (object.timestamp != null)
                if ($util.Long)
                    (message.timestamp = $util.Long.fromValue(object.timestamp)).unsigned = true;
                else if (typeof object.timestamp === "string")
                    message.timestamp = parseInt(object.timestamp, 10);
                else if (typeof object.timestamp === "number")
                    message.timestamp = object.timestamp;
                else if (typeof object.timestamp === "object")
                    message.timestamp = new $util.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);
            if (object.people) {
                if (!Array.isArray(object.people))
                    throw TypeError(".Peopletrack.StitchOutput.people: array expected");
                message.people = [];
                for (let i = 0; i < object.people.length; ++i) {
                    if (typeof object.people[i] !== "object")
                        throw TypeError(".Peopletrack.StitchOutput.people: object expected");
                    message.people[i] = $root.Peopletrack.People.fromObject(object.people[i]);
                }
            }
            if (object.backgroundChart != null)
                if (typeof object.backgroundChart === "string")
                    $util.base64.decode(object.backgroundChart, message.backgroundChart = $util.newBuffer($util.base64.length(object.backgroundChart)), 0);
                else if (object.backgroundChart.length)
                    message.backgroundChart = object.backgroundChart;
            if (object.parameters != null)
                if (typeof object.parameters === "string")
                    $util.base64.decode(object.parameters, message.parameters = $util.newBuffer($util.base64.length(object.parameters)), 0);
                else if (object.parameters.length)
                    message.parameters = object.parameters;
            return message;
        };

        /**
         * Creates a plain object from a StitchOutput message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.StitchOutput
         * @static
         * @param {Peopletrack.StitchOutput} message StitchOutput
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        StitchOutput.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults)
                object.people = [];
            if (options.defaults) {
                object.frameNumber = 0;
                if ($util.Long) {
                    let long = new $util.Long(0, 0, true);
                    object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.timestamp = options.longs === String ? "0" : 0;
                if (options.bytes === String)
                    object.backgroundChart = "";
                else {
                    object.backgroundChart = [];
                    if (options.bytes !== Array)
                        object.backgroundChart = $util.newBuffer(object.backgroundChart);
                }
                if (options.bytes === String)
                    object.parameters = "";
                else {
                    object.parameters = [];
                    if (options.bytes !== Array)
                        object.parameters = $util.newBuffer(object.parameters);
                }
            }
            if (message.frameNumber != null && message.hasOwnProperty("frameNumber"))
                object.frameNumber = message.frameNumber;
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (typeof message.timestamp === "number")
                    object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;
                else
                    object.timestamp = options.longs === String ? $util.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;
            if (message.people && message.people.length) {
                object.people = [];
                for (let j = 0; j < message.people.length; ++j)
                    object.people[j] = $root.Peopletrack.People.toObject(message.people[j], options);
            }
            if (message.backgroundChart != null && message.hasOwnProperty("backgroundChart"))
                object.backgroundChart = options.bytes === String ? $util.base64.encode(message.backgroundChart, 0, message.backgroundChart.length) : options.bytes === Array ? Array.prototype.slice.call(message.backgroundChart) : message.backgroundChart;
            if (message.parameters != null && message.hasOwnProperty("parameters"))
                object.parameters = options.bytes === String ? $util.base64.encode(message.parameters, 0, message.parameters.length) : options.bytes === Array ? Array.prototype.slice.call(message.parameters) : message.parameters;
            return object;
        };

        /**
         * Converts this StitchOutput to JSON.
         * @function toJSON
         * @memberof Peopletrack.StitchOutput
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        StitchOutput.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return StitchOutput;
    })();

    Peopletrack.Device = (function() {

        /**
         * Properties of a Device.
         * @memberof Peopletrack
         * @interface IDevice
         * @property {number|null} [stitchID] Device stitchID
         * @property {string|null} [token] Device token
         */

        /**
         * Constructs a new Device.
         * @memberof Peopletrack
         * @classdesc Represents a Device.
         * @implements IDevice
         * @constructor
         * @param {Peopletrack.IDevice=} [properties] Properties to set
         */
        function Device(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Device stitchID.
         * @member {number} stitchID
         * @memberof Peopletrack.Device
         * @instance
         */
        Device.prototype.stitchID = 0;

        /**
         * Device token.
         * @member {string} token
         * @memberof Peopletrack.Device
         * @instance
         */
        Device.prototype.token = "";

        /**
         * Creates a new Device instance using the specified properties.
         * @function create
         * @memberof Peopletrack.Device
         * @static
         * @param {Peopletrack.IDevice=} [properties] Properties to set
         * @returns {Peopletrack.Device} Device instance
         */
        Device.create = function create(properties) {
            return new Device(properties);
        };

        /**
         * Encodes the specified Device message. Does not implicitly {@link Peopletrack.Device.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.Device
         * @static
         * @param {Peopletrack.IDevice} message Device message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Device.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.stitchID != null && Object.hasOwnProperty.call(message, "stitchID"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.stitchID);
            if (message.token != null && Object.hasOwnProperty.call(message, "token"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.token);
            return writer;
        };

        /**
         * Encodes the specified Device message, length delimited. Does not implicitly {@link Peopletrack.Device.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.Device
         * @static
         * @param {Peopletrack.IDevice} message Device message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Device.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Device message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.Device
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.Device} Device
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Device.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.Device();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.stitchID = reader.uint32();
                    break;
                case 2:
                    message.token = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Device message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.Device
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.Device} Device
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Device.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Device message.
         * @function verify
         * @memberof Peopletrack.Device
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Device.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.stitchID != null && message.hasOwnProperty("stitchID"))
                if (!$util.isInteger(message.stitchID))
                    return "stitchID: integer expected";
            if (message.token != null && message.hasOwnProperty("token"))
                if (!$util.isString(message.token))
                    return "token: string expected";
            return null;
        };

        /**
         * Creates a Device message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.Device
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.Device} Device
         */
        Device.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.Device)
                return object;
            let message = new $root.Peopletrack.Device();
            if (object.stitchID != null)
                message.stitchID = object.stitchID >>> 0;
            if (object.token != null)
                message.token = String(object.token);
            return message;
        };

        /**
         * Creates a plain object from a Device message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.Device
         * @static
         * @param {Peopletrack.Device} message Device
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Device.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.stitchID = 0;
                object.token = "";
            }
            if (message.stitchID != null && message.hasOwnProperty("stitchID"))
                object.stitchID = message.stitchID;
            if (message.token != null && message.hasOwnProperty("token"))
                object.token = message.token;
            return object;
        };

        /**
         * Converts this Device to JSON.
         * @function toJSON
         * @memberof Peopletrack.Device
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Device.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Device;
    })();

    Peopletrack.Devices = (function() {

        /**
         * Properties of a Devices.
         * @memberof Peopletrack
         * @interface IDevices
         * @property {Array.<Peopletrack.IDevice>|null} [device] Devices device
         */

        /**
         * Constructs a new Devices.
         * @memberof Peopletrack
         * @classdesc Represents a Devices.
         * @implements IDevices
         * @constructor
         * @param {Peopletrack.IDevices=} [properties] Properties to set
         */
        function Devices(properties) {
            this.device = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Devices device.
         * @member {Array.<Peopletrack.IDevice>} device
         * @memberof Peopletrack.Devices
         * @instance
         */
        Devices.prototype.device = $util.emptyArray;

        /**
         * Creates a new Devices instance using the specified properties.
         * @function create
         * @memberof Peopletrack.Devices
         * @static
         * @param {Peopletrack.IDevices=} [properties] Properties to set
         * @returns {Peopletrack.Devices} Devices instance
         */
        Devices.create = function create(properties) {
            return new Devices(properties);
        };

        /**
         * Encodes the specified Devices message. Does not implicitly {@link Peopletrack.Devices.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.Devices
         * @static
         * @param {Peopletrack.IDevices} message Devices message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Devices.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.device != null && message.device.length)
                for (let i = 0; i < message.device.length; ++i)
                    $root.Peopletrack.Device.encode(message.device[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Devices message, length delimited. Does not implicitly {@link Peopletrack.Devices.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.Devices
         * @static
         * @param {Peopletrack.IDevices} message Devices message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Devices.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Devices message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.Devices
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.Devices} Devices
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Devices.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.Devices();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.device && message.device.length))
                        message.device = [];
                    message.device.push($root.Peopletrack.Device.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Devices message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.Devices
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.Devices} Devices
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Devices.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Devices message.
         * @function verify
         * @memberof Peopletrack.Devices
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Devices.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.device != null && message.hasOwnProperty("device")) {
                if (!Array.isArray(message.device))
                    return "device: array expected";
                for (let i = 0; i < message.device.length; ++i) {
                    let error = $root.Peopletrack.Device.verify(message.device[i]);
                    if (error)
                        return "device." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Devices message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.Devices
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.Devices} Devices
         */
        Devices.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.Devices)
                return object;
            let message = new $root.Peopletrack.Devices();
            if (object.device) {
                if (!Array.isArray(object.device))
                    throw TypeError(".Peopletrack.Devices.device: array expected");
                message.device = [];
                for (let i = 0; i < object.device.length; ++i) {
                    if (typeof object.device[i] !== "object")
                        throw TypeError(".Peopletrack.Devices.device: object expected");
                    message.device[i] = $root.Peopletrack.Device.fromObject(object.device[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a Devices message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.Devices
         * @static
         * @param {Peopletrack.Devices} message Devices
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Devices.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults)
                object.device = [];
            if (message.device && message.device.length) {
                object.device = [];
                for (let j = 0; j < message.device.length; ++j)
                    object.device[j] = $root.Peopletrack.Device.toObject(message.device[j], options);
            }
            return object;
        };

        /**
         * Converts this Devices to JSON.
         * @function toJSON
         * @memberof Peopletrack.Devices
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Devices.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Devices;
    })();

    Peopletrack.DeviceReq = (function() {

        /**
         * Properties of a DeviceReq.
         * @memberof Peopletrack
         * @interface IDeviceReq
         * @property {string|null} [token] DeviceReq token
         */

        /**
         * Constructs a new DeviceReq.
         * @memberof Peopletrack
         * @classdesc Represents a DeviceReq.
         * @implements IDeviceReq
         * @constructor
         * @param {Peopletrack.IDeviceReq=} [properties] Properties to set
         */
        function DeviceReq(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * DeviceReq token.
         * @member {string} token
         * @memberof Peopletrack.DeviceReq
         * @instance
         */
        DeviceReq.prototype.token = "";

        /**
         * Creates a new DeviceReq instance using the specified properties.
         * @function create
         * @memberof Peopletrack.DeviceReq
         * @static
         * @param {Peopletrack.IDeviceReq=} [properties] Properties to set
         * @returns {Peopletrack.DeviceReq} DeviceReq instance
         */
        DeviceReq.create = function create(properties) {
            return new DeviceReq(properties);
        };

        /**
         * Encodes the specified DeviceReq message. Does not implicitly {@link Peopletrack.DeviceReq.verify|verify} messages.
         * @function encode
         * @memberof Peopletrack.DeviceReq
         * @static
         * @param {Peopletrack.IDeviceReq} message DeviceReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DeviceReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.token != null && Object.hasOwnProperty.call(message, "token"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.token);
            return writer;
        };

        /**
         * Encodes the specified DeviceReq message, length delimited. Does not implicitly {@link Peopletrack.DeviceReq.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Peopletrack.DeviceReq
         * @static
         * @param {Peopletrack.IDeviceReq} message DeviceReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DeviceReq.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DeviceReq message from the specified reader or buffer.
         * @function decode
         * @memberof Peopletrack.DeviceReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Peopletrack.DeviceReq} DeviceReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DeviceReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Peopletrack.DeviceReq();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.token = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DeviceReq message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Peopletrack.DeviceReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Peopletrack.DeviceReq} DeviceReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DeviceReq.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DeviceReq message.
         * @function verify
         * @memberof Peopletrack.DeviceReq
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        DeviceReq.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.token != null && message.hasOwnProperty("token"))
                if (!$util.isString(message.token))
                    return "token: string expected";
            return null;
        };

        /**
         * Creates a DeviceReq message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Peopletrack.DeviceReq
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Peopletrack.DeviceReq} DeviceReq
         */
        DeviceReq.fromObject = function fromObject(object) {
            if (object instanceof $root.Peopletrack.DeviceReq)
                return object;
            let message = new $root.Peopletrack.DeviceReq();
            if (object.token != null)
                message.token = String(object.token);
            return message;
        };

        /**
         * Creates a plain object from a DeviceReq message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Peopletrack.DeviceReq
         * @static
         * @param {Peopletrack.DeviceReq} message DeviceReq
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DeviceReq.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults)
                object.token = "";
            if (message.token != null && message.hasOwnProperty("token"))
                object.token = message.token;
            return object;
        };

        /**
         * Converts this DeviceReq to JSON.
         * @function toJSON
         * @memberof Peopletrack.DeviceReq
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        DeviceReq.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DeviceReq;
    })();

    Peopletrack.AI = (function() {

        /**
         * Constructs a new AI service.
         * @memberof Peopletrack
         * @classdesc Represents a AI
         * @extends $protobuf.rpc.Service
         * @constructor
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         */
        function AI(rpcImpl, requestDelimited, responseDelimited) {
            $protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
        }

        (AI.prototype = Object.create($protobuf.rpc.Service.prototype)).constructor = AI;

        /**
         * Creates new AI service using the specified rpc implementation.
         * @function create
         * @memberof Peopletrack.AI
         * @static
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         * @returns {AI} RPC service. Useful where requests and/or responses are streamed.
         */
        AI.create = function create(rpcImpl, requestDelimited, responseDelimited) {
            return new this(rpcImpl, requestDelimited, responseDelimited);
        };

        /**
         * Callback as used by {@link Peopletrack.AI#echo}.
         * @memberof Peopletrack.AI
         * @typedef EchoCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {Peopletrack.Msg} [response] Msg
         */

        /**
         * Calls Echo.
         * @function echo
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.IMsg} request Msg message or plain object
         * @param {Peopletrack.AI.EchoCallback} callback Node-style callback called with the error, if any, and Msg
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(AI.prototype.echo = function echo(request, callback) {
            return this.rpcCall(echo, $root.Peopletrack.Msg, $root.Peopletrack.Msg, request, callback);
        }, "name", { value: "Echo" });

        /**
         * Calls Echo.
         * @function echo
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.IMsg} request Msg message or plain object
         * @returns {Promise<Peopletrack.Msg>} Promise
         * @variation 2
         */

        /**
         * Callback as used by {@link Peopletrack.AI#setup}.
         * @memberof Peopletrack.AI
         * @typedef SetupCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {Peopletrack.SetupOutput} [response] SetupOutput
         */

        /**
         * Calls Setup.
         * @function setup
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.ISetupInput} request SetupInput message or plain object
         * @param {Peopletrack.AI.SetupCallback} callback Node-style callback called with the error, if any, and SetupOutput
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(AI.prototype.setup = function setup(request, callback) {
            return this.rpcCall(setup, $root.Peopletrack.SetupInput, $root.Peopletrack.SetupOutput, request, callback);
        }, "name", { value: "Setup" });

        /**
         * Calls Setup.
         * @function setup
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.ISetupInput} request SetupInput message or plain object
         * @returns {Promise<Peopletrack.SetupOutput>} Promise
         * @variation 2
         */

        /**
         * Callback as used by {@link Peopletrack.AI#stitch}.
         * @memberof Peopletrack.AI
         * @typedef StitchCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {Peopletrack.StitchOutput} [response] StitchOutput
         */

        /**
         * Calls Stitch.
         * @function stitch
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.IStitchInput} request StitchInput message or plain object
         * @param {Peopletrack.AI.StitchCallback} callback Node-style callback called with the error, if any, and StitchOutput
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(AI.prototype.stitch = function stitch(request, callback) {
            return this.rpcCall(stitch, $root.Peopletrack.StitchInput, $root.Peopletrack.StitchOutput, request, callback);
        }, "name", { value: "Stitch" });

        /**
         * Calls Stitch.
         * @function stitch
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.IStitchInput} request StitchInput message or plain object
         * @returns {Promise<Peopletrack.StitchOutput>} Promise
         * @variation 2
         */

        /**
         * Callback as used by {@link Peopletrack.AI#stitchApp}.
         * @memberof Peopletrack.AI
         * @typedef StitchAppCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {Peopletrack.StitchOutput} [response] StitchOutput
         */

        /**
         * Calls StitchApp.
         * @function stitchApp
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.IDevices} request Devices message or plain object
         * @param {Peopletrack.AI.StitchAppCallback} callback Node-style callback called with the error, if any, and StitchOutput
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(AI.prototype.stitchApp = function stitchApp(request, callback) {
            return this.rpcCall(stitchApp, $root.Peopletrack.Devices, $root.Peopletrack.StitchOutput, request, callback);
        }, "name", { value: "StitchApp" });

        /**
         * Calls StitchApp.
         * @function stitchApp
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.IDevices} request Devices message or plain object
         * @returns {Promise<Peopletrack.StitchOutput>} Promise
         * @variation 2
         */

        /**
         * Callback as used by {@link Peopletrack.AI#getMsg}.
         * @memberof Peopletrack.AI
         * @typedef GetMsgCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {Peopletrack.Msg} [response] Msg
         */

        /**
         * Calls GetMsg.
         * @function getMsg
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.IDeviceReq} request DeviceReq message or plain object
         * @param {Peopletrack.AI.GetMsgCallback} callback Node-style callback called with the error, if any, and Msg
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(AI.prototype.getMsg = function getMsg(request, callback) {
            return this.rpcCall(getMsg, $root.Peopletrack.DeviceReq, $root.Peopletrack.Msg, request, callback);
        }, "name", { value: "GetMsg" });

        /**
         * Calls GetMsg.
         * @function getMsg
         * @memberof Peopletrack.AI
         * @instance
         * @param {Peopletrack.IDeviceReq} request DeviceReq message or plain object
         * @returns {Promise<Peopletrack.Msg>} Promise
         * @variation 2
         */

        return AI;
    })();

    return Peopletrack;
})();

export { $root as default };
