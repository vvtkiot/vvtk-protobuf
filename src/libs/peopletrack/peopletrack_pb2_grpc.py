# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import peopletrack_pb2 as peopletrack__pb2


class AIStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.Echo = channel.unary_unary(
                '/Peopletrack.AI/Echo',
                request_serializer=peopletrack__pb2.Msg.SerializeToString,
                response_deserializer=peopletrack__pb2.Msg.FromString,
                )
        self.Setup = channel.stream_stream(
                '/Peopletrack.AI/Setup',
                request_serializer=peopletrack__pb2.SetupInput.SerializeToString,
                response_deserializer=peopletrack__pb2.SetupOutput.FromString,
                )
        self.Stitch = channel.stream_stream(
                '/Peopletrack.AI/Stitch',
                request_serializer=peopletrack__pb2.StitchInput.SerializeToString,
                response_deserializer=peopletrack__pb2.StitchOutput.FromString,
                )
        self.StitchApp = channel.unary_stream(
                '/Peopletrack.AI/StitchApp',
                request_serializer=peopletrack__pb2.Devices.SerializeToString,
                response_deserializer=peopletrack__pb2.StitchOutput.FromString,
                )
        self.GetMsg = channel.unary_stream(
                '/Peopletrack.AI/GetMsg',
                request_serializer=peopletrack__pb2.DeviceReq.SerializeToString,
                response_deserializer=peopletrack__pb2.Msg.FromString,
                )


class AIServicer(object):
    """Missing associated documentation comment in .proto file."""

    def Echo(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Setup(self, request_iterator, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Stitch(self, request_iterator, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def StitchApp(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def GetMsg(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_AIServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'Echo': grpc.unary_unary_rpc_method_handler(
                    servicer.Echo,
                    request_deserializer=peopletrack__pb2.Msg.FromString,
                    response_serializer=peopletrack__pb2.Msg.SerializeToString,
            ),
            'Setup': grpc.stream_stream_rpc_method_handler(
                    servicer.Setup,
                    request_deserializer=peopletrack__pb2.SetupInput.FromString,
                    response_serializer=peopletrack__pb2.SetupOutput.SerializeToString,
            ),
            'Stitch': grpc.stream_stream_rpc_method_handler(
                    servicer.Stitch,
                    request_deserializer=peopletrack__pb2.StitchInput.FromString,
                    response_serializer=peopletrack__pb2.StitchOutput.SerializeToString,
            ),
            'StitchApp': grpc.unary_stream_rpc_method_handler(
                    servicer.StitchApp,
                    request_deserializer=peopletrack__pb2.Devices.FromString,
                    response_serializer=peopletrack__pb2.StitchOutput.SerializeToString,
            ),
            'GetMsg': grpc.unary_stream_rpc_method_handler(
                    servicer.GetMsg,
                    request_deserializer=peopletrack__pb2.DeviceReq.FromString,
                    response_serializer=peopletrack__pb2.Msg.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'Peopletrack.AI', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class AI(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def Echo(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/Peopletrack.AI/Echo',
            peopletrack__pb2.Msg.SerializeToString,
            peopletrack__pb2.Msg.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def Setup(request_iterator,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.stream_stream(request_iterator, target, '/Peopletrack.AI/Setup',
            peopletrack__pb2.SetupInput.SerializeToString,
            peopletrack__pb2.SetupOutput.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def Stitch(request_iterator,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.stream_stream(request_iterator, target, '/Peopletrack.AI/Stitch',
            peopletrack__pb2.StitchInput.SerializeToString,
            peopletrack__pb2.StitchOutput.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def StitchApp(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_stream(request, target, '/Peopletrack.AI/StitchApp',
            peopletrack__pb2.Devices.SerializeToString,
            peopletrack__pb2.StitchOutput.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def GetMsg(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_stream(request, target, '/Peopletrack.AI/GetMsg',
            peopletrack__pb2.DeviceReq.SerializeToString,
            peopletrack__pb2.Msg.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
