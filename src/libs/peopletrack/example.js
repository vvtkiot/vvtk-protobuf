const { Peopletrack } = require("./peopletrack.js");

const msg = {
  frameNumber: 1,
  timestamp: 2,
  people: [
    {
      tid: 1,
      posX: 2,
      posY: 3,
      posZ: 4,
      velX: 5,
      velY: 6,
      velZ: 7,
      accX: 8,
      accY: 9,
      accZ: 10,
      status: Peopletrack.People.Status.MOVING
    }
  ]
};
const msgBinary = Peopletrack.Msg.encode(msg).finish();
const msgDecoded = Peopletrack.Msg.decode(msgBinary);

console.log("Protobuf: ");
console.log(Buffer.from(msgBinary).toString("hex"));
console.log("Size: " + msgBinary.length + " bytes");
console.log("Decode protobuf: ");
console.log(JSON.stringify(msgDecoded));
