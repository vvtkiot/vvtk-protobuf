from concurrent import futures
import logging
import grpc
import peopletrack_pb2_grpc
import peopletrack_pb2

class AI(peopletrack_pb2_grpc.AIServicer):

    def Echo(self, request, context):
        print(f"{request.frameNumber}")
        return peopletrack_pb2.Msg(frameNumber=101)
    
    def Stitch(self, request_iterator, context):
        for stitchInput in request_iterator:
            print(f"{stitchInput.stitchID}")
            yield peopletrack_pb2.StitchOutput(frameNumber=1)

    def StitchApp(self, request, context):
        print(request)
        yield peopletrack_pb2.StitchOutput(frameNumber=1)

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    peopletrack_pb2_grpc.add_AIServicer_to_server(AI(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    logging.basicConfig()
    serve()