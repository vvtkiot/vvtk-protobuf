/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Occupancy = $root.Occupancy = (() => {

    /**
     * Namespace Occupancy.
     * @exports Occupancy
     * @namespace
     */
    const Occupancy = {};

    Occupancy.RoomConfig = (function() {

        /**
         * Properties of a RoomConfig.
         * @memberof Occupancy
         * @interface IRoomConfig
         * @property {Array.<string>|null} [token] RoomConfig token
         */

        /**
         * Constructs a new RoomConfig.
         * @memberof Occupancy
         * @classdesc Represents a RoomConfig.
         * @implements IRoomConfig
         * @constructor
         * @param {Occupancy.IRoomConfig=} [properties] Properties to set
         */
        function RoomConfig(properties) {
            this.token = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * RoomConfig token.
         * @member {Array.<string>} token
         * @memberof Occupancy.RoomConfig
         * @instance
         */
        RoomConfig.prototype.token = $util.emptyArray;

        /**
         * Creates a new RoomConfig instance using the specified properties.
         * @function create
         * @memberof Occupancy.RoomConfig
         * @static
         * @param {Occupancy.IRoomConfig=} [properties] Properties to set
         * @returns {Occupancy.RoomConfig} RoomConfig instance
         */
        RoomConfig.create = function create(properties) {
            return new RoomConfig(properties);
        };

        /**
         * Encodes the specified RoomConfig message. Does not implicitly {@link Occupancy.RoomConfig.verify|verify} messages.
         * @function encode
         * @memberof Occupancy.RoomConfig
         * @static
         * @param {Occupancy.IRoomConfig} message RoomConfig message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RoomConfig.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.token != null && message.token.length)
                for (let i = 0; i < message.token.length; ++i)
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.token[i]);
            return writer;
        };

        /**
         * Encodes the specified RoomConfig message, length delimited. Does not implicitly {@link Occupancy.RoomConfig.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Occupancy.RoomConfig
         * @static
         * @param {Occupancy.IRoomConfig} message RoomConfig message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RoomConfig.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RoomConfig message from the specified reader or buffer.
         * @function decode
         * @memberof Occupancy.RoomConfig
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Occupancy.RoomConfig} RoomConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RoomConfig.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Occupancy.RoomConfig();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.token && message.token.length))
                        message.token = [];
                    message.token.push(reader.string());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RoomConfig message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Occupancy.RoomConfig
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Occupancy.RoomConfig} RoomConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RoomConfig.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RoomConfig message.
         * @function verify
         * @memberof Occupancy.RoomConfig
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        RoomConfig.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.token != null && message.hasOwnProperty("token")) {
                if (!Array.isArray(message.token))
                    return "token: array expected";
                for (let i = 0; i < message.token.length; ++i)
                    if (!$util.isString(message.token[i]))
                        return "token: string[] expected";
            }
            return null;
        };

        /**
         * Creates a RoomConfig message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Occupancy.RoomConfig
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Occupancy.RoomConfig} RoomConfig
         */
        RoomConfig.fromObject = function fromObject(object) {
            if (object instanceof $root.Occupancy.RoomConfig)
                return object;
            let message = new $root.Occupancy.RoomConfig();
            if (object.token) {
                if (!Array.isArray(object.token))
                    throw TypeError(".Occupancy.RoomConfig.token: array expected");
                message.token = [];
                for (let i = 0; i < object.token.length; ++i)
                    message.token[i] = String(object.token[i]);
            }
            return message;
        };

        /**
         * Creates a plain object from a RoomConfig message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Occupancy.RoomConfig
         * @static
         * @param {Occupancy.RoomConfig} message RoomConfig
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RoomConfig.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults)
                object.token = [];
            if (message.token && message.token.length) {
                object.token = [];
                for (let j = 0; j < message.token.length; ++j)
                    object.token[j] = message.token[j];
            }
            return object;
        };

        /**
         * Converts this RoomConfig to JSON.
         * @function toJSON
         * @memberof Occupancy.RoomConfig
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        RoomConfig.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RoomConfig;
    })();

    Occupancy.Room = (function() {

        /**
         * Properties of a Room.
         * @memberof Occupancy
         * @interface IRoom
         * @property {number|null} [people] Room people
         * @property {boolean|null} [fallen] Room fallen
         */

        /**
         * Constructs a new Room.
         * @memberof Occupancy
         * @classdesc Represents a Room.
         * @implements IRoom
         * @constructor
         * @param {Occupancy.IRoom=} [properties] Properties to set
         */
        function Room(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Room people.
         * @member {number} people
         * @memberof Occupancy.Room
         * @instance
         */
        Room.prototype.people = 0;

        /**
         * Room fallen.
         * @member {boolean} fallen
         * @memberof Occupancy.Room
         * @instance
         */
        Room.prototype.fallen = false;

        /**
         * Creates a new Room instance using the specified properties.
         * @function create
         * @memberof Occupancy.Room
         * @static
         * @param {Occupancy.IRoom=} [properties] Properties to set
         * @returns {Occupancy.Room} Room instance
         */
        Room.create = function create(properties) {
            return new Room(properties);
        };

        /**
         * Encodes the specified Room message. Does not implicitly {@link Occupancy.Room.verify|verify} messages.
         * @function encode
         * @memberof Occupancy.Room
         * @static
         * @param {Occupancy.IRoom} message Room message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Room.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.people != null && Object.hasOwnProperty.call(message, "people"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.people);
            if (message.fallen != null && Object.hasOwnProperty.call(message, "fallen"))
                writer.uint32(/* id 2, wireType 0 =*/16).bool(message.fallen);
            return writer;
        };

        /**
         * Encodes the specified Room message, length delimited. Does not implicitly {@link Occupancy.Room.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Occupancy.Room
         * @static
         * @param {Occupancy.IRoom} message Room message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Room.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Room message from the specified reader or buffer.
         * @function decode
         * @memberof Occupancy.Room
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Occupancy.Room} Room
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Room.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Occupancy.Room();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.people = reader.uint32();
                    break;
                case 2:
                    message.fallen = reader.bool();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Room message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Occupancy.Room
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Occupancy.Room} Room
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Room.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Room message.
         * @function verify
         * @memberof Occupancy.Room
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Room.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.people != null && message.hasOwnProperty("people"))
                if (!$util.isInteger(message.people))
                    return "people: integer expected";
            if (message.fallen != null && message.hasOwnProperty("fallen"))
                if (typeof message.fallen !== "boolean")
                    return "fallen: boolean expected";
            return null;
        };

        /**
         * Creates a Room message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Occupancy.Room
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Occupancy.Room} Room
         */
        Room.fromObject = function fromObject(object) {
            if (object instanceof $root.Occupancy.Room)
                return object;
            let message = new $root.Occupancy.Room();
            if (object.people != null)
                message.people = object.people >>> 0;
            if (object.fallen != null)
                message.fallen = Boolean(object.fallen);
            return message;
        };

        /**
         * Creates a plain object from a Room message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Occupancy.Room
         * @static
         * @param {Occupancy.Room} message Room
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Room.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.people = 0;
                object.fallen = false;
            }
            if (message.people != null && message.hasOwnProperty("people"))
                object.people = message.people;
            if (message.fallen != null && message.hasOwnProperty("fallen"))
                object.fallen = message.fallen;
            return object;
        };

        /**
         * Converts this Room to JSON.
         * @function toJSON
         * @memberof Occupancy.Room
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Room.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Room;
    })();

    Occupancy.Occupancy = (function() {

        /**
         * Constructs a new Occupancy service.
         * @memberof Occupancy
         * @classdesc Represents an Occupancy
         * @extends $protobuf.rpc.Service
         * @constructor
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         */
        function Occupancy(rpcImpl, requestDelimited, responseDelimited) {
            $protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
        }

        (Occupancy.prototype = Object.create($protobuf.rpc.Service.prototype)).constructor = Occupancy;

        /**
         * Creates new Occupancy service using the specified rpc implementation.
         * @function create
         * @memberof Occupancy.Occupancy
         * @static
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         * @returns {Occupancy} RPC service. Useful where requests and/or responses are streamed.
         */
        Occupancy.create = function create(rpcImpl, requestDelimited, responseDelimited) {
            return new this(rpcImpl, requestDelimited, responseDelimited);
        };

        /**
         * Callback as used by {@link Occupancy.Occupancy#status}.
         * @memberof Occupancy.Occupancy
         * @typedef StatusCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {Occupancy.Room} [response] Room
         */

        /**
         * Calls Status.
         * @function status
         * @memberof Occupancy.Occupancy
         * @instance
         * @param {Occupancy.IRoomConfig} request RoomConfig message or plain object
         * @param {Occupancy.Occupancy.StatusCallback} callback Node-style callback called with the error, if any, and Room
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(Occupancy.prototype.status = function status(request, callback) {
            return this.rpcCall(status, $root.Occupancy.RoomConfig, $root.Occupancy.Room, request, callback);
        }, "name", { value: "Status" });

        /**
         * Calls Status.
         * @function status
         * @memberof Occupancy.Occupancy
         * @instance
         * @param {Occupancy.IRoomConfig} request RoomConfig message or plain object
         * @returns {Promise<Occupancy.Room>} Promise
         * @variation 2
         */

        return Occupancy;
    })();

    return Occupancy;
})();

export { $root as default };
