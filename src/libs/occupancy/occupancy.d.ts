import * as $protobuf from "protobufjs";
/** Namespace Occupancy. */
export namespace Occupancy {

    /** Properties of a RoomConfig. */
    interface IRoomConfig {

        /** RoomConfig token */
        token?: (string[]|null);
    }

    /** Represents a RoomConfig. */
    class RoomConfig implements IRoomConfig {

        /**
         * Constructs a new RoomConfig.
         * @param [properties] Properties to set
         */
        constructor(properties?: Occupancy.IRoomConfig);

        /** RoomConfig token. */
        public token: string[];

        /**
         * Creates a new RoomConfig instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RoomConfig instance
         */
        public static create(properties?: Occupancy.IRoomConfig): Occupancy.RoomConfig;

        /**
         * Encodes the specified RoomConfig message. Does not implicitly {@link Occupancy.RoomConfig.verify|verify} messages.
         * @param message RoomConfig message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Occupancy.IRoomConfig, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified RoomConfig message, length delimited. Does not implicitly {@link Occupancy.RoomConfig.verify|verify} messages.
         * @param message RoomConfig message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Occupancy.IRoomConfig, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a RoomConfig message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RoomConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Occupancy.RoomConfig;

        /**
         * Decodes a RoomConfig message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RoomConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Occupancy.RoomConfig;

        /**
         * Verifies a RoomConfig message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a RoomConfig message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns RoomConfig
         */
        public static fromObject(object: { [k: string]: any }): Occupancy.RoomConfig;

        /**
         * Creates a plain object from a RoomConfig message. Also converts values to other types if specified.
         * @param message RoomConfig
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Occupancy.RoomConfig, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this RoomConfig to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Room. */
    interface IRoom {

        /** Room people */
        people?: (number|null);

        /** Room fallen */
        fallen?: (boolean|null);
    }

    /** Represents a Room. */
    class Room implements IRoom {

        /**
         * Constructs a new Room.
         * @param [properties] Properties to set
         */
        constructor(properties?: Occupancy.IRoom);

        /** Room people. */
        public people: number;

        /** Room fallen. */
        public fallen: boolean;

        /**
         * Creates a new Room instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Room instance
         */
        public static create(properties?: Occupancy.IRoom): Occupancy.Room;

        /**
         * Encodes the specified Room message. Does not implicitly {@link Occupancy.Room.verify|verify} messages.
         * @param message Room message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Occupancy.IRoom, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Room message, length delimited. Does not implicitly {@link Occupancy.Room.verify|verify} messages.
         * @param message Room message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Occupancy.IRoom, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Room message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Room
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Occupancy.Room;

        /**
         * Decodes a Room message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Room
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Occupancy.Room;

        /**
         * Verifies a Room message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Room message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Room
         */
        public static fromObject(object: { [k: string]: any }): Occupancy.Room;

        /**
         * Creates a plain object from a Room message. Also converts values to other types if specified.
         * @param message Room
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Occupancy.Room, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Room to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Represents an Occupancy */
    class Occupancy extends $protobuf.rpc.Service {

        /**
         * Constructs a new Occupancy service.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         */
        constructor(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean);

        /**
         * Creates new Occupancy service using the specified rpc implementation.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         * @returns RPC service. Useful where requests and/or responses are streamed.
         */
        public static create(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean): Occupancy;

        /**
         * Calls Status.
         * @param request RoomConfig message or plain object
         * @param callback Node-style callback called with the error, if any, and Room
         */
        public status(request: Occupancy.IRoomConfig, callback: Occupancy.Occupancy.StatusCallback): void;

        /**
         * Calls Status.
         * @param request RoomConfig message or plain object
         * @returns Promise
         */
        public status(request: Occupancy.IRoomConfig): Promise<Occupancy.Room>;
    }

    namespace Occupancy {

        /**
         * Callback as used by {@link Occupancy.Occupancy#status}.
         * @param error Error, if any
         * @param [response] Room
         */
        type StatusCallback = (error: (Error|null), response?: Occupancy.Room) => void;
    }
}
