/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Recognition = $root.Recognition = (() => {

    /**
     * Namespace Recognition.
     * @exports Recognition
     * @namespace
     */
    const Recognition = {};

    Recognition.Result = (function() {

        /**
         * Properties of a Result.
         * @memberof Recognition
         * @interface IResult
         * @property {number|Long|null} [timestamp] Result timestamp
         * @property {Array.<string>|null} [names] Result names
         * @property {Array.<Recognition.IRectangle>|null} [rects] Result rects
         */

        /**
         * Constructs a new Result.
         * @memberof Recognition
         * @classdesc Represents a Result.
         * @implements IResult
         * @constructor
         * @param {Recognition.IResult=} [properties] Properties to set
         */
        function Result(properties) {
            this.names = [];
            this.rects = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Result timestamp.
         * @member {number|Long} timestamp
         * @memberof Recognition.Result
         * @instance
         */
        Result.prototype.timestamp = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Result names.
         * @member {Array.<string>} names
         * @memberof Recognition.Result
         * @instance
         */
        Result.prototype.names = $util.emptyArray;

        /**
         * Result rects.
         * @member {Array.<Recognition.IRectangle>} rects
         * @memberof Recognition.Result
         * @instance
         */
        Result.prototype.rects = $util.emptyArray;

        /**
         * Creates a new Result instance using the specified properties.
         * @function create
         * @memberof Recognition.Result
         * @static
         * @param {Recognition.IResult=} [properties] Properties to set
         * @returns {Recognition.Result} Result instance
         */
        Result.create = function create(properties) {
            return new Result(properties);
        };

        /**
         * Encodes the specified Result message. Does not implicitly {@link Recognition.Result.verify|verify} messages.
         * @function encode
         * @memberof Recognition.Result
         * @static
         * @param {Recognition.IResult} message Result message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Result.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp"))
                writer.uint32(/* id 1, wireType 0 =*/8).int64(message.timestamp);
            if (message.names != null && message.names.length)
                for (let i = 0; i < message.names.length; ++i)
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.names[i]);
            if (message.rects != null && message.rects.length)
                for (let i = 0; i < message.rects.length; ++i)
                    $root.Recognition.Rectangle.encode(message.rects[i], writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Result message, length delimited. Does not implicitly {@link Recognition.Result.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Recognition.Result
         * @static
         * @param {Recognition.IResult} message Result message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Result.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Result message from the specified reader or buffer.
         * @function decode
         * @memberof Recognition.Result
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Recognition.Result} Result
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Result.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Recognition.Result();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.timestamp = reader.int64();
                    break;
                case 2:
                    if (!(message.names && message.names.length))
                        message.names = [];
                    message.names.push(reader.string());
                    break;
                case 3:
                    if (!(message.rects && message.rects.length))
                        message.rects = [];
                    message.rects.push($root.Recognition.Rectangle.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Result message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Recognition.Result
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Recognition.Result} Result
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Result.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Result message.
         * @function verify
         * @memberof Recognition.Result
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Result.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (!$util.isInteger(message.timestamp) && !(message.timestamp && $util.isInteger(message.timestamp.low) && $util.isInteger(message.timestamp.high)))
                    return "timestamp: integer|Long expected";
            if (message.names != null && message.hasOwnProperty("names")) {
                if (!Array.isArray(message.names))
                    return "names: array expected";
                for (let i = 0; i < message.names.length; ++i)
                    if (!$util.isString(message.names[i]))
                        return "names: string[] expected";
            }
            if (message.rects != null && message.hasOwnProperty("rects")) {
                if (!Array.isArray(message.rects))
                    return "rects: array expected";
                for (let i = 0; i < message.rects.length; ++i) {
                    let error = $root.Recognition.Rectangle.verify(message.rects[i]);
                    if (error)
                        return "rects." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Result message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Recognition.Result
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Recognition.Result} Result
         */
        Result.fromObject = function fromObject(object) {
            if (object instanceof $root.Recognition.Result)
                return object;
            let message = new $root.Recognition.Result();
            if (object.timestamp != null)
                if ($util.Long)
                    (message.timestamp = $util.Long.fromValue(object.timestamp)).unsigned = false;
                else if (typeof object.timestamp === "string")
                    message.timestamp = parseInt(object.timestamp, 10);
                else if (typeof object.timestamp === "number")
                    message.timestamp = object.timestamp;
                else if (typeof object.timestamp === "object")
                    message.timestamp = new $util.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber();
            if (object.names) {
                if (!Array.isArray(object.names))
                    throw TypeError(".Recognition.Result.names: array expected");
                message.names = [];
                for (let i = 0; i < object.names.length; ++i)
                    message.names[i] = String(object.names[i]);
            }
            if (object.rects) {
                if (!Array.isArray(object.rects))
                    throw TypeError(".Recognition.Result.rects: array expected");
                message.rects = [];
                for (let i = 0; i < object.rects.length; ++i) {
                    if (typeof object.rects[i] !== "object")
                        throw TypeError(".Recognition.Result.rects: object expected");
                    message.rects[i] = $root.Recognition.Rectangle.fromObject(object.rects[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a Result message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Recognition.Result
         * @static
         * @param {Recognition.Result} message Result
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Result.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults) {
                object.names = [];
                object.rects = [];
            }
            if (options.defaults)
                if ($util.Long) {
                    let long = new $util.Long(0, 0, false);
                    object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.timestamp = options.longs === String ? "0" : 0;
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (typeof message.timestamp === "number")
                    object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;
                else
                    object.timestamp = options.longs === String ? $util.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber() : message.timestamp;
            if (message.names && message.names.length) {
                object.names = [];
                for (let j = 0; j < message.names.length; ++j)
                    object.names[j] = message.names[j];
            }
            if (message.rects && message.rects.length) {
                object.rects = [];
                for (let j = 0; j < message.rects.length; ++j)
                    object.rects[j] = $root.Recognition.Rectangle.toObject(message.rects[j], options);
            }
            return object;
        };

        /**
         * Converts this Result to JSON.
         * @function toJSON
         * @memberof Recognition.Result
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Result.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Result;
    })();

    Recognition.Rectangle = (function() {

        /**
         * Properties of a Rectangle.
         * @memberof Recognition
         * @interface IRectangle
         * @property {number|null} [x] Rectangle x
         * @property {number|null} [y] Rectangle y
         * @property {number|null} [w] Rectangle w
         * @property {number|null} [h] Rectangle h
         */

        /**
         * Constructs a new Rectangle.
         * @memberof Recognition
         * @classdesc Represents a Rectangle.
         * @implements IRectangle
         * @constructor
         * @param {Recognition.IRectangle=} [properties] Properties to set
         */
        function Rectangle(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Rectangle x.
         * @member {number} x
         * @memberof Recognition.Rectangle
         * @instance
         */
        Rectangle.prototype.x = 0;

        /**
         * Rectangle y.
         * @member {number} y
         * @memberof Recognition.Rectangle
         * @instance
         */
        Rectangle.prototype.y = 0;

        /**
         * Rectangle w.
         * @member {number} w
         * @memberof Recognition.Rectangle
         * @instance
         */
        Rectangle.prototype.w = 0;

        /**
         * Rectangle h.
         * @member {number} h
         * @memberof Recognition.Rectangle
         * @instance
         */
        Rectangle.prototype.h = 0;

        /**
         * Creates a new Rectangle instance using the specified properties.
         * @function create
         * @memberof Recognition.Rectangle
         * @static
         * @param {Recognition.IRectangle=} [properties] Properties to set
         * @returns {Recognition.Rectangle} Rectangle instance
         */
        Rectangle.create = function create(properties) {
            return new Rectangle(properties);
        };

        /**
         * Encodes the specified Rectangle message. Does not implicitly {@link Recognition.Rectangle.verify|verify} messages.
         * @function encode
         * @memberof Recognition.Rectangle
         * @static
         * @param {Recognition.IRectangle} message Rectangle message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Rectangle.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.x != null && Object.hasOwnProperty.call(message, "x"))
                writer.uint32(/* id 1, wireType 1 =*/9).double(message.x);
            if (message.y != null && Object.hasOwnProperty.call(message, "y"))
                writer.uint32(/* id 2, wireType 1 =*/17).double(message.y);
            if (message.w != null && Object.hasOwnProperty.call(message, "w"))
                writer.uint32(/* id 3, wireType 1 =*/25).double(message.w);
            if (message.h != null && Object.hasOwnProperty.call(message, "h"))
                writer.uint32(/* id 4, wireType 1 =*/33).double(message.h);
            return writer;
        };

        /**
         * Encodes the specified Rectangle message, length delimited. Does not implicitly {@link Recognition.Rectangle.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Recognition.Rectangle
         * @static
         * @param {Recognition.IRectangle} message Rectangle message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Rectangle.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Rectangle message from the specified reader or buffer.
         * @function decode
         * @memberof Recognition.Rectangle
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Recognition.Rectangle} Rectangle
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Rectangle.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Recognition.Rectangle();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.x = reader.double();
                    break;
                case 2:
                    message.y = reader.double();
                    break;
                case 3:
                    message.w = reader.double();
                    break;
                case 4:
                    message.h = reader.double();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Rectangle message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Recognition.Rectangle
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Recognition.Rectangle} Rectangle
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Rectangle.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Rectangle message.
         * @function verify
         * @memberof Recognition.Rectangle
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Rectangle.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.x != null && message.hasOwnProperty("x"))
                if (typeof message.x !== "number")
                    return "x: number expected";
            if (message.y != null && message.hasOwnProperty("y"))
                if (typeof message.y !== "number")
                    return "y: number expected";
            if (message.w != null && message.hasOwnProperty("w"))
                if (typeof message.w !== "number")
                    return "w: number expected";
            if (message.h != null && message.hasOwnProperty("h"))
                if (typeof message.h !== "number")
                    return "h: number expected";
            return null;
        };

        /**
         * Creates a Rectangle message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Recognition.Rectangle
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Recognition.Rectangle} Rectangle
         */
        Rectangle.fromObject = function fromObject(object) {
            if (object instanceof $root.Recognition.Rectangle)
                return object;
            let message = new $root.Recognition.Rectangle();
            if (object.x != null)
                message.x = Number(object.x);
            if (object.y != null)
                message.y = Number(object.y);
            if (object.w != null)
                message.w = Number(object.w);
            if (object.h != null)
                message.h = Number(object.h);
            return message;
        };

        /**
         * Creates a plain object from a Rectangle message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Recognition.Rectangle
         * @static
         * @param {Recognition.Rectangle} message Rectangle
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Rectangle.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.x = 0;
                object.y = 0;
                object.w = 0;
                object.h = 0;
            }
            if (message.x != null && message.hasOwnProperty("x"))
                object.x = options.json && !isFinite(message.x) ? String(message.x) : message.x;
            if (message.y != null && message.hasOwnProperty("y"))
                object.y = options.json && !isFinite(message.y) ? String(message.y) : message.y;
            if (message.w != null && message.hasOwnProperty("w"))
                object.w = options.json && !isFinite(message.w) ? String(message.w) : message.w;
            if (message.h != null && message.hasOwnProperty("h"))
                object.h = options.json && !isFinite(message.h) ? String(message.h) : message.h;
            return object;
        };

        /**
         * Converts this Rectangle to JSON.
         * @function toJSON
         * @memberof Recognition.Rectangle
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Rectangle.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Rectangle;
    })();

    return Recognition;
})();

export { $root as default };
