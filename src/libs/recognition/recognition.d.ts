import * as $protobuf from "protobufjs";
/** Namespace Recognition. */
export namespace Recognition {

    /** Properties of a Result. */
    interface IResult {

        /** Result timestamp */
        timestamp?: (number|Long|null);

        /** Result names */
        names?: (string[]|null);

        /** Result rects */
        rects?: (Recognition.IRectangle[]|null);
    }

    /** Represents a Result. */
    class Result implements IResult {

        /**
         * Constructs a new Result.
         * @param [properties] Properties to set
         */
        constructor(properties?: Recognition.IResult);

        /** Result timestamp. */
        public timestamp: (number|Long);

        /** Result names. */
        public names: string[];

        /** Result rects. */
        public rects: Recognition.IRectangle[];

        /**
         * Creates a new Result instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Result instance
         */
        public static create(properties?: Recognition.IResult): Recognition.Result;

        /**
         * Encodes the specified Result message. Does not implicitly {@link Recognition.Result.verify|verify} messages.
         * @param message Result message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Recognition.IResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Result message, length delimited. Does not implicitly {@link Recognition.Result.verify|verify} messages.
         * @param message Result message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Recognition.IResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Result message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Result
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Recognition.Result;

        /**
         * Decodes a Result message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Result
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Recognition.Result;

        /**
         * Verifies a Result message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Result message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Result
         */
        public static fromObject(object: { [k: string]: any }): Recognition.Result;

        /**
         * Creates a plain object from a Result message. Also converts values to other types if specified.
         * @param message Result
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Recognition.Result, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Result to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Rectangle. */
    interface IRectangle {

        /** Rectangle x */
        x?: (number|null);

        /** Rectangle y */
        y?: (number|null);

        /** Rectangle w */
        w?: (number|null);

        /** Rectangle h */
        h?: (number|null);
    }

    /** Represents a Rectangle. */
    class Rectangle implements IRectangle {

        /**
         * Constructs a new Rectangle.
         * @param [properties] Properties to set
         */
        constructor(properties?: Recognition.IRectangle);

        /** Rectangle x. */
        public x: number;

        /** Rectangle y. */
        public y: number;

        /** Rectangle w. */
        public w: number;

        /** Rectangle h. */
        public h: number;

        /**
         * Creates a new Rectangle instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Rectangle instance
         */
        public static create(properties?: Recognition.IRectangle): Recognition.Rectangle;

        /**
         * Encodes the specified Rectangle message. Does not implicitly {@link Recognition.Rectangle.verify|verify} messages.
         * @param message Rectangle message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Recognition.IRectangle, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Rectangle message, length delimited. Does not implicitly {@link Recognition.Rectangle.verify|verify} messages.
         * @param message Rectangle message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Recognition.IRectangle, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Rectangle message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Rectangle
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Recognition.Rectangle;

        /**
         * Decodes a Rectangle message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Rectangle
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Recognition.Rectangle;

        /**
         * Verifies a Rectangle message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Rectangle message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Rectangle
         */
        public static fromObject(object: { [k: string]: any }): Recognition.Rectangle;

        /**
         * Creates a plain object from a Rectangle message. Also converts values to other types if specified.
         * @param message Rectangle
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Recognition.Rectangle, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Rectangle to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }
}
