/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Accesscontrol = $root.Accesscontrol = (() => {

    /**
     * Namespace Accesscontrol.
     * @exports Accesscontrol
     * @namespace
     */
    const Accesscontrol = {};

    Accesscontrol.Status = (function() {

        /**
         * Properties of a Status.
         * @memberof Accesscontrol
         * @interface IStatus
         * @property {string|null} [nfcid] Status nfcid
         * @property {string|null} [rfid] Status rfid
         * @property {string|null} [bleid] Status bleid
         */

        /**
         * Constructs a new Status.
         * @memberof Accesscontrol
         * @classdesc Represents a Status.
         * @implements IStatus
         * @constructor
         * @param {Accesscontrol.IStatus=} [properties] Properties to set
         */
        function Status(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Status nfcid.
         * @member {string} nfcid
         * @memberof Accesscontrol.Status
         * @instance
         */
        Status.prototype.nfcid = "";

        /**
         * Status rfid.
         * @member {string} rfid
         * @memberof Accesscontrol.Status
         * @instance
         */
        Status.prototype.rfid = "";

        /**
         * Status bleid.
         * @member {string} bleid
         * @memberof Accesscontrol.Status
         * @instance
         */
        Status.prototype.bleid = "";

        /**
         * Creates a new Status instance using the specified properties.
         * @function create
         * @memberof Accesscontrol.Status
         * @static
         * @param {Accesscontrol.IStatus=} [properties] Properties to set
         * @returns {Accesscontrol.Status} Status instance
         */
        Status.create = function create(properties) {
            return new Status(properties);
        };

        /**
         * Encodes the specified Status message. Does not implicitly {@link Accesscontrol.Status.verify|verify} messages.
         * @function encode
         * @memberof Accesscontrol.Status
         * @static
         * @param {Accesscontrol.IStatus} message Status message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Status.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.nfcid != null && Object.hasOwnProperty.call(message, "nfcid"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.nfcid);
            if (message.rfid != null && Object.hasOwnProperty.call(message, "rfid"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.rfid);
            if (message.bleid != null && Object.hasOwnProperty.call(message, "bleid"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.bleid);
            return writer;
        };

        /**
         * Encodes the specified Status message, length delimited. Does not implicitly {@link Accesscontrol.Status.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Accesscontrol.Status
         * @static
         * @param {Accesscontrol.IStatus} message Status message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Status.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Status message from the specified reader or buffer.
         * @function decode
         * @memberof Accesscontrol.Status
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Accesscontrol.Status} Status
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Status.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Accesscontrol.Status();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nfcid = reader.string();
                    break;
                case 2:
                    message.rfid = reader.string();
                    break;
                case 3:
                    message.bleid = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Status message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Accesscontrol.Status
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Accesscontrol.Status} Status
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Status.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Status message.
         * @function verify
         * @memberof Accesscontrol.Status
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Status.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.nfcid != null && message.hasOwnProperty("nfcid"))
                if (!$util.isString(message.nfcid))
                    return "nfcid: string expected";
            if (message.rfid != null && message.hasOwnProperty("rfid"))
                if (!$util.isString(message.rfid))
                    return "rfid: string expected";
            if (message.bleid != null && message.hasOwnProperty("bleid"))
                if (!$util.isString(message.bleid))
                    return "bleid: string expected";
            return null;
        };

        /**
         * Creates a Status message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Accesscontrol.Status
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Accesscontrol.Status} Status
         */
        Status.fromObject = function fromObject(object) {
            if (object instanceof $root.Accesscontrol.Status)
                return object;
            let message = new $root.Accesscontrol.Status();
            if (object.nfcid != null)
                message.nfcid = String(object.nfcid);
            if (object.rfid != null)
                message.rfid = String(object.rfid);
            if (object.bleid != null)
                message.bleid = String(object.bleid);
            return message;
        };

        /**
         * Creates a plain object from a Status message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Accesscontrol.Status
         * @static
         * @param {Accesscontrol.Status} message Status
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Status.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.nfcid = "";
                object.rfid = "";
                object.bleid = "";
            }
            if (message.nfcid != null && message.hasOwnProperty("nfcid"))
                object.nfcid = message.nfcid;
            if (message.rfid != null && message.hasOwnProperty("rfid"))
                object.rfid = message.rfid;
            if (message.bleid != null && message.hasOwnProperty("bleid"))
                object.bleid = message.bleid;
            return object;
        };

        /**
         * Converts this Status to JSON.
         * @function toJSON
         * @memberof Accesscontrol.Status
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Status.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Status;
    })();

    return Accesscontrol;
})();

export { $root as default };
