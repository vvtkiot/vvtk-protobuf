import logging
import grpc
import peopletrack_pb2_grpc
import peopletrack_pb2
import time

def echo():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = peopletrack_pb2_grpc.AIStub(channel)
        response = stub.Echo(peopletrack_pb2.Msg(
            frameNumber=100
        ))
    print(f'frameNumber: {response.frameNumber}')

def genStitchInput():
    while 1:
        yield peopletrack_pb2.StitchInput(stitchID=1)
        time.sleep(0.1)

def stitch():
   with grpc.insecure_channel('localhost:50051') as channel:
        stub = peopletrack_pb2_grpc.AIStub(channel)
        response_iterator = stub.Stitch(genStitchInput())
        for response in response_iterator:
            print(f'frameNumber: {response.frameNumber}')

def stitchApp():
   with grpc.insecure_channel('localhost:50051') as channel:
        stub = peopletrack_pb2_grpc.AIStub(channel)
        dev0 = peopletrack_pb2.Device(stitchID=0, token="radar")
        dev1 = peopletrack_pb2.Device(stitchID=1, token="radardev")
        ds = peopletrack_pb2.Devices()
        ds.device.append(dev0)
        ds.device.append(dev1)
        response_iterator = stub.StitchApp(ds)
        for response in response_iterator:
            print(f'frameNumber: {response.frameNumber}')

if __name__ == '__main__':
    logging.basicConfig()
    echo()
    # stitchApp()
    # stitch()