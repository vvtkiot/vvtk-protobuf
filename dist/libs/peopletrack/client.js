const {Msg} = require('./peopletrack_pb.js');
const {AIClient} = require('./peopletrack_grpc_web_pb.js');

var client = new AIClient('http://localhost:8080');

var request = new Msg();
request.setFramenumber(1);

client.echo(request, {}, (err, response) => {
  console.log(response.getFramenumber());
});