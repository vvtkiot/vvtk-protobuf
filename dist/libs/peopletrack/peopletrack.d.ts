import * as $protobuf from "protobufjs";
/** Namespace Peopletrack. */
export namespace Peopletrack {

    /** Properties of a People. */
    interface IPeople {

        /** People tid */
        tid?: (number|null);

        /** People posX */
        posX?: (number|null);

        /** People posY */
        posY?: (number|null);

        /** People posZ */
        posZ?: (number|null);

        /** People state */
        state?: (Peopletrack.People.State|null);

        /** People stance */
        stance?: (Peopletrack.People.Stance|null);
    }

    /** Represents a People. */
    class People implements IPeople {

        /**
         * Constructs a new People.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.IPeople);

        /** People tid. */
        public tid: number;

        /** People posX. */
        public posX: number;

        /** People posY. */
        public posY: number;

        /** People posZ. */
        public posZ: number;

        /** People state. */
        public state: Peopletrack.People.State;

        /** People stance. */
        public stance: Peopletrack.People.Stance;

        /**
         * Creates a new People instance using the specified properties.
         * @param [properties] Properties to set
         * @returns People instance
         */
        public static create(properties?: Peopletrack.IPeople): Peopletrack.People;

        /**
         * Encodes the specified People message. Does not implicitly {@link Peopletrack.People.verify|verify} messages.
         * @param message People message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.IPeople, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified People message, length delimited. Does not implicitly {@link Peopletrack.People.verify|verify} messages.
         * @param message People message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.IPeople, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a People message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns People
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.People;

        /**
         * Decodes a People message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns People
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.People;

        /**
         * Verifies a People message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a People message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns People
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.People;

        /**
         * Creates a plain object from a People message. Also converts values to other types if specified.
         * @param message People
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.People, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this People to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace People {

        /** State enum. */
        enum State {
            NONE = 0,
            ACTIVE = 1,
            STATIC = 2
        }

        /** Stance enum. */
        enum Stance {
            UNKNOWN = 0,
            STAND = 1,
            SIT = 2,
            LAY = 3,
            SUSFALL = 4,
            COUNTDOWN = 5,
            DETFALL = 6
        }
    }

    /** Properties of a Msg. */
    interface IMsg {

        /** Msg frameNumber */
        frameNumber?: (number|null);

        /** Msg timestamp */
        timestamp?: (number|Long|null);

        /** Msg people */
        people?: (Peopletrack.IPeople[]|null);

        /** Msg cloudpoint */
        cloudpoint?: (Uint8Array|null);
    }

    /** Represents a Msg. */
    class Msg implements IMsg {

        /**
         * Constructs a new Msg.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.IMsg);

        /** Msg frameNumber. */
        public frameNumber: number;

        /** Msg timestamp. */
        public timestamp: (number|Long);

        /** Msg people. */
        public people: Peopletrack.IPeople[];

        /** Msg cloudpoint. */
        public cloudpoint: Uint8Array;

        /**
         * Creates a new Msg instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Msg instance
         */
        public static create(properties?: Peopletrack.IMsg): Peopletrack.Msg;

        /**
         * Encodes the specified Msg message. Does not implicitly {@link Peopletrack.Msg.verify|verify} messages.
         * @param message Msg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.IMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Msg message, length delimited. Does not implicitly {@link Peopletrack.Msg.verify|verify} messages.
         * @param message Msg message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.IMsg, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Msg message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.Msg;

        /**
         * Decodes a Msg message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.Msg;

        /**
         * Verifies a Msg message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Msg message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Msg
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.Msg;

        /**
         * Creates a plain object from a Msg message. Also converts values to other types if specified.
         * @param message Msg
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.Msg, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Msg to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a StitchParam. */
    interface IStitchParam {

        /** StitchParam height */
        height?: (number|null);

        /** StitchParam theta */
        theta?: (number|null);

        /** StitchParam shiftX */
        shiftX?: (number|null);

        /** StitchParam shiftZ */
        shiftZ?: (number|null);
    }

    /** Represents a StitchParam. */
    class StitchParam implements IStitchParam {

        /**
         * Constructs a new StitchParam.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.IStitchParam);

        /** StitchParam height. */
        public height: number;

        /** StitchParam theta. */
        public theta: number;

        /** StitchParam shiftX. */
        public shiftX: number;

        /** StitchParam shiftZ. */
        public shiftZ: number;

        /**
         * Creates a new StitchParam instance using the specified properties.
         * @param [properties] Properties to set
         * @returns StitchParam instance
         */
        public static create(properties?: Peopletrack.IStitchParam): Peopletrack.StitchParam;

        /**
         * Encodes the specified StitchParam message. Does not implicitly {@link Peopletrack.StitchParam.verify|verify} messages.
         * @param message StitchParam message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.IStitchParam, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified StitchParam message, length delimited. Does not implicitly {@link Peopletrack.StitchParam.verify|verify} messages.
         * @param message StitchParam message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.IStitchParam, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a StitchParam message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns StitchParam
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.StitchParam;

        /**
         * Decodes a StitchParam message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns StitchParam
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.StitchParam;

        /**
         * Verifies a StitchParam message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a StitchParam message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns StitchParam
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.StitchParam;

        /**
         * Creates a plain object from a StitchParam message. Also converts values to other types if specified.
         * @param message StitchParam
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.StitchParam, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this StitchParam to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SetupInput. */
    interface ISetupInput {

        /** SetupInput stitchID */
        stitchID?: (number|null);

        /** SetupInput frameNumber */
        frameNumber?: (number|null);

        /** SetupInput timestamp */
        timestamp?: (number|Long|null);

        /** SetupInput cloudpoint */
        cloudpoint?: (Uint8Array|null);
    }

    /** Represents a SetupInput. */
    class SetupInput implements ISetupInput {

        /**
         * Constructs a new SetupInput.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.ISetupInput);

        /** SetupInput stitchID. */
        public stitchID: number;

        /** SetupInput frameNumber. */
        public frameNumber: number;

        /** SetupInput timestamp. */
        public timestamp: (number|Long);

        /** SetupInput cloudpoint. */
        public cloudpoint: Uint8Array;

        /**
         * Creates a new SetupInput instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SetupInput instance
         */
        public static create(properties?: Peopletrack.ISetupInput): Peopletrack.SetupInput;

        /**
         * Encodes the specified SetupInput message. Does not implicitly {@link Peopletrack.SetupInput.verify|verify} messages.
         * @param message SetupInput message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.ISetupInput, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SetupInput message, length delimited. Does not implicitly {@link Peopletrack.SetupInput.verify|verify} messages.
         * @param message SetupInput message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.ISetupInput, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SetupInput message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SetupInput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.SetupInput;

        /**
         * Decodes a SetupInput message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SetupInput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.SetupInput;

        /**
         * Verifies a SetupInput message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SetupInput message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SetupInput
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.SetupInput;

        /**
         * Creates a plain object from a SetupInput message. Also converts values to other types if specified.
         * @param message SetupInput
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.SetupInput, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SetupInput to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SetupOutput. */
    interface ISetupOutput {

        /** SetupOutput frameNumber */
        frameNumber?: (number|null);

        /** SetupOutput timestamp */
        timestamp?: (number|Long|null);

        /** SetupOutput people */
        people?: (Peopletrack.IPeople[]|null);

        /** SetupOutput backgroundChart */
        backgroundChart?: (Uint8Array|null);

        /** SetupOutput stitchParam */
        stitchParam?: (Peopletrack.IStitchParam|null);
    }

    /** Represents a SetupOutput. */
    class SetupOutput implements ISetupOutput {

        /**
         * Constructs a new SetupOutput.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.ISetupOutput);

        /** SetupOutput frameNumber. */
        public frameNumber: number;

        /** SetupOutput timestamp. */
        public timestamp: (number|Long);

        /** SetupOutput people. */
        public people: Peopletrack.IPeople[];

        /** SetupOutput backgroundChart. */
        public backgroundChart: Uint8Array;

        /** SetupOutput stitchParam. */
        public stitchParam?: (Peopletrack.IStitchParam|null);

        /**
         * Creates a new SetupOutput instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SetupOutput instance
         */
        public static create(properties?: Peopletrack.ISetupOutput): Peopletrack.SetupOutput;

        /**
         * Encodes the specified SetupOutput message. Does not implicitly {@link Peopletrack.SetupOutput.verify|verify} messages.
         * @param message SetupOutput message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.ISetupOutput, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SetupOutput message, length delimited. Does not implicitly {@link Peopletrack.SetupOutput.verify|verify} messages.
         * @param message SetupOutput message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.ISetupOutput, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SetupOutput message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SetupOutput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.SetupOutput;

        /**
         * Decodes a SetupOutput message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SetupOutput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.SetupOutput;

        /**
         * Verifies a SetupOutput message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SetupOutput message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SetupOutput
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.SetupOutput;

        /**
         * Creates a plain object from a SetupOutput message. Also converts values to other types if specified.
         * @param message SetupOutput
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.SetupOutput, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SetupOutput to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a StitchInput. */
    interface IStitchInput {

        /** StitchInput stitchID */
        stitchID?: (number|null);

        /** StitchInput frameNumber */
        frameNumber?: (number|null);

        /** StitchInput timestamp */
        timestamp?: (number|Long|null);

        /** StitchInput people */
        people?: (Peopletrack.IPeople[]|null);

        /** StitchInput stitchParam */
        stitchParam?: (Peopletrack.IStitchParam|null);
    }

    /** Represents a StitchInput. */
    class StitchInput implements IStitchInput {

        /**
         * Constructs a new StitchInput.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.IStitchInput);

        /** StitchInput stitchID. */
        public stitchID: number;

        /** StitchInput frameNumber. */
        public frameNumber: number;

        /** StitchInput timestamp. */
        public timestamp: (number|Long);

        /** StitchInput people. */
        public people: Peopletrack.IPeople[];

        /** StitchInput stitchParam. */
        public stitchParam?: (Peopletrack.IStitchParam|null);

        /**
         * Creates a new StitchInput instance using the specified properties.
         * @param [properties] Properties to set
         * @returns StitchInput instance
         */
        public static create(properties?: Peopletrack.IStitchInput): Peopletrack.StitchInput;

        /**
         * Encodes the specified StitchInput message. Does not implicitly {@link Peopletrack.StitchInput.verify|verify} messages.
         * @param message StitchInput message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.IStitchInput, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified StitchInput message, length delimited. Does not implicitly {@link Peopletrack.StitchInput.verify|verify} messages.
         * @param message StitchInput message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.IStitchInput, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a StitchInput message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns StitchInput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.StitchInput;

        /**
         * Decodes a StitchInput message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns StitchInput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.StitchInput;

        /**
         * Verifies a StitchInput message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a StitchInput message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns StitchInput
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.StitchInput;

        /**
         * Creates a plain object from a StitchInput message. Also converts values to other types if specified.
         * @param message StitchInput
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.StitchInput, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this StitchInput to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a StitchOutput. */
    interface IStitchOutput {

        /** StitchOutput frameNumber */
        frameNumber?: (number|null);

        /** StitchOutput timestamp */
        timestamp?: (number|Long|null);

        /** StitchOutput people */
        people?: (Peopletrack.IPeople[]|null);

        /** StitchOutput backgroundChart */
        backgroundChart?: (Uint8Array|null);

        /** StitchOutput parameters */
        parameters?: (Uint8Array|null);
    }

    /** Represents a StitchOutput. */
    class StitchOutput implements IStitchOutput {

        /**
         * Constructs a new StitchOutput.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.IStitchOutput);

        /** StitchOutput frameNumber. */
        public frameNumber: number;

        /** StitchOutput timestamp. */
        public timestamp: (number|Long);

        /** StitchOutput people. */
        public people: Peopletrack.IPeople[];

        /** StitchOutput backgroundChart. */
        public backgroundChart: Uint8Array;

        /** StitchOutput parameters. */
        public parameters: Uint8Array;

        /**
         * Creates a new StitchOutput instance using the specified properties.
         * @param [properties] Properties to set
         * @returns StitchOutput instance
         */
        public static create(properties?: Peopletrack.IStitchOutput): Peopletrack.StitchOutput;

        /**
         * Encodes the specified StitchOutput message. Does not implicitly {@link Peopletrack.StitchOutput.verify|verify} messages.
         * @param message StitchOutput message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.IStitchOutput, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified StitchOutput message, length delimited. Does not implicitly {@link Peopletrack.StitchOutput.verify|verify} messages.
         * @param message StitchOutput message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.IStitchOutput, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a StitchOutput message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns StitchOutput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.StitchOutput;

        /**
         * Decodes a StitchOutput message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns StitchOutput
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.StitchOutput;

        /**
         * Verifies a StitchOutput message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a StitchOutput message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns StitchOutput
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.StitchOutput;

        /**
         * Creates a plain object from a StitchOutput message. Also converts values to other types if specified.
         * @param message StitchOutput
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.StitchOutput, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this StitchOutput to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Device. */
    interface IDevice {

        /** Device stitchID */
        stitchID?: (number|null);

        /** Device token */
        token?: (string|null);
    }

    /** Represents a Device. */
    class Device implements IDevice {

        /**
         * Constructs a new Device.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.IDevice);

        /** Device stitchID. */
        public stitchID: number;

        /** Device token. */
        public token: string;

        /**
         * Creates a new Device instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Device instance
         */
        public static create(properties?: Peopletrack.IDevice): Peopletrack.Device;

        /**
         * Encodes the specified Device message. Does not implicitly {@link Peopletrack.Device.verify|verify} messages.
         * @param message Device message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.IDevice, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Device message, length delimited. Does not implicitly {@link Peopletrack.Device.verify|verify} messages.
         * @param message Device message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.IDevice, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Device message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Device
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.Device;

        /**
         * Decodes a Device message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Device
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.Device;

        /**
         * Verifies a Device message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Device message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Device
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.Device;

        /**
         * Creates a plain object from a Device message. Also converts values to other types if specified.
         * @param message Device
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.Device, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Device to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Devices. */
    interface IDevices {

        /** Devices device */
        device?: (Peopletrack.IDevice[]|null);
    }

    /** Represents a Devices. */
    class Devices implements IDevices {

        /**
         * Constructs a new Devices.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.IDevices);

        /** Devices device. */
        public device: Peopletrack.IDevice[];

        /**
         * Creates a new Devices instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Devices instance
         */
        public static create(properties?: Peopletrack.IDevices): Peopletrack.Devices;

        /**
         * Encodes the specified Devices message. Does not implicitly {@link Peopletrack.Devices.verify|verify} messages.
         * @param message Devices message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.IDevices, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Devices message, length delimited. Does not implicitly {@link Peopletrack.Devices.verify|verify} messages.
         * @param message Devices message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.IDevices, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Devices message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Devices
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.Devices;

        /**
         * Decodes a Devices message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Devices
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.Devices;

        /**
         * Verifies a Devices message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Devices message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Devices
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.Devices;

        /**
         * Creates a plain object from a Devices message. Also converts values to other types if specified.
         * @param message Devices
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.Devices, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Devices to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a DeviceReq. */
    interface IDeviceReq {

        /** DeviceReq token */
        token?: (string|null);
    }

    /** Represents a DeviceReq. */
    class DeviceReq implements IDeviceReq {

        /**
         * Constructs a new DeviceReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: Peopletrack.IDeviceReq);

        /** DeviceReq token. */
        public token: string;

        /**
         * Creates a new DeviceReq instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DeviceReq instance
         */
        public static create(properties?: Peopletrack.IDeviceReq): Peopletrack.DeviceReq;

        /**
         * Encodes the specified DeviceReq message. Does not implicitly {@link Peopletrack.DeviceReq.verify|verify} messages.
         * @param message DeviceReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Peopletrack.IDeviceReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified DeviceReq message, length delimited. Does not implicitly {@link Peopletrack.DeviceReq.verify|verify} messages.
         * @param message DeviceReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Peopletrack.IDeviceReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a DeviceReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DeviceReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Peopletrack.DeviceReq;

        /**
         * Decodes a DeviceReq message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DeviceReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Peopletrack.DeviceReq;

        /**
         * Verifies a DeviceReq message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a DeviceReq message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns DeviceReq
         */
        public static fromObject(object: { [k: string]: any }): Peopletrack.DeviceReq;

        /**
         * Creates a plain object from a DeviceReq message. Also converts values to other types if specified.
         * @param message DeviceReq
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Peopletrack.DeviceReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this DeviceReq to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Represents a AI */
    class AI extends $protobuf.rpc.Service {

        /**
         * Constructs a new AI service.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         */
        constructor(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean);

        /**
         * Creates new AI service using the specified rpc implementation.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         * @returns RPC service. Useful where requests and/or responses are streamed.
         */
        public static create(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean): AI;

        /**
         * Calls Echo.
         * @param request Msg message or plain object
         * @param callback Node-style callback called with the error, if any, and Msg
         */
        public echo(request: Peopletrack.IMsg, callback: Peopletrack.AI.EchoCallback): void;

        /**
         * Calls Echo.
         * @param request Msg message or plain object
         * @returns Promise
         */
        public echo(request: Peopletrack.IMsg): Promise<Peopletrack.Msg>;

        /**
         * Calls Setup.
         * @param request SetupInput message or plain object
         * @param callback Node-style callback called with the error, if any, and SetupOutput
         */
        public setup(request: Peopletrack.ISetupInput, callback: Peopletrack.AI.SetupCallback): void;

        /**
         * Calls Setup.
         * @param request SetupInput message or plain object
         * @returns Promise
         */
        public setup(request: Peopletrack.ISetupInput): Promise<Peopletrack.SetupOutput>;

        /**
         * Calls Stitch.
         * @param request StitchInput message or plain object
         * @param callback Node-style callback called with the error, if any, and StitchOutput
         */
        public stitch(request: Peopletrack.IStitchInput, callback: Peopletrack.AI.StitchCallback): void;

        /**
         * Calls Stitch.
         * @param request StitchInput message or plain object
         * @returns Promise
         */
        public stitch(request: Peopletrack.IStitchInput): Promise<Peopletrack.StitchOutput>;

        /**
         * Calls StitchApp.
         * @param request Devices message or plain object
         * @param callback Node-style callback called with the error, if any, and StitchOutput
         */
        public stitchApp(request: Peopletrack.IDevices, callback: Peopletrack.AI.StitchAppCallback): void;

        /**
         * Calls StitchApp.
         * @param request Devices message or plain object
         * @returns Promise
         */
        public stitchApp(request: Peopletrack.IDevices): Promise<Peopletrack.StitchOutput>;

        /**
         * Calls GetMsg.
         * @param request DeviceReq message or plain object
         * @param callback Node-style callback called with the error, if any, and Msg
         */
        public getMsg(request: Peopletrack.IDeviceReq, callback: Peopletrack.AI.GetMsgCallback): void;

        /**
         * Calls GetMsg.
         * @param request DeviceReq message or plain object
         * @returns Promise
         */
        public getMsg(request: Peopletrack.IDeviceReq): Promise<Peopletrack.Msg>;
    }

    namespace AI {

        /**
         * Callback as used by {@link Peopletrack.AI#echo}.
         * @param error Error, if any
         * @param [response] Msg
         */
        type EchoCallback = (error: (Error|null), response?: Peopletrack.Msg) => void;

        /**
         * Callback as used by {@link Peopletrack.AI#setup}.
         * @param error Error, if any
         * @param [response] SetupOutput
         */
        type SetupCallback = (error: (Error|null), response?: Peopletrack.SetupOutput) => void;

        /**
         * Callback as used by {@link Peopletrack.AI#stitch}.
         * @param error Error, if any
         * @param [response] StitchOutput
         */
        type StitchCallback = (error: (Error|null), response?: Peopletrack.StitchOutput) => void;

        /**
         * Callback as used by {@link Peopletrack.AI#stitchApp}.
         * @param error Error, if any
         * @param [response] StitchOutput
         */
        type StitchAppCallback = (error: (Error|null), response?: Peopletrack.StitchOutput) => void;

        /**
         * Callback as used by {@link Peopletrack.AI#getMsg}.
         * @param error Error, if any
         * @param [response] Msg
         */
        type GetMsgCallback = (error: (Error|null), response?: Peopletrack.Msg) => void;
    }
}
