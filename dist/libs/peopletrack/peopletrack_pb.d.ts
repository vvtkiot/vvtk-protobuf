import * as jspb from 'google-protobuf'



export class People extends jspb.Message {
  getTid(): number;
  setTid(value: number): People;

  getPosx(): number;
  setPosx(value: number): People;

  getPosy(): number;
  setPosy(value: number): People;

  getPosz(): number;
  setPosz(value: number): People;

  getState(): People.State;
  setState(value: People.State): People;

  getStance(): People.Stance;
  setStance(value: People.Stance): People;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): People.AsObject;
  static toObject(includeInstance: boolean, msg: People): People.AsObject;
  static serializeBinaryToWriter(message: People, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): People;
  static deserializeBinaryFromReader(message: People, reader: jspb.BinaryReader): People;
}

export namespace People {
  export type AsObject = {
    tid: number,
    posx: number,
    posy: number,
    posz: number,
    state: People.State,
    stance: People.Stance,
  }

  export enum State { 
    NONE = 0,
    ACTIVE = 1,
    STATIC = 2,
  }

  export enum Stance { 
    UNKNOWN = 0,
    STAND = 1,
    SIT = 2,
    LAY = 3,
    SUSFALL = 4,
    COUNTDOWN = 5,
    DETFALL = 6,
  }
}

export class Msg extends jspb.Message {
  getFramenumber(): number;
  setFramenumber(value: number): Msg;

  getTimestamp(): number;
  setTimestamp(value: number): Msg;

  getPeopleList(): Array<People>;
  setPeopleList(value: Array<People>): Msg;
  clearPeopleList(): Msg;
  addPeople(value?: People, index?: number): People;

  getCloudpoint(): Uint8Array | string;
  getCloudpoint_asU8(): Uint8Array;
  getCloudpoint_asB64(): string;
  setCloudpoint(value: Uint8Array | string): Msg;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Msg.AsObject;
  static toObject(includeInstance: boolean, msg: Msg): Msg.AsObject;
  static serializeBinaryToWriter(message: Msg, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Msg;
  static deserializeBinaryFromReader(message: Msg, reader: jspb.BinaryReader): Msg;
}

export namespace Msg {
  export type AsObject = {
    framenumber: number,
    timestamp: number,
    peopleList: Array<People.AsObject>,
    cloudpoint: Uint8Array | string,
  }
}

export class StitchParam extends jspb.Message {
  getHeight(): number;
  setHeight(value: number): StitchParam;

  getTheta(): number;
  setTheta(value: number): StitchParam;

  getShiftx(): number;
  setShiftx(value: number): StitchParam;

  getShiftz(): number;
  setShiftz(value: number): StitchParam;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StitchParam.AsObject;
  static toObject(includeInstance: boolean, msg: StitchParam): StitchParam.AsObject;
  static serializeBinaryToWriter(message: StitchParam, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StitchParam;
  static deserializeBinaryFromReader(message: StitchParam, reader: jspb.BinaryReader): StitchParam;
}

export namespace StitchParam {
  export type AsObject = {
    height: number,
    theta: number,
    shiftx: number,
    shiftz: number,
  }
}

export class SetupInput extends jspb.Message {
  getStitchid(): number;
  setStitchid(value: number): SetupInput;

  getFramenumber(): number;
  setFramenumber(value: number): SetupInput;

  getTimestamp(): number;
  setTimestamp(value: number): SetupInput;

  getCloudpoint(): Uint8Array | string;
  getCloudpoint_asU8(): Uint8Array;
  getCloudpoint_asB64(): string;
  setCloudpoint(value: Uint8Array | string): SetupInput;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetupInput.AsObject;
  static toObject(includeInstance: boolean, msg: SetupInput): SetupInput.AsObject;
  static serializeBinaryToWriter(message: SetupInput, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetupInput;
  static deserializeBinaryFromReader(message: SetupInput, reader: jspb.BinaryReader): SetupInput;
}

export namespace SetupInput {
  export type AsObject = {
    stitchid: number,
    framenumber: number,
    timestamp: number,
    cloudpoint: Uint8Array | string,
  }
}

export class SetupOutput extends jspb.Message {
  getFramenumber(): number;
  setFramenumber(value: number): SetupOutput;

  getTimestamp(): number;
  setTimestamp(value: number): SetupOutput;

  getPeopleList(): Array<People>;
  setPeopleList(value: Array<People>): SetupOutput;
  clearPeopleList(): SetupOutput;
  addPeople(value?: People, index?: number): People;

  getBackgroundchart(): Uint8Array | string;
  getBackgroundchart_asU8(): Uint8Array;
  getBackgroundchart_asB64(): string;
  setBackgroundchart(value: Uint8Array | string): SetupOutput;

  getStitchparam(): StitchParam | undefined;
  setStitchparam(value?: StitchParam): SetupOutput;
  hasStitchparam(): boolean;
  clearStitchparam(): SetupOutput;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetupOutput.AsObject;
  static toObject(includeInstance: boolean, msg: SetupOutput): SetupOutput.AsObject;
  static serializeBinaryToWriter(message: SetupOutput, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetupOutput;
  static deserializeBinaryFromReader(message: SetupOutput, reader: jspb.BinaryReader): SetupOutput;
}

export namespace SetupOutput {
  export type AsObject = {
    framenumber: number,
    timestamp: number,
    peopleList: Array<People.AsObject>,
    backgroundchart: Uint8Array | string,
    stitchparam?: StitchParam.AsObject,
  }
}

export class StitchInput extends jspb.Message {
  getStitchid(): number;
  setStitchid(value: number): StitchInput;

  getFramenumber(): number;
  setFramenumber(value: number): StitchInput;

  getTimestamp(): number;
  setTimestamp(value: number): StitchInput;

  getPeopleList(): Array<People>;
  setPeopleList(value: Array<People>): StitchInput;
  clearPeopleList(): StitchInput;
  addPeople(value?: People, index?: number): People;

  getStitchparam(): StitchParam | undefined;
  setStitchparam(value?: StitchParam): StitchInput;
  hasStitchparam(): boolean;
  clearStitchparam(): StitchInput;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StitchInput.AsObject;
  static toObject(includeInstance: boolean, msg: StitchInput): StitchInput.AsObject;
  static serializeBinaryToWriter(message: StitchInput, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StitchInput;
  static deserializeBinaryFromReader(message: StitchInput, reader: jspb.BinaryReader): StitchInput;
}

export namespace StitchInput {
  export type AsObject = {
    stitchid: number,
    framenumber: number,
    timestamp: number,
    peopleList: Array<People.AsObject>,
    stitchparam?: StitchParam.AsObject,
  }
}

export class StitchOutput extends jspb.Message {
  getFramenumber(): number;
  setFramenumber(value: number): StitchOutput;

  getTimestamp(): number;
  setTimestamp(value: number): StitchOutput;

  getPeopleList(): Array<People>;
  setPeopleList(value: Array<People>): StitchOutput;
  clearPeopleList(): StitchOutput;
  addPeople(value?: People, index?: number): People;

  getBackgroundchart(): Uint8Array | string;
  getBackgroundchart_asU8(): Uint8Array;
  getBackgroundchart_asB64(): string;
  setBackgroundchart(value: Uint8Array | string): StitchOutput;

  getParameters(): Uint8Array | string;
  getParameters_asU8(): Uint8Array;
  getParameters_asB64(): string;
  setParameters(value: Uint8Array | string): StitchOutput;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StitchOutput.AsObject;
  static toObject(includeInstance: boolean, msg: StitchOutput): StitchOutput.AsObject;
  static serializeBinaryToWriter(message: StitchOutput, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StitchOutput;
  static deserializeBinaryFromReader(message: StitchOutput, reader: jspb.BinaryReader): StitchOutput;
}

export namespace StitchOutput {
  export type AsObject = {
    framenumber: number,
    timestamp: number,
    peopleList: Array<People.AsObject>,
    backgroundchart: Uint8Array | string,
    parameters: Uint8Array | string,
  }
}

export class Device extends jspb.Message {
  getStitchid(): number;
  setStitchid(value: number): Device;

  getToken(): string;
  setToken(value: string): Device;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Device.AsObject;
  static toObject(includeInstance: boolean, msg: Device): Device.AsObject;
  static serializeBinaryToWriter(message: Device, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Device;
  static deserializeBinaryFromReader(message: Device, reader: jspb.BinaryReader): Device;
}

export namespace Device {
  export type AsObject = {
    stitchid: number,
    token: string,
  }
}

export class Devices extends jspb.Message {
  getDeviceList(): Array<Device>;
  setDeviceList(value: Array<Device>): Devices;
  clearDeviceList(): Devices;
  addDevice(value?: Device, index?: number): Device;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Devices.AsObject;
  static toObject(includeInstance: boolean, msg: Devices): Devices.AsObject;
  static serializeBinaryToWriter(message: Devices, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Devices;
  static deserializeBinaryFromReader(message: Devices, reader: jspb.BinaryReader): Devices;
}

export namespace Devices {
  export type AsObject = {
    deviceList: Array<Device.AsObject>,
  }
}

export class DeviceReq extends jspb.Message {
  getToken(): string;
  setToken(value: string): DeviceReq;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeviceReq.AsObject;
  static toObject(includeInstance: boolean, msg: DeviceReq): DeviceReq.AsObject;
  static serializeBinaryToWriter(message: DeviceReq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeviceReq;
  static deserializeBinaryFromReader(message: DeviceReq, reader: jspb.BinaryReader): DeviceReq;
}

export namespace DeviceReq {
  export type AsObject = {
    token: string,
  }
}

