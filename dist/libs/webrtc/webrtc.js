/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Webrtc = $root.Webrtc = (() => {

    /**
     * Namespace Webrtc.
     * @exports Webrtc
     * @namespace
     */
    const Webrtc = {};

    Webrtc.Msg = (function() {

        /**
         * Properties of a Msg.
         * @memberof Webrtc
         * @interface IMsg
         * @property {number|null} [timestamp] Msg timestamp
         * @property {Webrtc.Msg.Type|null} [type] Msg type
         * @property {string|null} [payload] Msg payload
         * @property {string|null} [clientid] Msg clientid
         */

        /**
         * Constructs a new Msg.
         * @memberof Webrtc
         * @classdesc Represents a Msg.
         * @implements IMsg
         * @constructor
         * @param {Webrtc.IMsg=} [properties] Properties to set
         */
        function Msg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Msg timestamp.
         * @member {number} timestamp
         * @memberof Webrtc.Msg
         * @instance
         */
        Msg.prototype.timestamp = 0;

        /**
         * Msg type.
         * @member {Webrtc.Msg.Type} type
         * @memberof Webrtc.Msg
         * @instance
         */
        Msg.prototype.type = 0;

        /**
         * Msg payload.
         * @member {string} payload
         * @memberof Webrtc.Msg
         * @instance
         */
        Msg.prototype.payload = "";

        /**
         * Msg clientid.
         * @member {string} clientid
         * @memberof Webrtc.Msg
         * @instance
         */
        Msg.prototype.clientid = "";

        /**
         * Creates a new Msg instance using the specified properties.
         * @function create
         * @memberof Webrtc.Msg
         * @static
         * @param {Webrtc.IMsg=} [properties] Properties to set
         * @returns {Webrtc.Msg} Msg instance
         */
        Msg.create = function create(properties) {
            return new Msg(properties);
        };

        /**
         * Encodes the specified Msg message. Does not implicitly {@link Webrtc.Msg.verify|verify} messages.
         * @function encode
         * @memberof Webrtc.Msg
         * @static
         * @param {Webrtc.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.timestamp);
            if (message.type != null && Object.hasOwnProperty.call(message, "type"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.type);
            if (message.payload != null && Object.hasOwnProperty.call(message, "payload"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.payload);
            if (message.clientid != null && Object.hasOwnProperty.call(message, "clientid"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.clientid);
            return writer;
        };

        /**
         * Encodes the specified Msg message, length delimited. Does not implicitly {@link Webrtc.Msg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Webrtc.Msg
         * @static
         * @param {Webrtc.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Msg message from the specified reader or buffer.
         * @function decode
         * @memberof Webrtc.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Webrtc.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Webrtc.Msg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.timestamp = reader.uint32();
                    break;
                case 2:
                    message.type = reader.int32();
                    break;
                case 3:
                    message.payload = reader.string();
                    break;
                case 4:
                    message.clientid = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Msg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Webrtc.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Webrtc.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Msg message.
         * @function verify
         * @memberof Webrtc.Msg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Msg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (!$util.isInteger(message.timestamp))
                    return "timestamp: integer expected";
            if (message.type != null && message.hasOwnProperty("type"))
                switch (message.type) {
                default:
                    return "type: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.payload != null && message.hasOwnProperty("payload"))
                if (!$util.isString(message.payload))
                    return "payload: string expected";
            if (message.clientid != null && message.hasOwnProperty("clientid"))
                if (!$util.isString(message.clientid))
                    return "clientid: string expected";
            return null;
        };

        /**
         * Creates a Msg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Webrtc.Msg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Webrtc.Msg} Msg
         */
        Msg.fromObject = function fromObject(object) {
            if (object instanceof $root.Webrtc.Msg)
                return object;
            let message = new $root.Webrtc.Msg();
            if (object.timestamp != null)
                message.timestamp = object.timestamp >>> 0;
            switch (object.type) {
            case "OFFER":
            case 0:
                message.type = 0;
                break;
            case "ANSWER":
            case 1:
                message.type = 1;
                break;
            case "CANDIDATE":
            case 2:
                message.type = 2;
                break;
            }
            if (object.payload != null)
                message.payload = String(object.payload);
            if (object.clientid != null)
                message.clientid = String(object.clientid);
            return message;
        };

        /**
         * Creates a plain object from a Msg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Webrtc.Msg
         * @static
         * @param {Webrtc.Msg} message Msg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Msg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.timestamp = 0;
                object.type = options.enums === String ? "OFFER" : 0;
                object.payload = "";
                object.clientid = "";
            }
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                object.timestamp = message.timestamp;
            if (message.type != null && message.hasOwnProperty("type"))
                object.type = options.enums === String ? $root.Webrtc.Msg.Type[message.type] : message.type;
            if (message.payload != null && message.hasOwnProperty("payload"))
                object.payload = message.payload;
            if (message.clientid != null && message.hasOwnProperty("clientid"))
                object.clientid = message.clientid;
            return object;
        };

        /**
         * Converts this Msg to JSON.
         * @function toJSON
         * @memberof Webrtc.Msg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Msg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Type enum.
         * @name Webrtc.Msg.Type
         * @enum {number}
         * @property {number} OFFER=0 OFFER value
         * @property {number} ANSWER=1 ANSWER value
         * @property {number} CANDIDATE=2 CANDIDATE value
         */
        Msg.Type = (function() {
            const valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "OFFER"] = 0;
            values[valuesById[1] = "ANSWER"] = 1;
            values[valuesById[2] = "CANDIDATE"] = 2;
            return values;
        })();

        return Msg;
    })();

    return Webrtc;
})();

export { $root as default };
