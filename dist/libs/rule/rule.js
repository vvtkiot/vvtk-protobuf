/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Rule = $root.Rule = (() => {

    /**
     * Namespace Rule.
     * @exports Rule
     * @namespace
     */
    const Rule = {};

    Rule.Msg = (function() {

        /**
         * Properties of a Msg.
         * @memberof Rule
         * @interface IMsg
         * @property {Rule.Msg.Type|null} [type] Msg type
         */

        /**
         * Constructs a new Msg.
         * @memberof Rule
         * @classdesc Represents a Msg.
         * @implements IMsg
         * @constructor
         * @param {Rule.IMsg=} [properties] Properties to set
         */
        function Msg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Msg type.
         * @member {Rule.Msg.Type} type
         * @memberof Rule.Msg
         * @instance
         */
        Msg.prototype.type = 0;

        /**
         * Creates a new Msg instance using the specified properties.
         * @function create
         * @memberof Rule.Msg
         * @static
         * @param {Rule.IMsg=} [properties] Properties to set
         * @returns {Rule.Msg} Msg instance
         */
        Msg.create = function create(properties) {
            return new Msg(properties);
        };

        /**
         * Encodes the specified Msg message. Does not implicitly {@link Rule.Msg.verify|verify} messages.
         * @function encode
         * @memberof Rule.Msg
         * @static
         * @param {Rule.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.type != null && Object.hasOwnProperty.call(message, "type"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.type);
            return writer;
        };

        /**
         * Encodes the specified Msg message, length delimited. Does not implicitly {@link Rule.Msg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Rule.Msg
         * @static
         * @param {Rule.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Msg message from the specified reader or buffer.
         * @function decode
         * @memberof Rule.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Rule.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Rule.Msg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.type = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Msg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Rule.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Rule.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Msg message.
         * @function verify
         * @memberof Rule.Msg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Msg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.type != null && message.hasOwnProperty("type"))
                switch (message.type) {
                default:
                    return "type: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                    break;
                }
            return null;
        };

        /**
         * Creates a Msg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Rule.Msg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Rule.Msg} Msg
         */
        Msg.fromObject = function fromObject(object) {
            if (object instanceof $root.Rule.Msg)
                return object;
            let message = new $root.Rule.Msg();
            switch (object.type) {
            case "None":
            case 0:
                message.type = 0;
                break;
            case "Online":
            case 1:
                message.type = 1;
                break;
            case "Offline":
            case 2:
                message.type = 2;
                break;
            case "Ring":
            case 3:
                message.type = 3;
                break;
            }
            return message;
        };

        /**
         * Creates a plain object from a Msg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Rule.Msg
         * @static
         * @param {Rule.Msg} message Msg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Msg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults)
                object.type = options.enums === String ? "None" : 0;
            if (message.type != null && message.hasOwnProperty("type"))
                object.type = options.enums === String ? $root.Rule.Msg.Type[message.type] : message.type;
            return object;
        };

        /**
         * Converts this Msg to JSON.
         * @function toJSON
         * @memberof Rule.Msg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Msg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Type enum.
         * @name Rule.Msg.Type
         * @enum {number}
         * @property {number} None=0 None value
         * @property {number} Online=1 Online value
         * @property {number} Offline=2 Offline value
         * @property {number} Ring=3 Ring value
         */
        Msg.Type = (function() {
            const valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "None"] = 0;
            values[valuesById[1] = "Online"] = 1;
            values[valuesById[2] = "Offline"] = 2;
            values[valuesById[3] = "Ring"] = 3;
            return values;
        })();

        return Msg;
    })();

    return Rule;
})();

export { $root as default };
