/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Log = $root.Log = (() => {

    /**
     * Namespace Log.
     * @exports Log
     * @namespace
     */
    const Log = {};

    Log.Data = (function() {

        /**
         * Properties of a Data.
         * @memberof Log
         * @interface IData
         * @property {string|null} [source] Data source
         * @property {Log.Data.Level|null} [level] Data level
         * @property {string|null} [user] Data user
         * @property {string|null} [caller] Data caller
         * @property {string|null} [time] Data time
         * @property {string|null} [msg] Data msg
         * @property {string|null} [deviceToken] Data deviceToken
         * @property {string|null} [websocketID] Data websocketID
         * @property {string|null} [webrtcID] Data webrtcID
         * @property {number|null} [count] Data count
         * @property {string|null} [hostname] Data hostname
         */

        /**
         * Constructs a new Data.
         * @memberof Log
         * @classdesc Represents a Data.
         * @implements IData
         * @constructor
         * @param {Log.IData=} [properties] Properties to set
         */
        function Data(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Data source.
         * @member {string} source
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.source = "";

        /**
         * Data level.
         * @member {Log.Data.Level} level
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.level = 0;

        /**
         * Data user.
         * @member {string} user
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.user = "";

        /**
         * Data caller.
         * @member {string} caller
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.caller = "";

        /**
         * Data time.
         * @member {string} time
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.time = "";

        /**
         * Data msg.
         * @member {string} msg
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.msg = "";

        /**
         * Data deviceToken.
         * @member {string} deviceToken
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.deviceToken = "";

        /**
         * Data websocketID.
         * @member {string} websocketID
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.websocketID = "";

        /**
         * Data webrtcID.
         * @member {string} webrtcID
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.webrtcID = "";

        /**
         * Data count.
         * @member {number} count
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.count = 0;

        /**
         * Data hostname.
         * @member {string} hostname
         * @memberof Log.Data
         * @instance
         */
        Data.prototype.hostname = "";

        /**
         * Creates a new Data instance using the specified properties.
         * @function create
         * @memberof Log.Data
         * @static
         * @param {Log.IData=} [properties] Properties to set
         * @returns {Log.Data} Data instance
         */
        Data.create = function create(properties) {
            return new Data(properties);
        };

        /**
         * Encodes the specified Data message. Does not implicitly {@link Log.Data.verify|verify} messages.
         * @function encode
         * @memberof Log.Data
         * @static
         * @param {Log.IData} message Data message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Data.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.source != null && Object.hasOwnProperty.call(message, "source"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.source);
            if (message.level != null && Object.hasOwnProperty.call(message, "level"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.level);
            if (message.user != null && Object.hasOwnProperty.call(message, "user"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.user);
            if (message.caller != null && Object.hasOwnProperty.call(message, "caller"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.caller);
            if (message.time != null && Object.hasOwnProperty.call(message, "time"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.time);
            if (message.msg != null && Object.hasOwnProperty.call(message, "msg"))
                writer.uint32(/* id 6, wireType 2 =*/50).string(message.msg);
            if (message.deviceToken != null && Object.hasOwnProperty.call(message, "deviceToken"))
                writer.uint32(/* id 7, wireType 2 =*/58).string(message.deviceToken);
            if (message.websocketID != null && Object.hasOwnProperty.call(message, "websocketID"))
                writer.uint32(/* id 8, wireType 2 =*/66).string(message.websocketID);
            if (message.webrtcID != null && Object.hasOwnProperty.call(message, "webrtcID"))
                writer.uint32(/* id 9, wireType 2 =*/74).string(message.webrtcID);
            if (message.count != null && Object.hasOwnProperty.call(message, "count"))
                writer.uint32(/* id 10, wireType 0 =*/80).uint32(message.count);
            if (message.hostname != null && Object.hasOwnProperty.call(message, "hostname"))
                writer.uint32(/* id 11, wireType 2 =*/90).string(message.hostname);
            return writer;
        };

        /**
         * Encodes the specified Data message, length delimited. Does not implicitly {@link Log.Data.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Log.Data
         * @static
         * @param {Log.IData} message Data message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Data.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Data message from the specified reader or buffer.
         * @function decode
         * @memberof Log.Data
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Log.Data} Data
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Data.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Log.Data();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.source = reader.string();
                    break;
                case 2:
                    message.level = reader.int32();
                    break;
                case 3:
                    message.user = reader.string();
                    break;
                case 4:
                    message.caller = reader.string();
                    break;
                case 5:
                    message.time = reader.string();
                    break;
                case 6:
                    message.msg = reader.string();
                    break;
                case 7:
                    message.deviceToken = reader.string();
                    break;
                case 8:
                    message.websocketID = reader.string();
                    break;
                case 9:
                    message.webrtcID = reader.string();
                    break;
                case 10:
                    message.count = reader.uint32();
                    break;
                case 11:
                    message.hostname = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Data message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Log.Data
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Log.Data} Data
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Data.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Data message.
         * @function verify
         * @memberof Log.Data
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Data.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.source != null && message.hasOwnProperty("source"))
                if (!$util.isString(message.source))
                    return "source: string expected";
            if (message.level != null && message.hasOwnProperty("level"))
                switch (message.level) {
                default:
                    return "level: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    break;
                }
            if (message.user != null && message.hasOwnProperty("user"))
                if (!$util.isString(message.user))
                    return "user: string expected";
            if (message.caller != null && message.hasOwnProperty("caller"))
                if (!$util.isString(message.caller))
                    return "caller: string expected";
            if (message.time != null && message.hasOwnProperty("time"))
                if (!$util.isString(message.time))
                    return "time: string expected";
            if (message.msg != null && message.hasOwnProperty("msg"))
                if (!$util.isString(message.msg))
                    return "msg: string expected";
            if (message.deviceToken != null && message.hasOwnProperty("deviceToken"))
                if (!$util.isString(message.deviceToken))
                    return "deviceToken: string expected";
            if (message.websocketID != null && message.hasOwnProperty("websocketID"))
                if (!$util.isString(message.websocketID))
                    return "websocketID: string expected";
            if (message.webrtcID != null && message.hasOwnProperty("webrtcID"))
                if (!$util.isString(message.webrtcID))
                    return "webrtcID: string expected";
            if (message.count != null && message.hasOwnProperty("count"))
                if (!$util.isInteger(message.count))
                    return "count: integer expected";
            if (message.hostname != null && message.hasOwnProperty("hostname"))
                if (!$util.isString(message.hostname))
                    return "hostname: string expected";
            return null;
        };

        /**
         * Creates a Data message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Log.Data
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Log.Data} Data
         */
        Data.fromObject = function fromObject(object) {
            if (object instanceof $root.Log.Data)
                return object;
            let message = new $root.Log.Data();
            if (object.source != null)
                message.source = String(object.source);
            switch (object.level) {
            case "Debug":
            case 0:
                message.level = 0;
                break;
            case "Info":
            case 1:
                message.level = 1;
                break;
            case "Warn":
            case 2:
                message.level = 2;
                break;
            case "Error":
            case 3:
                message.level = 3;
                break;
            case "Fatal":
            case 4:
                message.level = 4;
                break;
            }
            if (object.user != null)
                message.user = String(object.user);
            if (object.caller != null)
                message.caller = String(object.caller);
            if (object.time != null)
                message.time = String(object.time);
            if (object.msg != null)
                message.msg = String(object.msg);
            if (object.deviceToken != null)
                message.deviceToken = String(object.deviceToken);
            if (object.websocketID != null)
                message.websocketID = String(object.websocketID);
            if (object.webrtcID != null)
                message.webrtcID = String(object.webrtcID);
            if (object.count != null)
                message.count = object.count >>> 0;
            if (object.hostname != null)
                message.hostname = String(object.hostname);
            return message;
        };

        /**
         * Creates a plain object from a Data message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Log.Data
         * @static
         * @param {Log.Data} message Data
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Data.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.source = "";
                object.level = options.enums === String ? "Debug" : 0;
                object.user = "";
                object.caller = "";
                object.time = "";
                object.msg = "";
                object.deviceToken = "";
                object.websocketID = "";
                object.webrtcID = "";
                object.count = 0;
                object.hostname = "";
            }
            if (message.source != null && message.hasOwnProperty("source"))
                object.source = message.source;
            if (message.level != null && message.hasOwnProperty("level"))
                object.level = options.enums === String ? $root.Log.Data.Level[message.level] : message.level;
            if (message.user != null && message.hasOwnProperty("user"))
                object.user = message.user;
            if (message.caller != null && message.hasOwnProperty("caller"))
                object.caller = message.caller;
            if (message.time != null && message.hasOwnProperty("time"))
                object.time = message.time;
            if (message.msg != null && message.hasOwnProperty("msg"))
                object.msg = message.msg;
            if (message.deviceToken != null && message.hasOwnProperty("deviceToken"))
                object.deviceToken = message.deviceToken;
            if (message.websocketID != null && message.hasOwnProperty("websocketID"))
                object.websocketID = message.websocketID;
            if (message.webrtcID != null && message.hasOwnProperty("webrtcID"))
                object.webrtcID = message.webrtcID;
            if (message.count != null && message.hasOwnProperty("count"))
                object.count = message.count;
            if (message.hostname != null && message.hasOwnProperty("hostname"))
                object.hostname = message.hostname;
            return object;
        };

        /**
         * Converts this Data to JSON.
         * @function toJSON
         * @memberof Log.Data
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Data.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Level enum.
         * @name Log.Data.Level
         * @enum {number}
         * @property {number} Debug=0 Debug value
         * @property {number} Info=1 Info value
         * @property {number} Warn=2 Warn value
         * @property {number} Error=3 Error value
         * @property {number} Fatal=4 Fatal value
         */
        Data.Level = (function() {
            const valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "Debug"] = 0;
            values[valuesById[1] = "Info"] = 1;
            values[valuesById[2] = "Warn"] = 2;
            values[valuesById[3] = "Error"] = 3;
            values[valuesById[4] = "Fatal"] = 4;
            return values;
        })();

        return Data;
    })();

    return Log;
})();

export { $root as default };
