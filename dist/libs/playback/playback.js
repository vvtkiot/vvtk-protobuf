/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Playback = $root.Playback = (() => {

    /**
     * Namespace Playback.
     * @exports Playback
     * @namespace
     */
    const Playback = {};

    Playback.Msg = (function() {

        /**
         * Properties of a Msg.
         * @memberof Playback
         * @interface IMsg
         * @property {Playback.Msg.Type|null} [type] Msg type
         * @property {string|null} [startTime] Msg startTime
         * @property {string|null} [stopTime] Msg stopTime
         */

        /**
         * Constructs a new Msg.
         * @memberof Playback
         * @classdesc Represents a Msg.
         * @implements IMsg
         * @constructor
         * @param {Playback.IMsg=} [properties] Properties to set
         */
        function Msg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Msg type.
         * @member {Playback.Msg.Type} type
         * @memberof Playback.Msg
         * @instance
         */
        Msg.prototype.type = 0;

        /**
         * Msg startTime.
         * @member {string} startTime
         * @memberof Playback.Msg
         * @instance
         */
        Msg.prototype.startTime = "";

        /**
         * Msg stopTime.
         * @member {string} stopTime
         * @memberof Playback.Msg
         * @instance
         */
        Msg.prototype.stopTime = "";

        /**
         * Creates a new Msg instance using the specified properties.
         * @function create
         * @memberof Playback.Msg
         * @static
         * @param {Playback.IMsg=} [properties] Properties to set
         * @returns {Playback.Msg} Msg instance
         */
        Msg.create = function create(properties) {
            return new Msg(properties);
        };

        /**
         * Encodes the specified Msg message. Does not implicitly {@link Playback.Msg.verify|verify} messages.
         * @function encode
         * @memberof Playback.Msg
         * @static
         * @param {Playback.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.type != null && Object.hasOwnProperty.call(message, "type"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.type);
            if (message.startTime != null && Object.hasOwnProperty.call(message, "startTime"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.startTime);
            if (message.stopTime != null && Object.hasOwnProperty.call(message, "stopTime"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.stopTime);
            return writer;
        };

        /**
         * Encodes the specified Msg message, length delimited. Does not implicitly {@link Playback.Msg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Playback.Msg
         * @static
         * @param {Playback.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Msg message from the specified reader or buffer.
         * @function decode
         * @memberof Playback.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Playback.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Playback.Msg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.type = reader.int32();
                    break;
                case 2:
                    message.startTime = reader.string();
                    break;
                case 3:
                    message.stopTime = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Msg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Playback.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Playback.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Msg message.
         * @function verify
         * @memberof Playback.Msg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Msg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.type != null && message.hasOwnProperty("type"))
                switch (message.type) {
                default:
                    return "type: enum value expected";
                case 0:
                case 1:
                    break;
                }
            if (message.startTime != null && message.hasOwnProperty("startTime"))
                if (!$util.isString(message.startTime))
                    return "startTime: string expected";
            if (message.stopTime != null && message.hasOwnProperty("stopTime"))
                if (!$util.isString(message.stopTime))
                    return "stopTime: string expected";
            return null;
        };

        /**
         * Creates a Msg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Playback.Msg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Playback.Msg} Msg
         */
        Msg.fromObject = function fromObject(object) {
            if (object instanceof $root.Playback.Msg)
                return object;
            let message = new $root.Playback.Msg();
            switch (object.type) {
            case "PLAY":
            case 0:
                message.type = 0;
                break;
            case "KEEPALIVE":
            case 1:
                message.type = 1;
                break;
            }
            if (object.startTime != null)
                message.startTime = String(object.startTime);
            if (object.stopTime != null)
                message.stopTime = String(object.stopTime);
            return message;
        };

        /**
         * Creates a plain object from a Msg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Playback.Msg
         * @static
         * @param {Playback.Msg} message Msg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Msg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.type = options.enums === String ? "PLAY" : 0;
                object.startTime = "";
                object.stopTime = "";
            }
            if (message.type != null && message.hasOwnProperty("type"))
                object.type = options.enums === String ? $root.Playback.Msg.Type[message.type] : message.type;
            if (message.startTime != null && message.hasOwnProperty("startTime"))
                object.startTime = message.startTime;
            if (message.stopTime != null && message.hasOwnProperty("stopTime"))
                object.stopTime = message.stopTime;
            return object;
        };

        /**
         * Converts this Msg to JSON.
         * @function toJSON
         * @memberof Playback.Msg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Msg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Type enum.
         * @name Playback.Msg.Type
         * @enum {number}
         * @property {number} PLAY=0 PLAY value
         * @property {number} KEEPALIVE=1 KEEPALIVE value
         */
        Msg.Type = (function() {
            const valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "PLAY"] = 0;
            values[valuesById[1] = "KEEPALIVE"] = 1;
            return values;
        })();

        return Msg;
    })();

    return Playback;
})();

export { $root as default };
