/* Generated by the protocol buffer compiler.  DO NOT EDIT! */
/* Generated from: shadow.proto */

#ifndef PROTOBUF_C_shadow_2eproto__INCLUDED
#define PROTOBUF_C_shadow_2eproto__INCLUDED

#include <protobuf-c/protobuf-c.h>

PROTOBUF_C__BEGIN_DECLS

#if PROTOBUF_C_VERSION_NUMBER < 1003000
# error This file was generated by a newer version of protoc-c which is incompatible with your libprotobuf-c headers. Please update your headers.
#elif 1003003 < PROTOBUF_C_MIN_COMPILER_VERSION
# error This file was generated by an older version of protoc-c which is incompatible with your libprotobuf-c headers. Please regenerate this file with a newer version of protoc-c.
#endif


typedef struct _Shadow__Msg Shadow__Msg;
typedef struct _Shadow__StateMsg Shadow__StateMsg;
typedef struct _Shadow__DesiredMsg Shadow__DesiredMsg;
typedef struct _Shadow__VideoSource Shadow__VideoSource;
typedef struct _Shadow__ReportedMsg Shadow__ReportedMsg;
typedef struct _Shadow__VersionMsg Shadow__VersionMsg;
typedef struct _Shadow__AvsMsg Shadow__AvsMsg;
typedef struct _Shadow__CalibrationMsg Shadow__CalibrationMsg;
typedef struct _Shadow__ChripParamMsg Shadow__ChripParamMsg;
typedef struct _Shadow__EnvironmentMsg Shadow__EnvironmentMsg;
typedef struct _Shadow__StitchParamMsg Shadow__StitchParamMsg;
typedef struct _Shadow__HealthMsg Shadow__HealthMsg;
typedef struct _Shadow__DeviceReq Shadow__DeviceReq;
typedef struct _Shadow__SetMsgReq Shadow__SetMsgReq;
typedef struct _Shadow__Res Shadow__Res;


/* --- enums --- */

typedef enum _Shadow__CalibrationMsg__State {
  SHADOW__CALIBRATION_MSG__STATE__None = 0,
  SHADOW__CALIBRATION_MSG__STATE__Calibrating = 1,
  SHADOW__CALIBRATION_MSG__STATE__Fail = 2,
  SHADOW__CALIBRATION_MSG__STATE__Success = 3,
  SHADOW__CALIBRATION_MSG__STATE__Collecting = 4
    PROTOBUF_C__FORCE_ENUM_TO_BE_INT_SIZE(SHADOW__CALIBRATION_MSG__STATE)
} Shadow__CalibrationMsg__State;
typedef enum _Shadow__Res__Status {
  SHADOW__RES__STATUS__OK = 0,
  SHADOW__RES__STATUS__Fail = 1
    PROTOBUF_C__FORCE_ENUM_TO_BE_INT_SIZE(SHADOW__RES__STATUS)
} Shadow__Res__Status;

/* --- messages --- */

struct  _Shadow__Msg
{
  ProtobufCMessage base;
  Shadow__StateMsg *state;
};
#define SHADOW__MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__msg__descriptor) \
    , NULL }


struct  _Shadow__StateMsg
{
  ProtobufCMessage base;
  char *id;
  Shadow__DesiredMsg *desired;
  Shadow__ReportedMsg *reported;
};
#define SHADOW__STATE_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__state_msg__descriptor) \
    , (char *)protobuf_c_empty_string, NULL, NULL }


struct  _Shadow__DesiredMsg
{
  ProtobufCMessage base;
  size_t n_videosources;
  Shadow__VideoSource **videosources;
  Shadow__VersionMsg *versioninfo;
  Shadow__AvsMsg *avs;
  protobuf_c_boolean autoupgrade;
  Shadow__CalibrationMsg *calibration;
  Shadow__StitchParamMsg *stitchparam;
  Shadow__ChripParamMsg *chripparam;
};
#define SHADOW__DESIRED_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__desired_msg__descriptor) \
    , 0,NULL, NULL, NULL, 0, NULL, NULL, NULL }


struct  _Shadow__VideoSource
{
  ProtobufCMessage base;
  char *id;
  char *password;
};
#define SHADOW__VIDEO_SOURCE__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__video_source__descriptor) \
    , (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string }


struct  _Shadow__ReportedMsg
{
  ProtobufCMessage base;
  size_t n_videosources;
  Shadow__VideoSource **videosources;
  size_t n_discoveredvideosources;
  char **discoveredvideosources;
  protobuf_c_boolean connected;
  char *status;
  char *ip;
  Shadow__VersionMsg *versioninfo;
  Shadow__VersionMsg *lastversioninfo;
  Shadow__HealthMsg *health;
  Shadow__EnvironmentMsg *environment;
};
#define SHADOW__REPORTED_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__reported_msg__descriptor) \
    , 0,NULL, 0,NULL, 0, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, NULL, NULL, NULL, NULL }


struct  _Shadow__VersionMsg
{
  ProtobufCMessage base;
  char *branch;
  char *chripversion;
  char *mmwaveversion;
  char *esp32version;
  char *portalversion;
};
#define SHADOW__VERSION_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__version_msg__descriptor) \
    , (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string }


struct  _Shadow__AvsMsg
{
  ProtobufCMessage base;
  char *clientid;
  char *redirecturl;
  char *authcode;
  char *codeverifier;
};
#define SHADOW__AVS_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__avs_msg__descriptor) \
    , (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string }


struct  _Shadow__CalibrationMsg
{
  ProtobufCMessage base;
  uint32_t starttime;
  uint32_t stoptime;
  protobuf_c_boolean auto_;
  Shadow__CalibrationMsg__State calibrationstate;
  Shadow__EnvironmentMsg *environment;
};
#define SHADOW__CALIBRATION_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__calibration_msg__descriptor) \
    , 0, 0, 0, SHADOW__CALIBRATION_MSG__STATE__None, NULL }


struct  _Shadow__ChripParamMsg
{
  ProtobufCMessage base;
  char *occupancyflagscfg;
  char *occupancyparamcfg;
  char *correctionparam;
  char *occustanceparam;
  char *sceenroiparam;
  char *profilecfg;
};
#define SHADOW__CHRIP_PARAM_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__chrip_param_msg__descriptor) \
    , (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string }


struct  _Shadow__EnvironmentMsg
{
  ProtobufCMessage base;
  double rotation;
  double updis;
  double downdis;
  double leftdis;
  double rightdis;
  double height;
};
#define SHADOW__ENVIRONMENT_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__environment_msg__descriptor) \
    , 0, 0, 0, 0, 0, 0 }


struct  _Shadow__StitchParamMsg
{
  ProtobufCMessage base;
  float height;
  float theta;
  float shiftx;
  float shiftz;
};
#define SHADOW__STITCH_PARAM_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__stitch_param_msg__descriptor) \
    , 0, 0, 0, 0 }


struct  _Shadow__HealthMsg
{
  ProtobufCMessage base;
  uint32_t uptime;
  char *rebootreason;
};
#define SHADOW__HEALTH_MSG__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__health_msg__descriptor) \
    , 0, (char *)protobuf_c_empty_string }


struct  _Shadow__DeviceReq
{
  ProtobufCMessage base;
  char *token;
};
#define SHADOW__DEVICE_REQ__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__device_req__descriptor) \
    , (char *)protobuf_c_empty_string }


struct  _Shadow__SetMsgReq
{
  ProtobufCMessage base;
  char *token;
  char *msg;
};
#define SHADOW__SET_MSG_REQ__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__set_msg_req__descriptor) \
    , (char *)protobuf_c_empty_string, (char *)protobuf_c_empty_string }


struct  _Shadow__Res
{
  ProtobufCMessage base;
  Shadow__Res__Status status;
  char *error;
};
#define SHADOW__RES__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&shadow__res__descriptor) \
    , SHADOW__RES__STATUS__OK, (char *)protobuf_c_empty_string }


/* Shadow__Msg methods */
void   shadow__msg__init
                     (Shadow__Msg         *message);
size_t shadow__msg__get_packed_size
                     (const Shadow__Msg   *message);
size_t shadow__msg__pack
                     (const Shadow__Msg   *message,
                      uint8_t             *out);
size_t shadow__msg__pack_to_buffer
                     (const Shadow__Msg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__Msg *
       shadow__msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__msg__free_unpacked
                     (Shadow__Msg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__StateMsg methods */
void   shadow__state_msg__init
                     (Shadow__StateMsg         *message);
size_t shadow__state_msg__get_packed_size
                     (const Shadow__StateMsg   *message);
size_t shadow__state_msg__pack
                     (const Shadow__StateMsg   *message,
                      uint8_t             *out);
size_t shadow__state_msg__pack_to_buffer
                     (const Shadow__StateMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__StateMsg *
       shadow__state_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__state_msg__free_unpacked
                     (Shadow__StateMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__DesiredMsg methods */
void   shadow__desired_msg__init
                     (Shadow__DesiredMsg         *message);
size_t shadow__desired_msg__get_packed_size
                     (const Shadow__DesiredMsg   *message);
size_t shadow__desired_msg__pack
                     (const Shadow__DesiredMsg   *message,
                      uint8_t             *out);
size_t shadow__desired_msg__pack_to_buffer
                     (const Shadow__DesiredMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__DesiredMsg *
       shadow__desired_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__desired_msg__free_unpacked
                     (Shadow__DesiredMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__VideoSource methods */
void   shadow__video_source__init
                     (Shadow__VideoSource         *message);
size_t shadow__video_source__get_packed_size
                     (const Shadow__VideoSource   *message);
size_t shadow__video_source__pack
                     (const Shadow__VideoSource   *message,
                      uint8_t             *out);
size_t shadow__video_source__pack_to_buffer
                     (const Shadow__VideoSource   *message,
                      ProtobufCBuffer     *buffer);
Shadow__VideoSource *
       shadow__video_source__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__video_source__free_unpacked
                     (Shadow__VideoSource *message,
                      ProtobufCAllocator *allocator);
/* Shadow__ReportedMsg methods */
void   shadow__reported_msg__init
                     (Shadow__ReportedMsg         *message);
size_t shadow__reported_msg__get_packed_size
                     (const Shadow__ReportedMsg   *message);
size_t shadow__reported_msg__pack
                     (const Shadow__ReportedMsg   *message,
                      uint8_t             *out);
size_t shadow__reported_msg__pack_to_buffer
                     (const Shadow__ReportedMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__ReportedMsg *
       shadow__reported_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__reported_msg__free_unpacked
                     (Shadow__ReportedMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__VersionMsg methods */
void   shadow__version_msg__init
                     (Shadow__VersionMsg         *message);
size_t shadow__version_msg__get_packed_size
                     (const Shadow__VersionMsg   *message);
size_t shadow__version_msg__pack
                     (const Shadow__VersionMsg   *message,
                      uint8_t             *out);
size_t shadow__version_msg__pack_to_buffer
                     (const Shadow__VersionMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__VersionMsg *
       shadow__version_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__version_msg__free_unpacked
                     (Shadow__VersionMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__AvsMsg methods */
void   shadow__avs_msg__init
                     (Shadow__AvsMsg         *message);
size_t shadow__avs_msg__get_packed_size
                     (const Shadow__AvsMsg   *message);
size_t shadow__avs_msg__pack
                     (const Shadow__AvsMsg   *message,
                      uint8_t             *out);
size_t shadow__avs_msg__pack_to_buffer
                     (const Shadow__AvsMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__AvsMsg *
       shadow__avs_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__avs_msg__free_unpacked
                     (Shadow__AvsMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__CalibrationMsg methods */
void   shadow__calibration_msg__init
                     (Shadow__CalibrationMsg         *message);
size_t shadow__calibration_msg__get_packed_size
                     (const Shadow__CalibrationMsg   *message);
size_t shadow__calibration_msg__pack
                     (const Shadow__CalibrationMsg   *message,
                      uint8_t             *out);
size_t shadow__calibration_msg__pack_to_buffer
                     (const Shadow__CalibrationMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__CalibrationMsg *
       shadow__calibration_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__calibration_msg__free_unpacked
                     (Shadow__CalibrationMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__ChripParamMsg methods */
void   shadow__chrip_param_msg__init
                     (Shadow__ChripParamMsg         *message);
size_t shadow__chrip_param_msg__get_packed_size
                     (const Shadow__ChripParamMsg   *message);
size_t shadow__chrip_param_msg__pack
                     (const Shadow__ChripParamMsg   *message,
                      uint8_t             *out);
size_t shadow__chrip_param_msg__pack_to_buffer
                     (const Shadow__ChripParamMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__ChripParamMsg *
       shadow__chrip_param_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__chrip_param_msg__free_unpacked
                     (Shadow__ChripParamMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__EnvironmentMsg methods */
void   shadow__environment_msg__init
                     (Shadow__EnvironmentMsg         *message);
size_t shadow__environment_msg__get_packed_size
                     (const Shadow__EnvironmentMsg   *message);
size_t shadow__environment_msg__pack
                     (const Shadow__EnvironmentMsg   *message,
                      uint8_t             *out);
size_t shadow__environment_msg__pack_to_buffer
                     (const Shadow__EnvironmentMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__EnvironmentMsg *
       shadow__environment_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__environment_msg__free_unpacked
                     (Shadow__EnvironmentMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__StitchParamMsg methods */
void   shadow__stitch_param_msg__init
                     (Shadow__StitchParamMsg         *message);
size_t shadow__stitch_param_msg__get_packed_size
                     (const Shadow__StitchParamMsg   *message);
size_t shadow__stitch_param_msg__pack
                     (const Shadow__StitchParamMsg   *message,
                      uint8_t             *out);
size_t shadow__stitch_param_msg__pack_to_buffer
                     (const Shadow__StitchParamMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__StitchParamMsg *
       shadow__stitch_param_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__stitch_param_msg__free_unpacked
                     (Shadow__StitchParamMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__HealthMsg methods */
void   shadow__health_msg__init
                     (Shadow__HealthMsg         *message);
size_t shadow__health_msg__get_packed_size
                     (const Shadow__HealthMsg   *message);
size_t shadow__health_msg__pack
                     (const Shadow__HealthMsg   *message,
                      uint8_t             *out);
size_t shadow__health_msg__pack_to_buffer
                     (const Shadow__HealthMsg   *message,
                      ProtobufCBuffer     *buffer);
Shadow__HealthMsg *
       shadow__health_msg__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__health_msg__free_unpacked
                     (Shadow__HealthMsg *message,
                      ProtobufCAllocator *allocator);
/* Shadow__DeviceReq methods */
void   shadow__device_req__init
                     (Shadow__DeviceReq         *message);
size_t shadow__device_req__get_packed_size
                     (const Shadow__DeviceReq   *message);
size_t shadow__device_req__pack
                     (const Shadow__DeviceReq   *message,
                      uint8_t             *out);
size_t shadow__device_req__pack_to_buffer
                     (const Shadow__DeviceReq   *message,
                      ProtobufCBuffer     *buffer);
Shadow__DeviceReq *
       shadow__device_req__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__device_req__free_unpacked
                     (Shadow__DeviceReq *message,
                      ProtobufCAllocator *allocator);
/* Shadow__SetMsgReq methods */
void   shadow__set_msg_req__init
                     (Shadow__SetMsgReq         *message);
size_t shadow__set_msg_req__get_packed_size
                     (const Shadow__SetMsgReq   *message);
size_t shadow__set_msg_req__pack
                     (const Shadow__SetMsgReq   *message,
                      uint8_t             *out);
size_t shadow__set_msg_req__pack_to_buffer
                     (const Shadow__SetMsgReq   *message,
                      ProtobufCBuffer     *buffer);
Shadow__SetMsgReq *
       shadow__set_msg_req__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__set_msg_req__free_unpacked
                     (Shadow__SetMsgReq *message,
                      ProtobufCAllocator *allocator);
/* Shadow__Res methods */
void   shadow__res__init
                     (Shadow__Res         *message);
size_t shadow__res__get_packed_size
                     (const Shadow__Res   *message);
size_t shadow__res__pack
                     (const Shadow__Res   *message,
                      uint8_t             *out);
size_t shadow__res__pack_to_buffer
                     (const Shadow__Res   *message,
                      ProtobufCBuffer     *buffer);
Shadow__Res *
       shadow__res__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   shadow__res__free_unpacked
                     (Shadow__Res *message,
                      ProtobufCAllocator *allocator);
/* --- per-message closures --- */

typedef void (*Shadow__Msg_Closure)
                 (const Shadow__Msg *message,
                  void *closure_data);
typedef void (*Shadow__StateMsg_Closure)
                 (const Shadow__StateMsg *message,
                  void *closure_data);
typedef void (*Shadow__DesiredMsg_Closure)
                 (const Shadow__DesiredMsg *message,
                  void *closure_data);
typedef void (*Shadow__VideoSource_Closure)
                 (const Shadow__VideoSource *message,
                  void *closure_data);
typedef void (*Shadow__ReportedMsg_Closure)
                 (const Shadow__ReportedMsg *message,
                  void *closure_data);
typedef void (*Shadow__VersionMsg_Closure)
                 (const Shadow__VersionMsg *message,
                  void *closure_data);
typedef void (*Shadow__AvsMsg_Closure)
                 (const Shadow__AvsMsg *message,
                  void *closure_data);
typedef void (*Shadow__CalibrationMsg_Closure)
                 (const Shadow__CalibrationMsg *message,
                  void *closure_data);
typedef void (*Shadow__ChripParamMsg_Closure)
                 (const Shadow__ChripParamMsg *message,
                  void *closure_data);
typedef void (*Shadow__EnvironmentMsg_Closure)
                 (const Shadow__EnvironmentMsg *message,
                  void *closure_data);
typedef void (*Shadow__StitchParamMsg_Closure)
                 (const Shadow__StitchParamMsg *message,
                  void *closure_data);
typedef void (*Shadow__HealthMsg_Closure)
                 (const Shadow__HealthMsg *message,
                  void *closure_data);
typedef void (*Shadow__DeviceReq_Closure)
                 (const Shadow__DeviceReq *message,
                  void *closure_data);
typedef void (*Shadow__SetMsgReq_Closure)
                 (const Shadow__SetMsgReq *message,
                  void *closure_data);
typedef void (*Shadow__Res_Closure)
                 (const Shadow__Res *message,
                  void *closure_data);

/* --- services --- */

typedef struct _Shadow__Service_Service Shadow__Service_Service;
struct _Shadow__Service_Service
{
  ProtobufCService base;
  void (*get_msg)(Shadow__Service_Service *service,
                  const Shadow__DeviceReq *input,
                  Shadow__Msg_Closure closure,
                  void *closure_data);
  void (*set_msg)(Shadow__Service_Service *service,
                  const Shadow__SetMsgReq *input,
                  Shadow__Res_Closure closure,
                  void *closure_data);
};
typedef void (*Shadow__Service_ServiceDestroy)(Shadow__Service_Service *);
void shadow__service__init (Shadow__Service_Service *service,
                            Shadow__Service_ServiceDestroy destroy);
#define SHADOW__SERVICE__BASE_INIT \
    { &shadow__service__descriptor, protobuf_c_service_invoke_internal, NULL }
#define SHADOW__SERVICE__INIT(function_prefix__) \
    { SHADOW__SERVICE__BASE_INIT,\
      function_prefix__ ## get_msg,\
      function_prefix__ ## set_msg  }
void shadow__service__get_msg(ProtobufCService *service,
                              const Shadow__DeviceReq *input,
                              Shadow__Msg_Closure closure,
                              void *closure_data);
void shadow__service__set_msg(ProtobufCService *service,
                              const Shadow__SetMsgReq *input,
                              Shadow__Res_Closure closure,
                              void *closure_data);

/* --- descriptors --- */

extern const ProtobufCMessageDescriptor shadow__msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__state_msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__desired_msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__video_source__descriptor;
extern const ProtobufCMessageDescriptor shadow__reported_msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__version_msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__avs_msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__calibration_msg__descriptor;
extern const ProtobufCEnumDescriptor    shadow__calibration_msg__state__descriptor;
extern const ProtobufCMessageDescriptor shadow__chrip_param_msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__environment_msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__stitch_param_msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__health_msg__descriptor;
extern const ProtobufCMessageDescriptor shadow__device_req__descriptor;
extern const ProtobufCMessageDescriptor shadow__set_msg_req__descriptor;
extern const ProtobufCMessageDescriptor shadow__res__descriptor;
extern const ProtobufCEnumDescriptor    shadow__res__status__descriptor;
extern const ProtobufCServiceDescriptor shadow__service__descriptor;

PROTOBUF_C__END_DECLS


#endif  /* PROTOBUF_C_shadow_2eproto__INCLUDED */
