/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const Shadow = $root.Shadow = (() => {

    /**
     * Namespace Shadow.
     * @exports Shadow
     * @namespace
     */
    const Shadow = {};

    Shadow.Msg = (function() {

        /**
         * Properties of a Msg.
         * @memberof Shadow
         * @interface IMsg
         * @property {Shadow.IstateMsg|null} [state] Msg state
         */

        /**
         * Constructs a new Msg.
         * @memberof Shadow
         * @classdesc Represents a Msg.
         * @implements IMsg
         * @constructor
         * @param {Shadow.IMsg=} [properties] Properties to set
         */
        function Msg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Msg state.
         * @member {Shadow.IstateMsg|null|undefined} state
         * @memberof Shadow.Msg
         * @instance
         */
        Msg.prototype.state = null;

        /**
         * Creates a new Msg instance using the specified properties.
         * @function create
         * @memberof Shadow.Msg
         * @static
         * @param {Shadow.IMsg=} [properties] Properties to set
         * @returns {Shadow.Msg} Msg instance
         */
        Msg.create = function create(properties) {
            return new Msg(properties);
        };

        /**
         * Encodes the specified Msg message. Does not implicitly {@link Shadow.Msg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.Msg
         * @static
         * @param {Shadow.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.state != null && Object.hasOwnProperty.call(message, "state"))
                $root.Shadow.stateMsg.encode(message.state, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Msg message, length delimited. Does not implicitly {@link Shadow.Msg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.Msg
         * @static
         * @param {Shadow.IMsg} message Msg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Msg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Msg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.Msg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.state = $root.Shadow.stateMsg.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Msg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.Msg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.Msg} Msg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Msg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Msg message.
         * @function verify
         * @memberof Shadow.Msg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Msg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.state != null && message.hasOwnProperty("state")) {
                let error = $root.Shadow.stateMsg.verify(message.state);
                if (error)
                    return "state." + error;
            }
            return null;
        };

        /**
         * Creates a Msg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.Msg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.Msg} Msg
         */
        Msg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.Msg)
                return object;
            let message = new $root.Shadow.Msg();
            if (object.state != null) {
                if (typeof object.state !== "object")
                    throw TypeError(".Shadow.Msg.state: object expected");
                message.state = $root.Shadow.stateMsg.fromObject(object.state);
            }
            return message;
        };

        /**
         * Creates a plain object from a Msg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.Msg
         * @static
         * @param {Shadow.Msg} message Msg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Msg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults)
                object.state = null;
            if (message.state != null && message.hasOwnProperty("state"))
                object.state = $root.Shadow.stateMsg.toObject(message.state, options);
            return object;
        };

        /**
         * Converts this Msg to JSON.
         * @function toJSON
         * @memberof Shadow.Msg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Msg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Msg;
    })();

    Shadow.stateMsg = (function() {

        /**
         * Properties of a stateMsg.
         * @memberof Shadow
         * @interface IstateMsg
         * @property {string|null} [id] stateMsg id
         * @property {Shadow.IdesiredMsg|null} [desired] stateMsg desired
         * @property {Shadow.IreportedMsg|null} [reported] stateMsg reported
         */

        /**
         * Constructs a new stateMsg.
         * @memberof Shadow
         * @classdesc Represents a stateMsg.
         * @implements IstateMsg
         * @constructor
         * @param {Shadow.IstateMsg=} [properties] Properties to set
         */
        function stateMsg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * stateMsg id.
         * @member {string} id
         * @memberof Shadow.stateMsg
         * @instance
         */
        stateMsg.prototype.id = "";

        /**
         * stateMsg desired.
         * @member {Shadow.IdesiredMsg|null|undefined} desired
         * @memberof Shadow.stateMsg
         * @instance
         */
        stateMsg.prototype.desired = null;

        /**
         * stateMsg reported.
         * @member {Shadow.IreportedMsg|null|undefined} reported
         * @memberof Shadow.stateMsg
         * @instance
         */
        stateMsg.prototype.reported = null;

        /**
         * Creates a new stateMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.stateMsg
         * @static
         * @param {Shadow.IstateMsg=} [properties] Properties to set
         * @returns {Shadow.stateMsg} stateMsg instance
         */
        stateMsg.create = function create(properties) {
            return new stateMsg(properties);
        };

        /**
         * Encodes the specified stateMsg message. Does not implicitly {@link Shadow.stateMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.stateMsg
         * @static
         * @param {Shadow.IstateMsg} message stateMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        stateMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.id != null && Object.hasOwnProperty.call(message, "id"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.id);
            if (message.desired != null && Object.hasOwnProperty.call(message, "desired"))
                $root.Shadow.desiredMsg.encode(message.desired, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            if (message.reported != null && Object.hasOwnProperty.call(message, "reported"))
                $root.Shadow.reportedMsg.encode(message.reported, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified stateMsg message, length delimited. Does not implicitly {@link Shadow.stateMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.stateMsg
         * @static
         * @param {Shadow.IstateMsg} message stateMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        stateMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a stateMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.stateMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.stateMsg} stateMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        stateMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.stateMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.id = reader.string();
                    break;
                case 2:
                    message.desired = $root.Shadow.desiredMsg.decode(reader, reader.uint32());
                    break;
                case 3:
                    message.reported = $root.Shadow.reportedMsg.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a stateMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.stateMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.stateMsg} stateMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        stateMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a stateMsg message.
         * @function verify
         * @memberof Shadow.stateMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        stateMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.id != null && message.hasOwnProperty("id"))
                if (!$util.isString(message.id))
                    return "id: string expected";
            if (message.desired != null && message.hasOwnProperty("desired")) {
                let error = $root.Shadow.desiredMsg.verify(message.desired);
                if (error)
                    return "desired." + error;
            }
            if (message.reported != null && message.hasOwnProperty("reported")) {
                let error = $root.Shadow.reportedMsg.verify(message.reported);
                if (error)
                    return "reported." + error;
            }
            return null;
        };

        /**
         * Creates a stateMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.stateMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.stateMsg} stateMsg
         */
        stateMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.stateMsg)
                return object;
            let message = new $root.Shadow.stateMsg();
            if (object.id != null)
                message.id = String(object.id);
            if (object.desired != null) {
                if (typeof object.desired !== "object")
                    throw TypeError(".Shadow.stateMsg.desired: object expected");
                message.desired = $root.Shadow.desiredMsg.fromObject(object.desired);
            }
            if (object.reported != null) {
                if (typeof object.reported !== "object")
                    throw TypeError(".Shadow.stateMsg.reported: object expected");
                message.reported = $root.Shadow.reportedMsg.fromObject(object.reported);
            }
            return message;
        };

        /**
         * Creates a plain object from a stateMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.stateMsg
         * @static
         * @param {Shadow.stateMsg} message stateMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        stateMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.id = "";
                object.desired = null;
                object.reported = null;
            }
            if (message.id != null && message.hasOwnProperty("id"))
                object.id = message.id;
            if (message.desired != null && message.hasOwnProperty("desired"))
                object.desired = $root.Shadow.desiredMsg.toObject(message.desired, options);
            if (message.reported != null && message.hasOwnProperty("reported"))
                object.reported = $root.Shadow.reportedMsg.toObject(message.reported, options);
            return object;
        };

        /**
         * Converts this stateMsg to JSON.
         * @function toJSON
         * @memberof Shadow.stateMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        stateMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return stateMsg;
    })();

    Shadow.desiredMsg = (function() {

        /**
         * Properties of a desiredMsg.
         * @memberof Shadow
         * @interface IdesiredMsg
         * @property {Array.<Shadow.IvideoSource>|null} [videoSources] desiredMsg videoSources
         * @property {Shadow.IversionMsg|null} [versionInfo] desiredMsg versionInfo
         * @property {Shadow.IavsMsg|null} [avs] desiredMsg avs
         * @property {boolean|null} [autoUpgrade] desiredMsg autoUpgrade
         * @property {Shadow.IcalibrationMsg|null} [calibration] desiredMsg calibration
         * @property {Shadow.IstitchParamMsg|null} [stitchParam] desiredMsg stitchParam
         * @property {Shadow.IchripParamMsg|null} [chripParam] desiredMsg chripParam
         */

        /**
         * Constructs a new desiredMsg.
         * @memberof Shadow
         * @classdesc Represents a desiredMsg.
         * @implements IdesiredMsg
         * @constructor
         * @param {Shadow.IdesiredMsg=} [properties] Properties to set
         */
        function desiredMsg(properties) {
            this.videoSources = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * desiredMsg videoSources.
         * @member {Array.<Shadow.IvideoSource>} videoSources
         * @memberof Shadow.desiredMsg
         * @instance
         */
        desiredMsg.prototype.videoSources = $util.emptyArray;

        /**
         * desiredMsg versionInfo.
         * @member {Shadow.IversionMsg|null|undefined} versionInfo
         * @memberof Shadow.desiredMsg
         * @instance
         */
        desiredMsg.prototype.versionInfo = null;

        /**
         * desiredMsg avs.
         * @member {Shadow.IavsMsg|null|undefined} avs
         * @memberof Shadow.desiredMsg
         * @instance
         */
        desiredMsg.prototype.avs = null;

        /**
         * desiredMsg autoUpgrade.
         * @member {boolean} autoUpgrade
         * @memberof Shadow.desiredMsg
         * @instance
         */
        desiredMsg.prototype.autoUpgrade = false;

        /**
         * desiredMsg calibration.
         * @member {Shadow.IcalibrationMsg|null|undefined} calibration
         * @memberof Shadow.desiredMsg
         * @instance
         */
        desiredMsg.prototype.calibration = null;

        /**
         * desiredMsg stitchParam.
         * @member {Shadow.IstitchParamMsg|null|undefined} stitchParam
         * @memberof Shadow.desiredMsg
         * @instance
         */
        desiredMsg.prototype.stitchParam = null;

        /**
         * desiredMsg chripParam.
         * @member {Shadow.IchripParamMsg|null|undefined} chripParam
         * @memberof Shadow.desiredMsg
         * @instance
         */
        desiredMsg.prototype.chripParam = null;

        /**
         * Creates a new desiredMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.desiredMsg
         * @static
         * @param {Shadow.IdesiredMsg=} [properties] Properties to set
         * @returns {Shadow.desiredMsg} desiredMsg instance
         */
        desiredMsg.create = function create(properties) {
            return new desiredMsg(properties);
        };

        /**
         * Encodes the specified desiredMsg message. Does not implicitly {@link Shadow.desiredMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.desiredMsg
         * @static
         * @param {Shadow.IdesiredMsg} message desiredMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        desiredMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.videoSources != null && message.videoSources.length)
                for (let i = 0; i < message.videoSources.length; ++i)
                    $root.Shadow.videoSource.encode(message.videoSources[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            if (message.versionInfo != null && Object.hasOwnProperty.call(message, "versionInfo"))
                $root.Shadow.versionMsg.encode(message.versionInfo, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            if (message.avs != null && Object.hasOwnProperty.call(message, "avs"))
                $root.Shadow.avsMsg.encode(message.avs, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.autoUpgrade != null && Object.hasOwnProperty.call(message, "autoUpgrade"))
                writer.uint32(/* id 4, wireType 0 =*/32).bool(message.autoUpgrade);
            if (message.calibration != null && Object.hasOwnProperty.call(message, "calibration"))
                $root.Shadow.calibrationMsg.encode(message.calibration, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            if (message.stitchParam != null && Object.hasOwnProperty.call(message, "stitchParam"))
                $root.Shadow.stitchParamMsg.encode(message.stitchParam, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
            if (message.chripParam != null && Object.hasOwnProperty.call(message, "chripParam"))
                $root.Shadow.chripParamMsg.encode(message.chripParam, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified desiredMsg message, length delimited. Does not implicitly {@link Shadow.desiredMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.desiredMsg
         * @static
         * @param {Shadow.IdesiredMsg} message desiredMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        desiredMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a desiredMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.desiredMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.desiredMsg} desiredMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        desiredMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.desiredMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.videoSources && message.videoSources.length))
                        message.videoSources = [];
                    message.videoSources.push($root.Shadow.videoSource.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.versionInfo = $root.Shadow.versionMsg.decode(reader, reader.uint32());
                    break;
                case 3:
                    message.avs = $root.Shadow.avsMsg.decode(reader, reader.uint32());
                    break;
                case 4:
                    message.autoUpgrade = reader.bool();
                    break;
                case 5:
                    message.calibration = $root.Shadow.calibrationMsg.decode(reader, reader.uint32());
                    break;
                case 6:
                    message.stitchParam = $root.Shadow.stitchParamMsg.decode(reader, reader.uint32());
                    break;
                case 7:
                    message.chripParam = $root.Shadow.chripParamMsg.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a desiredMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.desiredMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.desiredMsg} desiredMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        desiredMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a desiredMsg message.
         * @function verify
         * @memberof Shadow.desiredMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        desiredMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.videoSources != null && message.hasOwnProperty("videoSources")) {
                if (!Array.isArray(message.videoSources))
                    return "videoSources: array expected";
                for (let i = 0; i < message.videoSources.length; ++i) {
                    let error = $root.Shadow.videoSource.verify(message.videoSources[i]);
                    if (error)
                        return "videoSources." + error;
                }
            }
            if (message.versionInfo != null && message.hasOwnProperty("versionInfo")) {
                let error = $root.Shadow.versionMsg.verify(message.versionInfo);
                if (error)
                    return "versionInfo." + error;
            }
            if (message.avs != null && message.hasOwnProperty("avs")) {
                let error = $root.Shadow.avsMsg.verify(message.avs);
                if (error)
                    return "avs." + error;
            }
            if (message.autoUpgrade != null && message.hasOwnProperty("autoUpgrade"))
                if (typeof message.autoUpgrade !== "boolean")
                    return "autoUpgrade: boolean expected";
            if (message.calibration != null && message.hasOwnProperty("calibration")) {
                let error = $root.Shadow.calibrationMsg.verify(message.calibration);
                if (error)
                    return "calibration." + error;
            }
            if (message.stitchParam != null && message.hasOwnProperty("stitchParam")) {
                let error = $root.Shadow.stitchParamMsg.verify(message.stitchParam);
                if (error)
                    return "stitchParam." + error;
            }
            if (message.chripParam != null && message.hasOwnProperty("chripParam")) {
                let error = $root.Shadow.chripParamMsg.verify(message.chripParam);
                if (error)
                    return "chripParam." + error;
            }
            return null;
        };

        /**
         * Creates a desiredMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.desiredMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.desiredMsg} desiredMsg
         */
        desiredMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.desiredMsg)
                return object;
            let message = new $root.Shadow.desiredMsg();
            if (object.videoSources) {
                if (!Array.isArray(object.videoSources))
                    throw TypeError(".Shadow.desiredMsg.videoSources: array expected");
                message.videoSources = [];
                for (let i = 0; i < object.videoSources.length; ++i) {
                    if (typeof object.videoSources[i] !== "object")
                        throw TypeError(".Shadow.desiredMsg.videoSources: object expected");
                    message.videoSources[i] = $root.Shadow.videoSource.fromObject(object.videoSources[i]);
                }
            }
            if (object.versionInfo != null) {
                if (typeof object.versionInfo !== "object")
                    throw TypeError(".Shadow.desiredMsg.versionInfo: object expected");
                message.versionInfo = $root.Shadow.versionMsg.fromObject(object.versionInfo);
            }
            if (object.avs != null) {
                if (typeof object.avs !== "object")
                    throw TypeError(".Shadow.desiredMsg.avs: object expected");
                message.avs = $root.Shadow.avsMsg.fromObject(object.avs);
            }
            if (object.autoUpgrade != null)
                message.autoUpgrade = Boolean(object.autoUpgrade);
            if (object.calibration != null) {
                if (typeof object.calibration !== "object")
                    throw TypeError(".Shadow.desiredMsg.calibration: object expected");
                message.calibration = $root.Shadow.calibrationMsg.fromObject(object.calibration);
            }
            if (object.stitchParam != null) {
                if (typeof object.stitchParam !== "object")
                    throw TypeError(".Shadow.desiredMsg.stitchParam: object expected");
                message.stitchParam = $root.Shadow.stitchParamMsg.fromObject(object.stitchParam);
            }
            if (object.chripParam != null) {
                if (typeof object.chripParam !== "object")
                    throw TypeError(".Shadow.desiredMsg.chripParam: object expected");
                message.chripParam = $root.Shadow.chripParamMsg.fromObject(object.chripParam);
            }
            return message;
        };

        /**
         * Creates a plain object from a desiredMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.desiredMsg
         * @static
         * @param {Shadow.desiredMsg} message desiredMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        desiredMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults)
                object.videoSources = [];
            if (options.defaults) {
                object.versionInfo = null;
                object.avs = null;
                object.autoUpgrade = false;
                object.calibration = null;
                object.stitchParam = null;
                object.chripParam = null;
            }
            if (message.videoSources && message.videoSources.length) {
                object.videoSources = [];
                for (let j = 0; j < message.videoSources.length; ++j)
                    object.videoSources[j] = $root.Shadow.videoSource.toObject(message.videoSources[j], options);
            }
            if (message.versionInfo != null && message.hasOwnProperty("versionInfo"))
                object.versionInfo = $root.Shadow.versionMsg.toObject(message.versionInfo, options);
            if (message.avs != null && message.hasOwnProperty("avs"))
                object.avs = $root.Shadow.avsMsg.toObject(message.avs, options);
            if (message.autoUpgrade != null && message.hasOwnProperty("autoUpgrade"))
                object.autoUpgrade = message.autoUpgrade;
            if (message.calibration != null && message.hasOwnProperty("calibration"))
                object.calibration = $root.Shadow.calibrationMsg.toObject(message.calibration, options);
            if (message.stitchParam != null && message.hasOwnProperty("stitchParam"))
                object.stitchParam = $root.Shadow.stitchParamMsg.toObject(message.stitchParam, options);
            if (message.chripParam != null && message.hasOwnProperty("chripParam"))
                object.chripParam = $root.Shadow.chripParamMsg.toObject(message.chripParam, options);
            return object;
        };

        /**
         * Converts this desiredMsg to JSON.
         * @function toJSON
         * @memberof Shadow.desiredMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        desiredMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return desiredMsg;
    })();

    Shadow.videoSource = (function() {

        /**
         * Properties of a videoSource.
         * @memberof Shadow
         * @interface IvideoSource
         * @property {string|null} [id] videoSource id
         * @property {string|null} [password] videoSource password
         */

        /**
         * Constructs a new videoSource.
         * @memberof Shadow
         * @classdesc Represents a videoSource.
         * @implements IvideoSource
         * @constructor
         * @param {Shadow.IvideoSource=} [properties] Properties to set
         */
        function videoSource(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * videoSource id.
         * @member {string} id
         * @memberof Shadow.videoSource
         * @instance
         */
        videoSource.prototype.id = "";

        /**
         * videoSource password.
         * @member {string} password
         * @memberof Shadow.videoSource
         * @instance
         */
        videoSource.prototype.password = "";

        /**
         * Creates a new videoSource instance using the specified properties.
         * @function create
         * @memberof Shadow.videoSource
         * @static
         * @param {Shadow.IvideoSource=} [properties] Properties to set
         * @returns {Shadow.videoSource} videoSource instance
         */
        videoSource.create = function create(properties) {
            return new videoSource(properties);
        };

        /**
         * Encodes the specified videoSource message. Does not implicitly {@link Shadow.videoSource.verify|verify} messages.
         * @function encode
         * @memberof Shadow.videoSource
         * @static
         * @param {Shadow.IvideoSource} message videoSource message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        videoSource.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.id != null && Object.hasOwnProperty.call(message, "id"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.id);
            if (message.password != null && Object.hasOwnProperty.call(message, "password"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.password);
            return writer;
        };

        /**
         * Encodes the specified videoSource message, length delimited. Does not implicitly {@link Shadow.videoSource.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.videoSource
         * @static
         * @param {Shadow.IvideoSource} message videoSource message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        videoSource.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a videoSource message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.videoSource
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.videoSource} videoSource
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        videoSource.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.videoSource();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.id = reader.string();
                    break;
                case 2:
                    message.password = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a videoSource message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.videoSource
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.videoSource} videoSource
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        videoSource.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a videoSource message.
         * @function verify
         * @memberof Shadow.videoSource
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        videoSource.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.id != null && message.hasOwnProperty("id"))
                if (!$util.isString(message.id))
                    return "id: string expected";
            if (message.password != null && message.hasOwnProperty("password"))
                if (!$util.isString(message.password))
                    return "password: string expected";
            return null;
        };

        /**
         * Creates a videoSource message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.videoSource
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.videoSource} videoSource
         */
        videoSource.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.videoSource)
                return object;
            let message = new $root.Shadow.videoSource();
            if (object.id != null)
                message.id = String(object.id);
            if (object.password != null)
                message.password = String(object.password);
            return message;
        };

        /**
         * Creates a plain object from a videoSource message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.videoSource
         * @static
         * @param {Shadow.videoSource} message videoSource
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        videoSource.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.id = "";
                object.password = "";
            }
            if (message.id != null && message.hasOwnProperty("id"))
                object.id = message.id;
            if (message.password != null && message.hasOwnProperty("password"))
                object.password = message.password;
            return object;
        };

        /**
         * Converts this videoSource to JSON.
         * @function toJSON
         * @memberof Shadow.videoSource
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        videoSource.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return videoSource;
    })();

    Shadow.reportedMsg = (function() {

        /**
         * Properties of a reportedMsg.
         * @memberof Shadow
         * @interface IreportedMsg
         * @property {Array.<Shadow.IvideoSource>|null} [videoSources] reportedMsg videoSources
         * @property {Array.<string>|null} [discoveredVideoSources] reportedMsg discoveredVideoSources
         * @property {boolean|null} [connected] reportedMsg connected
         * @property {string|null} [status] reportedMsg status
         * @property {string|null} [ip] reportedMsg ip
         * @property {Shadow.IversionMsg|null} [versionInfo] reportedMsg versionInfo
         * @property {Shadow.IversionMsg|null} [lastVersionInfo] reportedMsg lastVersionInfo
         * @property {Shadow.IhealthMsg|null} [health] reportedMsg health
         * @property {Shadow.IenvironmentMsg|null} [environment] reportedMsg environment
         */

        /**
         * Constructs a new reportedMsg.
         * @memberof Shadow
         * @classdesc Represents a reportedMsg.
         * @implements IreportedMsg
         * @constructor
         * @param {Shadow.IreportedMsg=} [properties] Properties to set
         */
        function reportedMsg(properties) {
            this.videoSources = [];
            this.discoveredVideoSources = [];
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * reportedMsg videoSources.
         * @member {Array.<Shadow.IvideoSource>} videoSources
         * @memberof Shadow.reportedMsg
         * @instance
         */
        reportedMsg.prototype.videoSources = $util.emptyArray;

        /**
         * reportedMsg discoveredVideoSources.
         * @member {Array.<string>} discoveredVideoSources
         * @memberof Shadow.reportedMsg
         * @instance
         */
        reportedMsg.prototype.discoveredVideoSources = $util.emptyArray;

        /**
         * reportedMsg connected.
         * @member {boolean} connected
         * @memberof Shadow.reportedMsg
         * @instance
         */
        reportedMsg.prototype.connected = false;

        /**
         * reportedMsg status.
         * @member {string} status
         * @memberof Shadow.reportedMsg
         * @instance
         */
        reportedMsg.prototype.status = "";

        /**
         * reportedMsg ip.
         * @member {string} ip
         * @memberof Shadow.reportedMsg
         * @instance
         */
        reportedMsg.prototype.ip = "";

        /**
         * reportedMsg versionInfo.
         * @member {Shadow.IversionMsg|null|undefined} versionInfo
         * @memberof Shadow.reportedMsg
         * @instance
         */
        reportedMsg.prototype.versionInfo = null;

        /**
         * reportedMsg lastVersionInfo.
         * @member {Shadow.IversionMsg|null|undefined} lastVersionInfo
         * @memberof Shadow.reportedMsg
         * @instance
         */
        reportedMsg.prototype.lastVersionInfo = null;

        /**
         * reportedMsg health.
         * @member {Shadow.IhealthMsg|null|undefined} health
         * @memberof Shadow.reportedMsg
         * @instance
         */
        reportedMsg.prototype.health = null;

        /**
         * reportedMsg environment.
         * @member {Shadow.IenvironmentMsg|null|undefined} environment
         * @memberof Shadow.reportedMsg
         * @instance
         */
        reportedMsg.prototype.environment = null;

        /**
         * Creates a new reportedMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.reportedMsg
         * @static
         * @param {Shadow.IreportedMsg=} [properties] Properties to set
         * @returns {Shadow.reportedMsg} reportedMsg instance
         */
        reportedMsg.create = function create(properties) {
            return new reportedMsg(properties);
        };

        /**
         * Encodes the specified reportedMsg message. Does not implicitly {@link Shadow.reportedMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.reportedMsg
         * @static
         * @param {Shadow.IreportedMsg} message reportedMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        reportedMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.videoSources != null && message.videoSources.length)
                for (let i = 0; i < message.videoSources.length; ++i)
                    $root.Shadow.videoSource.encode(message.videoSources[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            if (message.discoveredVideoSources != null && message.discoveredVideoSources.length)
                for (let i = 0; i < message.discoveredVideoSources.length; ++i)
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.discoveredVideoSources[i]);
            if (message.connected != null && Object.hasOwnProperty.call(message, "connected"))
                writer.uint32(/* id 3, wireType 0 =*/24).bool(message.connected);
            if (message.status != null && Object.hasOwnProperty.call(message, "status"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.status);
            if (message.ip != null && Object.hasOwnProperty.call(message, "ip"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.ip);
            if (message.versionInfo != null && Object.hasOwnProperty.call(message, "versionInfo"))
                $root.Shadow.versionMsg.encode(message.versionInfo, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
            if (message.lastVersionInfo != null && Object.hasOwnProperty.call(message, "lastVersionInfo"))
                $root.Shadow.versionMsg.encode(message.lastVersionInfo, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
            if (message.health != null && Object.hasOwnProperty.call(message, "health"))
                $root.Shadow.healthMsg.encode(message.health, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
            if (message.environment != null && Object.hasOwnProperty.call(message, "environment"))
                $root.Shadow.environmentMsg.encode(message.environment, writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified reportedMsg message, length delimited. Does not implicitly {@link Shadow.reportedMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.reportedMsg
         * @static
         * @param {Shadow.IreportedMsg} message reportedMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        reportedMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a reportedMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.reportedMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.reportedMsg} reportedMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        reportedMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.reportedMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.videoSources && message.videoSources.length))
                        message.videoSources = [];
                    message.videoSources.push($root.Shadow.videoSource.decode(reader, reader.uint32()));
                    break;
                case 2:
                    if (!(message.discoveredVideoSources && message.discoveredVideoSources.length))
                        message.discoveredVideoSources = [];
                    message.discoveredVideoSources.push(reader.string());
                    break;
                case 3:
                    message.connected = reader.bool();
                    break;
                case 4:
                    message.status = reader.string();
                    break;
                case 5:
                    message.ip = reader.string();
                    break;
                case 6:
                    message.versionInfo = $root.Shadow.versionMsg.decode(reader, reader.uint32());
                    break;
                case 7:
                    message.lastVersionInfo = $root.Shadow.versionMsg.decode(reader, reader.uint32());
                    break;
                case 8:
                    message.health = $root.Shadow.healthMsg.decode(reader, reader.uint32());
                    break;
                case 9:
                    message.environment = $root.Shadow.environmentMsg.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a reportedMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.reportedMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.reportedMsg} reportedMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        reportedMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a reportedMsg message.
         * @function verify
         * @memberof Shadow.reportedMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        reportedMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.videoSources != null && message.hasOwnProperty("videoSources")) {
                if (!Array.isArray(message.videoSources))
                    return "videoSources: array expected";
                for (let i = 0; i < message.videoSources.length; ++i) {
                    let error = $root.Shadow.videoSource.verify(message.videoSources[i]);
                    if (error)
                        return "videoSources." + error;
                }
            }
            if (message.discoveredVideoSources != null && message.hasOwnProperty("discoveredVideoSources")) {
                if (!Array.isArray(message.discoveredVideoSources))
                    return "discoveredVideoSources: array expected";
                for (let i = 0; i < message.discoveredVideoSources.length; ++i)
                    if (!$util.isString(message.discoveredVideoSources[i]))
                        return "discoveredVideoSources: string[] expected";
            }
            if (message.connected != null && message.hasOwnProperty("connected"))
                if (typeof message.connected !== "boolean")
                    return "connected: boolean expected";
            if (message.status != null && message.hasOwnProperty("status"))
                if (!$util.isString(message.status))
                    return "status: string expected";
            if (message.ip != null && message.hasOwnProperty("ip"))
                if (!$util.isString(message.ip))
                    return "ip: string expected";
            if (message.versionInfo != null && message.hasOwnProperty("versionInfo")) {
                let error = $root.Shadow.versionMsg.verify(message.versionInfo);
                if (error)
                    return "versionInfo." + error;
            }
            if (message.lastVersionInfo != null && message.hasOwnProperty("lastVersionInfo")) {
                let error = $root.Shadow.versionMsg.verify(message.lastVersionInfo);
                if (error)
                    return "lastVersionInfo." + error;
            }
            if (message.health != null && message.hasOwnProperty("health")) {
                let error = $root.Shadow.healthMsg.verify(message.health);
                if (error)
                    return "health." + error;
            }
            if (message.environment != null && message.hasOwnProperty("environment")) {
                let error = $root.Shadow.environmentMsg.verify(message.environment);
                if (error)
                    return "environment." + error;
            }
            return null;
        };

        /**
         * Creates a reportedMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.reportedMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.reportedMsg} reportedMsg
         */
        reportedMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.reportedMsg)
                return object;
            let message = new $root.Shadow.reportedMsg();
            if (object.videoSources) {
                if (!Array.isArray(object.videoSources))
                    throw TypeError(".Shadow.reportedMsg.videoSources: array expected");
                message.videoSources = [];
                for (let i = 0; i < object.videoSources.length; ++i) {
                    if (typeof object.videoSources[i] !== "object")
                        throw TypeError(".Shadow.reportedMsg.videoSources: object expected");
                    message.videoSources[i] = $root.Shadow.videoSource.fromObject(object.videoSources[i]);
                }
            }
            if (object.discoveredVideoSources) {
                if (!Array.isArray(object.discoveredVideoSources))
                    throw TypeError(".Shadow.reportedMsg.discoveredVideoSources: array expected");
                message.discoveredVideoSources = [];
                for (let i = 0; i < object.discoveredVideoSources.length; ++i)
                    message.discoveredVideoSources[i] = String(object.discoveredVideoSources[i]);
            }
            if (object.connected != null)
                message.connected = Boolean(object.connected);
            if (object.status != null)
                message.status = String(object.status);
            if (object.ip != null)
                message.ip = String(object.ip);
            if (object.versionInfo != null) {
                if (typeof object.versionInfo !== "object")
                    throw TypeError(".Shadow.reportedMsg.versionInfo: object expected");
                message.versionInfo = $root.Shadow.versionMsg.fromObject(object.versionInfo);
            }
            if (object.lastVersionInfo != null) {
                if (typeof object.lastVersionInfo !== "object")
                    throw TypeError(".Shadow.reportedMsg.lastVersionInfo: object expected");
                message.lastVersionInfo = $root.Shadow.versionMsg.fromObject(object.lastVersionInfo);
            }
            if (object.health != null) {
                if (typeof object.health !== "object")
                    throw TypeError(".Shadow.reportedMsg.health: object expected");
                message.health = $root.Shadow.healthMsg.fromObject(object.health);
            }
            if (object.environment != null) {
                if (typeof object.environment !== "object")
                    throw TypeError(".Shadow.reportedMsg.environment: object expected");
                message.environment = $root.Shadow.environmentMsg.fromObject(object.environment);
            }
            return message;
        };

        /**
         * Creates a plain object from a reportedMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.reportedMsg
         * @static
         * @param {Shadow.reportedMsg} message reportedMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        reportedMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.arrays || options.defaults) {
                object.videoSources = [];
                object.discoveredVideoSources = [];
            }
            if (options.defaults) {
                object.connected = false;
                object.status = "";
                object.ip = "";
                object.versionInfo = null;
                object.lastVersionInfo = null;
                object.health = null;
                object.environment = null;
            }
            if (message.videoSources && message.videoSources.length) {
                object.videoSources = [];
                for (let j = 0; j < message.videoSources.length; ++j)
                    object.videoSources[j] = $root.Shadow.videoSource.toObject(message.videoSources[j], options);
            }
            if (message.discoveredVideoSources && message.discoveredVideoSources.length) {
                object.discoveredVideoSources = [];
                for (let j = 0; j < message.discoveredVideoSources.length; ++j)
                    object.discoveredVideoSources[j] = message.discoveredVideoSources[j];
            }
            if (message.connected != null && message.hasOwnProperty("connected"))
                object.connected = message.connected;
            if (message.status != null && message.hasOwnProperty("status"))
                object.status = message.status;
            if (message.ip != null && message.hasOwnProperty("ip"))
                object.ip = message.ip;
            if (message.versionInfo != null && message.hasOwnProperty("versionInfo"))
                object.versionInfo = $root.Shadow.versionMsg.toObject(message.versionInfo, options);
            if (message.lastVersionInfo != null && message.hasOwnProperty("lastVersionInfo"))
                object.lastVersionInfo = $root.Shadow.versionMsg.toObject(message.lastVersionInfo, options);
            if (message.health != null && message.hasOwnProperty("health"))
                object.health = $root.Shadow.healthMsg.toObject(message.health, options);
            if (message.environment != null && message.hasOwnProperty("environment"))
                object.environment = $root.Shadow.environmentMsg.toObject(message.environment, options);
            return object;
        };

        /**
         * Converts this reportedMsg to JSON.
         * @function toJSON
         * @memberof Shadow.reportedMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        reportedMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return reportedMsg;
    })();

    Shadow.versionMsg = (function() {

        /**
         * Properties of a versionMsg.
         * @memberof Shadow
         * @interface IversionMsg
         * @property {string|null} [branch] versionMsg branch
         * @property {string|null} [chripVersion] versionMsg chripVersion
         * @property {string|null} [mmwaveVersion] versionMsg mmwaveVersion
         * @property {string|null} [esp32Version] versionMsg esp32Version
         * @property {string|null} [portalVersion] versionMsg portalVersion
         */

        /**
         * Constructs a new versionMsg.
         * @memberof Shadow
         * @classdesc Represents a versionMsg.
         * @implements IversionMsg
         * @constructor
         * @param {Shadow.IversionMsg=} [properties] Properties to set
         */
        function versionMsg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * versionMsg branch.
         * @member {string} branch
         * @memberof Shadow.versionMsg
         * @instance
         */
        versionMsg.prototype.branch = "";

        /**
         * versionMsg chripVersion.
         * @member {string} chripVersion
         * @memberof Shadow.versionMsg
         * @instance
         */
        versionMsg.prototype.chripVersion = "";

        /**
         * versionMsg mmwaveVersion.
         * @member {string} mmwaveVersion
         * @memberof Shadow.versionMsg
         * @instance
         */
        versionMsg.prototype.mmwaveVersion = "";

        /**
         * versionMsg esp32Version.
         * @member {string} esp32Version
         * @memberof Shadow.versionMsg
         * @instance
         */
        versionMsg.prototype.esp32Version = "";

        /**
         * versionMsg portalVersion.
         * @member {string} portalVersion
         * @memberof Shadow.versionMsg
         * @instance
         */
        versionMsg.prototype.portalVersion = "";

        /**
         * Creates a new versionMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.versionMsg
         * @static
         * @param {Shadow.IversionMsg=} [properties] Properties to set
         * @returns {Shadow.versionMsg} versionMsg instance
         */
        versionMsg.create = function create(properties) {
            return new versionMsg(properties);
        };

        /**
         * Encodes the specified versionMsg message. Does not implicitly {@link Shadow.versionMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.versionMsg
         * @static
         * @param {Shadow.IversionMsg} message versionMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        versionMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.branch != null && Object.hasOwnProperty.call(message, "branch"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.branch);
            if (message.chripVersion != null && Object.hasOwnProperty.call(message, "chripVersion"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.chripVersion);
            if (message.mmwaveVersion != null && Object.hasOwnProperty.call(message, "mmwaveVersion"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.mmwaveVersion);
            if (message.esp32Version != null && Object.hasOwnProperty.call(message, "esp32Version"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.esp32Version);
            if (message.portalVersion != null && Object.hasOwnProperty.call(message, "portalVersion"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.portalVersion);
            return writer;
        };

        /**
         * Encodes the specified versionMsg message, length delimited. Does not implicitly {@link Shadow.versionMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.versionMsg
         * @static
         * @param {Shadow.IversionMsg} message versionMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        versionMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a versionMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.versionMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.versionMsg} versionMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        versionMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.versionMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.branch = reader.string();
                    break;
                case 2:
                    message.chripVersion = reader.string();
                    break;
                case 3:
                    message.mmwaveVersion = reader.string();
                    break;
                case 4:
                    message.esp32Version = reader.string();
                    break;
                case 5:
                    message.portalVersion = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a versionMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.versionMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.versionMsg} versionMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        versionMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a versionMsg message.
         * @function verify
         * @memberof Shadow.versionMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        versionMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.branch != null && message.hasOwnProperty("branch"))
                if (!$util.isString(message.branch))
                    return "branch: string expected";
            if (message.chripVersion != null && message.hasOwnProperty("chripVersion"))
                if (!$util.isString(message.chripVersion))
                    return "chripVersion: string expected";
            if (message.mmwaveVersion != null && message.hasOwnProperty("mmwaveVersion"))
                if (!$util.isString(message.mmwaveVersion))
                    return "mmwaveVersion: string expected";
            if (message.esp32Version != null && message.hasOwnProperty("esp32Version"))
                if (!$util.isString(message.esp32Version))
                    return "esp32Version: string expected";
            if (message.portalVersion != null && message.hasOwnProperty("portalVersion"))
                if (!$util.isString(message.portalVersion))
                    return "portalVersion: string expected";
            return null;
        };

        /**
         * Creates a versionMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.versionMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.versionMsg} versionMsg
         */
        versionMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.versionMsg)
                return object;
            let message = new $root.Shadow.versionMsg();
            if (object.branch != null)
                message.branch = String(object.branch);
            if (object.chripVersion != null)
                message.chripVersion = String(object.chripVersion);
            if (object.mmwaveVersion != null)
                message.mmwaveVersion = String(object.mmwaveVersion);
            if (object.esp32Version != null)
                message.esp32Version = String(object.esp32Version);
            if (object.portalVersion != null)
                message.portalVersion = String(object.portalVersion);
            return message;
        };

        /**
         * Creates a plain object from a versionMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.versionMsg
         * @static
         * @param {Shadow.versionMsg} message versionMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        versionMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.branch = "";
                object.chripVersion = "";
                object.mmwaveVersion = "";
                object.esp32Version = "";
                object.portalVersion = "";
            }
            if (message.branch != null && message.hasOwnProperty("branch"))
                object.branch = message.branch;
            if (message.chripVersion != null && message.hasOwnProperty("chripVersion"))
                object.chripVersion = message.chripVersion;
            if (message.mmwaveVersion != null && message.hasOwnProperty("mmwaveVersion"))
                object.mmwaveVersion = message.mmwaveVersion;
            if (message.esp32Version != null && message.hasOwnProperty("esp32Version"))
                object.esp32Version = message.esp32Version;
            if (message.portalVersion != null && message.hasOwnProperty("portalVersion"))
                object.portalVersion = message.portalVersion;
            return object;
        };

        /**
         * Converts this versionMsg to JSON.
         * @function toJSON
         * @memberof Shadow.versionMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        versionMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return versionMsg;
    })();

    Shadow.avsMsg = (function() {

        /**
         * Properties of an avsMsg.
         * @memberof Shadow
         * @interface IavsMsg
         * @property {string|null} [clientId] avsMsg clientId
         * @property {string|null} [redirectUrl] avsMsg redirectUrl
         * @property {string|null} [authCode] avsMsg authCode
         * @property {string|null} [codeVerifier] avsMsg codeVerifier
         */

        /**
         * Constructs a new avsMsg.
         * @memberof Shadow
         * @classdesc Represents an avsMsg.
         * @implements IavsMsg
         * @constructor
         * @param {Shadow.IavsMsg=} [properties] Properties to set
         */
        function avsMsg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * avsMsg clientId.
         * @member {string} clientId
         * @memberof Shadow.avsMsg
         * @instance
         */
        avsMsg.prototype.clientId = "";

        /**
         * avsMsg redirectUrl.
         * @member {string} redirectUrl
         * @memberof Shadow.avsMsg
         * @instance
         */
        avsMsg.prototype.redirectUrl = "";

        /**
         * avsMsg authCode.
         * @member {string} authCode
         * @memberof Shadow.avsMsg
         * @instance
         */
        avsMsg.prototype.authCode = "";

        /**
         * avsMsg codeVerifier.
         * @member {string} codeVerifier
         * @memberof Shadow.avsMsg
         * @instance
         */
        avsMsg.prototype.codeVerifier = "";

        /**
         * Creates a new avsMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.avsMsg
         * @static
         * @param {Shadow.IavsMsg=} [properties] Properties to set
         * @returns {Shadow.avsMsg} avsMsg instance
         */
        avsMsg.create = function create(properties) {
            return new avsMsg(properties);
        };

        /**
         * Encodes the specified avsMsg message. Does not implicitly {@link Shadow.avsMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.avsMsg
         * @static
         * @param {Shadow.IavsMsg} message avsMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        avsMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.clientId != null && Object.hasOwnProperty.call(message, "clientId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.clientId);
            if (message.redirectUrl != null && Object.hasOwnProperty.call(message, "redirectUrl"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.redirectUrl);
            if (message.authCode != null && Object.hasOwnProperty.call(message, "authCode"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.authCode);
            if (message.codeVerifier != null && Object.hasOwnProperty.call(message, "codeVerifier"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.codeVerifier);
            return writer;
        };

        /**
         * Encodes the specified avsMsg message, length delimited. Does not implicitly {@link Shadow.avsMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.avsMsg
         * @static
         * @param {Shadow.IavsMsg} message avsMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        avsMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an avsMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.avsMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.avsMsg} avsMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        avsMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.avsMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.clientId = reader.string();
                    break;
                case 2:
                    message.redirectUrl = reader.string();
                    break;
                case 3:
                    message.authCode = reader.string();
                    break;
                case 4:
                    message.codeVerifier = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an avsMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.avsMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.avsMsg} avsMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        avsMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an avsMsg message.
         * @function verify
         * @memberof Shadow.avsMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        avsMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.clientId != null && message.hasOwnProperty("clientId"))
                if (!$util.isString(message.clientId))
                    return "clientId: string expected";
            if (message.redirectUrl != null && message.hasOwnProperty("redirectUrl"))
                if (!$util.isString(message.redirectUrl))
                    return "redirectUrl: string expected";
            if (message.authCode != null && message.hasOwnProperty("authCode"))
                if (!$util.isString(message.authCode))
                    return "authCode: string expected";
            if (message.codeVerifier != null && message.hasOwnProperty("codeVerifier"))
                if (!$util.isString(message.codeVerifier))
                    return "codeVerifier: string expected";
            return null;
        };

        /**
         * Creates an avsMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.avsMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.avsMsg} avsMsg
         */
        avsMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.avsMsg)
                return object;
            let message = new $root.Shadow.avsMsg();
            if (object.clientId != null)
                message.clientId = String(object.clientId);
            if (object.redirectUrl != null)
                message.redirectUrl = String(object.redirectUrl);
            if (object.authCode != null)
                message.authCode = String(object.authCode);
            if (object.codeVerifier != null)
                message.codeVerifier = String(object.codeVerifier);
            return message;
        };

        /**
         * Creates a plain object from an avsMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.avsMsg
         * @static
         * @param {Shadow.avsMsg} message avsMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        avsMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.clientId = "";
                object.redirectUrl = "";
                object.authCode = "";
                object.codeVerifier = "";
            }
            if (message.clientId != null && message.hasOwnProperty("clientId"))
                object.clientId = message.clientId;
            if (message.redirectUrl != null && message.hasOwnProperty("redirectUrl"))
                object.redirectUrl = message.redirectUrl;
            if (message.authCode != null && message.hasOwnProperty("authCode"))
                object.authCode = message.authCode;
            if (message.codeVerifier != null && message.hasOwnProperty("codeVerifier"))
                object.codeVerifier = message.codeVerifier;
            return object;
        };

        /**
         * Converts this avsMsg to JSON.
         * @function toJSON
         * @memberof Shadow.avsMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        avsMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return avsMsg;
    })();

    Shadow.calibrationMsg = (function() {

        /**
         * Properties of a calibrationMsg.
         * @memberof Shadow
         * @interface IcalibrationMsg
         * @property {number|null} [startTime] calibrationMsg startTime
         * @property {number|null} [stopTime] calibrationMsg stopTime
         * @property {boolean|null} [auto] calibrationMsg auto
         * @property {Shadow.calibrationMsg.State|null} [calibrationState] calibrationMsg calibrationState
         * @property {Shadow.IenvironmentMsg|null} [environment] calibrationMsg environment
         */

        /**
         * Constructs a new calibrationMsg.
         * @memberof Shadow
         * @classdesc Represents a calibrationMsg.
         * @implements IcalibrationMsg
         * @constructor
         * @param {Shadow.IcalibrationMsg=} [properties] Properties to set
         */
        function calibrationMsg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * calibrationMsg startTime.
         * @member {number} startTime
         * @memberof Shadow.calibrationMsg
         * @instance
         */
        calibrationMsg.prototype.startTime = 0;

        /**
         * calibrationMsg stopTime.
         * @member {number} stopTime
         * @memberof Shadow.calibrationMsg
         * @instance
         */
        calibrationMsg.prototype.stopTime = 0;

        /**
         * calibrationMsg auto.
         * @member {boolean} auto
         * @memberof Shadow.calibrationMsg
         * @instance
         */
        calibrationMsg.prototype.auto = false;

        /**
         * calibrationMsg calibrationState.
         * @member {Shadow.calibrationMsg.State} calibrationState
         * @memberof Shadow.calibrationMsg
         * @instance
         */
        calibrationMsg.prototype.calibrationState = 0;

        /**
         * calibrationMsg environment.
         * @member {Shadow.IenvironmentMsg|null|undefined} environment
         * @memberof Shadow.calibrationMsg
         * @instance
         */
        calibrationMsg.prototype.environment = null;

        /**
         * Creates a new calibrationMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.calibrationMsg
         * @static
         * @param {Shadow.IcalibrationMsg=} [properties] Properties to set
         * @returns {Shadow.calibrationMsg} calibrationMsg instance
         */
        calibrationMsg.create = function create(properties) {
            return new calibrationMsg(properties);
        };

        /**
         * Encodes the specified calibrationMsg message. Does not implicitly {@link Shadow.calibrationMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.calibrationMsg
         * @static
         * @param {Shadow.IcalibrationMsg} message calibrationMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        calibrationMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.startTime != null && Object.hasOwnProperty.call(message, "startTime"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.startTime);
            if (message.stopTime != null && Object.hasOwnProperty.call(message, "stopTime"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.stopTime);
            if (message.auto != null && Object.hasOwnProperty.call(message, "auto"))
                writer.uint32(/* id 3, wireType 0 =*/24).bool(message.auto);
            if (message.calibrationState != null && Object.hasOwnProperty.call(message, "calibrationState"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.calibrationState);
            if (message.environment != null && Object.hasOwnProperty.call(message, "environment"))
                $root.Shadow.environmentMsg.encode(message.environment, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified calibrationMsg message, length delimited. Does not implicitly {@link Shadow.calibrationMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.calibrationMsg
         * @static
         * @param {Shadow.IcalibrationMsg} message calibrationMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        calibrationMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a calibrationMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.calibrationMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.calibrationMsg} calibrationMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        calibrationMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.calibrationMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.startTime = reader.uint32();
                    break;
                case 2:
                    message.stopTime = reader.uint32();
                    break;
                case 3:
                    message.auto = reader.bool();
                    break;
                case 4:
                    message.calibrationState = reader.int32();
                    break;
                case 5:
                    message.environment = $root.Shadow.environmentMsg.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a calibrationMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.calibrationMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.calibrationMsg} calibrationMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        calibrationMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a calibrationMsg message.
         * @function verify
         * @memberof Shadow.calibrationMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        calibrationMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.startTime != null && message.hasOwnProperty("startTime"))
                if (!$util.isInteger(message.startTime))
                    return "startTime: integer expected";
            if (message.stopTime != null && message.hasOwnProperty("stopTime"))
                if (!$util.isInteger(message.stopTime))
                    return "stopTime: integer expected";
            if (message.auto != null && message.hasOwnProperty("auto"))
                if (typeof message.auto !== "boolean")
                    return "auto: boolean expected";
            if (message.calibrationState != null && message.hasOwnProperty("calibrationState"))
                switch (message.calibrationState) {
                default:
                    return "calibrationState: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    break;
                }
            if (message.environment != null && message.hasOwnProperty("environment")) {
                let error = $root.Shadow.environmentMsg.verify(message.environment);
                if (error)
                    return "environment." + error;
            }
            return null;
        };

        /**
         * Creates a calibrationMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.calibrationMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.calibrationMsg} calibrationMsg
         */
        calibrationMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.calibrationMsg)
                return object;
            let message = new $root.Shadow.calibrationMsg();
            if (object.startTime != null)
                message.startTime = object.startTime >>> 0;
            if (object.stopTime != null)
                message.stopTime = object.stopTime >>> 0;
            if (object.auto != null)
                message.auto = Boolean(object.auto);
            switch (object.calibrationState) {
            case "None":
            case 0:
                message.calibrationState = 0;
                break;
            case "Calibrating":
            case 1:
                message.calibrationState = 1;
                break;
            case "Fail":
            case 2:
                message.calibrationState = 2;
                break;
            case "Success":
            case 3:
                message.calibrationState = 3;
                break;
            case "Collecting":
            case 4:
                message.calibrationState = 4;
                break;
            }
            if (object.environment != null) {
                if (typeof object.environment !== "object")
                    throw TypeError(".Shadow.calibrationMsg.environment: object expected");
                message.environment = $root.Shadow.environmentMsg.fromObject(object.environment);
            }
            return message;
        };

        /**
         * Creates a plain object from a calibrationMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.calibrationMsg
         * @static
         * @param {Shadow.calibrationMsg} message calibrationMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        calibrationMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.startTime = 0;
                object.stopTime = 0;
                object.auto = false;
                object.calibrationState = options.enums === String ? "None" : 0;
                object.environment = null;
            }
            if (message.startTime != null && message.hasOwnProperty("startTime"))
                object.startTime = message.startTime;
            if (message.stopTime != null && message.hasOwnProperty("stopTime"))
                object.stopTime = message.stopTime;
            if (message.auto != null && message.hasOwnProperty("auto"))
                object.auto = message.auto;
            if (message.calibrationState != null && message.hasOwnProperty("calibrationState"))
                object.calibrationState = options.enums === String ? $root.Shadow.calibrationMsg.State[message.calibrationState] : message.calibrationState;
            if (message.environment != null && message.hasOwnProperty("environment"))
                object.environment = $root.Shadow.environmentMsg.toObject(message.environment, options);
            return object;
        };

        /**
         * Converts this calibrationMsg to JSON.
         * @function toJSON
         * @memberof Shadow.calibrationMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        calibrationMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * State enum.
         * @name Shadow.calibrationMsg.State
         * @enum {number}
         * @property {number} None=0 None value
         * @property {number} Calibrating=1 Calibrating value
         * @property {number} Fail=2 Fail value
         * @property {number} Success=3 Success value
         * @property {number} Collecting=4 Collecting value
         */
        calibrationMsg.State = (function() {
            const valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "None"] = 0;
            values[valuesById[1] = "Calibrating"] = 1;
            values[valuesById[2] = "Fail"] = 2;
            values[valuesById[3] = "Success"] = 3;
            values[valuesById[4] = "Collecting"] = 4;
            return values;
        })();

        return calibrationMsg;
    })();

    Shadow.chripParamMsg = (function() {

        /**
         * Properties of a chripParamMsg.
         * @memberof Shadow
         * @interface IchripParamMsg
         * @property {string|null} [occupancyFlagsCfg] chripParamMsg occupancyFlagsCfg
         * @property {string|null} [occupancyParamCfg] chripParamMsg occupancyParamCfg
         * @property {string|null} [correctionParam] chripParamMsg correctionParam
         * @property {string|null} [occuStanceParam] chripParamMsg occuStanceParam
         * @property {string|null} [sceenRoiParam] chripParamMsg sceenRoiParam
         * @property {string|null} [profileCfg] chripParamMsg profileCfg
         */

        /**
         * Constructs a new chripParamMsg.
         * @memberof Shadow
         * @classdesc Represents a chripParamMsg.
         * @implements IchripParamMsg
         * @constructor
         * @param {Shadow.IchripParamMsg=} [properties] Properties to set
         */
        function chripParamMsg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * chripParamMsg occupancyFlagsCfg.
         * @member {string} occupancyFlagsCfg
         * @memberof Shadow.chripParamMsg
         * @instance
         */
        chripParamMsg.prototype.occupancyFlagsCfg = "";

        /**
         * chripParamMsg occupancyParamCfg.
         * @member {string} occupancyParamCfg
         * @memberof Shadow.chripParamMsg
         * @instance
         */
        chripParamMsg.prototype.occupancyParamCfg = "";

        /**
         * chripParamMsg correctionParam.
         * @member {string} correctionParam
         * @memberof Shadow.chripParamMsg
         * @instance
         */
        chripParamMsg.prototype.correctionParam = "";

        /**
         * chripParamMsg occuStanceParam.
         * @member {string} occuStanceParam
         * @memberof Shadow.chripParamMsg
         * @instance
         */
        chripParamMsg.prototype.occuStanceParam = "";

        /**
         * chripParamMsg sceenRoiParam.
         * @member {string} sceenRoiParam
         * @memberof Shadow.chripParamMsg
         * @instance
         */
        chripParamMsg.prototype.sceenRoiParam = "";

        /**
         * chripParamMsg profileCfg.
         * @member {string} profileCfg
         * @memberof Shadow.chripParamMsg
         * @instance
         */
        chripParamMsg.prototype.profileCfg = "";

        /**
         * Creates a new chripParamMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.chripParamMsg
         * @static
         * @param {Shadow.IchripParamMsg=} [properties] Properties to set
         * @returns {Shadow.chripParamMsg} chripParamMsg instance
         */
        chripParamMsg.create = function create(properties) {
            return new chripParamMsg(properties);
        };

        /**
         * Encodes the specified chripParamMsg message. Does not implicitly {@link Shadow.chripParamMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.chripParamMsg
         * @static
         * @param {Shadow.IchripParamMsg} message chripParamMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        chripParamMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.occupancyFlagsCfg != null && Object.hasOwnProperty.call(message, "occupancyFlagsCfg"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.occupancyFlagsCfg);
            if (message.occupancyParamCfg != null && Object.hasOwnProperty.call(message, "occupancyParamCfg"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.occupancyParamCfg);
            if (message.correctionParam != null && Object.hasOwnProperty.call(message, "correctionParam"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.correctionParam);
            if (message.occuStanceParam != null && Object.hasOwnProperty.call(message, "occuStanceParam"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.occuStanceParam);
            if (message.sceenRoiParam != null && Object.hasOwnProperty.call(message, "sceenRoiParam"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.sceenRoiParam);
            if (message.profileCfg != null && Object.hasOwnProperty.call(message, "profileCfg"))
                writer.uint32(/* id 6, wireType 2 =*/50).string(message.profileCfg);
            return writer;
        };

        /**
         * Encodes the specified chripParamMsg message, length delimited. Does not implicitly {@link Shadow.chripParamMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.chripParamMsg
         * @static
         * @param {Shadow.IchripParamMsg} message chripParamMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        chripParamMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a chripParamMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.chripParamMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.chripParamMsg} chripParamMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        chripParamMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.chripParamMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.occupancyFlagsCfg = reader.string();
                    break;
                case 2:
                    message.occupancyParamCfg = reader.string();
                    break;
                case 3:
                    message.correctionParam = reader.string();
                    break;
                case 4:
                    message.occuStanceParam = reader.string();
                    break;
                case 5:
                    message.sceenRoiParam = reader.string();
                    break;
                case 6:
                    message.profileCfg = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a chripParamMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.chripParamMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.chripParamMsg} chripParamMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        chripParamMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a chripParamMsg message.
         * @function verify
         * @memberof Shadow.chripParamMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        chripParamMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.occupancyFlagsCfg != null && message.hasOwnProperty("occupancyFlagsCfg"))
                if (!$util.isString(message.occupancyFlagsCfg))
                    return "occupancyFlagsCfg: string expected";
            if (message.occupancyParamCfg != null && message.hasOwnProperty("occupancyParamCfg"))
                if (!$util.isString(message.occupancyParamCfg))
                    return "occupancyParamCfg: string expected";
            if (message.correctionParam != null && message.hasOwnProperty("correctionParam"))
                if (!$util.isString(message.correctionParam))
                    return "correctionParam: string expected";
            if (message.occuStanceParam != null && message.hasOwnProperty("occuStanceParam"))
                if (!$util.isString(message.occuStanceParam))
                    return "occuStanceParam: string expected";
            if (message.sceenRoiParam != null && message.hasOwnProperty("sceenRoiParam"))
                if (!$util.isString(message.sceenRoiParam))
                    return "sceenRoiParam: string expected";
            if (message.profileCfg != null && message.hasOwnProperty("profileCfg"))
                if (!$util.isString(message.profileCfg))
                    return "profileCfg: string expected";
            return null;
        };

        /**
         * Creates a chripParamMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.chripParamMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.chripParamMsg} chripParamMsg
         */
        chripParamMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.chripParamMsg)
                return object;
            let message = new $root.Shadow.chripParamMsg();
            if (object.occupancyFlagsCfg != null)
                message.occupancyFlagsCfg = String(object.occupancyFlagsCfg);
            if (object.occupancyParamCfg != null)
                message.occupancyParamCfg = String(object.occupancyParamCfg);
            if (object.correctionParam != null)
                message.correctionParam = String(object.correctionParam);
            if (object.occuStanceParam != null)
                message.occuStanceParam = String(object.occuStanceParam);
            if (object.sceenRoiParam != null)
                message.sceenRoiParam = String(object.sceenRoiParam);
            if (object.profileCfg != null)
                message.profileCfg = String(object.profileCfg);
            return message;
        };

        /**
         * Creates a plain object from a chripParamMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.chripParamMsg
         * @static
         * @param {Shadow.chripParamMsg} message chripParamMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        chripParamMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.occupancyFlagsCfg = "";
                object.occupancyParamCfg = "";
                object.correctionParam = "";
                object.occuStanceParam = "";
                object.sceenRoiParam = "";
                object.profileCfg = "";
            }
            if (message.occupancyFlagsCfg != null && message.hasOwnProperty("occupancyFlagsCfg"))
                object.occupancyFlagsCfg = message.occupancyFlagsCfg;
            if (message.occupancyParamCfg != null && message.hasOwnProperty("occupancyParamCfg"))
                object.occupancyParamCfg = message.occupancyParamCfg;
            if (message.correctionParam != null && message.hasOwnProperty("correctionParam"))
                object.correctionParam = message.correctionParam;
            if (message.occuStanceParam != null && message.hasOwnProperty("occuStanceParam"))
                object.occuStanceParam = message.occuStanceParam;
            if (message.sceenRoiParam != null && message.hasOwnProperty("sceenRoiParam"))
                object.sceenRoiParam = message.sceenRoiParam;
            if (message.profileCfg != null && message.hasOwnProperty("profileCfg"))
                object.profileCfg = message.profileCfg;
            return object;
        };

        /**
         * Converts this chripParamMsg to JSON.
         * @function toJSON
         * @memberof Shadow.chripParamMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        chripParamMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return chripParamMsg;
    })();

    Shadow.environmentMsg = (function() {

        /**
         * Properties of an environmentMsg.
         * @memberof Shadow
         * @interface IenvironmentMsg
         * @property {number|null} [rotation] environmentMsg rotation
         * @property {number|null} [upDis] environmentMsg upDis
         * @property {number|null} [downDis] environmentMsg downDis
         * @property {number|null} [leftDis] environmentMsg leftDis
         * @property {number|null} [rightDis] environmentMsg rightDis
         * @property {number|null} [height] environmentMsg height
         */

        /**
         * Constructs a new environmentMsg.
         * @memberof Shadow
         * @classdesc Represents an environmentMsg.
         * @implements IenvironmentMsg
         * @constructor
         * @param {Shadow.IenvironmentMsg=} [properties] Properties to set
         */
        function environmentMsg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * environmentMsg rotation.
         * @member {number} rotation
         * @memberof Shadow.environmentMsg
         * @instance
         */
        environmentMsg.prototype.rotation = 0;

        /**
         * environmentMsg upDis.
         * @member {number} upDis
         * @memberof Shadow.environmentMsg
         * @instance
         */
        environmentMsg.prototype.upDis = 0;

        /**
         * environmentMsg downDis.
         * @member {number} downDis
         * @memberof Shadow.environmentMsg
         * @instance
         */
        environmentMsg.prototype.downDis = 0;

        /**
         * environmentMsg leftDis.
         * @member {number} leftDis
         * @memberof Shadow.environmentMsg
         * @instance
         */
        environmentMsg.prototype.leftDis = 0;

        /**
         * environmentMsg rightDis.
         * @member {number} rightDis
         * @memberof Shadow.environmentMsg
         * @instance
         */
        environmentMsg.prototype.rightDis = 0;

        /**
         * environmentMsg height.
         * @member {number} height
         * @memberof Shadow.environmentMsg
         * @instance
         */
        environmentMsg.prototype.height = 0;

        /**
         * Creates a new environmentMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.environmentMsg
         * @static
         * @param {Shadow.IenvironmentMsg=} [properties] Properties to set
         * @returns {Shadow.environmentMsg} environmentMsg instance
         */
        environmentMsg.create = function create(properties) {
            return new environmentMsg(properties);
        };

        /**
         * Encodes the specified environmentMsg message. Does not implicitly {@link Shadow.environmentMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.environmentMsg
         * @static
         * @param {Shadow.IenvironmentMsg} message environmentMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        environmentMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.rotation != null && Object.hasOwnProperty.call(message, "rotation"))
                writer.uint32(/* id 1, wireType 1 =*/9).double(message.rotation);
            if (message.upDis != null && Object.hasOwnProperty.call(message, "upDis"))
                writer.uint32(/* id 2, wireType 1 =*/17).double(message.upDis);
            if (message.downDis != null && Object.hasOwnProperty.call(message, "downDis"))
                writer.uint32(/* id 3, wireType 1 =*/25).double(message.downDis);
            if (message.leftDis != null && Object.hasOwnProperty.call(message, "leftDis"))
                writer.uint32(/* id 4, wireType 1 =*/33).double(message.leftDis);
            if (message.rightDis != null && Object.hasOwnProperty.call(message, "rightDis"))
                writer.uint32(/* id 5, wireType 1 =*/41).double(message.rightDis);
            if (message.height != null && Object.hasOwnProperty.call(message, "height"))
                writer.uint32(/* id 6, wireType 1 =*/49).double(message.height);
            return writer;
        };

        /**
         * Encodes the specified environmentMsg message, length delimited. Does not implicitly {@link Shadow.environmentMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.environmentMsg
         * @static
         * @param {Shadow.IenvironmentMsg} message environmentMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        environmentMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an environmentMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.environmentMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.environmentMsg} environmentMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        environmentMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.environmentMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.rotation = reader.double();
                    break;
                case 2:
                    message.upDis = reader.double();
                    break;
                case 3:
                    message.downDis = reader.double();
                    break;
                case 4:
                    message.leftDis = reader.double();
                    break;
                case 5:
                    message.rightDis = reader.double();
                    break;
                case 6:
                    message.height = reader.double();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an environmentMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.environmentMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.environmentMsg} environmentMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        environmentMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an environmentMsg message.
         * @function verify
         * @memberof Shadow.environmentMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        environmentMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.rotation != null && message.hasOwnProperty("rotation"))
                if (typeof message.rotation !== "number")
                    return "rotation: number expected";
            if (message.upDis != null && message.hasOwnProperty("upDis"))
                if (typeof message.upDis !== "number")
                    return "upDis: number expected";
            if (message.downDis != null && message.hasOwnProperty("downDis"))
                if (typeof message.downDis !== "number")
                    return "downDis: number expected";
            if (message.leftDis != null && message.hasOwnProperty("leftDis"))
                if (typeof message.leftDis !== "number")
                    return "leftDis: number expected";
            if (message.rightDis != null && message.hasOwnProperty("rightDis"))
                if (typeof message.rightDis !== "number")
                    return "rightDis: number expected";
            if (message.height != null && message.hasOwnProperty("height"))
                if (typeof message.height !== "number")
                    return "height: number expected";
            return null;
        };

        /**
         * Creates an environmentMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.environmentMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.environmentMsg} environmentMsg
         */
        environmentMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.environmentMsg)
                return object;
            let message = new $root.Shadow.environmentMsg();
            if (object.rotation != null)
                message.rotation = Number(object.rotation);
            if (object.upDis != null)
                message.upDis = Number(object.upDis);
            if (object.downDis != null)
                message.downDis = Number(object.downDis);
            if (object.leftDis != null)
                message.leftDis = Number(object.leftDis);
            if (object.rightDis != null)
                message.rightDis = Number(object.rightDis);
            if (object.height != null)
                message.height = Number(object.height);
            return message;
        };

        /**
         * Creates a plain object from an environmentMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.environmentMsg
         * @static
         * @param {Shadow.environmentMsg} message environmentMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        environmentMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.rotation = 0;
                object.upDis = 0;
                object.downDis = 0;
                object.leftDis = 0;
                object.rightDis = 0;
                object.height = 0;
            }
            if (message.rotation != null && message.hasOwnProperty("rotation"))
                object.rotation = options.json && !isFinite(message.rotation) ? String(message.rotation) : message.rotation;
            if (message.upDis != null && message.hasOwnProperty("upDis"))
                object.upDis = options.json && !isFinite(message.upDis) ? String(message.upDis) : message.upDis;
            if (message.downDis != null && message.hasOwnProperty("downDis"))
                object.downDis = options.json && !isFinite(message.downDis) ? String(message.downDis) : message.downDis;
            if (message.leftDis != null && message.hasOwnProperty("leftDis"))
                object.leftDis = options.json && !isFinite(message.leftDis) ? String(message.leftDis) : message.leftDis;
            if (message.rightDis != null && message.hasOwnProperty("rightDis"))
                object.rightDis = options.json && !isFinite(message.rightDis) ? String(message.rightDis) : message.rightDis;
            if (message.height != null && message.hasOwnProperty("height"))
                object.height = options.json && !isFinite(message.height) ? String(message.height) : message.height;
            return object;
        };

        /**
         * Converts this environmentMsg to JSON.
         * @function toJSON
         * @memberof Shadow.environmentMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        environmentMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return environmentMsg;
    })();

    Shadow.stitchParamMsg = (function() {

        /**
         * Properties of a stitchParamMsg.
         * @memberof Shadow
         * @interface IstitchParamMsg
         * @property {number|null} [height] stitchParamMsg height
         * @property {number|null} [theta] stitchParamMsg theta
         * @property {number|null} [shiftX] stitchParamMsg shiftX
         * @property {number|null} [shiftZ] stitchParamMsg shiftZ
         */

        /**
         * Constructs a new stitchParamMsg.
         * @memberof Shadow
         * @classdesc Represents a stitchParamMsg.
         * @implements IstitchParamMsg
         * @constructor
         * @param {Shadow.IstitchParamMsg=} [properties] Properties to set
         */
        function stitchParamMsg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * stitchParamMsg height.
         * @member {number} height
         * @memberof Shadow.stitchParamMsg
         * @instance
         */
        stitchParamMsg.prototype.height = 0;

        /**
         * stitchParamMsg theta.
         * @member {number} theta
         * @memberof Shadow.stitchParamMsg
         * @instance
         */
        stitchParamMsg.prototype.theta = 0;

        /**
         * stitchParamMsg shiftX.
         * @member {number} shiftX
         * @memberof Shadow.stitchParamMsg
         * @instance
         */
        stitchParamMsg.prototype.shiftX = 0;

        /**
         * stitchParamMsg shiftZ.
         * @member {number} shiftZ
         * @memberof Shadow.stitchParamMsg
         * @instance
         */
        stitchParamMsg.prototype.shiftZ = 0;

        /**
         * Creates a new stitchParamMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.stitchParamMsg
         * @static
         * @param {Shadow.IstitchParamMsg=} [properties] Properties to set
         * @returns {Shadow.stitchParamMsg} stitchParamMsg instance
         */
        stitchParamMsg.create = function create(properties) {
            return new stitchParamMsg(properties);
        };

        /**
         * Encodes the specified stitchParamMsg message. Does not implicitly {@link Shadow.stitchParamMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.stitchParamMsg
         * @static
         * @param {Shadow.IstitchParamMsg} message stitchParamMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        stitchParamMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.height != null && Object.hasOwnProperty.call(message, "height"))
                writer.uint32(/* id 1, wireType 5 =*/13).float(message.height);
            if (message.theta != null && Object.hasOwnProperty.call(message, "theta"))
                writer.uint32(/* id 2, wireType 5 =*/21).float(message.theta);
            if (message.shiftX != null && Object.hasOwnProperty.call(message, "shiftX"))
                writer.uint32(/* id 3, wireType 5 =*/29).float(message.shiftX);
            if (message.shiftZ != null && Object.hasOwnProperty.call(message, "shiftZ"))
                writer.uint32(/* id 4, wireType 5 =*/37).float(message.shiftZ);
            return writer;
        };

        /**
         * Encodes the specified stitchParamMsg message, length delimited. Does not implicitly {@link Shadow.stitchParamMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.stitchParamMsg
         * @static
         * @param {Shadow.IstitchParamMsg} message stitchParamMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        stitchParamMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a stitchParamMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.stitchParamMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.stitchParamMsg} stitchParamMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        stitchParamMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.stitchParamMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.height = reader.float();
                    break;
                case 2:
                    message.theta = reader.float();
                    break;
                case 3:
                    message.shiftX = reader.float();
                    break;
                case 4:
                    message.shiftZ = reader.float();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a stitchParamMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.stitchParamMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.stitchParamMsg} stitchParamMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        stitchParamMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a stitchParamMsg message.
         * @function verify
         * @memberof Shadow.stitchParamMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        stitchParamMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.height != null && message.hasOwnProperty("height"))
                if (typeof message.height !== "number")
                    return "height: number expected";
            if (message.theta != null && message.hasOwnProperty("theta"))
                if (typeof message.theta !== "number")
                    return "theta: number expected";
            if (message.shiftX != null && message.hasOwnProperty("shiftX"))
                if (typeof message.shiftX !== "number")
                    return "shiftX: number expected";
            if (message.shiftZ != null && message.hasOwnProperty("shiftZ"))
                if (typeof message.shiftZ !== "number")
                    return "shiftZ: number expected";
            return null;
        };

        /**
         * Creates a stitchParamMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.stitchParamMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.stitchParamMsg} stitchParamMsg
         */
        stitchParamMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.stitchParamMsg)
                return object;
            let message = new $root.Shadow.stitchParamMsg();
            if (object.height != null)
                message.height = Number(object.height);
            if (object.theta != null)
                message.theta = Number(object.theta);
            if (object.shiftX != null)
                message.shiftX = Number(object.shiftX);
            if (object.shiftZ != null)
                message.shiftZ = Number(object.shiftZ);
            return message;
        };

        /**
         * Creates a plain object from a stitchParamMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.stitchParamMsg
         * @static
         * @param {Shadow.stitchParamMsg} message stitchParamMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        stitchParamMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.height = 0;
                object.theta = 0;
                object.shiftX = 0;
                object.shiftZ = 0;
            }
            if (message.height != null && message.hasOwnProperty("height"))
                object.height = options.json && !isFinite(message.height) ? String(message.height) : message.height;
            if (message.theta != null && message.hasOwnProperty("theta"))
                object.theta = options.json && !isFinite(message.theta) ? String(message.theta) : message.theta;
            if (message.shiftX != null && message.hasOwnProperty("shiftX"))
                object.shiftX = options.json && !isFinite(message.shiftX) ? String(message.shiftX) : message.shiftX;
            if (message.shiftZ != null && message.hasOwnProperty("shiftZ"))
                object.shiftZ = options.json && !isFinite(message.shiftZ) ? String(message.shiftZ) : message.shiftZ;
            return object;
        };

        /**
         * Converts this stitchParamMsg to JSON.
         * @function toJSON
         * @memberof Shadow.stitchParamMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        stitchParamMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return stitchParamMsg;
    })();

    Shadow.healthMsg = (function() {

        /**
         * Properties of a healthMsg.
         * @memberof Shadow
         * @interface IhealthMsg
         * @property {number|null} [uptime] healthMsg uptime
         * @property {string|null} [rebootReason] healthMsg rebootReason
         */

        /**
         * Constructs a new healthMsg.
         * @memberof Shadow
         * @classdesc Represents a healthMsg.
         * @implements IhealthMsg
         * @constructor
         * @param {Shadow.IhealthMsg=} [properties] Properties to set
         */
        function healthMsg(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * healthMsg uptime.
         * @member {number} uptime
         * @memberof Shadow.healthMsg
         * @instance
         */
        healthMsg.prototype.uptime = 0;

        /**
         * healthMsg rebootReason.
         * @member {string} rebootReason
         * @memberof Shadow.healthMsg
         * @instance
         */
        healthMsg.prototype.rebootReason = "";

        /**
         * Creates a new healthMsg instance using the specified properties.
         * @function create
         * @memberof Shadow.healthMsg
         * @static
         * @param {Shadow.IhealthMsg=} [properties] Properties to set
         * @returns {Shadow.healthMsg} healthMsg instance
         */
        healthMsg.create = function create(properties) {
            return new healthMsg(properties);
        };

        /**
         * Encodes the specified healthMsg message. Does not implicitly {@link Shadow.healthMsg.verify|verify} messages.
         * @function encode
         * @memberof Shadow.healthMsg
         * @static
         * @param {Shadow.IhealthMsg} message healthMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        healthMsg.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uptime != null && Object.hasOwnProperty.call(message, "uptime"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.uptime);
            if (message.rebootReason != null && Object.hasOwnProperty.call(message, "rebootReason"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.rebootReason);
            return writer;
        };

        /**
         * Encodes the specified healthMsg message, length delimited. Does not implicitly {@link Shadow.healthMsg.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.healthMsg
         * @static
         * @param {Shadow.IhealthMsg} message healthMsg message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        healthMsg.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a healthMsg message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.healthMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.healthMsg} healthMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        healthMsg.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.healthMsg();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.uptime = reader.uint32();
                    break;
                case 2:
                    message.rebootReason = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a healthMsg message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.healthMsg
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.healthMsg} healthMsg
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        healthMsg.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a healthMsg message.
         * @function verify
         * @memberof Shadow.healthMsg
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        healthMsg.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uptime != null && message.hasOwnProperty("uptime"))
                if (!$util.isInteger(message.uptime))
                    return "uptime: integer expected";
            if (message.rebootReason != null && message.hasOwnProperty("rebootReason"))
                if (!$util.isString(message.rebootReason))
                    return "rebootReason: string expected";
            return null;
        };

        /**
         * Creates a healthMsg message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.healthMsg
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.healthMsg} healthMsg
         */
        healthMsg.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.healthMsg)
                return object;
            let message = new $root.Shadow.healthMsg();
            if (object.uptime != null)
                message.uptime = object.uptime >>> 0;
            if (object.rebootReason != null)
                message.rebootReason = String(object.rebootReason);
            return message;
        };

        /**
         * Creates a plain object from a healthMsg message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.healthMsg
         * @static
         * @param {Shadow.healthMsg} message healthMsg
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        healthMsg.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.uptime = 0;
                object.rebootReason = "";
            }
            if (message.uptime != null && message.hasOwnProperty("uptime"))
                object.uptime = message.uptime;
            if (message.rebootReason != null && message.hasOwnProperty("rebootReason"))
                object.rebootReason = message.rebootReason;
            return object;
        };

        /**
         * Converts this healthMsg to JSON.
         * @function toJSON
         * @memberof Shadow.healthMsg
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        healthMsg.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return healthMsg;
    })();

    Shadow.DeviceReq = (function() {

        /**
         * Properties of a DeviceReq.
         * @memberof Shadow
         * @interface IDeviceReq
         * @property {string|null} [token] DeviceReq token
         */

        /**
         * Constructs a new DeviceReq.
         * @memberof Shadow
         * @classdesc Represents a DeviceReq.
         * @implements IDeviceReq
         * @constructor
         * @param {Shadow.IDeviceReq=} [properties] Properties to set
         */
        function DeviceReq(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * DeviceReq token.
         * @member {string} token
         * @memberof Shadow.DeviceReq
         * @instance
         */
        DeviceReq.prototype.token = "";

        /**
         * Creates a new DeviceReq instance using the specified properties.
         * @function create
         * @memberof Shadow.DeviceReq
         * @static
         * @param {Shadow.IDeviceReq=} [properties] Properties to set
         * @returns {Shadow.DeviceReq} DeviceReq instance
         */
        DeviceReq.create = function create(properties) {
            return new DeviceReq(properties);
        };

        /**
         * Encodes the specified DeviceReq message. Does not implicitly {@link Shadow.DeviceReq.verify|verify} messages.
         * @function encode
         * @memberof Shadow.DeviceReq
         * @static
         * @param {Shadow.IDeviceReq} message DeviceReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DeviceReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.token != null && Object.hasOwnProperty.call(message, "token"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.token);
            return writer;
        };

        /**
         * Encodes the specified DeviceReq message, length delimited. Does not implicitly {@link Shadow.DeviceReq.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.DeviceReq
         * @static
         * @param {Shadow.IDeviceReq} message DeviceReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DeviceReq.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DeviceReq message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.DeviceReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.DeviceReq} DeviceReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DeviceReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.DeviceReq();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.token = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DeviceReq message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.DeviceReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.DeviceReq} DeviceReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DeviceReq.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DeviceReq message.
         * @function verify
         * @memberof Shadow.DeviceReq
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        DeviceReq.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.token != null && message.hasOwnProperty("token"))
                if (!$util.isString(message.token))
                    return "token: string expected";
            return null;
        };

        /**
         * Creates a DeviceReq message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.DeviceReq
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.DeviceReq} DeviceReq
         */
        DeviceReq.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.DeviceReq)
                return object;
            let message = new $root.Shadow.DeviceReq();
            if (object.token != null)
                message.token = String(object.token);
            return message;
        };

        /**
         * Creates a plain object from a DeviceReq message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.DeviceReq
         * @static
         * @param {Shadow.DeviceReq} message DeviceReq
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DeviceReq.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults)
                object.token = "";
            if (message.token != null && message.hasOwnProperty("token"))
                object.token = message.token;
            return object;
        };

        /**
         * Converts this DeviceReq to JSON.
         * @function toJSON
         * @memberof Shadow.DeviceReq
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        DeviceReq.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DeviceReq;
    })();

    Shadow.SetMsgReq = (function() {

        /**
         * Properties of a SetMsgReq.
         * @memberof Shadow
         * @interface ISetMsgReq
         * @property {string|null} [token] SetMsgReq token
         * @property {string|null} [msg] SetMsgReq msg
         */

        /**
         * Constructs a new SetMsgReq.
         * @memberof Shadow
         * @classdesc Represents a SetMsgReq.
         * @implements ISetMsgReq
         * @constructor
         * @param {Shadow.ISetMsgReq=} [properties] Properties to set
         */
        function SetMsgReq(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SetMsgReq token.
         * @member {string} token
         * @memberof Shadow.SetMsgReq
         * @instance
         */
        SetMsgReq.prototype.token = "";

        /**
         * SetMsgReq msg.
         * @member {string} msg
         * @memberof Shadow.SetMsgReq
         * @instance
         */
        SetMsgReq.prototype.msg = "";

        /**
         * Creates a new SetMsgReq instance using the specified properties.
         * @function create
         * @memberof Shadow.SetMsgReq
         * @static
         * @param {Shadow.ISetMsgReq=} [properties] Properties to set
         * @returns {Shadow.SetMsgReq} SetMsgReq instance
         */
        SetMsgReq.create = function create(properties) {
            return new SetMsgReq(properties);
        };

        /**
         * Encodes the specified SetMsgReq message. Does not implicitly {@link Shadow.SetMsgReq.verify|verify} messages.
         * @function encode
         * @memberof Shadow.SetMsgReq
         * @static
         * @param {Shadow.ISetMsgReq} message SetMsgReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetMsgReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.token != null && Object.hasOwnProperty.call(message, "token"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.token);
            if (message.msg != null && Object.hasOwnProperty.call(message, "msg"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.msg);
            return writer;
        };

        /**
         * Encodes the specified SetMsgReq message, length delimited. Does not implicitly {@link Shadow.SetMsgReq.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.SetMsgReq
         * @static
         * @param {Shadow.ISetMsgReq} message SetMsgReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetMsgReq.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SetMsgReq message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.SetMsgReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.SetMsgReq} SetMsgReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetMsgReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.SetMsgReq();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.token = reader.string();
                    break;
                case 2:
                    message.msg = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SetMsgReq message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.SetMsgReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.SetMsgReq} SetMsgReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetMsgReq.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SetMsgReq message.
         * @function verify
         * @memberof Shadow.SetMsgReq
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SetMsgReq.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.token != null && message.hasOwnProperty("token"))
                if (!$util.isString(message.token))
                    return "token: string expected";
            if (message.msg != null && message.hasOwnProperty("msg"))
                if (!$util.isString(message.msg))
                    return "msg: string expected";
            return null;
        };

        /**
         * Creates a SetMsgReq message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.SetMsgReq
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.SetMsgReq} SetMsgReq
         */
        SetMsgReq.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.SetMsgReq)
                return object;
            let message = new $root.Shadow.SetMsgReq();
            if (object.token != null)
                message.token = String(object.token);
            if (object.msg != null)
                message.msg = String(object.msg);
            return message;
        };

        /**
         * Creates a plain object from a SetMsgReq message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.SetMsgReq
         * @static
         * @param {Shadow.SetMsgReq} message SetMsgReq
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SetMsgReq.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.token = "";
                object.msg = "";
            }
            if (message.token != null && message.hasOwnProperty("token"))
                object.token = message.token;
            if (message.msg != null && message.hasOwnProperty("msg"))
                object.msg = message.msg;
            return object;
        };

        /**
         * Converts this SetMsgReq to JSON.
         * @function toJSON
         * @memberof Shadow.SetMsgReq
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SetMsgReq.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return SetMsgReq;
    })();

    Shadow.Res = (function() {

        /**
         * Properties of a Res.
         * @memberof Shadow
         * @interface IRes
         * @property {Shadow.Res.Status|null} [status] Res status
         * @property {string|null} [error] Res error
         */

        /**
         * Constructs a new Res.
         * @memberof Shadow
         * @classdesc Represents a Res.
         * @implements IRes
         * @constructor
         * @param {Shadow.IRes=} [properties] Properties to set
         */
        function Res(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Res status.
         * @member {Shadow.Res.Status} status
         * @memberof Shadow.Res
         * @instance
         */
        Res.prototype.status = 0;

        /**
         * Res error.
         * @member {string} error
         * @memberof Shadow.Res
         * @instance
         */
        Res.prototype.error = "";

        /**
         * Creates a new Res instance using the specified properties.
         * @function create
         * @memberof Shadow.Res
         * @static
         * @param {Shadow.IRes=} [properties] Properties to set
         * @returns {Shadow.Res} Res instance
         */
        Res.create = function create(properties) {
            return new Res(properties);
        };

        /**
         * Encodes the specified Res message. Does not implicitly {@link Shadow.Res.verify|verify} messages.
         * @function encode
         * @memberof Shadow.Res
         * @static
         * @param {Shadow.IRes} message Res message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Res.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.status != null && Object.hasOwnProperty.call(message, "status"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.status);
            if (message.error != null && Object.hasOwnProperty.call(message, "error"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.error);
            return writer;
        };

        /**
         * Encodes the specified Res message, length delimited. Does not implicitly {@link Shadow.Res.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Shadow.Res
         * @static
         * @param {Shadow.IRes} message Res message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Res.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Res message from the specified reader or buffer.
         * @function decode
         * @memberof Shadow.Res
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Shadow.Res} Res
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Res.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Shadow.Res();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.status = reader.int32();
                    break;
                case 2:
                    message.error = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Res message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Shadow.Res
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Shadow.Res} Res
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Res.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Res message.
         * @function verify
         * @memberof Shadow.Res
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Res.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.status != null && message.hasOwnProperty("status"))
                switch (message.status) {
                default:
                    return "status: enum value expected";
                case 0:
                case 1:
                    break;
                }
            if (message.error != null && message.hasOwnProperty("error"))
                if (!$util.isString(message.error))
                    return "error: string expected";
            return null;
        };

        /**
         * Creates a Res message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Shadow.Res
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Shadow.Res} Res
         */
        Res.fromObject = function fromObject(object) {
            if (object instanceof $root.Shadow.Res)
                return object;
            let message = new $root.Shadow.Res();
            switch (object.status) {
            case "OK":
            case 0:
                message.status = 0;
                break;
            case "Fail":
            case 1:
                message.status = 1;
                break;
            }
            if (object.error != null)
                message.error = String(object.error);
            return message;
        };

        /**
         * Creates a plain object from a Res message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Shadow.Res
         * @static
         * @param {Shadow.Res} message Res
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Res.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.status = options.enums === String ? "OK" : 0;
                object.error = "";
            }
            if (message.status != null && message.hasOwnProperty("status"))
                object.status = options.enums === String ? $root.Shadow.Res.Status[message.status] : message.status;
            if (message.error != null && message.hasOwnProperty("error"))
                object.error = message.error;
            return object;
        };

        /**
         * Converts this Res to JSON.
         * @function toJSON
         * @memberof Shadow.Res
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Res.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Status enum.
         * @name Shadow.Res.Status
         * @enum {number}
         * @property {number} OK=0 OK value
         * @property {number} Fail=1 Fail value
         */
        Res.Status = (function() {
            const valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "OK"] = 0;
            values[valuesById[1] = "Fail"] = 1;
            return values;
        })();

        return Res;
    })();

    Shadow.Service = (function() {

        /**
         * Constructs a new Service service.
         * @memberof Shadow
         * @classdesc Represents a Service
         * @extends $protobuf.rpc.Service
         * @constructor
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         */
        function Service(rpcImpl, requestDelimited, responseDelimited) {
            $protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
        }

        (Service.prototype = Object.create($protobuf.rpc.Service.prototype)).constructor = Service;

        /**
         * Creates new Service service using the specified rpc implementation.
         * @function create
         * @memberof Shadow.Service
         * @static
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         * @returns {Service} RPC service. Useful where requests and/or responses are streamed.
         */
        Service.create = function create(rpcImpl, requestDelimited, responseDelimited) {
            return new this(rpcImpl, requestDelimited, responseDelimited);
        };

        /**
         * Callback as used by {@link Shadow.Service#getMsg}.
         * @memberof Shadow.Service
         * @typedef GetMsgCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {Shadow.Msg} [response] Msg
         */

        /**
         * Calls GetMsg.
         * @function getMsg
         * @memberof Shadow.Service
         * @instance
         * @param {Shadow.IDeviceReq} request DeviceReq message or plain object
         * @param {Shadow.Service.GetMsgCallback} callback Node-style callback called with the error, if any, and Msg
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(Service.prototype.getMsg = function getMsg(request, callback) {
            return this.rpcCall(getMsg, $root.Shadow.DeviceReq, $root.Shadow.Msg, request, callback);
        }, "name", { value: "GetMsg" });

        /**
         * Calls GetMsg.
         * @function getMsg
         * @memberof Shadow.Service
         * @instance
         * @param {Shadow.IDeviceReq} request DeviceReq message or plain object
         * @returns {Promise<Shadow.Msg>} Promise
         * @variation 2
         */

        /**
         * Callback as used by {@link Shadow.Service#setMsg}.
         * @memberof Shadow.Service
         * @typedef SetMsgCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {Shadow.Res} [response] Res
         */

        /**
         * Calls SetMsg.
         * @function setMsg
         * @memberof Shadow.Service
         * @instance
         * @param {Shadow.ISetMsgReq} request SetMsgReq message or plain object
         * @param {Shadow.Service.SetMsgCallback} callback Node-style callback called with the error, if any, and Res
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(Service.prototype.setMsg = function setMsg(request, callback) {
            return this.rpcCall(setMsg, $root.Shadow.SetMsgReq, $root.Shadow.Res, request, callback);
        }, "name", { value: "SetMsg" });

        /**
         * Calls SetMsg.
         * @function setMsg
         * @memberof Shadow.Service
         * @instance
         * @param {Shadow.ISetMsgReq} request SetMsgReq message or plain object
         * @returns {Promise<Shadow.Res>} Promise
         * @variation 2
         */

        return Service;
    })();

    return Shadow;
})();

export { $root as default };
