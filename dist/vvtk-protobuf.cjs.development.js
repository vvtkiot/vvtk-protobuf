'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var $protobuf = require('protobufjs/minimal');

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader = $protobuf.Reader,
      $Writer = $protobuf.Writer,
      $util = $protobuf.util; // Exported root namespace

const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Event = $root.Event = /*#__PURE__*/(() => {
  /**
   * Namespace Event.
   * @exports Event
   * @namespace
   */
  const Event = {};

  Event.Msg = /*#__PURE__*/function () {
    /**
     * Properties of a Msg.
     * @memberof Event
     * @interface IMsg
     * @property {number|null} [timestamp] Msg timestamp
     * @property {Event.Msg.Type|null} [type] Msg type
     */

    /**
     * Constructs a new Msg.
     * @memberof Event
     * @classdesc Represents a Msg.
     * @implements IMsg
     * @constructor
     * @param {Event.IMsg=} [properties] Properties to set
     */
    function Msg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Msg timestamp.
     * @member {number} timestamp
     * @memberof Event.Msg
     * @instance
     */


    Msg.prototype.timestamp = 0;
    /**
     * Msg type.
     * @member {Event.Msg.Type} type
     * @memberof Event.Msg
     * @instance
     */

    Msg.prototype.type = 0;
    /**
     * Creates a new Msg instance using the specified properties.
     * @function create
     * @memberof Event.Msg
     * @static
     * @param {Event.IMsg=} [properties] Properties to set
     * @returns {Event.Msg} Msg instance
     */

    Msg.create = function create(properties) {
      return new Msg(properties);
    };
    /**
     * Encodes the specified Msg message. Does not implicitly {@link Event.Msg.verify|verify} messages.
     * @function encode
     * @memberof Event.Msg
     * @static
     * @param {Event.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.timestamp);
      if (message.type != null && Object.hasOwnProperty.call(message, "type")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).int32(message.type);
      return writer;
    };
    /**
     * Encodes the specified Msg message, length delimited. Does not implicitly {@link Event.Msg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Event.Msg
     * @static
     * @param {Event.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Msg message from the specified reader or buffer.
     * @function decode
     * @memberof Event.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Event.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Event.Msg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.timestamp = reader.uint32();
            break;

          case 2:
            message.type = reader.int32();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Msg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Event.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Event.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Msg message.
     * @function verify
     * @memberof Event.Msg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Msg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util.isInteger(message.timestamp)) return "timestamp: integer expected";
      if (message.type != null && message.hasOwnProperty("type")) switch (message.type) {
        default:
          return "type: enum value expected";

        case 0:
        case 1:
          break;
      }
      return null;
    };
    /**
     * Creates a Msg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Event.Msg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Event.Msg} Msg
     */


    Msg.fromObject = function fromObject(object) {
      if (object instanceof $root.Event.Msg) return object;
      let message = new $root.Event.Msg();
      if (object.timestamp != null) message.timestamp = object.timestamp >>> 0;

      switch (object.type) {
        case "NONE":
        case 0:
          message.type = 0;
          break;

        case "EMERGENCE":
        case 1:
          message.type = 1;
          break;
      }

      return message;
    };
    /**
     * Creates a plain object from a Msg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Event.Msg
     * @static
     * @param {Event.Msg} message Msg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Msg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.timestamp = 0;
        object.type = options.enums === String ? "NONE" : 0;
      }

      if (message.timestamp != null && message.hasOwnProperty("timestamp")) object.timestamp = message.timestamp;
      if (message.type != null && message.hasOwnProperty("type")) object.type = options.enums === String ? $root.Event.Msg.Type[message.type] : message.type;
      return object;
    };
    /**
     * Converts this Msg to JSON.
     * @function toJSON
     * @memberof Event.Msg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Msg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * Type enum.
     * @name Event.Msg.Type
     * @enum {number}
     * @property {number} NONE=0 NONE value
     * @property {number} EMERGENCE=1 EMERGENCE value
     */


    Msg.Type = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "NONE"] = 0;
      values[valuesById[1] = "EMERGENCE"] = 1;
      return values;
    }();

    return Msg;
  }();

  return Event;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$1 = $protobuf.Reader,
      $Writer$1 = $protobuf.Writer,
      $util$1 = $protobuf.util; // Exported root namespace

const $root$1 = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Livestream = $root$1.Livestream = /*#__PURE__*/(() => {
  /**
   * Namespace Livestream.
   * @exports Livestream
   * @namespace
   */
  const Livestream = {};

  Livestream.Msg = /*#__PURE__*/function () {
    /**
     * Properties of a Msg.
     * @memberof Livestream
     * @interface IMsg
     * @property {number|null} [timestamp] Msg timestamp
     * @property {Livestream.Msg.Type|null} [type] Msg type
     * @property {string|null} [streamid] Msg streamid
     * @property {string|null} [ip] Msg ip
     * @property {string|null} [audioPort] Msg audioPort
     * @property {string|null} [videoPort] Msg videoPort
     * @property {string|null} [videoSource] Msg videoSource
     */

    /**
     * Constructs a new Msg.
     * @memberof Livestream
     * @classdesc Represents a Msg.
     * @implements IMsg
     * @constructor
     * @param {Livestream.IMsg=} [properties] Properties to set
     */
    function Msg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Msg timestamp.
     * @member {number} timestamp
     * @memberof Livestream.Msg
     * @instance
     */


    Msg.prototype.timestamp = 0;
    /**
     * Msg type.
     * @member {Livestream.Msg.Type} type
     * @memberof Livestream.Msg
     * @instance
     */

    Msg.prototype.type = 0;
    /**
     * Msg streamid.
     * @member {string} streamid
     * @memberof Livestream.Msg
     * @instance
     */

    Msg.prototype.streamid = "";
    /**
     * Msg ip.
     * @member {string} ip
     * @memberof Livestream.Msg
     * @instance
     */

    Msg.prototype.ip = "";
    /**
     * Msg audioPort.
     * @member {string} audioPort
     * @memberof Livestream.Msg
     * @instance
     */

    Msg.prototype.audioPort = "";
    /**
     * Msg videoPort.
     * @member {string} videoPort
     * @memberof Livestream.Msg
     * @instance
     */

    Msg.prototype.videoPort = "";
    /**
     * Msg videoSource.
     * @member {string} videoSource
     * @memberof Livestream.Msg
     * @instance
     */

    Msg.prototype.videoSource = "";
    /**
     * Creates a new Msg instance using the specified properties.
     * @function create
     * @memberof Livestream.Msg
     * @static
     * @param {Livestream.IMsg=} [properties] Properties to set
     * @returns {Livestream.Msg} Msg instance
     */

    Msg.create = function create(properties) {
      return new Msg(properties);
    };
    /**
     * Encodes the specified Msg message. Does not implicitly {@link Livestream.Msg.verify|verify} messages.
     * @function encode
     * @memberof Livestream.Msg
     * @static
     * @param {Livestream.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$1.create();
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.timestamp);
      if (message.type != null && Object.hasOwnProperty.call(message, "type")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).int32(message.type);
      if (message.streamid != null && Object.hasOwnProperty.call(message, "streamid")) writer.uint32(
      /* id 3, wireType 2 =*/
      26).string(message.streamid);
      if (message.ip != null && Object.hasOwnProperty.call(message, "ip")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).string(message.ip);
      if (message.audioPort != null && Object.hasOwnProperty.call(message, "audioPort")) writer.uint32(
      /* id 5, wireType 2 =*/
      42).string(message.audioPort);
      if (message.videoPort != null && Object.hasOwnProperty.call(message, "videoPort")) writer.uint32(
      /* id 6, wireType 2 =*/
      50).string(message.videoPort);
      if (message.videoSource != null && Object.hasOwnProperty.call(message, "videoSource")) writer.uint32(
      /* id 7, wireType 2 =*/
      58).string(message.videoSource);
      return writer;
    };
    /**
     * Encodes the specified Msg message, length delimited. Does not implicitly {@link Livestream.Msg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Livestream.Msg
     * @static
     * @param {Livestream.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Msg message from the specified reader or buffer.
     * @function decode
     * @memberof Livestream.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Livestream.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$1)) reader = $Reader$1.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$1.Livestream.Msg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.timestamp = reader.uint32();
            break;

          case 2:
            message.type = reader.int32();
            break;

          case 3:
            message.streamid = reader.string();
            break;

          case 4:
            message.ip = reader.string();
            break;

          case 5:
            message.audioPort = reader.string();
            break;

          case 6:
            message.videoPort = reader.string();
            break;

          case 7:
            message.videoSource = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Msg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Livestream.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Livestream.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$1)) reader = new $Reader$1(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Msg message.
     * @function verify
     * @memberof Livestream.Msg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Msg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$1.isInteger(message.timestamp)) return "timestamp: integer expected";
      if (message.type != null && message.hasOwnProperty("type")) switch (message.type) {
        default:
          return "type: enum value expected";

        case 0:
        case 1:
        case 2:
          break;
      }
      if (message.streamid != null && message.hasOwnProperty("streamid")) if (!$util$1.isString(message.streamid)) return "streamid: string expected";
      if (message.ip != null && message.hasOwnProperty("ip")) if (!$util$1.isString(message.ip)) return "ip: string expected";
      if (message.audioPort != null && message.hasOwnProperty("audioPort")) if (!$util$1.isString(message.audioPort)) return "audioPort: string expected";
      if (message.videoPort != null && message.hasOwnProperty("videoPort")) if (!$util$1.isString(message.videoPort)) return "videoPort: string expected";
      if (message.videoSource != null && message.hasOwnProperty("videoSource")) if (!$util$1.isString(message.videoSource)) return "videoSource: string expected";
      return null;
    };
    /**
     * Creates a Msg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Livestream.Msg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Livestream.Msg} Msg
     */


    Msg.fromObject = function fromObject(object) {
      if (object instanceof $root$1.Livestream.Msg) return object;
      let message = new $root$1.Livestream.Msg();
      if (object.timestamp != null) message.timestamp = object.timestamp >>> 0;

      switch (object.type) {
        case "PLAY":
        case 0:
          message.type = 0;
          break;

        case "STOP":
        case 1:
          message.type = 1;
          break;

        case "KEEPALIVE":
        case 2:
          message.type = 2;
          break;
      }

      if (object.streamid != null) message.streamid = String(object.streamid);
      if (object.ip != null) message.ip = String(object.ip);
      if (object.audioPort != null) message.audioPort = String(object.audioPort);
      if (object.videoPort != null) message.videoPort = String(object.videoPort);
      if (object.videoSource != null) message.videoSource = String(object.videoSource);
      return message;
    };
    /**
     * Creates a plain object from a Msg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Livestream.Msg
     * @static
     * @param {Livestream.Msg} message Msg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Msg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.timestamp = 0;
        object.type = options.enums === String ? "PLAY" : 0;
        object.streamid = "";
        object.ip = "";
        object.audioPort = "";
        object.videoPort = "";
        object.videoSource = "";
      }

      if (message.timestamp != null && message.hasOwnProperty("timestamp")) object.timestamp = message.timestamp;
      if (message.type != null && message.hasOwnProperty("type")) object.type = options.enums === String ? $root$1.Livestream.Msg.Type[message.type] : message.type;
      if (message.streamid != null && message.hasOwnProperty("streamid")) object.streamid = message.streamid;
      if (message.ip != null && message.hasOwnProperty("ip")) object.ip = message.ip;
      if (message.audioPort != null && message.hasOwnProperty("audioPort")) object.audioPort = message.audioPort;
      if (message.videoPort != null && message.hasOwnProperty("videoPort")) object.videoPort = message.videoPort;
      if (message.videoSource != null && message.hasOwnProperty("videoSource")) object.videoSource = message.videoSource;
      return object;
    };
    /**
     * Converts this Msg to JSON.
     * @function toJSON
     * @memberof Livestream.Msg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Msg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * Type enum.
     * @name Livestream.Msg.Type
     * @enum {number}
     * @property {number} PLAY=0 PLAY value
     * @property {number} STOP=1 STOP value
     * @property {number} KEEPALIVE=2 KEEPALIVE value
     */


    Msg.Type = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "PLAY"] = 0;
      values[valuesById[1] = "STOP"] = 1;
      values[valuesById[2] = "KEEPALIVE"] = 2;
      return values;
    }();

    return Msg;
  }();

  return Livestream;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$2 = $protobuf.Reader,
      $Writer$2 = $protobuf.Writer,
      $util$2 = $protobuf.util; // Exported root namespace

const $root$2 = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Shadow = $root$2.Shadow = /*#__PURE__*/(() => {
  /**
   * Namespace Shadow.
   * @exports Shadow
   * @namespace
   */
  const Shadow = {};

  Shadow.Msg = /*#__PURE__*/function () {
    /**
     * Properties of a Msg.
     * @memberof Shadow
     * @interface IMsg
     * @property {Shadow.IstateMsg|null} [state] Msg state
     */

    /**
     * Constructs a new Msg.
     * @memberof Shadow
     * @classdesc Represents a Msg.
     * @implements IMsg
     * @constructor
     * @param {Shadow.IMsg=} [properties] Properties to set
     */
    function Msg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Msg state.
     * @member {Shadow.IstateMsg|null|undefined} state
     * @memberof Shadow.Msg
     * @instance
     */


    Msg.prototype.state = null;
    /**
     * Creates a new Msg instance using the specified properties.
     * @function create
     * @memberof Shadow.Msg
     * @static
     * @param {Shadow.IMsg=} [properties] Properties to set
     * @returns {Shadow.Msg} Msg instance
     */

    Msg.create = function create(properties) {
      return new Msg(properties);
    };
    /**
     * Encodes the specified Msg message. Does not implicitly {@link Shadow.Msg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.Msg
     * @static
     * @param {Shadow.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.state != null && Object.hasOwnProperty.call(message, "state")) $root$2.Shadow.stateMsg.encode(message.state, writer.uint32(
      /* id 1, wireType 2 =*/
      10).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified Msg message, length delimited. Does not implicitly {@link Shadow.Msg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.Msg
     * @static
     * @param {Shadow.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Msg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.Msg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.state = $root$2.Shadow.stateMsg.decode(reader, reader.uint32());
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Msg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Msg message.
     * @function verify
     * @memberof Shadow.Msg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Msg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";

      if (message.state != null && message.hasOwnProperty("state")) {
        let error = $root$2.Shadow.stateMsg.verify(message.state);
        if (error) return "state." + error;
      }

      return null;
    };
    /**
     * Creates a Msg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.Msg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.Msg} Msg
     */


    Msg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.Msg) return object;
      let message = new $root$2.Shadow.Msg();

      if (object.state != null) {
        if (typeof object.state !== "object") throw TypeError(".Shadow.Msg.state: object expected");
        message.state = $root$2.Shadow.stateMsg.fromObject(object.state);
      }

      return message;
    };
    /**
     * Creates a plain object from a Msg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.Msg
     * @static
     * @param {Shadow.Msg} message Msg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Msg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.defaults) object.state = null;
      if (message.state != null && message.hasOwnProperty("state")) object.state = $root$2.Shadow.stateMsg.toObject(message.state, options);
      return object;
    };
    /**
     * Converts this Msg to JSON.
     * @function toJSON
     * @memberof Shadow.Msg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Msg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Msg;
  }();

  Shadow.stateMsg = /*#__PURE__*/function () {
    /**
     * Properties of a stateMsg.
     * @memberof Shadow
     * @interface IstateMsg
     * @property {string|null} [id] stateMsg id
     * @property {Shadow.IdesiredMsg|null} [desired] stateMsg desired
     * @property {Shadow.IreportedMsg|null} [reported] stateMsg reported
     */

    /**
     * Constructs a new stateMsg.
     * @memberof Shadow
     * @classdesc Represents a stateMsg.
     * @implements IstateMsg
     * @constructor
     * @param {Shadow.IstateMsg=} [properties] Properties to set
     */
    function stateMsg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * stateMsg id.
     * @member {string} id
     * @memberof Shadow.stateMsg
     * @instance
     */


    stateMsg.prototype.id = "";
    /**
     * stateMsg desired.
     * @member {Shadow.IdesiredMsg|null|undefined} desired
     * @memberof Shadow.stateMsg
     * @instance
     */

    stateMsg.prototype.desired = null;
    /**
     * stateMsg reported.
     * @member {Shadow.IreportedMsg|null|undefined} reported
     * @memberof Shadow.stateMsg
     * @instance
     */

    stateMsg.prototype.reported = null;
    /**
     * Creates a new stateMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.stateMsg
     * @static
     * @param {Shadow.IstateMsg=} [properties] Properties to set
     * @returns {Shadow.stateMsg} stateMsg instance
     */

    stateMsg.create = function create(properties) {
      return new stateMsg(properties);
    };
    /**
     * Encodes the specified stateMsg message. Does not implicitly {@link Shadow.stateMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.stateMsg
     * @static
     * @param {Shadow.IstateMsg} message stateMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    stateMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.id != null && Object.hasOwnProperty.call(message, "id")) writer.uint32(
      /* id 1, wireType 2 =*/
      10).string(message.id);
      if (message.desired != null && Object.hasOwnProperty.call(message, "desired")) $root$2.Shadow.desiredMsg.encode(message.desired, writer.uint32(
      /* id 2, wireType 2 =*/
      18).fork()).ldelim();
      if (message.reported != null && Object.hasOwnProperty.call(message, "reported")) $root$2.Shadow.reportedMsg.encode(message.reported, writer.uint32(
      /* id 3, wireType 2 =*/
      26).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified stateMsg message, length delimited. Does not implicitly {@link Shadow.stateMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.stateMsg
     * @static
     * @param {Shadow.IstateMsg} message stateMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    stateMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a stateMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.stateMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.stateMsg} stateMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    stateMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.stateMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.id = reader.string();
            break;

          case 2:
            message.desired = $root$2.Shadow.desiredMsg.decode(reader, reader.uint32());
            break;

          case 3:
            message.reported = $root$2.Shadow.reportedMsg.decode(reader, reader.uint32());
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a stateMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.stateMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.stateMsg} stateMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    stateMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a stateMsg message.
     * @function verify
     * @memberof Shadow.stateMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    stateMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.id != null && message.hasOwnProperty("id")) if (!$util$2.isString(message.id)) return "id: string expected";

      if (message.desired != null && message.hasOwnProperty("desired")) {
        let error = $root$2.Shadow.desiredMsg.verify(message.desired);
        if (error) return "desired." + error;
      }

      if (message.reported != null && message.hasOwnProperty("reported")) {
        let error = $root$2.Shadow.reportedMsg.verify(message.reported);
        if (error) return "reported." + error;
      }

      return null;
    };
    /**
     * Creates a stateMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.stateMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.stateMsg} stateMsg
     */


    stateMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.stateMsg) return object;
      let message = new $root$2.Shadow.stateMsg();
      if (object.id != null) message.id = String(object.id);

      if (object.desired != null) {
        if (typeof object.desired !== "object") throw TypeError(".Shadow.stateMsg.desired: object expected");
        message.desired = $root$2.Shadow.desiredMsg.fromObject(object.desired);
      }

      if (object.reported != null) {
        if (typeof object.reported !== "object") throw TypeError(".Shadow.stateMsg.reported: object expected");
        message.reported = $root$2.Shadow.reportedMsg.fromObject(object.reported);
      }

      return message;
    };
    /**
     * Creates a plain object from a stateMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.stateMsg
     * @static
     * @param {Shadow.stateMsg} message stateMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    stateMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.id = "";
        object.desired = null;
        object.reported = null;
      }

      if (message.id != null && message.hasOwnProperty("id")) object.id = message.id;
      if (message.desired != null && message.hasOwnProperty("desired")) object.desired = $root$2.Shadow.desiredMsg.toObject(message.desired, options);
      if (message.reported != null && message.hasOwnProperty("reported")) object.reported = $root$2.Shadow.reportedMsg.toObject(message.reported, options);
      return object;
    };
    /**
     * Converts this stateMsg to JSON.
     * @function toJSON
     * @memberof Shadow.stateMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    stateMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return stateMsg;
  }();

  Shadow.desiredMsg = /*#__PURE__*/function () {
    /**
     * Properties of a desiredMsg.
     * @memberof Shadow
     * @interface IdesiredMsg
     * @property {Array.<Shadow.IvideoSource>|null} [videoSources] desiredMsg videoSources
     * @property {Shadow.IversionMsg|null} [versionInfo] desiredMsg versionInfo
     * @property {Shadow.IavsMsg|null} [avs] desiredMsg avs
     * @property {boolean|null} [autoUpgrade] desiredMsg autoUpgrade
     * @property {Shadow.IcalibrationMsg|null} [calibration] desiredMsg calibration
     * @property {Shadow.IstitchParamMsg|null} [stitchParam] desiredMsg stitchParam
     * @property {Shadow.IchripParamMsg|null} [chripParam] desiredMsg chripParam
     */

    /**
     * Constructs a new desiredMsg.
     * @memberof Shadow
     * @classdesc Represents a desiredMsg.
     * @implements IdesiredMsg
     * @constructor
     * @param {Shadow.IdesiredMsg=} [properties] Properties to set
     */
    function desiredMsg(properties) {
      this.videoSources = [];
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * desiredMsg videoSources.
     * @member {Array.<Shadow.IvideoSource>} videoSources
     * @memberof Shadow.desiredMsg
     * @instance
     */


    desiredMsg.prototype.videoSources = $util$2.emptyArray;
    /**
     * desiredMsg versionInfo.
     * @member {Shadow.IversionMsg|null|undefined} versionInfo
     * @memberof Shadow.desiredMsg
     * @instance
     */

    desiredMsg.prototype.versionInfo = null;
    /**
     * desiredMsg avs.
     * @member {Shadow.IavsMsg|null|undefined} avs
     * @memberof Shadow.desiredMsg
     * @instance
     */

    desiredMsg.prototype.avs = null;
    /**
     * desiredMsg autoUpgrade.
     * @member {boolean} autoUpgrade
     * @memberof Shadow.desiredMsg
     * @instance
     */

    desiredMsg.prototype.autoUpgrade = false;
    /**
     * desiredMsg calibration.
     * @member {Shadow.IcalibrationMsg|null|undefined} calibration
     * @memberof Shadow.desiredMsg
     * @instance
     */

    desiredMsg.prototype.calibration = null;
    /**
     * desiredMsg stitchParam.
     * @member {Shadow.IstitchParamMsg|null|undefined} stitchParam
     * @memberof Shadow.desiredMsg
     * @instance
     */

    desiredMsg.prototype.stitchParam = null;
    /**
     * desiredMsg chripParam.
     * @member {Shadow.IchripParamMsg|null|undefined} chripParam
     * @memberof Shadow.desiredMsg
     * @instance
     */

    desiredMsg.prototype.chripParam = null;
    /**
     * Creates a new desiredMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.desiredMsg
     * @static
     * @param {Shadow.IdesiredMsg=} [properties] Properties to set
     * @returns {Shadow.desiredMsg} desiredMsg instance
     */

    desiredMsg.create = function create(properties) {
      return new desiredMsg(properties);
    };
    /**
     * Encodes the specified desiredMsg message. Does not implicitly {@link Shadow.desiredMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.desiredMsg
     * @static
     * @param {Shadow.IdesiredMsg} message desiredMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    desiredMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.videoSources != null && message.videoSources.length) for (let i = 0; i < message.videoSources.length; ++i) $root$2.Shadow.videoSource.encode(message.videoSources[i], writer.uint32(
      /* id 1, wireType 2 =*/
      10).fork()).ldelim();
      if (message.versionInfo != null && Object.hasOwnProperty.call(message, "versionInfo")) $root$2.Shadow.versionMsg.encode(message.versionInfo, writer.uint32(
      /* id 2, wireType 2 =*/
      18).fork()).ldelim();
      if (message.avs != null && Object.hasOwnProperty.call(message, "avs")) $root$2.Shadow.avsMsg.encode(message.avs, writer.uint32(
      /* id 3, wireType 2 =*/
      26).fork()).ldelim();
      if (message.autoUpgrade != null && Object.hasOwnProperty.call(message, "autoUpgrade")) writer.uint32(
      /* id 4, wireType 0 =*/
      32).bool(message.autoUpgrade);
      if (message.calibration != null && Object.hasOwnProperty.call(message, "calibration")) $root$2.Shadow.calibrationMsg.encode(message.calibration, writer.uint32(
      /* id 5, wireType 2 =*/
      42).fork()).ldelim();
      if (message.stitchParam != null && Object.hasOwnProperty.call(message, "stitchParam")) $root$2.Shadow.stitchParamMsg.encode(message.stitchParam, writer.uint32(
      /* id 6, wireType 2 =*/
      50).fork()).ldelim();
      if (message.chripParam != null && Object.hasOwnProperty.call(message, "chripParam")) $root$2.Shadow.chripParamMsg.encode(message.chripParam, writer.uint32(
      /* id 7, wireType 2 =*/
      58).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified desiredMsg message, length delimited. Does not implicitly {@link Shadow.desiredMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.desiredMsg
     * @static
     * @param {Shadow.IdesiredMsg} message desiredMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    desiredMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a desiredMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.desiredMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.desiredMsg} desiredMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    desiredMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.desiredMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            if (!(message.videoSources && message.videoSources.length)) message.videoSources = [];
            message.videoSources.push($root$2.Shadow.videoSource.decode(reader, reader.uint32()));
            break;

          case 2:
            message.versionInfo = $root$2.Shadow.versionMsg.decode(reader, reader.uint32());
            break;

          case 3:
            message.avs = $root$2.Shadow.avsMsg.decode(reader, reader.uint32());
            break;

          case 4:
            message.autoUpgrade = reader.bool();
            break;

          case 5:
            message.calibration = $root$2.Shadow.calibrationMsg.decode(reader, reader.uint32());
            break;

          case 6:
            message.stitchParam = $root$2.Shadow.stitchParamMsg.decode(reader, reader.uint32());
            break;

          case 7:
            message.chripParam = $root$2.Shadow.chripParamMsg.decode(reader, reader.uint32());
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a desiredMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.desiredMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.desiredMsg} desiredMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    desiredMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a desiredMsg message.
     * @function verify
     * @memberof Shadow.desiredMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    desiredMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";

      if (message.videoSources != null && message.hasOwnProperty("videoSources")) {
        if (!Array.isArray(message.videoSources)) return "videoSources: array expected";

        for (let i = 0; i < message.videoSources.length; ++i) {
          let error = $root$2.Shadow.videoSource.verify(message.videoSources[i]);
          if (error) return "videoSources." + error;
        }
      }

      if (message.versionInfo != null && message.hasOwnProperty("versionInfo")) {
        let error = $root$2.Shadow.versionMsg.verify(message.versionInfo);
        if (error) return "versionInfo." + error;
      }

      if (message.avs != null && message.hasOwnProperty("avs")) {
        let error = $root$2.Shadow.avsMsg.verify(message.avs);
        if (error) return "avs." + error;
      }

      if (message.autoUpgrade != null && message.hasOwnProperty("autoUpgrade")) if (typeof message.autoUpgrade !== "boolean") return "autoUpgrade: boolean expected";

      if (message.calibration != null && message.hasOwnProperty("calibration")) {
        let error = $root$2.Shadow.calibrationMsg.verify(message.calibration);
        if (error) return "calibration." + error;
      }

      if (message.stitchParam != null && message.hasOwnProperty("stitchParam")) {
        let error = $root$2.Shadow.stitchParamMsg.verify(message.stitchParam);
        if (error) return "stitchParam." + error;
      }

      if (message.chripParam != null && message.hasOwnProperty("chripParam")) {
        let error = $root$2.Shadow.chripParamMsg.verify(message.chripParam);
        if (error) return "chripParam." + error;
      }

      return null;
    };
    /**
     * Creates a desiredMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.desiredMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.desiredMsg} desiredMsg
     */


    desiredMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.desiredMsg) return object;
      let message = new $root$2.Shadow.desiredMsg();

      if (object.videoSources) {
        if (!Array.isArray(object.videoSources)) throw TypeError(".Shadow.desiredMsg.videoSources: array expected");
        message.videoSources = [];

        for (let i = 0; i < object.videoSources.length; ++i) {
          if (typeof object.videoSources[i] !== "object") throw TypeError(".Shadow.desiredMsg.videoSources: object expected");
          message.videoSources[i] = $root$2.Shadow.videoSource.fromObject(object.videoSources[i]);
        }
      }

      if (object.versionInfo != null) {
        if (typeof object.versionInfo !== "object") throw TypeError(".Shadow.desiredMsg.versionInfo: object expected");
        message.versionInfo = $root$2.Shadow.versionMsg.fromObject(object.versionInfo);
      }

      if (object.avs != null) {
        if (typeof object.avs !== "object") throw TypeError(".Shadow.desiredMsg.avs: object expected");
        message.avs = $root$2.Shadow.avsMsg.fromObject(object.avs);
      }

      if (object.autoUpgrade != null) message.autoUpgrade = Boolean(object.autoUpgrade);

      if (object.calibration != null) {
        if (typeof object.calibration !== "object") throw TypeError(".Shadow.desiredMsg.calibration: object expected");
        message.calibration = $root$2.Shadow.calibrationMsg.fromObject(object.calibration);
      }

      if (object.stitchParam != null) {
        if (typeof object.stitchParam !== "object") throw TypeError(".Shadow.desiredMsg.stitchParam: object expected");
        message.stitchParam = $root$2.Shadow.stitchParamMsg.fromObject(object.stitchParam);
      }

      if (object.chripParam != null) {
        if (typeof object.chripParam !== "object") throw TypeError(".Shadow.desiredMsg.chripParam: object expected");
        message.chripParam = $root$2.Shadow.chripParamMsg.fromObject(object.chripParam);
      }

      return message;
    };
    /**
     * Creates a plain object from a desiredMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.desiredMsg
     * @static
     * @param {Shadow.desiredMsg} message desiredMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    desiredMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.arrays || options.defaults) object.videoSources = [];

      if (options.defaults) {
        object.versionInfo = null;
        object.avs = null;
        object.autoUpgrade = false;
        object.calibration = null;
        object.stitchParam = null;
        object.chripParam = null;
      }

      if (message.videoSources && message.videoSources.length) {
        object.videoSources = [];

        for (let j = 0; j < message.videoSources.length; ++j) object.videoSources[j] = $root$2.Shadow.videoSource.toObject(message.videoSources[j], options);
      }

      if (message.versionInfo != null && message.hasOwnProperty("versionInfo")) object.versionInfo = $root$2.Shadow.versionMsg.toObject(message.versionInfo, options);
      if (message.avs != null && message.hasOwnProperty("avs")) object.avs = $root$2.Shadow.avsMsg.toObject(message.avs, options);
      if (message.autoUpgrade != null && message.hasOwnProperty("autoUpgrade")) object.autoUpgrade = message.autoUpgrade;
      if (message.calibration != null && message.hasOwnProperty("calibration")) object.calibration = $root$2.Shadow.calibrationMsg.toObject(message.calibration, options);
      if (message.stitchParam != null && message.hasOwnProperty("stitchParam")) object.stitchParam = $root$2.Shadow.stitchParamMsg.toObject(message.stitchParam, options);
      if (message.chripParam != null && message.hasOwnProperty("chripParam")) object.chripParam = $root$2.Shadow.chripParamMsg.toObject(message.chripParam, options);
      return object;
    };
    /**
     * Converts this desiredMsg to JSON.
     * @function toJSON
     * @memberof Shadow.desiredMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    desiredMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return desiredMsg;
  }();

  Shadow.videoSource = /*#__PURE__*/function () {
    /**
     * Properties of a videoSource.
     * @memberof Shadow
     * @interface IvideoSource
     * @property {string|null} [id] videoSource id
     * @property {string|null} [password] videoSource password
     */

    /**
     * Constructs a new videoSource.
     * @memberof Shadow
     * @classdesc Represents a videoSource.
     * @implements IvideoSource
     * @constructor
     * @param {Shadow.IvideoSource=} [properties] Properties to set
     */
    function videoSource(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * videoSource id.
     * @member {string} id
     * @memberof Shadow.videoSource
     * @instance
     */


    videoSource.prototype.id = "";
    /**
     * videoSource password.
     * @member {string} password
     * @memberof Shadow.videoSource
     * @instance
     */

    videoSource.prototype.password = "";
    /**
     * Creates a new videoSource instance using the specified properties.
     * @function create
     * @memberof Shadow.videoSource
     * @static
     * @param {Shadow.IvideoSource=} [properties] Properties to set
     * @returns {Shadow.videoSource} videoSource instance
     */

    videoSource.create = function create(properties) {
      return new videoSource(properties);
    };
    /**
     * Encodes the specified videoSource message. Does not implicitly {@link Shadow.videoSource.verify|verify} messages.
     * @function encode
     * @memberof Shadow.videoSource
     * @static
     * @param {Shadow.IvideoSource} message videoSource message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    videoSource.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.id != null && Object.hasOwnProperty.call(message, "id")) writer.uint32(
      /* id 1, wireType 2 =*/
      10).string(message.id);
      if (message.password != null && Object.hasOwnProperty.call(message, "password")) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.password);
      return writer;
    };
    /**
     * Encodes the specified videoSource message, length delimited. Does not implicitly {@link Shadow.videoSource.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.videoSource
     * @static
     * @param {Shadow.IvideoSource} message videoSource message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    videoSource.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a videoSource message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.videoSource
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.videoSource} videoSource
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    videoSource.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.videoSource();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.id = reader.string();
            break;

          case 2:
            message.password = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a videoSource message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.videoSource
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.videoSource} videoSource
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    videoSource.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a videoSource message.
     * @function verify
     * @memberof Shadow.videoSource
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    videoSource.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.id != null && message.hasOwnProperty("id")) if (!$util$2.isString(message.id)) return "id: string expected";
      if (message.password != null && message.hasOwnProperty("password")) if (!$util$2.isString(message.password)) return "password: string expected";
      return null;
    };
    /**
     * Creates a videoSource message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.videoSource
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.videoSource} videoSource
     */


    videoSource.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.videoSource) return object;
      let message = new $root$2.Shadow.videoSource();
      if (object.id != null) message.id = String(object.id);
      if (object.password != null) message.password = String(object.password);
      return message;
    };
    /**
     * Creates a plain object from a videoSource message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.videoSource
     * @static
     * @param {Shadow.videoSource} message videoSource
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    videoSource.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.id = "";
        object.password = "";
      }

      if (message.id != null && message.hasOwnProperty("id")) object.id = message.id;
      if (message.password != null && message.hasOwnProperty("password")) object.password = message.password;
      return object;
    };
    /**
     * Converts this videoSource to JSON.
     * @function toJSON
     * @memberof Shadow.videoSource
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    videoSource.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return videoSource;
  }();

  Shadow.reportedMsg = /*#__PURE__*/function () {
    /**
     * Properties of a reportedMsg.
     * @memberof Shadow
     * @interface IreportedMsg
     * @property {Array.<Shadow.IvideoSource>|null} [videoSources] reportedMsg videoSources
     * @property {Array.<string>|null} [discoveredVideoSources] reportedMsg discoveredVideoSources
     * @property {boolean|null} [connected] reportedMsg connected
     * @property {string|null} [status] reportedMsg status
     * @property {string|null} [ip] reportedMsg ip
     * @property {Shadow.IversionMsg|null} [versionInfo] reportedMsg versionInfo
     * @property {Shadow.IversionMsg|null} [lastVersionInfo] reportedMsg lastVersionInfo
     * @property {Shadow.IhealthMsg|null} [health] reportedMsg health
     * @property {Shadow.IenvironmentMsg|null} [environment] reportedMsg environment
     */

    /**
     * Constructs a new reportedMsg.
     * @memberof Shadow
     * @classdesc Represents a reportedMsg.
     * @implements IreportedMsg
     * @constructor
     * @param {Shadow.IreportedMsg=} [properties] Properties to set
     */
    function reportedMsg(properties) {
      this.videoSources = [];
      this.discoveredVideoSources = [];
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * reportedMsg videoSources.
     * @member {Array.<Shadow.IvideoSource>} videoSources
     * @memberof Shadow.reportedMsg
     * @instance
     */


    reportedMsg.prototype.videoSources = $util$2.emptyArray;
    /**
     * reportedMsg discoveredVideoSources.
     * @member {Array.<string>} discoveredVideoSources
     * @memberof Shadow.reportedMsg
     * @instance
     */

    reportedMsg.prototype.discoveredVideoSources = $util$2.emptyArray;
    /**
     * reportedMsg connected.
     * @member {boolean} connected
     * @memberof Shadow.reportedMsg
     * @instance
     */

    reportedMsg.prototype.connected = false;
    /**
     * reportedMsg status.
     * @member {string} status
     * @memberof Shadow.reportedMsg
     * @instance
     */

    reportedMsg.prototype.status = "";
    /**
     * reportedMsg ip.
     * @member {string} ip
     * @memberof Shadow.reportedMsg
     * @instance
     */

    reportedMsg.prototype.ip = "";
    /**
     * reportedMsg versionInfo.
     * @member {Shadow.IversionMsg|null|undefined} versionInfo
     * @memberof Shadow.reportedMsg
     * @instance
     */

    reportedMsg.prototype.versionInfo = null;
    /**
     * reportedMsg lastVersionInfo.
     * @member {Shadow.IversionMsg|null|undefined} lastVersionInfo
     * @memberof Shadow.reportedMsg
     * @instance
     */

    reportedMsg.prototype.lastVersionInfo = null;
    /**
     * reportedMsg health.
     * @member {Shadow.IhealthMsg|null|undefined} health
     * @memberof Shadow.reportedMsg
     * @instance
     */

    reportedMsg.prototype.health = null;
    /**
     * reportedMsg environment.
     * @member {Shadow.IenvironmentMsg|null|undefined} environment
     * @memberof Shadow.reportedMsg
     * @instance
     */

    reportedMsg.prototype.environment = null;
    /**
     * Creates a new reportedMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.reportedMsg
     * @static
     * @param {Shadow.IreportedMsg=} [properties] Properties to set
     * @returns {Shadow.reportedMsg} reportedMsg instance
     */

    reportedMsg.create = function create(properties) {
      return new reportedMsg(properties);
    };
    /**
     * Encodes the specified reportedMsg message. Does not implicitly {@link Shadow.reportedMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.reportedMsg
     * @static
     * @param {Shadow.IreportedMsg} message reportedMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    reportedMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.videoSources != null && message.videoSources.length) for (let i = 0; i < message.videoSources.length; ++i) $root$2.Shadow.videoSource.encode(message.videoSources[i], writer.uint32(
      /* id 1, wireType 2 =*/
      10).fork()).ldelim();
      if (message.discoveredVideoSources != null && message.discoveredVideoSources.length) for (let i = 0; i < message.discoveredVideoSources.length; ++i) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.discoveredVideoSources[i]);
      if (message.connected != null && Object.hasOwnProperty.call(message, "connected")) writer.uint32(
      /* id 3, wireType 0 =*/
      24).bool(message.connected);
      if (message.status != null && Object.hasOwnProperty.call(message, "status")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).string(message.status);
      if (message.ip != null && Object.hasOwnProperty.call(message, "ip")) writer.uint32(
      /* id 5, wireType 2 =*/
      42).string(message.ip);
      if (message.versionInfo != null && Object.hasOwnProperty.call(message, "versionInfo")) $root$2.Shadow.versionMsg.encode(message.versionInfo, writer.uint32(
      /* id 6, wireType 2 =*/
      50).fork()).ldelim();
      if (message.lastVersionInfo != null && Object.hasOwnProperty.call(message, "lastVersionInfo")) $root$2.Shadow.versionMsg.encode(message.lastVersionInfo, writer.uint32(
      /* id 7, wireType 2 =*/
      58).fork()).ldelim();
      if (message.health != null && Object.hasOwnProperty.call(message, "health")) $root$2.Shadow.healthMsg.encode(message.health, writer.uint32(
      /* id 8, wireType 2 =*/
      66).fork()).ldelim();
      if (message.environment != null && Object.hasOwnProperty.call(message, "environment")) $root$2.Shadow.environmentMsg.encode(message.environment, writer.uint32(
      /* id 9, wireType 2 =*/
      74).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified reportedMsg message, length delimited. Does not implicitly {@link Shadow.reportedMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.reportedMsg
     * @static
     * @param {Shadow.IreportedMsg} message reportedMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    reportedMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a reportedMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.reportedMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.reportedMsg} reportedMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    reportedMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.reportedMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            if (!(message.videoSources && message.videoSources.length)) message.videoSources = [];
            message.videoSources.push($root$2.Shadow.videoSource.decode(reader, reader.uint32()));
            break;

          case 2:
            if (!(message.discoveredVideoSources && message.discoveredVideoSources.length)) message.discoveredVideoSources = [];
            message.discoveredVideoSources.push(reader.string());
            break;

          case 3:
            message.connected = reader.bool();
            break;

          case 4:
            message.status = reader.string();
            break;

          case 5:
            message.ip = reader.string();
            break;

          case 6:
            message.versionInfo = $root$2.Shadow.versionMsg.decode(reader, reader.uint32());
            break;

          case 7:
            message.lastVersionInfo = $root$2.Shadow.versionMsg.decode(reader, reader.uint32());
            break;

          case 8:
            message.health = $root$2.Shadow.healthMsg.decode(reader, reader.uint32());
            break;

          case 9:
            message.environment = $root$2.Shadow.environmentMsg.decode(reader, reader.uint32());
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a reportedMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.reportedMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.reportedMsg} reportedMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    reportedMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a reportedMsg message.
     * @function verify
     * @memberof Shadow.reportedMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    reportedMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";

      if (message.videoSources != null && message.hasOwnProperty("videoSources")) {
        if (!Array.isArray(message.videoSources)) return "videoSources: array expected";

        for (let i = 0; i < message.videoSources.length; ++i) {
          let error = $root$2.Shadow.videoSource.verify(message.videoSources[i]);
          if (error) return "videoSources." + error;
        }
      }

      if (message.discoveredVideoSources != null && message.hasOwnProperty("discoveredVideoSources")) {
        if (!Array.isArray(message.discoveredVideoSources)) return "discoveredVideoSources: array expected";

        for (let i = 0; i < message.discoveredVideoSources.length; ++i) if (!$util$2.isString(message.discoveredVideoSources[i])) return "discoveredVideoSources: string[] expected";
      }

      if (message.connected != null && message.hasOwnProperty("connected")) if (typeof message.connected !== "boolean") return "connected: boolean expected";
      if (message.status != null && message.hasOwnProperty("status")) if (!$util$2.isString(message.status)) return "status: string expected";
      if (message.ip != null && message.hasOwnProperty("ip")) if (!$util$2.isString(message.ip)) return "ip: string expected";

      if (message.versionInfo != null && message.hasOwnProperty("versionInfo")) {
        let error = $root$2.Shadow.versionMsg.verify(message.versionInfo);
        if (error) return "versionInfo." + error;
      }

      if (message.lastVersionInfo != null && message.hasOwnProperty("lastVersionInfo")) {
        let error = $root$2.Shadow.versionMsg.verify(message.lastVersionInfo);
        if (error) return "lastVersionInfo." + error;
      }

      if (message.health != null && message.hasOwnProperty("health")) {
        let error = $root$2.Shadow.healthMsg.verify(message.health);
        if (error) return "health." + error;
      }

      if (message.environment != null && message.hasOwnProperty("environment")) {
        let error = $root$2.Shadow.environmentMsg.verify(message.environment);
        if (error) return "environment." + error;
      }

      return null;
    };
    /**
     * Creates a reportedMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.reportedMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.reportedMsg} reportedMsg
     */


    reportedMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.reportedMsg) return object;
      let message = new $root$2.Shadow.reportedMsg();

      if (object.videoSources) {
        if (!Array.isArray(object.videoSources)) throw TypeError(".Shadow.reportedMsg.videoSources: array expected");
        message.videoSources = [];

        for (let i = 0; i < object.videoSources.length; ++i) {
          if (typeof object.videoSources[i] !== "object") throw TypeError(".Shadow.reportedMsg.videoSources: object expected");
          message.videoSources[i] = $root$2.Shadow.videoSource.fromObject(object.videoSources[i]);
        }
      }

      if (object.discoveredVideoSources) {
        if (!Array.isArray(object.discoveredVideoSources)) throw TypeError(".Shadow.reportedMsg.discoveredVideoSources: array expected");
        message.discoveredVideoSources = [];

        for (let i = 0; i < object.discoveredVideoSources.length; ++i) message.discoveredVideoSources[i] = String(object.discoveredVideoSources[i]);
      }

      if (object.connected != null) message.connected = Boolean(object.connected);
      if (object.status != null) message.status = String(object.status);
      if (object.ip != null) message.ip = String(object.ip);

      if (object.versionInfo != null) {
        if (typeof object.versionInfo !== "object") throw TypeError(".Shadow.reportedMsg.versionInfo: object expected");
        message.versionInfo = $root$2.Shadow.versionMsg.fromObject(object.versionInfo);
      }

      if (object.lastVersionInfo != null) {
        if (typeof object.lastVersionInfo !== "object") throw TypeError(".Shadow.reportedMsg.lastVersionInfo: object expected");
        message.lastVersionInfo = $root$2.Shadow.versionMsg.fromObject(object.lastVersionInfo);
      }

      if (object.health != null) {
        if (typeof object.health !== "object") throw TypeError(".Shadow.reportedMsg.health: object expected");
        message.health = $root$2.Shadow.healthMsg.fromObject(object.health);
      }

      if (object.environment != null) {
        if (typeof object.environment !== "object") throw TypeError(".Shadow.reportedMsg.environment: object expected");
        message.environment = $root$2.Shadow.environmentMsg.fromObject(object.environment);
      }

      return message;
    };
    /**
     * Creates a plain object from a reportedMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.reportedMsg
     * @static
     * @param {Shadow.reportedMsg} message reportedMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    reportedMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.arrays || options.defaults) {
        object.videoSources = [];
        object.discoveredVideoSources = [];
      }

      if (options.defaults) {
        object.connected = false;
        object.status = "";
        object.ip = "";
        object.versionInfo = null;
        object.lastVersionInfo = null;
        object.health = null;
        object.environment = null;
      }

      if (message.videoSources && message.videoSources.length) {
        object.videoSources = [];

        for (let j = 0; j < message.videoSources.length; ++j) object.videoSources[j] = $root$2.Shadow.videoSource.toObject(message.videoSources[j], options);
      }

      if (message.discoveredVideoSources && message.discoveredVideoSources.length) {
        object.discoveredVideoSources = [];

        for (let j = 0; j < message.discoveredVideoSources.length; ++j) object.discoveredVideoSources[j] = message.discoveredVideoSources[j];
      }

      if (message.connected != null && message.hasOwnProperty("connected")) object.connected = message.connected;
      if (message.status != null && message.hasOwnProperty("status")) object.status = message.status;
      if (message.ip != null && message.hasOwnProperty("ip")) object.ip = message.ip;
      if (message.versionInfo != null && message.hasOwnProperty("versionInfo")) object.versionInfo = $root$2.Shadow.versionMsg.toObject(message.versionInfo, options);
      if (message.lastVersionInfo != null && message.hasOwnProperty("lastVersionInfo")) object.lastVersionInfo = $root$2.Shadow.versionMsg.toObject(message.lastVersionInfo, options);
      if (message.health != null && message.hasOwnProperty("health")) object.health = $root$2.Shadow.healthMsg.toObject(message.health, options);
      if (message.environment != null && message.hasOwnProperty("environment")) object.environment = $root$2.Shadow.environmentMsg.toObject(message.environment, options);
      return object;
    };
    /**
     * Converts this reportedMsg to JSON.
     * @function toJSON
     * @memberof Shadow.reportedMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    reportedMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return reportedMsg;
  }();

  Shadow.versionMsg = /*#__PURE__*/function () {
    /**
     * Properties of a versionMsg.
     * @memberof Shadow
     * @interface IversionMsg
     * @property {string|null} [branch] versionMsg branch
     * @property {string|null} [chripVersion] versionMsg chripVersion
     * @property {string|null} [mmwaveVersion] versionMsg mmwaveVersion
     * @property {string|null} [esp32Version] versionMsg esp32Version
     * @property {string|null} [portalVersion] versionMsg portalVersion
     */

    /**
     * Constructs a new versionMsg.
     * @memberof Shadow
     * @classdesc Represents a versionMsg.
     * @implements IversionMsg
     * @constructor
     * @param {Shadow.IversionMsg=} [properties] Properties to set
     */
    function versionMsg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * versionMsg branch.
     * @member {string} branch
     * @memberof Shadow.versionMsg
     * @instance
     */


    versionMsg.prototype.branch = "";
    /**
     * versionMsg chripVersion.
     * @member {string} chripVersion
     * @memberof Shadow.versionMsg
     * @instance
     */

    versionMsg.prototype.chripVersion = "";
    /**
     * versionMsg mmwaveVersion.
     * @member {string} mmwaveVersion
     * @memberof Shadow.versionMsg
     * @instance
     */

    versionMsg.prototype.mmwaveVersion = "";
    /**
     * versionMsg esp32Version.
     * @member {string} esp32Version
     * @memberof Shadow.versionMsg
     * @instance
     */

    versionMsg.prototype.esp32Version = "";
    /**
     * versionMsg portalVersion.
     * @member {string} portalVersion
     * @memberof Shadow.versionMsg
     * @instance
     */

    versionMsg.prototype.portalVersion = "";
    /**
     * Creates a new versionMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.versionMsg
     * @static
     * @param {Shadow.IversionMsg=} [properties] Properties to set
     * @returns {Shadow.versionMsg} versionMsg instance
     */

    versionMsg.create = function create(properties) {
      return new versionMsg(properties);
    };
    /**
     * Encodes the specified versionMsg message. Does not implicitly {@link Shadow.versionMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.versionMsg
     * @static
     * @param {Shadow.IversionMsg} message versionMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    versionMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.branch != null && Object.hasOwnProperty.call(message, "branch")) writer.uint32(
      /* id 1, wireType 2 =*/
      10).string(message.branch);
      if (message.chripVersion != null && Object.hasOwnProperty.call(message, "chripVersion")) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.chripVersion);
      if (message.mmwaveVersion != null && Object.hasOwnProperty.call(message, "mmwaveVersion")) writer.uint32(
      /* id 3, wireType 2 =*/
      26).string(message.mmwaveVersion);
      if (message.esp32Version != null && Object.hasOwnProperty.call(message, "esp32Version")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).string(message.esp32Version);
      if (message.portalVersion != null && Object.hasOwnProperty.call(message, "portalVersion")) writer.uint32(
      /* id 5, wireType 2 =*/
      42).string(message.portalVersion);
      return writer;
    };
    /**
     * Encodes the specified versionMsg message, length delimited. Does not implicitly {@link Shadow.versionMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.versionMsg
     * @static
     * @param {Shadow.IversionMsg} message versionMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    versionMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a versionMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.versionMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.versionMsg} versionMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    versionMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.versionMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.branch = reader.string();
            break;

          case 2:
            message.chripVersion = reader.string();
            break;

          case 3:
            message.mmwaveVersion = reader.string();
            break;

          case 4:
            message.esp32Version = reader.string();
            break;

          case 5:
            message.portalVersion = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a versionMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.versionMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.versionMsg} versionMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    versionMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a versionMsg message.
     * @function verify
     * @memberof Shadow.versionMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    versionMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.branch != null && message.hasOwnProperty("branch")) if (!$util$2.isString(message.branch)) return "branch: string expected";
      if (message.chripVersion != null && message.hasOwnProperty("chripVersion")) if (!$util$2.isString(message.chripVersion)) return "chripVersion: string expected";
      if (message.mmwaveVersion != null && message.hasOwnProperty("mmwaveVersion")) if (!$util$2.isString(message.mmwaveVersion)) return "mmwaveVersion: string expected";
      if (message.esp32Version != null && message.hasOwnProperty("esp32Version")) if (!$util$2.isString(message.esp32Version)) return "esp32Version: string expected";
      if (message.portalVersion != null && message.hasOwnProperty("portalVersion")) if (!$util$2.isString(message.portalVersion)) return "portalVersion: string expected";
      return null;
    };
    /**
     * Creates a versionMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.versionMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.versionMsg} versionMsg
     */


    versionMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.versionMsg) return object;
      let message = new $root$2.Shadow.versionMsg();
      if (object.branch != null) message.branch = String(object.branch);
      if (object.chripVersion != null) message.chripVersion = String(object.chripVersion);
      if (object.mmwaveVersion != null) message.mmwaveVersion = String(object.mmwaveVersion);
      if (object.esp32Version != null) message.esp32Version = String(object.esp32Version);
      if (object.portalVersion != null) message.portalVersion = String(object.portalVersion);
      return message;
    };
    /**
     * Creates a plain object from a versionMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.versionMsg
     * @static
     * @param {Shadow.versionMsg} message versionMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    versionMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.branch = "";
        object.chripVersion = "";
        object.mmwaveVersion = "";
        object.esp32Version = "";
        object.portalVersion = "";
      }

      if (message.branch != null && message.hasOwnProperty("branch")) object.branch = message.branch;
      if (message.chripVersion != null && message.hasOwnProperty("chripVersion")) object.chripVersion = message.chripVersion;
      if (message.mmwaveVersion != null && message.hasOwnProperty("mmwaveVersion")) object.mmwaveVersion = message.mmwaveVersion;
      if (message.esp32Version != null && message.hasOwnProperty("esp32Version")) object.esp32Version = message.esp32Version;
      if (message.portalVersion != null && message.hasOwnProperty("portalVersion")) object.portalVersion = message.portalVersion;
      return object;
    };
    /**
     * Converts this versionMsg to JSON.
     * @function toJSON
     * @memberof Shadow.versionMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    versionMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return versionMsg;
  }();

  Shadow.avsMsg = /*#__PURE__*/function () {
    /**
     * Properties of an avsMsg.
     * @memberof Shadow
     * @interface IavsMsg
     * @property {string|null} [clientId] avsMsg clientId
     * @property {string|null} [redirectUrl] avsMsg redirectUrl
     * @property {string|null} [authCode] avsMsg authCode
     * @property {string|null} [codeVerifier] avsMsg codeVerifier
     */

    /**
     * Constructs a new avsMsg.
     * @memberof Shadow
     * @classdesc Represents an avsMsg.
     * @implements IavsMsg
     * @constructor
     * @param {Shadow.IavsMsg=} [properties] Properties to set
     */
    function avsMsg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * avsMsg clientId.
     * @member {string} clientId
     * @memberof Shadow.avsMsg
     * @instance
     */


    avsMsg.prototype.clientId = "";
    /**
     * avsMsg redirectUrl.
     * @member {string} redirectUrl
     * @memberof Shadow.avsMsg
     * @instance
     */

    avsMsg.prototype.redirectUrl = "";
    /**
     * avsMsg authCode.
     * @member {string} authCode
     * @memberof Shadow.avsMsg
     * @instance
     */

    avsMsg.prototype.authCode = "";
    /**
     * avsMsg codeVerifier.
     * @member {string} codeVerifier
     * @memberof Shadow.avsMsg
     * @instance
     */

    avsMsg.prototype.codeVerifier = "";
    /**
     * Creates a new avsMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.avsMsg
     * @static
     * @param {Shadow.IavsMsg=} [properties] Properties to set
     * @returns {Shadow.avsMsg} avsMsg instance
     */

    avsMsg.create = function create(properties) {
      return new avsMsg(properties);
    };
    /**
     * Encodes the specified avsMsg message. Does not implicitly {@link Shadow.avsMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.avsMsg
     * @static
     * @param {Shadow.IavsMsg} message avsMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    avsMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.clientId != null && Object.hasOwnProperty.call(message, "clientId")) writer.uint32(
      /* id 1, wireType 2 =*/
      10).string(message.clientId);
      if (message.redirectUrl != null && Object.hasOwnProperty.call(message, "redirectUrl")) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.redirectUrl);
      if (message.authCode != null && Object.hasOwnProperty.call(message, "authCode")) writer.uint32(
      /* id 3, wireType 2 =*/
      26).string(message.authCode);
      if (message.codeVerifier != null && Object.hasOwnProperty.call(message, "codeVerifier")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).string(message.codeVerifier);
      return writer;
    };
    /**
     * Encodes the specified avsMsg message, length delimited. Does not implicitly {@link Shadow.avsMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.avsMsg
     * @static
     * @param {Shadow.IavsMsg} message avsMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    avsMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes an avsMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.avsMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.avsMsg} avsMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    avsMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.avsMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.clientId = reader.string();
            break;

          case 2:
            message.redirectUrl = reader.string();
            break;

          case 3:
            message.authCode = reader.string();
            break;

          case 4:
            message.codeVerifier = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes an avsMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.avsMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.avsMsg} avsMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    avsMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies an avsMsg message.
     * @function verify
     * @memberof Shadow.avsMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    avsMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.clientId != null && message.hasOwnProperty("clientId")) if (!$util$2.isString(message.clientId)) return "clientId: string expected";
      if (message.redirectUrl != null && message.hasOwnProperty("redirectUrl")) if (!$util$2.isString(message.redirectUrl)) return "redirectUrl: string expected";
      if (message.authCode != null && message.hasOwnProperty("authCode")) if (!$util$2.isString(message.authCode)) return "authCode: string expected";
      if (message.codeVerifier != null && message.hasOwnProperty("codeVerifier")) if (!$util$2.isString(message.codeVerifier)) return "codeVerifier: string expected";
      return null;
    };
    /**
     * Creates an avsMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.avsMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.avsMsg} avsMsg
     */


    avsMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.avsMsg) return object;
      let message = new $root$2.Shadow.avsMsg();
      if (object.clientId != null) message.clientId = String(object.clientId);
      if (object.redirectUrl != null) message.redirectUrl = String(object.redirectUrl);
      if (object.authCode != null) message.authCode = String(object.authCode);
      if (object.codeVerifier != null) message.codeVerifier = String(object.codeVerifier);
      return message;
    };
    /**
     * Creates a plain object from an avsMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.avsMsg
     * @static
     * @param {Shadow.avsMsg} message avsMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    avsMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.clientId = "";
        object.redirectUrl = "";
        object.authCode = "";
        object.codeVerifier = "";
      }

      if (message.clientId != null && message.hasOwnProperty("clientId")) object.clientId = message.clientId;
      if (message.redirectUrl != null && message.hasOwnProperty("redirectUrl")) object.redirectUrl = message.redirectUrl;
      if (message.authCode != null && message.hasOwnProperty("authCode")) object.authCode = message.authCode;
      if (message.codeVerifier != null && message.hasOwnProperty("codeVerifier")) object.codeVerifier = message.codeVerifier;
      return object;
    };
    /**
     * Converts this avsMsg to JSON.
     * @function toJSON
     * @memberof Shadow.avsMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    avsMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return avsMsg;
  }();

  Shadow.calibrationMsg = /*#__PURE__*/function () {
    /**
     * Properties of a calibrationMsg.
     * @memberof Shadow
     * @interface IcalibrationMsg
     * @property {number|null} [startTime] calibrationMsg startTime
     * @property {number|null} [stopTime] calibrationMsg stopTime
     * @property {boolean|null} [auto] calibrationMsg auto
     * @property {Shadow.calibrationMsg.State|null} [calibrationState] calibrationMsg calibrationState
     * @property {Shadow.IenvironmentMsg|null} [environment] calibrationMsg environment
     */

    /**
     * Constructs a new calibrationMsg.
     * @memberof Shadow
     * @classdesc Represents a calibrationMsg.
     * @implements IcalibrationMsg
     * @constructor
     * @param {Shadow.IcalibrationMsg=} [properties] Properties to set
     */
    function calibrationMsg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * calibrationMsg startTime.
     * @member {number} startTime
     * @memberof Shadow.calibrationMsg
     * @instance
     */


    calibrationMsg.prototype.startTime = 0;
    /**
     * calibrationMsg stopTime.
     * @member {number} stopTime
     * @memberof Shadow.calibrationMsg
     * @instance
     */

    calibrationMsg.prototype.stopTime = 0;
    /**
     * calibrationMsg auto.
     * @member {boolean} auto
     * @memberof Shadow.calibrationMsg
     * @instance
     */

    calibrationMsg.prototype.auto = false;
    /**
     * calibrationMsg calibrationState.
     * @member {Shadow.calibrationMsg.State} calibrationState
     * @memberof Shadow.calibrationMsg
     * @instance
     */

    calibrationMsg.prototype.calibrationState = 0;
    /**
     * calibrationMsg environment.
     * @member {Shadow.IenvironmentMsg|null|undefined} environment
     * @memberof Shadow.calibrationMsg
     * @instance
     */

    calibrationMsg.prototype.environment = null;
    /**
     * Creates a new calibrationMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.calibrationMsg
     * @static
     * @param {Shadow.IcalibrationMsg=} [properties] Properties to set
     * @returns {Shadow.calibrationMsg} calibrationMsg instance
     */

    calibrationMsg.create = function create(properties) {
      return new calibrationMsg(properties);
    };
    /**
     * Encodes the specified calibrationMsg message. Does not implicitly {@link Shadow.calibrationMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.calibrationMsg
     * @static
     * @param {Shadow.IcalibrationMsg} message calibrationMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    calibrationMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.startTime != null && Object.hasOwnProperty.call(message, "startTime")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.startTime);
      if (message.stopTime != null && Object.hasOwnProperty.call(message, "stopTime")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).uint32(message.stopTime);
      if (message.auto != null && Object.hasOwnProperty.call(message, "auto")) writer.uint32(
      /* id 3, wireType 0 =*/
      24).bool(message.auto);
      if (message.calibrationState != null && Object.hasOwnProperty.call(message, "calibrationState")) writer.uint32(
      /* id 4, wireType 0 =*/
      32).int32(message.calibrationState);
      if (message.environment != null && Object.hasOwnProperty.call(message, "environment")) $root$2.Shadow.environmentMsg.encode(message.environment, writer.uint32(
      /* id 5, wireType 2 =*/
      42).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified calibrationMsg message, length delimited. Does not implicitly {@link Shadow.calibrationMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.calibrationMsg
     * @static
     * @param {Shadow.IcalibrationMsg} message calibrationMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    calibrationMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a calibrationMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.calibrationMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.calibrationMsg} calibrationMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    calibrationMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.calibrationMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.startTime = reader.uint32();
            break;

          case 2:
            message.stopTime = reader.uint32();
            break;

          case 3:
            message.auto = reader.bool();
            break;

          case 4:
            message.calibrationState = reader.int32();
            break;

          case 5:
            message.environment = $root$2.Shadow.environmentMsg.decode(reader, reader.uint32());
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a calibrationMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.calibrationMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.calibrationMsg} calibrationMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    calibrationMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a calibrationMsg message.
     * @function verify
     * @memberof Shadow.calibrationMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    calibrationMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.startTime != null && message.hasOwnProperty("startTime")) if (!$util$2.isInteger(message.startTime)) return "startTime: integer expected";
      if (message.stopTime != null && message.hasOwnProperty("stopTime")) if (!$util$2.isInteger(message.stopTime)) return "stopTime: integer expected";
      if (message.auto != null && message.hasOwnProperty("auto")) if (typeof message.auto !== "boolean") return "auto: boolean expected";
      if (message.calibrationState != null && message.hasOwnProperty("calibrationState")) switch (message.calibrationState) {
        default:
          return "calibrationState: enum value expected";

        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
          break;
      }

      if (message.environment != null && message.hasOwnProperty("environment")) {
        let error = $root$2.Shadow.environmentMsg.verify(message.environment);
        if (error) return "environment." + error;
      }

      return null;
    };
    /**
     * Creates a calibrationMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.calibrationMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.calibrationMsg} calibrationMsg
     */


    calibrationMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.calibrationMsg) return object;
      let message = new $root$2.Shadow.calibrationMsg();
      if (object.startTime != null) message.startTime = object.startTime >>> 0;
      if (object.stopTime != null) message.stopTime = object.stopTime >>> 0;
      if (object.auto != null) message.auto = Boolean(object.auto);

      switch (object.calibrationState) {
        case "None":
        case 0:
          message.calibrationState = 0;
          break;

        case "Calibrating":
        case 1:
          message.calibrationState = 1;
          break;

        case "Fail":
        case 2:
          message.calibrationState = 2;
          break;

        case "Success":
        case 3:
          message.calibrationState = 3;
          break;

        case "Collecting":
        case 4:
          message.calibrationState = 4;
          break;
      }

      if (object.environment != null) {
        if (typeof object.environment !== "object") throw TypeError(".Shadow.calibrationMsg.environment: object expected");
        message.environment = $root$2.Shadow.environmentMsg.fromObject(object.environment);
      }

      return message;
    };
    /**
     * Creates a plain object from a calibrationMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.calibrationMsg
     * @static
     * @param {Shadow.calibrationMsg} message calibrationMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    calibrationMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.startTime = 0;
        object.stopTime = 0;
        object.auto = false;
        object.calibrationState = options.enums === String ? "None" : 0;
        object.environment = null;
      }

      if (message.startTime != null && message.hasOwnProperty("startTime")) object.startTime = message.startTime;
      if (message.stopTime != null && message.hasOwnProperty("stopTime")) object.stopTime = message.stopTime;
      if (message.auto != null && message.hasOwnProperty("auto")) object.auto = message.auto;
      if (message.calibrationState != null && message.hasOwnProperty("calibrationState")) object.calibrationState = options.enums === String ? $root$2.Shadow.calibrationMsg.State[message.calibrationState] : message.calibrationState;
      if (message.environment != null && message.hasOwnProperty("environment")) object.environment = $root$2.Shadow.environmentMsg.toObject(message.environment, options);
      return object;
    };
    /**
     * Converts this calibrationMsg to JSON.
     * @function toJSON
     * @memberof Shadow.calibrationMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    calibrationMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * State enum.
     * @name Shadow.calibrationMsg.State
     * @enum {number}
     * @property {number} None=0 None value
     * @property {number} Calibrating=1 Calibrating value
     * @property {number} Fail=2 Fail value
     * @property {number} Success=3 Success value
     * @property {number} Collecting=4 Collecting value
     */


    calibrationMsg.State = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "None"] = 0;
      values[valuesById[1] = "Calibrating"] = 1;
      values[valuesById[2] = "Fail"] = 2;
      values[valuesById[3] = "Success"] = 3;
      values[valuesById[4] = "Collecting"] = 4;
      return values;
    }();

    return calibrationMsg;
  }();

  Shadow.chripParamMsg = /*#__PURE__*/function () {
    /**
     * Properties of a chripParamMsg.
     * @memberof Shadow
     * @interface IchripParamMsg
     * @property {string|null} [occupancyFlagsCfg] chripParamMsg occupancyFlagsCfg
     * @property {string|null} [occupancyParamCfg] chripParamMsg occupancyParamCfg
     * @property {string|null} [correctionParam] chripParamMsg correctionParam
     * @property {string|null} [occuStanceParam] chripParamMsg occuStanceParam
     * @property {string|null} [sceenRoiParam] chripParamMsg sceenRoiParam
     * @property {string|null} [profileCfg] chripParamMsg profileCfg
     */

    /**
     * Constructs a new chripParamMsg.
     * @memberof Shadow
     * @classdesc Represents a chripParamMsg.
     * @implements IchripParamMsg
     * @constructor
     * @param {Shadow.IchripParamMsg=} [properties] Properties to set
     */
    function chripParamMsg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * chripParamMsg occupancyFlagsCfg.
     * @member {string} occupancyFlagsCfg
     * @memberof Shadow.chripParamMsg
     * @instance
     */


    chripParamMsg.prototype.occupancyFlagsCfg = "";
    /**
     * chripParamMsg occupancyParamCfg.
     * @member {string} occupancyParamCfg
     * @memberof Shadow.chripParamMsg
     * @instance
     */

    chripParamMsg.prototype.occupancyParamCfg = "";
    /**
     * chripParamMsg correctionParam.
     * @member {string} correctionParam
     * @memberof Shadow.chripParamMsg
     * @instance
     */

    chripParamMsg.prototype.correctionParam = "";
    /**
     * chripParamMsg occuStanceParam.
     * @member {string} occuStanceParam
     * @memberof Shadow.chripParamMsg
     * @instance
     */

    chripParamMsg.prototype.occuStanceParam = "";
    /**
     * chripParamMsg sceenRoiParam.
     * @member {string} sceenRoiParam
     * @memberof Shadow.chripParamMsg
     * @instance
     */

    chripParamMsg.prototype.sceenRoiParam = "";
    /**
     * chripParamMsg profileCfg.
     * @member {string} profileCfg
     * @memberof Shadow.chripParamMsg
     * @instance
     */

    chripParamMsg.prototype.profileCfg = "";
    /**
     * Creates a new chripParamMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.chripParamMsg
     * @static
     * @param {Shadow.IchripParamMsg=} [properties] Properties to set
     * @returns {Shadow.chripParamMsg} chripParamMsg instance
     */

    chripParamMsg.create = function create(properties) {
      return new chripParamMsg(properties);
    };
    /**
     * Encodes the specified chripParamMsg message. Does not implicitly {@link Shadow.chripParamMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.chripParamMsg
     * @static
     * @param {Shadow.IchripParamMsg} message chripParamMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    chripParamMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.occupancyFlagsCfg != null && Object.hasOwnProperty.call(message, "occupancyFlagsCfg")) writer.uint32(
      /* id 1, wireType 2 =*/
      10).string(message.occupancyFlagsCfg);
      if (message.occupancyParamCfg != null && Object.hasOwnProperty.call(message, "occupancyParamCfg")) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.occupancyParamCfg);
      if (message.correctionParam != null && Object.hasOwnProperty.call(message, "correctionParam")) writer.uint32(
      /* id 3, wireType 2 =*/
      26).string(message.correctionParam);
      if (message.occuStanceParam != null && Object.hasOwnProperty.call(message, "occuStanceParam")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).string(message.occuStanceParam);
      if (message.sceenRoiParam != null && Object.hasOwnProperty.call(message, "sceenRoiParam")) writer.uint32(
      /* id 5, wireType 2 =*/
      42).string(message.sceenRoiParam);
      if (message.profileCfg != null && Object.hasOwnProperty.call(message, "profileCfg")) writer.uint32(
      /* id 6, wireType 2 =*/
      50).string(message.profileCfg);
      return writer;
    };
    /**
     * Encodes the specified chripParamMsg message, length delimited. Does not implicitly {@link Shadow.chripParamMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.chripParamMsg
     * @static
     * @param {Shadow.IchripParamMsg} message chripParamMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    chripParamMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a chripParamMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.chripParamMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.chripParamMsg} chripParamMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    chripParamMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.chripParamMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.occupancyFlagsCfg = reader.string();
            break;

          case 2:
            message.occupancyParamCfg = reader.string();
            break;

          case 3:
            message.correctionParam = reader.string();
            break;

          case 4:
            message.occuStanceParam = reader.string();
            break;

          case 5:
            message.sceenRoiParam = reader.string();
            break;

          case 6:
            message.profileCfg = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a chripParamMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.chripParamMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.chripParamMsg} chripParamMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    chripParamMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a chripParamMsg message.
     * @function verify
     * @memberof Shadow.chripParamMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    chripParamMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.occupancyFlagsCfg != null && message.hasOwnProperty("occupancyFlagsCfg")) if (!$util$2.isString(message.occupancyFlagsCfg)) return "occupancyFlagsCfg: string expected";
      if (message.occupancyParamCfg != null && message.hasOwnProperty("occupancyParamCfg")) if (!$util$2.isString(message.occupancyParamCfg)) return "occupancyParamCfg: string expected";
      if (message.correctionParam != null && message.hasOwnProperty("correctionParam")) if (!$util$2.isString(message.correctionParam)) return "correctionParam: string expected";
      if (message.occuStanceParam != null && message.hasOwnProperty("occuStanceParam")) if (!$util$2.isString(message.occuStanceParam)) return "occuStanceParam: string expected";
      if (message.sceenRoiParam != null && message.hasOwnProperty("sceenRoiParam")) if (!$util$2.isString(message.sceenRoiParam)) return "sceenRoiParam: string expected";
      if (message.profileCfg != null && message.hasOwnProperty("profileCfg")) if (!$util$2.isString(message.profileCfg)) return "profileCfg: string expected";
      return null;
    };
    /**
     * Creates a chripParamMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.chripParamMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.chripParamMsg} chripParamMsg
     */


    chripParamMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.chripParamMsg) return object;
      let message = new $root$2.Shadow.chripParamMsg();
      if (object.occupancyFlagsCfg != null) message.occupancyFlagsCfg = String(object.occupancyFlagsCfg);
      if (object.occupancyParamCfg != null) message.occupancyParamCfg = String(object.occupancyParamCfg);
      if (object.correctionParam != null) message.correctionParam = String(object.correctionParam);
      if (object.occuStanceParam != null) message.occuStanceParam = String(object.occuStanceParam);
      if (object.sceenRoiParam != null) message.sceenRoiParam = String(object.sceenRoiParam);
      if (object.profileCfg != null) message.profileCfg = String(object.profileCfg);
      return message;
    };
    /**
     * Creates a plain object from a chripParamMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.chripParamMsg
     * @static
     * @param {Shadow.chripParamMsg} message chripParamMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    chripParamMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.occupancyFlagsCfg = "";
        object.occupancyParamCfg = "";
        object.correctionParam = "";
        object.occuStanceParam = "";
        object.sceenRoiParam = "";
        object.profileCfg = "";
      }

      if (message.occupancyFlagsCfg != null && message.hasOwnProperty("occupancyFlagsCfg")) object.occupancyFlagsCfg = message.occupancyFlagsCfg;
      if (message.occupancyParamCfg != null && message.hasOwnProperty("occupancyParamCfg")) object.occupancyParamCfg = message.occupancyParamCfg;
      if (message.correctionParam != null && message.hasOwnProperty("correctionParam")) object.correctionParam = message.correctionParam;
      if (message.occuStanceParam != null && message.hasOwnProperty("occuStanceParam")) object.occuStanceParam = message.occuStanceParam;
      if (message.sceenRoiParam != null && message.hasOwnProperty("sceenRoiParam")) object.sceenRoiParam = message.sceenRoiParam;
      if (message.profileCfg != null && message.hasOwnProperty("profileCfg")) object.profileCfg = message.profileCfg;
      return object;
    };
    /**
     * Converts this chripParamMsg to JSON.
     * @function toJSON
     * @memberof Shadow.chripParamMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    chripParamMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return chripParamMsg;
  }();

  Shadow.environmentMsg = /*#__PURE__*/function () {
    /**
     * Properties of an environmentMsg.
     * @memberof Shadow
     * @interface IenvironmentMsg
     * @property {number|null} [rotation] environmentMsg rotation
     * @property {number|null} [upDis] environmentMsg upDis
     * @property {number|null} [downDis] environmentMsg downDis
     * @property {number|null} [leftDis] environmentMsg leftDis
     * @property {number|null} [rightDis] environmentMsg rightDis
     * @property {number|null} [height] environmentMsg height
     */

    /**
     * Constructs a new environmentMsg.
     * @memberof Shadow
     * @classdesc Represents an environmentMsg.
     * @implements IenvironmentMsg
     * @constructor
     * @param {Shadow.IenvironmentMsg=} [properties] Properties to set
     */
    function environmentMsg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * environmentMsg rotation.
     * @member {number} rotation
     * @memberof Shadow.environmentMsg
     * @instance
     */


    environmentMsg.prototype.rotation = 0;
    /**
     * environmentMsg upDis.
     * @member {number} upDis
     * @memberof Shadow.environmentMsg
     * @instance
     */

    environmentMsg.prototype.upDis = 0;
    /**
     * environmentMsg downDis.
     * @member {number} downDis
     * @memberof Shadow.environmentMsg
     * @instance
     */

    environmentMsg.prototype.downDis = 0;
    /**
     * environmentMsg leftDis.
     * @member {number} leftDis
     * @memberof Shadow.environmentMsg
     * @instance
     */

    environmentMsg.prototype.leftDis = 0;
    /**
     * environmentMsg rightDis.
     * @member {number} rightDis
     * @memberof Shadow.environmentMsg
     * @instance
     */

    environmentMsg.prototype.rightDis = 0;
    /**
     * environmentMsg height.
     * @member {number} height
     * @memberof Shadow.environmentMsg
     * @instance
     */

    environmentMsg.prototype.height = 0;
    /**
     * Creates a new environmentMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.environmentMsg
     * @static
     * @param {Shadow.IenvironmentMsg=} [properties] Properties to set
     * @returns {Shadow.environmentMsg} environmentMsg instance
     */

    environmentMsg.create = function create(properties) {
      return new environmentMsg(properties);
    };
    /**
     * Encodes the specified environmentMsg message. Does not implicitly {@link Shadow.environmentMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.environmentMsg
     * @static
     * @param {Shadow.IenvironmentMsg} message environmentMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    environmentMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.rotation != null && Object.hasOwnProperty.call(message, "rotation")) writer.uint32(
      /* id 1, wireType 1 =*/
      9).double(message.rotation);
      if (message.upDis != null && Object.hasOwnProperty.call(message, "upDis")) writer.uint32(
      /* id 2, wireType 1 =*/
      17).double(message.upDis);
      if (message.downDis != null && Object.hasOwnProperty.call(message, "downDis")) writer.uint32(
      /* id 3, wireType 1 =*/
      25).double(message.downDis);
      if (message.leftDis != null && Object.hasOwnProperty.call(message, "leftDis")) writer.uint32(
      /* id 4, wireType 1 =*/
      33).double(message.leftDis);
      if (message.rightDis != null && Object.hasOwnProperty.call(message, "rightDis")) writer.uint32(
      /* id 5, wireType 1 =*/
      41).double(message.rightDis);
      if (message.height != null && Object.hasOwnProperty.call(message, "height")) writer.uint32(
      /* id 6, wireType 1 =*/
      49).double(message.height);
      return writer;
    };
    /**
     * Encodes the specified environmentMsg message, length delimited. Does not implicitly {@link Shadow.environmentMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.environmentMsg
     * @static
     * @param {Shadow.IenvironmentMsg} message environmentMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    environmentMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes an environmentMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.environmentMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.environmentMsg} environmentMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    environmentMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.environmentMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.rotation = reader.double();
            break;

          case 2:
            message.upDis = reader.double();
            break;

          case 3:
            message.downDis = reader.double();
            break;

          case 4:
            message.leftDis = reader.double();
            break;

          case 5:
            message.rightDis = reader.double();
            break;

          case 6:
            message.height = reader.double();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes an environmentMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.environmentMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.environmentMsg} environmentMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    environmentMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies an environmentMsg message.
     * @function verify
     * @memberof Shadow.environmentMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    environmentMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.rotation != null && message.hasOwnProperty("rotation")) if (typeof message.rotation !== "number") return "rotation: number expected";
      if (message.upDis != null && message.hasOwnProperty("upDis")) if (typeof message.upDis !== "number") return "upDis: number expected";
      if (message.downDis != null && message.hasOwnProperty("downDis")) if (typeof message.downDis !== "number") return "downDis: number expected";
      if (message.leftDis != null && message.hasOwnProperty("leftDis")) if (typeof message.leftDis !== "number") return "leftDis: number expected";
      if (message.rightDis != null && message.hasOwnProperty("rightDis")) if (typeof message.rightDis !== "number") return "rightDis: number expected";
      if (message.height != null && message.hasOwnProperty("height")) if (typeof message.height !== "number") return "height: number expected";
      return null;
    };
    /**
     * Creates an environmentMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.environmentMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.environmentMsg} environmentMsg
     */


    environmentMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.environmentMsg) return object;
      let message = new $root$2.Shadow.environmentMsg();
      if (object.rotation != null) message.rotation = Number(object.rotation);
      if (object.upDis != null) message.upDis = Number(object.upDis);
      if (object.downDis != null) message.downDis = Number(object.downDis);
      if (object.leftDis != null) message.leftDis = Number(object.leftDis);
      if (object.rightDis != null) message.rightDis = Number(object.rightDis);
      if (object.height != null) message.height = Number(object.height);
      return message;
    };
    /**
     * Creates a plain object from an environmentMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.environmentMsg
     * @static
     * @param {Shadow.environmentMsg} message environmentMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    environmentMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.rotation = 0;
        object.upDis = 0;
        object.downDis = 0;
        object.leftDis = 0;
        object.rightDis = 0;
        object.height = 0;
      }

      if (message.rotation != null && message.hasOwnProperty("rotation")) object.rotation = options.json && !isFinite(message.rotation) ? String(message.rotation) : message.rotation;
      if (message.upDis != null && message.hasOwnProperty("upDis")) object.upDis = options.json && !isFinite(message.upDis) ? String(message.upDis) : message.upDis;
      if (message.downDis != null && message.hasOwnProperty("downDis")) object.downDis = options.json && !isFinite(message.downDis) ? String(message.downDis) : message.downDis;
      if (message.leftDis != null && message.hasOwnProperty("leftDis")) object.leftDis = options.json && !isFinite(message.leftDis) ? String(message.leftDis) : message.leftDis;
      if (message.rightDis != null && message.hasOwnProperty("rightDis")) object.rightDis = options.json && !isFinite(message.rightDis) ? String(message.rightDis) : message.rightDis;
      if (message.height != null && message.hasOwnProperty("height")) object.height = options.json && !isFinite(message.height) ? String(message.height) : message.height;
      return object;
    };
    /**
     * Converts this environmentMsg to JSON.
     * @function toJSON
     * @memberof Shadow.environmentMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    environmentMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return environmentMsg;
  }();

  Shadow.stitchParamMsg = /*#__PURE__*/function () {
    /**
     * Properties of a stitchParamMsg.
     * @memberof Shadow
     * @interface IstitchParamMsg
     * @property {number|null} [height] stitchParamMsg height
     * @property {number|null} [theta] stitchParamMsg theta
     * @property {number|null} [shiftX] stitchParamMsg shiftX
     * @property {number|null} [shiftZ] stitchParamMsg shiftZ
     */

    /**
     * Constructs a new stitchParamMsg.
     * @memberof Shadow
     * @classdesc Represents a stitchParamMsg.
     * @implements IstitchParamMsg
     * @constructor
     * @param {Shadow.IstitchParamMsg=} [properties] Properties to set
     */
    function stitchParamMsg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * stitchParamMsg height.
     * @member {number} height
     * @memberof Shadow.stitchParamMsg
     * @instance
     */


    stitchParamMsg.prototype.height = 0;
    /**
     * stitchParamMsg theta.
     * @member {number} theta
     * @memberof Shadow.stitchParamMsg
     * @instance
     */

    stitchParamMsg.prototype.theta = 0;
    /**
     * stitchParamMsg shiftX.
     * @member {number} shiftX
     * @memberof Shadow.stitchParamMsg
     * @instance
     */

    stitchParamMsg.prototype.shiftX = 0;
    /**
     * stitchParamMsg shiftZ.
     * @member {number} shiftZ
     * @memberof Shadow.stitchParamMsg
     * @instance
     */

    stitchParamMsg.prototype.shiftZ = 0;
    /**
     * Creates a new stitchParamMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.stitchParamMsg
     * @static
     * @param {Shadow.IstitchParamMsg=} [properties] Properties to set
     * @returns {Shadow.stitchParamMsg} stitchParamMsg instance
     */

    stitchParamMsg.create = function create(properties) {
      return new stitchParamMsg(properties);
    };
    /**
     * Encodes the specified stitchParamMsg message. Does not implicitly {@link Shadow.stitchParamMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.stitchParamMsg
     * @static
     * @param {Shadow.IstitchParamMsg} message stitchParamMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    stitchParamMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.height != null && Object.hasOwnProperty.call(message, "height")) writer.uint32(
      /* id 1, wireType 5 =*/
      13).float(message.height);
      if (message.theta != null && Object.hasOwnProperty.call(message, "theta")) writer.uint32(
      /* id 2, wireType 5 =*/
      21).float(message.theta);
      if (message.shiftX != null && Object.hasOwnProperty.call(message, "shiftX")) writer.uint32(
      /* id 3, wireType 5 =*/
      29).float(message.shiftX);
      if (message.shiftZ != null && Object.hasOwnProperty.call(message, "shiftZ")) writer.uint32(
      /* id 4, wireType 5 =*/
      37).float(message.shiftZ);
      return writer;
    };
    /**
     * Encodes the specified stitchParamMsg message, length delimited. Does not implicitly {@link Shadow.stitchParamMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.stitchParamMsg
     * @static
     * @param {Shadow.IstitchParamMsg} message stitchParamMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    stitchParamMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a stitchParamMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.stitchParamMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.stitchParamMsg} stitchParamMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    stitchParamMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.stitchParamMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.height = reader.float();
            break;

          case 2:
            message.theta = reader.float();
            break;

          case 3:
            message.shiftX = reader.float();
            break;

          case 4:
            message.shiftZ = reader.float();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a stitchParamMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.stitchParamMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.stitchParamMsg} stitchParamMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    stitchParamMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a stitchParamMsg message.
     * @function verify
     * @memberof Shadow.stitchParamMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    stitchParamMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.height != null && message.hasOwnProperty("height")) if (typeof message.height !== "number") return "height: number expected";
      if (message.theta != null && message.hasOwnProperty("theta")) if (typeof message.theta !== "number") return "theta: number expected";
      if (message.shiftX != null && message.hasOwnProperty("shiftX")) if (typeof message.shiftX !== "number") return "shiftX: number expected";
      if (message.shiftZ != null && message.hasOwnProperty("shiftZ")) if (typeof message.shiftZ !== "number") return "shiftZ: number expected";
      return null;
    };
    /**
     * Creates a stitchParamMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.stitchParamMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.stitchParamMsg} stitchParamMsg
     */


    stitchParamMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.stitchParamMsg) return object;
      let message = new $root$2.Shadow.stitchParamMsg();
      if (object.height != null) message.height = Number(object.height);
      if (object.theta != null) message.theta = Number(object.theta);
      if (object.shiftX != null) message.shiftX = Number(object.shiftX);
      if (object.shiftZ != null) message.shiftZ = Number(object.shiftZ);
      return message;
    };
    /**
     * Creates a plain object from a stitchParamMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.stitchParamMsg
     * @static
     * @param {Shadow.stitchParamMsg} message stitchParamMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    stitchParamMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.height = 0;
        object.theta = 0;
        object.shiftX = 0;
        object.shiftZ = 0;
      }

      if (message.height != null && message.hasOwnProperty("height")) object.height = options.json && !isFinite(message.height) ? String(message.height) : message.height;
      if (message.theta != null && message.hasOwnProperty("theta")) object.theta = options.json && !isFinite(message.theta) ? String(message.theta) : message.theta;
      if (message.shiftX != null && message.hasOwnProperty("shiftX")) object.shiftX = options.json && !isFinite(message.shiftX) ? String(message.shiftX) : message.shiftX;
      if (message.shiftZ != null && message.hasOwnProperty("shiftZ")) object.shiftZ = options.json && !isFinite(message.shiftZ) ? String(message.shiftZ) : message.shiftZ;
      return object;
    };
    /**
     * Converts this stitchParamMsg to JSON.
     * @function toJSON
     * @memberof Shadow.stitchParamMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    stitchParamMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return stitchParamMsg;
  }();

  Shadow.healthMsg = /*#__PURE__*/function () {
    /**
     * Properties of a healthMsg.
     * @memberof Shadow
     * @interface IhealthMsg
     * @property {number|null} [uptime] healthMsg uptime
     * @property {string|null} [rebootReason] healthMsg rebootReason
     */

    /**
     * Constructs a new healthMsg.
     * @memberof Shadow
     * @classdesc Represents a healthMsg.
     * @implements IhealthMsg
     * @constructor
     * @param {Shadow.IhealthMsg=} [properties] Properties to set
     */
    function healthMsg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * healthMsg uptime.
     * @member {number} uptime
     * @memberof Shadow.healthMsg
     * @instance
     */


    healthMsg.prototype.uptime = 0;
    /**
     * healthMsg rebootReason.
     * @member {string} rebootReason
     * @memberof Shadow.healthMsg
     * @instance
     */

    healthMsg.prototype.rebootReason = "";
    /**
     * Creates a new healthMsg instance using the specified properties.
     * @function create
     * @memberof Shadow.healthMsg
     * @static
     * @param {Shadow.IhealthMsg=} [properties] Properties to set
     * @returns {Shadow.healthMsg} healthMsg instance
     */

    healthMsg.create = function create(properties) {
      return new healthMsg(properties);
    };
    /**
     * Encodes the specified healthMsg message. Does not implicitly {@link Shadow.healthMsg.verify|verify} messages.
     * @function encode
     * @memberof Shadow.healthMsg
     * @static
     * @param {Shadow.IhealthMsg} message healthMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    healthMsg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.uptime != null && Object.hasOwnProperty.call(message, "uptime")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.uptime);
      if (message.rebootReason != null && Object.hasOwnProperty.call(message, "rebootReason")) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.rebootReason);
      return writer;
    };
    /**
     * Encodes the specified healthMsg message, length delimited. Does not implicitly {@link Shadow.healthMsg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.healthMsg
     * @static
     * @param {Shadow.IhealthMsg} message healthMsg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    healthMsg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a healthMsg message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.healthMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.healthMsg} healthMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    healthMsg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.healthMsg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.uptime = reader.uint32();
            break;

          case 2:
            message.rebootReason = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a healthMsg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.healthMsg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.healthMsg} healthMsg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    healthMsg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a healthMsg message.
     * @function verify
     * @memberof Shadow.healthMsg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    healthMsg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.uptime != null && message.hasOwnProperty("uptime")) if (!$util$2.isInteger(message.uptime)) return "uptime: integer expected";
      if (message.rebootReason != null && message.hasOwnProperty("rebootReason")) if (!$util$2.isString(message.rebootReason)) return "rebootReason: string expected";
      return null;
    };
    /**
     * Creates a healthMsg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.healthMsg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.healthMsg} healthMsg
     */


    healthMsg.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.healthMsg) return object;
      let message = new $root$2.Shadow.healthMsg();
      if (object.uptime != null) message.uptime = object.uptime >>> 0;
      if (object.rebootReason != null) message.rebootReason = String(object.rebootReason);
      return message;
    };
    /**
     * Creates a plain object from a healthMsg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.healthMsg
     * @static
     * @param {Shadow.healthMsg} message healthMsg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    healthMsg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.uptime = 0;
        object.rebootReason = "";
      }

      if (message.uptime != null && message.hasOwnProperty("uptime")) object.uptime = message.uptime;
      if (message.rebootReason != null && message.hasOwnProperty("rebootReason")) object.rebootReason = message.rebootReason;
      return object;
    };
    /**
     * Converts this healthMsg to JSON.
     * @function toJSON
     * @memberof Shadow.healthMsg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    healthMsg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return healthMsg;
  }();

  Shadow.DeviceReq = /*#__PURE__*/function () {
    /**
     * Properties of a DeviceReq.
     * @memberof Shadow
     * @interface IDeviceReq
     * @property {string|null} [token] DeviceReq token
     */

    /**
     * Constructs a new DeviceReq.
     * @memberof Shadow
     * @classdesc Represents a DeviceReq.
     * @implements IDeviceReq
     * @constructor
     * @param {Shadow.IDeviceReq=} [properties] Properties to set
     */
    function DeviceReq(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * DeviceReq token.
     * @member {string} token
     * @memberof Shadow.DeviceReq
     * @instance
     */


    DeviceReq.prototype.token = "";
    /**
     * Creates a new DeviceReq instance using the specified properties.
     * @function create
     * @memberof Shadow.DeviceReq
     * @static
     * @param {Shadow.IDeviceReq=} [properties] Properties to set
     * @returns {Shadow.DeviceReq} DeviceReq instance
     */

    DeviceReq.create = function create(properties) {
      return new DeviceReq(properties);
    };
    /**
     * Encodes the specified DeviceReq message. Does not implicitly {@link Shadow.DeviceReq.verify|verify} messages.
     * @function encode
     * @memberof Shadow.DeviceReq
     * @static
     * @param {Shadow.IDeviceReq} message DeviceReq message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    DeviceReq.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.token != null && Object.hasOwnProperty.call(message, "token")) writer.uint32(
      /* id 1, wireType 2 =*/
      10).string(message.token);
      return writer;
    };
    /**
     * Encodes the specified DeviceReq message, length delimited. Does not implicitly {@link Shadow.DeviceReq.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.DeviceReq
     * @static
     * @param {Shadow.IDeviceReq} message DeviceReq message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    DeviceReq.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a DeviceReq message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.DeviceReq
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.DeviceReq} DeviceReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    DeviceReq.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.DeviceReq();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.token = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a DeviceReq message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.DeviceReq
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.DeviceReq} DeviceReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    DeviceReq.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a DeviceReq message.
     * @function verify
     * @memberof Shadow.DeviceReq
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    DeviceReq.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.token != null && message.hasOwnProperty("token")) if (!$util$2.isString(message.token)) return "token: string expected";
      return null;
    };
    /**
     * Creates a DeviceReq message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.DeviceReq
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.DeviceReq} DeviceReq
     */


    DeviceReq.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.DeviceReq) return object;
      let message = new $root$2.Shadow.DeviceReq();
      if (object.token != null) message.token = String(object.token);
      return message;
    };
    /**
     * Creates a plain object from a DeviceReq message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.DeviceReq
     * @static
     * @param {Shadow.DeviceReq} message DeviceReq
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    DeviceReq.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.defaults) object.token = "";
      if (message.token != null && message.hasOwnProperty("token")) object.token = message.token;
      return object;
    };
    /**
     * Converts this DeviceReq to JSON.
     * @function toJSON
     * @memberof Shadow.DeviceReq
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    DeviceReq.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return DeviceReq;
  }();

  Shadow.SetMsgReq = /*#__PURE__*/function () {
    /**
     * Properties of a SetMsgReq.
     * @memberof Shadow
     * @interface ISetMsgReq
     * @property {string|null} [token] SetMsgReq token
     * @property {string|null} [msg] SetMsgReq msg
     */

    /**
     * Constructs a new SetMsgReq.
     * @memberof Shadow
     * @classdesc Represents a SetMsgReq.
     * @implements ISetMsgReq
     * @constructor
     * @param {Shadow.ISetMsgReq=} [properties] Properties to set
     */
    function SetMsgReq(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * SetMsgReq token.
     * @member {string} token
     * @memberof Shadow.SetMsgReq
     * @instance
     */


    SetMsgReq.prototype.token = "";
    /**
     * SetMsgReq msg.
     * @member {string} msg
     * @memberof Shadow.SetMsgReq
     * @instance
     */

    SetMsgReq.prototype.msg = "";
    /**
     * Creates a new SetMsgReq instance using the specified properties.
     * @function create
     * @memberof Shadow.SetMsgReq
     * @static
     * @param {Shadow.ISetMsgReq=} [properties] Properties to set
     * @returns {Shadow.SetMsgReq} SetMsgReq instance
     */

    SetMsgReq.create = function create(properties) {
      return new SetMsgReq(properties);
    };
    /**
     * Encodes the specified SetMsgReq message. Does not implicitly {@link Shadow.SetMsgReq.verify|verify} messages.
     * @function encode
     * @memberof Shadow.SetMsgReq
     * @static
     * @param {Shadow.ISetMsgReq} message SetMsgReq message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    SetMsgReq.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.token != null && Object.hasOwnProperty.call(message, "token")) writer.uint32(
      /* id 1, wireType 2 =*/
      10).string(message.token);
      if (message.msg != null && Object.hasOwnProperty.call(message, "msg")) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.msg);
      return writer;
    };
    /**
     * Encodes the specified SetMsgReq message, length delimited. Does not implicitly {@link Shadow.SetMsgReq.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.SetMsgReq
     * @static
     * @param {Shadow.ISetMsgReq} message SetMsgReq message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    SetMsgReq.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a SetMsgReq message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.SetMsgReq
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.SetMsgReq} SetMsgReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    SetMsgReq.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.SetMsgReq();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.token = reader.string();
            break;

          case 2:
            message.msg = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a SetMsgReq message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.SetMsgReq
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.SetMsgReq} SetMsgReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    SetMsgReq.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a SetMsgReq message.
     * @function verify
     * @memberof Shadow.SetMsgReq
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    SetMsgReq.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.token != null && message.hasOwnProperty("token")) if (!$util$2.isString(message.token)) return "token: string expected";
      if (message.msg != null && message.hasOwnProperty("msg")) if (!$util$2.isString(message.msg)) return "msg: string expected";
      return null;
    };
    /**
     * Creates a SetMsgReq message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.SetMsgReq
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.SetMsgReq} SetMsgReq
     */


    SetMsgReq.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.SetMsgReq) return object;
      let message = new $root$2.Shadow.SetMsgReq();
      if (object.token != null) message.token = String(object.token);
      if (object.msg != null) message.msg = String(object.msg);
      return message;
    };
    /**
     * Creates a plain object from a SetMsgReq message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.SetMsgReq
     * @static
     * @param {Shadow.SetMsgReq} message SetMsgReq
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    SetMsgReq.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.token = "";
        object.msg = "";
      }

      if (message.token != null && message.hasOwnProperty("token")) object.token = message.token;
      if (message.msg != null && message.hasOwnProperty("msg")) object.msg = message.msg;
      return object;
    };
    /**
     * Converts this SetMsgReq to JSON.
     * @function toJSON
     * @memberof Shadow.SetMsgReq
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    SetMsgReq.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return SetMsgReq;
  }();

  Shadow.Res = /*#__PURE__*/function () {
    /**
     * Properties of a Res.
     * @memberof Shadow
     * @interface IRes
     * @property {Shadow.Res.Status|null} [status] Res status
     * @property {string|null} [error] Res error
     */

    /**
     * Constructs a new Res.
     * @memberof Shadow
     * @classdesc Represents a Res.
     * @implements IRes
     * @constructor
     * @param {Shadow.IRes=} [properties] Properties to set
     */
    function Res(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Res status.
     * @member {Shadow.Res.Status} status
     * @memberof Shadow.Res
     * @instance
     */


    Res.prototype.status = 0;
    /**
     * Res error.
     * @member {string} error
     * @memberof Shadow.Res
     * @instance
     */

    Res.prototype.error = "";
    /**
     * Creates a new Res instance using the specified properties.
     * @function create
     * @memberof Shadow.Res
     * @static
     * @param {Shadow.IRes=} [properties] Properties to set
     * @returns {Shadow.Res} Res instance
     */

    Res.create = function create(properties) {
      return new Res(properties);
    };
    /**
     * Encodes the specified Res message. Does not implicitly {@link Shadow.Res.verify|verify} messages.
     * @function encode
     * @memberof Shadow.Res
     * @static
     * @param {Shadow.IRes} message Res message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Res.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$2.create();
      if (message.status != null && Object.hasOwnProperty.call(message, "status")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).int32(message.status);
      if (message.error != null && Object.hasOwnProperty.call(message, "error")) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.error);
      return writer;
    };
    /**
     * Encodes the specified Res message, length delimited. Does not implicitly {@link Shadow.Res.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Shadow.Res
     * @static
     * @param {Shadow.IRes} message Res message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Res.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Res message from the specified reader or buffer.
     * @function decode
     * @memberof Shadow.Res
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Shadow.Res} Res
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Res.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$2)) reader = $Reader$2.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$2.Shadow.Res();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.status = reader.int32();
            break;

          case 2:
            message.error = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Res message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Shadow.Res
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Shadow.Res} Res
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Res.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$2)) reader = new $Reader$2(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Res message.
     * @function verify
     * @memberof Shadow.Res
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Res.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.status != null && message.hasOwnProperty("status")) switch (message.status) {
        default:
          return "status: enum value expected";

        case 0:
        case 1:
          break;
      }
      if (message.error != null && message.hasOwnProperty("error")) if (!$util$2.isString(message.error)) return "error: string expected";
      return null;
    };
    /**
     * Creates a Res message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Shadow.Res
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Shadow.Res} Res
     */


    Res.fromObject = function fromObject(object) {
      if (object instanceof $root$2.Shadow.Res) return object;
      let message = new $root$2.Shadow.Res();

      switch (object.status) {
        case "OK":
        case 0:
          message.status = 0;
          break;

        case "Fail":
        case 1:
          message.status = 1;
          break;
      }

      if (object.error != null) message.error = String(object.error);
      return message;
    };
    /**
     * Creates a plain object from a Res message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Shadow.Res
     * @static
     * @param {Shadow.Res} message Res
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Res.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.status = options.enums === String ? "OK" : 0;
        object.error = "";
      }

      if (message.status != null && message.hasOwnProperty("status")) object.status = options.enums === String ? $root$2.Shadow.Res.Status[message.status] : message.status;
      if (message.error != null && message.hasOwnProperty("error")) object.error = message.error;
      return object;
    };
    /**
     * Converts this Res to JSON.
     * @function toJSON
     * @memberof Shadow.Res
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Res.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * Status enum.
     * @name Shadow.Res.Status
     * @enum {number}
     * @property {number} OK=0 OK value
     * @property {number} Fail=1 Fail value
     */


    Res.Status = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "OK"] = 0;
      values[valuesById[1] = "Fail"] = 1;
      return values;
    }();

    return Res;
  }();

  Shadow.Service = /*#__PURE__*/function () {
    /**
     * Constructs a new Service service.
     * @memberof Shadow
     * @classdesc Represents a Service
     * @extends $protobuf.rpc.Service
     * @constructor
     * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
     * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
     * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
     */
    function Service(rpcImpl, requestDelimited, responseDelimited) {
      $protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
    }

    (Service.prototype = /*#__PURE__*/Object.create($protobuf.rpc.Service.prototype)).constructor = Service;
    /**
     * Creates new Service service using the specified rpc implementation.
     * @function create
     * @memberof Shadow.Service
     * @static
     * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
     * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
     * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
     * @returns {Service} RPC service. Useful where requests and/or responses are streamed.
     */

    Service.create = function create(rpcImpl, requestDelimited, responseDelimited) {
      return new this(rpcImpl, requestDelimited, responseDelimited);
    };
    /**
     * Callback as used by {@link Shadow.Service#getMsg}.
     * @memberof Shadow.Service
     * @typedef GetMsgCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Shadow.Msg} [response] Msg
     */

    /**
     * Calls GetMsg.
     * @function getMsg
     * @memberof Shadow.Service
     * @instance
     * @param {Shadow.IDeviceReq} request DeviceReq message or plain object
     * @param {Shadow.Service.GetMsgCallback} callback Node-style callback called with the error, if any, and Msg
     * @returns {undefined}
     * @variation 1
     */


    Object.defineProperty(Service.prototype.getMsg = function getMsg(request, callback) {
      return this.rpcCall(getMsg, $root$2.Shadow.DeviceReq, $root$2.Shadow.Msg, request, callback);
    }, "name", {
      value: "GetMsg"
    });
    /**
     * Calls GetMsg.
     * @function getMsg
     * @memberof Shadow.Service
     * @instance
     * @param {Shadow.IDeviceReq} request DeviceReq message or plain object
     * @returns {Promise<Shadow.Msg>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link Shadow.Service#setMsg}.
     * @memberof Shadow.Service
     * @typedef SetMsgCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Shadow.Res} [response] Res
     */

    /**
     * Calls SetMsg.
     * @function setMsg
     * @memberof Shadow.Service
     * @instance
     * @param {Shadow.ISetMsgReq} request SetMsgReq message or plain object
     * @param {Shadow.Service.SetMsgCallback} callback Node-style callback called with the error, if any, and Res
     * @returns {undefined}
     * @variation 1
     */

    Object.defineProperty(Service.prototype.setMsg = function setMsg(request, callback) {
      return this.rpcCall(setMsg, $root$2.Shadow.SetMsgReq, $root$2.Shadow.Res, request, callback);
    }, "name", {
      value: "SetMsg"
    });
    /**
     * Calls SetMsg.
     * @function setMsg
     * @memberof Shadow.Service
     * @instance
     * @param {Shadow.ISetMsgReq} request SetMsgReq message or plain object
     * @returns {Promise<Shadow.Res>} Promise
     * @variation 2
     */

    return Service;
  }();

  return Shadow;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$3 = $protobuf.Reader,
      $Writer$3 = $protobuf.Writer,
      $util$3 = $protobuf.util; // Exported root namespace

const $root$3 = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Webrtc = $root$3.Webrtc = /*#__PURE__*/(() => {
  /**
   * Namespace Webrtc.
   * @exports Webrtc
   * @namespace
   */
  const Webrtc = {};

  Webrtc.Msg = /*#__PURE__*/function () {
    /**
     * Properties of a Msg.
     * @memberof Webrtc
     * @interface IMsg
     * @property {number|null} [timestamp] Msg timestamp
     * @property {Webrtc.Msg.Type|null} [type] Msg type
     * @property {string|null} [payload] Msg payload
     * @property {string|null} [clientid] Msg clientid
     */

    /**
     * Constructs a new Msg.
     * @memberof Webrtc
     * @classdesc Represents a Msg.
     * @implements IMsg
     * @constructor
     * @param {Webrtc.IMsg=} [properties] Properties to set
     */
    function Msg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Msg timestamp.
     * @member {number} timestamp
     * @memberof Webrtc.Msg
     * @instance
     */


    Msg.prototype.timestamp = 0;
    /**
     * Msg type.
     * @member {Webrtc.Msg.Type} type
     * @memberof Webrtc.Msg
     * @instance
     */

    Msg.prototype.type = 0;
    /**
     * Msg payload.
     * @member {string} payload
     * @memberof Webrtc.Msg
     * @instance
     */

    Msg.prototype.payload = "";
    /**
     * Msg clientid.
     * @member {string} clientid
     * @memberof Webrtc.Msg
     * @instance
     */

    Msg.prototype.clientid = "";
    /**
     * Creates a new Msg instance using the specified properties.
     * @function create
     * @memberof Webrtc.Msg
     * @static
     * @param {Webrtc.IMsg=} [properties] Properties to set
     * @returns {Webrtc.Msg} Msg instance
     */

    Msg.create = function create(properties) {
      return new Msg(properties);
    };
    /**
     * Encodes the specified Msg message. Does not implicitly {@link Webrtc.Msg.verify|verify} messages.
     * @function encode
     * @memberof Webrtc.Msg
     * @static
     * @param {Webrtc.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$3.create();
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.timestamp);
      if (message.type != null && Object.hasOwnProperty.call(message, "type")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).int32(message.type);
      if (message.payload != null && Object.hasOwnProperty.call(message, "payload")) writer.uint32(
      /* id 3, wireType 2 =*/
      26).string(message.payload);
      if (message.clientid != null && Object.hasOwnProperty.call(message, "clientid")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).string(message.clientid);
      return writer;
    };
    /**
     * Encodes the specified Msg message, length delimited. Does not implicitly {@link Webrtc.Msg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Webrtc.Msg
     * @static
     * @param {Webrtc.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Msg message from the specified reader or buffer.
     * @function decode
     * @memberof Webrtc.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Webrtc.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$3)) reader = $Reader$3.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$3.Webrtc.Msg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.timestamp = reader.uint32();
            break;

          case 2:
            message.type = reader.int32();
            break;

          case 3:
            message.payload = reader.string();
            break;

          case 4:
            message.clientid = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Msg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Webrtc.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Webrtc.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$3)) reader = new $Reader$3(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Msg message.
     * @function verify
     * @memberof Webrtc.Msg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Msg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$3.isInteger(message.timestamp)) return "timestamp: integer expected";
      if (message.type != null && message.hasOwnProperty("type")) switch (message.type) {
        default:
          return "type: enum value expected";

        case 0:
        case 1:
        case 2:
          break;
      }
      if (message.payload != null && message.hasOwnProperty("payload")) if (!$util$3.isString(message.payload)) return "payload: string expected";
      if (message.clientid != null && message.hasOwnProperty("clientid")) if (!$util$3.isString(message.clientid)) return "clientid: string expected";
      return null;
    };
    /**
     * Creates a Msg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Webrtc.Msg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Webrtc.Msg} Msg
     */


    Msg.fromObject = function fromObject(object) {
      if (object instanceof $root$3.Webrtc.Msg) return object;
      let message = new $root$3.Webrtc.Msg();
      if (object.timestamp != null) message.timestamp = object.timestamp >>> 0;

      switch (object.type) {
        case "OFFER":
        case 0:
          message.type = 0;
          break;

        case "ANSWER":
        case 1:
          message.type = 1;
          break;

        case "CANDIDATE":
        case 2:
          message.type = 2;
          break;
      }

      if (object.payload != null) message.payload = String(object.payload);
      if (object.clientid != null) message.clientid = String(object.clientid);
      return message;
    };
    /**
     * Creates a plain object from a Msg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Webrtc.Msg
     * @static
     * @param {Webrtc.Msg} message Msg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Msg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.timestamp = 0;
        object.type = options.enums === String ? "OFFER" : 0;
        object.payload = "";
        object.clientid = "";
      }

      if (message.timestamp != null && message.hasOwnProperty("timestamp")) object.timestamp = message.timestamp;
      if (message.type != null && message.hasOwnProperty("type")) object.type = options.enums === String ? $root$3.Webrtc.Msg.Type[message.type] : message.type;
      if (message.payload != null && message.hasOwnProperty("payload")) object.payload = message.payload;
      if (message.clientid != null && message.hasOwnProperty("clientid")) object.clientid = message.clientid;
      return object;
    };
    /**
     * Converts this Msg to JSON.
     * @function toJSON
     * @memberof Webrtc.Msg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Msg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * Type enum.
     * @name Webrtc.Msg.Type
     * @enum {number}
     * @property {number} OFFER=0 OFFER value
     * @property {number} ANSWER=1 ANSWER value
     * @property {number} CANDIDATE=2 CANDIDATE value
     */


    Msg.Type = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "OFFER"] = 0;
      values[valuesById[1] = "ANSWER"] = 1;
      values[valuesById[2] = "CANDIDATE"] = 2;
      return values;
    }();

    return Msg;
  }();

  return Webrtc;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$4 = $protobuf.Reader,
      $Writer$4 = $protobuf.Writer;
 // Exported root namespace

const $root$4 = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Rule = $root$4.Rule = /*#__PURE__*/(() => {
  /**
   * Namespace Rule.
   * @exports Rule
   * @namespace
   */
  const Rule = {};

  Rule.Msg = /*#__PURE__*/function () {
    /**
     * Properties of a Msg.
     * @memberof Rule
     * @interface IMsg
     * @property {Rule.Msg.Type|null} [type] Msg type
     */

    /**
     * Constructs a new Msg.
     * @memberof Rule
     * @classdesc Represents a Msg.
     * @implements IMsg
     * @constructor
     * @param {Rule.IMsg=} [properties] Properties to set
     */
    function Msg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Msg type.
     * @member {Rule.Msg.Type} type
     * @memberof Rule.Msg
     * @instance
     */


    Msg.prototype.type = 0;
    /**
     * Creates a new Msg instance using the specified properties.
     * @function create
     * @memberof Rule.Msg
     * @static
     * @param {Rule.IMsg=} [properties] Properties to set
     * @returns {Rule.Msg} Msg instance
     */

    Msg.create = function create(properties) {
      return new Msg(properties);
    };
    /**
     * Encodes the specified Msg message. Does not implicitly {@link Rule.Msg.verify|verify} messages.
     * @function encode
     * @memberof Rule.Msg
     * @static
     * @param {Rule.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$4.create();
      if (message.type != null && Object.hasOwnProperty.call(message, "type")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).int32(message.type);
      return writer;
    };
    /**
     * Encodes the specified Msg message, length delimited. Does not implicitly {@link Rule.Msg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Rule.Msg
     * @static
     * @param {Rule.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Msg message from the specified reader or buffer.
     * @function decode
     * @memberof Rule.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Rule.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$4)) reader = $Reader$4.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$4.Rule.Msg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.type = reader.int32();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Msg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Rule.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Rule.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$4)) reader = new $Reader$4(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Msg message.
     * @function verify
     * @memberof Rule.Msg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Msg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.type != null && message.hasOwnProperty("type")) switch (message.type) {
        default:
          return "type: enum value expected";

        case 0:
        case 1:
        case 2:
        case 3:
          break;
      }
      return null;
    };
    /**
     * Creates a Msg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Rule.Msg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Rule.Msg} Msg
     */


    Msg.fromObject = function fromObject(object) {
      if (object instanceof $root$4.Rule.Msg) return object;
      let message = new $root$4.Rule.Msg();

      switch (object.type) {
        case "None":
        case 0:
          message.type = 0;
          break;

        case "Online":
        case 1:
          message.type = 1;
          break;

        case "Offline":
        case 2:
          message.type = 2;
          break;

        case "Ring":
        case 3:
          message.type = 3;
          break;
      }

      return message;
    };
    /**
     * Creates a plain object from a Msg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Rule.Msg
     * @static
     * @param {Rule.Msg} message Msg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Msg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.defaults) object.type = options.enums === String ? "None" : 0;
      if (message.type != null && message.hasOwnProperty("type")) object.type = options.enums === String ? $root$4.Rule.Msg.Type[message.type] : message.type;
      return object;
    };
    /**
     * Converts this Msg to JSON.
     * @function toJSON
     * @memberof Rule.Msg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Msg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * Type enum.
     * @name Rule.Msg.Type
     * @enum {number}
     * @property {number} None=0 None value
     * @property {number} Online=1 Online value
     * @property {number} Offline=2 Offline value
     * @property {number} Ring=3 Ring value
     */


    Msg.Type = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "None"] = 0;
      values[valuesById[1] = "Online"] = 1;
      values[valuesById[2] = "Offline"] = 2;
      values[valuesById[3] = "Ring"] = 3;
      return values;
    }();

    return Msg;
  }();

  return Rule;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$5 = $protobuf.Reader,
      $Writer$5 = $protobuf.Writer,
      $util$4 = $protobuf.util; // Exported root namespace

const $root$5 = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Peopletrack = $root$5.Peopletrack = /*#__PURE__*/(() => {
  /**
   * Namespace Peopletrack.
   * @exports Peopletrack
   * @namespace
   */
  const Peopletrack = {};

  Peopletrack.People = /*#__PURE__*/function () {
    /**
     * Properties of a People.
     * @memberof Peopletrack
     * @interface IPeople
     * @property {number|null} [tid] People tid
     * @property {number|null} [posX] People posX
     * @property {number|null} [posY] People posY
     * @property {number|null} [posZ] People posZ
     * @property {Peopletrack.People.State|null} [state] People state
     * @property {Peopletrack.People.Stance|null} [stance] People stance
     */

    /**
     * Constructs a new People.
     * @memberof Peopletrack
     * @classdesc Represents a People.
     * @implements IPeople
     * @constructor
     * @param {Peopletrack.IPeople=} [properties] Properties to set
     */
    function People(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * People tid.
     * @member {number} tid
     * @memberof Peopletrack.People
     * @instance
     */


    People.prototype.tid = 0;
    /**
     * People posX.
     * @member {number} posX
     * @memberof Peopletrack.People
     * @instance
     */

    People.prototype.posX = 0;
    /**
     * People posY.
     * @member {number} posY
     * @memberof Peopletrack.People
     * @instance
     */

    People.prototype.posY = 0;
    /**
     * People posZ.
     * @member {number} posZ
     * @memberof Peopletrack.People
     * @instance
     */

    People.prototype.posZ = 0;
    /**
     * People state.
     * @member {Peopletrack.People.State} state
     * @memberof Peopletrack.People
     * @instance
     */

    People.prototype.state = 0;
    /**
     * People stance.
     * @member {Peopletrack.People.Stance} stance
     * @memberof Peopletrack.People
     * @instance
     */

    People.prototype.stance = 0;
    /**
     * Creates a new People instance using the specified properties.
     * @function create
     * @memberof Peopletrack.People
     * @static
     * @param {Peopletrack.IPeople=} [properties] Properties to set
     * @returns {Peopletrack.People} People instance
     */

    People.create = function create(properties) {
      return new People(properties);
    };
    /**
     * Encodes the specified People message. Does not implicitly {@link Peopletrack.People.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.People
     * @static
     * @param {Peopletrack.IPeople} message People message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    People.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.tid != null && Object.hasOwnProperty.call(message, "tid")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.tid);
      if (message.posX != null && Object.hasOwnProperty.call(message, "posX")) writer.uint32(
      /* id 2, wireType 5 =*/
      21).float(message.posX);
      if (message.posY != null && Object.hasOwnProperty.call(message, "posY")) writer.uint32(
      /* id 3, wireType 5 =*/
      29).float(message.posY);
      if (message.posZ != null && Object.hasOwnProperty.call(message, "posZ")) writer.uint32(
      /* id 4, wireType 5 =*/
      37).float(message.posZ);
      if (message.state != null && Object.hasOwnProperty.call(message, "state")) writer.uint32(
      /* id 5, wireType 0 =*/
      40).int32(message.state);
      if (message.stance != null && Object.hasOwnProperty.call(message, "stance")) writer.uint32(
      /* id 6, wireType 0 =*/
      48).int32(message.stance);
      return writer;
    };
    /**
     * Encodes the specified People message, length delimited. Does not implicitly {@link Peopletrack.People.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.People
     * @static
     * @param {Peopletrack.IPeople} message People message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    People.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a People message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.People
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.People} People
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    People.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.People();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.tid = reader.uint32();
            break;

          case 2:
            message.posX = reader.float();
            break;

          case 3:
            message.posY = reader.float();
            break;

          case 4:
            message.posZ = reader.float();
            break;

          case 5:
            message.state = reader.int32();
            break;

          case 6:
            message.stance = reader.int32();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a People message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.People
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.People} People
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    People.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a People message.
     * @function verify
     * @memberof Peopletrack.People
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    People.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.tid != null && message.hasOwnProperty("tid")) if (!$util$4.isInteger(message.tid)) return "tid: integer expected";
      if (message.posX != null && message.hasOwnProperty("posX")) if (typeof message.posX !== "number") return "posX: number expected";
      if (message.posY != null && message.hasOwnProperty("posY")) if (typeof message.posY !== "number") return "posY: number expected";
      if (message.posZ != null && message.hasOwnProperty("posZ")) if (typeof message.posZ !== "number") return "posZ: number expected";
      if (message.state != null && message.hasOwnProperty("state")) switch (message.state) {
        default:
          return "state: enum value expected";

        case 0:
        case 1:
        case 2:
          break;
      }
      if (message.stance != null && message.hasOwnProperty("stance")) switch (message.stance) {
        default:
          return "stance: enum value expected";

        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
          break;
      }
      return null;
    };
    /**
     * Creates a People message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.People
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.People} People
     */


    People.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.People) return object;
      let message = new $root$5.Peopletrack.People();
      if (object.tid != null) message.tid = object.tid >>> 0;
      if (object.posX != null) message.posX = Number(object.posX);
      if (object.posY != null) message.posY = Number(object.posY);
      if (object.posZ != null) message.posZ = Number(object.posZ);

      switch (object.state) {
        case "NONE":
        case 0:
          message.state = 0;
          break;

        case "ACTIVE":
        case 1:
          message.state = 1;
          break;

        case "STATIC":
        case 2:
          message.state = 2;
          break;
      }

      switch (object.stance) {
        case "UNKNOWN":
        case 0:
          message.stance = 0;
          break;

        case "STAND":
        case 1:
          message.stance = 1;
          break;

        case "SIT":
        case 2:
          message.stance = 2;
          break;

        case "LAY":
        case 3:
          message.stance = 3;
          break;

        case "SUSFALL":
        case 4:
          message.stance = 4;
          break;

        case "COUNTDOWN":
        case 5:
          message.stance = 5;
          break;

        case "DETFALL":
        case 6:
          message.stance = 6;
          break;
      }

      return message;
    };
    /**
     * Creates a plain object from a People message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.People
     * @static
     * @param {Peopletrack.People} message People
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    People.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.tid = 0;
        object.posX = 0;
        object.posY = 0;
        object.posZ = 0;
        object.state = options.enums === String ? "NONE" : 0;
        object.stance = options.enums === String ? "UNKNOWN" : 0;
      }

      if (message.tid != null && message.hasOwnProperty("tid")) object.tid = message.tid;
      if (message.posX != null && message.hasOwnProperty("posX")) object.posX = options.json && !isFinite(message.posX) ? String(message.posX) : message.posX;
      if (message.posY != null && message.hasOwnProperty("posY")) object.posY = options.json && !isFinite(message.posY) ? String(message.posY) : message.posY;
      if (message.posZ != null && message.hasOwnProperty("posZ")) object.posZ = options.json && !isFinite(message.posZ) ? String(message.posZ) : message.posZ;
      if (message.state != null && message.hasOwnProperty("state")) object.state = options.enums === String ? $root$5.Peopletrack.People.State[message.state] : message.state;
      if (message.stance != null && message.hasOwnProperty("stance")) object.stance = options.enums === String ? $root$5.Peopletrack.People.Stance[message.stance] : message.stance;
      return object;
    };
    /**
     * Converts this People to JSON.
     * @function toJSON
     * @memberof Peopletrack.People
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    People.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * State enum.
     * @name Peopletrack.People.State
     * @enum {number}
     * @property {number} NONE=0 NONE value
     * @property {number} ACTIVE=1 ACTIVE value
     * @property {number} STATIC=2 STATIC value
     */


    People.State = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "NONE"] = 0;
      values[valuesById[1] = "ACTIVE"] = 1;
      values[valuesById[2] = "STATIC"] = 2;
      return values;
    }();
    /**
     * Stance enum.
     * @name Peopletrack.People.Stance
     * @enum {number}
     * @property {number} UNKNOWN=0 UNKNOWN value
     * @property {number} STAND=1 STAND value
     * @property {number} SIT=2 SIT value
     * @property {number} LAY=3 LAY value
     * @property {number} SUSFALL=4 SUSFALL value
     * @property {number} COUNTDOWN=5 COUNTDOWN value
     * @property {number} DETFALL=6 DETFALL value
     */


    People.Stance = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "UNKNOWN"] = 0;
      values[valuesById[1] = "STAND"] = 1;
      values[valuesById[2] = "SIT"] = 2;
      values[valuesById[3] = "LAY"] = 3;
      values[valuesById[4] = "SUSFALL"] = 4;
      values[valuesById[5] = "COUNTDOWN"] = 5;
      values[valuesById[6] = "DETFALL"] = 6;
      return values;
    }();

    return People;
  }();

  Peopletrack.Msg = /*#__PURE__*/function () {
    /**
     * Properties of a Msg.
     * @memberof Peopletrack
     * @interface IMsg
     * @property {number|null} [frameNumber] Msg frameNumber
     * @property {number|Long|null} [timestamp] Msg timestamp
     * @property {Array.<Peopletrack.IPeople>|null} [people] Msg people
     * @property {Uint8Array|null} [cloudpoint] Msg cloudpoint
     */

    /**
     * Constructs a new Msg.
     * @memberof Peopletrack
     * @classdesc Represents a Msg.
     * @implements IMsg
     * @constructor
     * @param {Peopletrack.IMsg=} [properties] Properties to set
     */
    function Msg(properties) {
      this.people = [];
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Msg frameNumber.
     * @member {number} frameNumber
     * @memberof Peopletrack.Msg
     * @instance
     */


    Msg.prototype.frameNumber = 0;
    /**
     * Msg timestamp.
     * @member {number|Long} timestamp
     * @memberof Peopletrack.Msg
     * @instance
     */

    Msg.prototype.timestamp = $util$4.Long ? /*#__PURE__*/$util$4.Long.fromBits(0, 0, true) : 0;
    /**
     * Msg people.
     * @member {Array.<Peopletrack.IPeople>} people
     * @memberof Peopletrack.Msg
     * @instance
     */

    Msg.prototype.people = $util$4.emptyArray;
    /**
     * Msg cloudpoint.
     * @member {Uint8Array} cloudpoint
     * @memberof Peopletrack.Msg
     * @instance
     */

    Msg.prototype.cloudpoint = /*#__PURE__*/$util$4.newBuffer([]);
    /**
     * Creates a new Msg instance using the specified properties.
     * @function create
     * @memberof Peopletrack.Msg
     * @static
     * @param {Peopletrack.IMsg=} [properties] Properties to set
     * @returns {Peopletrack.Msg} Msg instance
     */

    Msg.create = function create(properties) {
      return new Msg(properties);
    };
    /**
     * Encodes the specified Msg message. Does not implicitly {@link Peopletrack.Msg.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.Msg
     * @static
     * @param {Peopletrack.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.frameNumber);
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).uint64(message.timestamp);
      if (message.people != null && message.people.length) for (let i = 0; i < message.people.length; ++i) $root$5.Peopletrack.People.encode(message.people[i], writer.uint32(
      /* id 3, wireType 2 =*/
      26).fork()).ldelim();
      if (message.cloudpoint != null && Object.hasOwnProperty.call(message, "cloudpoint")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).bytes(message.cloudpoint);
      return writer;
    };
    /**
     * Encodes the specified Msg message, length delimited. Does not implicitly {@link Peopletrack.Msg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.Msg
     * @static
     * @param {Peopletrack.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Msg message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.Msg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.frameNumber = reader.uint32();
            break;

          case 2:
            message.timestamp = reader.uint64();
            break;

          case 3:
            if (!(message.people && message.people.length)) message.people = [];
            message.people.push($root$5.Peopletrack.People.decode(reader, reader.uint32()));
            break;

          case 4:
            message.cloudpoint = reader.bytes();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Msg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Msg message.
     * @function verify
     * @memberof Peopletrack.Msg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Msg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) if (!$util$4.isInteger(message.frameNumber)) return "frameNumber: integer expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$4.isInteger(message.timestamp) && !(message.timestamp && $util$4.isInteger(message.timestamp.low) && $util$4.isInteger(message.timestamp.high))) return "timestamp: integer|Long expected";

      if (message.people != null && message.hasOwnProperty("people")) {
        if (!Array.isArray(message.people)) return "people: array expected";

        for (let i = 0; i < message.people.length; ++i) {
          let error = $root$5.Peopletrack.People.verify(message.people[i]);
          if (error) return "people." + error;
        }
      }

      if (message.cloudpoint != null && message.hasOwnProperty("cloudpoint")) if (!(message.cloudpoint && typeof message.cloudpoint.length === "number" || $util$4.isString(message.cloudpoint))) return "cloudpoint: buffer expected";
      return null;
    };
    /**
     * Creates a Msg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.Msg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.Msg} Msg
     */


    Msg.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.Msg) return object;
      let message = new $root$5.Peopletrack.Msg();
      if (object.frameNumber != null) message.frameNumber = object.frameNumber >>> 0;
      if (object.timestamp != null) if ($util$4.Long) (message.timestamp = $util$4.Long.fromValue(object.timestamp)).unsigned = true;else if (typeof object.timestamp === "string") message.timestamp = parseInt(object.timestamp, 10);else if (typeof object.timestamp === "number") message.timestamp = object.timestamp;else if (typeof object.timestamp === "object") message.timestamp = new $util$4.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);

      if (object.people) {
        if (!Array.isArray(object.people)) throw TypeError(".Peopletrack.Msg.people: array expected");
        message.people = [];

        for (let i = 0; i < object.people.length; ++i) {
          if (typeof object.people[i] !== "object") throw TypeError(".Peopletrack.Msg.people: object expected");
          message.people[i] = $root$5.Peopletrack.People.fromObject(object.people[i]);
        }
      }

      if (object.cloudpoint != null) if (typeof object.cloudpoint === "string") $util$4.base64.decode(object.cloudpoint, message.cloudpoint = $util$4.newBuffer($util$4.base64.length(object.cloudpoint)), 0);else if (object.cloudpoint.length) message.cloudpoint = object.cloudpoint;
      return message;
    };
    /**
     * Creates a plain object from a Msg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.Msg
     * @static
     * @param {Peopletrack.Msg} message Msg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Msg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.arrays || options.defaults) object.people = [];

      if (options.defaults) {
        object.frameNumber = 0;

        if ($util$4.Long) {
          let long = new $util$4.Long(0, 0, true);
          object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
        } else object.timestamp = options.longs === String ? "0" : 0;

        if (options.bytes === String) object.cloudpoint = "";else {
          object.cloudpoint = [];
          if (options.bytes !== Array) object.cloudpoint = $util$4.newBuffer(object.cloudpoint);
        }
      }

      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) object.frameNumber = message.frameNumber;
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (typeof message.timestamp === "number") object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;else object.timestamp = options.longs === String ? $util$4.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util$4.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;

      if (message.people && message.people.length) {
        object.people = [];

        for (let j = 0; j < message.people.length; ++j) object.people[j] = $root$5.Peopletrack.People.toObject(message.people[j], options);
      }

      if (message.cloudpoint != null && message.hasOwnProperty("cloudpoint")) object.cloudpoint = options.bytes === String ? $util$4.base64.encode(message.cloudpoint, 0, message.cloudpoint.length) : options.bytes === Array ? Array.prototype.slice.call(message.cloudpoint) : message.cloudpoint;
      return object;
    };
    /**
     * Converts this Msg to JSON.
     * @function toJSON
     * @memberof Peopletrack.Msg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Msg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Msg;
  }();

  Peopletrack.StitchParam = /*#__PURE__*/function () {
    /**
     * Properties of a StitchParam.
     * @memberof Peopletrack
     * @interface IStitchParam
     * @property {number|null} [height] StitchParam height
     * @property {number|null} [theta] StitchParam theta
     * @property {number|null} [shiftX] StitchParam shiftX
     * @property {number|null} [shiftZ] StitchParam shiftZ
     */

    /**
     * Constructs a new StitchParam.
     * @memberof Peopletrack
     * @classdesc Represents a StitchParam.
     * @implements IStitchParam
     * @constructor
     * @param {Peopletrack.IStitchParam=} [properties] Properties to set
     */
    function StitchParam(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * StitchParam height.
     * @member {number} height
     * @memberof Peopletrack.StitchParam
     * @instance
     */


    StitchParam.prototype.height = 0;
    /**
     * StitchParam theta.
     * @member {number} theta
     * @memberof Peopletrack.StitchParam
     * @instance
     */

    StitchParam.prototype.theta = 0;
    /**
     * StitchParam shiftX.
     * @member {number} shiftX
     * @memberof Peopletrack.StitchParam
     * @instance
     */

    StitchParam.prototype.shiftX = 0;
    /**
     * StitchParam shiftZ.
     * @member {number} shiftZ
     * @memberof Peopletrack.StitchParam
     * @instance
     */

    StitchParam.prototype.shiftZ = 0;
    /**
     * Creates a new StitchParam instance using the specified properties.
     * @function create
     * @memberof Peopletrack.StitchParam
     * @static
     * @param {Peopletrack.IStitchParam=} [properties] Properties to set
     * @returns {Peopletrack.StitchParam} StitchParam instance
     */

    StitchParam.create = function create(properties) {
      return new StitchParam(properties);
    };
    /**
     * Encodes the specified StitchParam message. Does not implicitly {@link Peopletrack.StitchParam.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.StitchParam
     * @static
     * @param {Peopletrack.IStitchParam} message StitchParam message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    StitchParam.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.height != null && Object.hasOwnProperty.call(message, "height")) writer.uint32(
      /* id 1, wireType 5 =*/
      13).float(message.height);
      if (message.theta != null && Object.hasOwnProperty.call(message, "theta")) writer.uint32(
      /* id 2, wireType 5 =*/
      21).float(message.theta);
      if (message.shiftX != null && Object.hasOwnProperty.call(message, "shiftX")) writer.uint32(
      /* id 3, wireType 5 =*/
      29).float(message.shiftX);
      if (message.shiftZ != null && Object.hasOwnProperty.call(message, "shiftZ")) writer.uint32(
      /* id 4, wireType 5 =*/
      37).float(message.shiftZ);
      return writer;
    };
    /**
     * Encodes the specified StitchParam message, length delimited. Does not implicitly {@link Peopletrack.StitchParam.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.StitchParam
     * @static
     * @param {Peopletrack.IStitchParam} message StitchParam message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    StitchParam.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a StitchParam message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.StitchParam
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.StitchParam} StitchParam
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    StitchParam.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.StitchParam();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.height = reader.float();
            break;

          case 2:
            message.theta = reader.float();
            break;

          case 3:
            message.shiftX = reader.float();
            break;

          case 4:
            message.shiftZ = reader.float();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a StitchParam message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.StitchParam
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.StitchParam} StitchParam
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    StitchParam.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a StitchParam message.
     * @function verify
     * @memberof Peopletrack.StitchParam
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    StitchParam.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.height != null && message.hasOwnProperty("height")) if (typeof message.height !== "number") return "height: number expected";
      if (message.theta != null && message.hasOwnProperty("theta")) if (typeof message.theta !== "number") return "theta: number expected";
      if (message.shiftX != null && message.hasOwnProperty("shiftX")) if (typeof message.shiftX !== "number") return "shiftX: number expected";
      if (message.shiftZ != null && message.hasOwnProperty("shiftZ")) if (typeof message.shiftZ !== "number") return "shiftZ: number expected";
      return null;
    };
    /**
     * Creates a StitchParam message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.StitchParam
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.StitchParam} StitchParam
     */


    StitchParam.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.StitchParam) return object;
      let message = new $root$5.Peopletrack.StitchParam();
      if (object.height != null) message.height = Number(object.height);
      if (object.theta != null) message.theta = Number(object.theta);
      if (object.shiftX != null) message.shiftX = Number(object.shiftX);
      if (object.shiftZ != null) message.shiftZ = Number(object.shiftZ);
      return message;
    };
    /**
     * Creates a plain object from a StitchParam message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.StitchParam
     * @static
     * @param {Peopletrack.StitchParam} message StitchParam
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    StitchParam.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.height = 0;
        object.theta = 0;
        object.shiftX = 0;
        object.shiftZ = 0;
      }

      if (message.height != null && message.hasOwnProperty("height")) object.height = options.json && !isFinite(message.height) ? String(message.height) : message.height;
      if (message.theta != null && message.hasOwnProperty("theta")) object.theta = options.json && !isFinite(message.theta) ? String(message.theta) : message.theta;
      if (message.shiftX != null && message.hasOwnProperty("shiftX")) object.shiftX = options.json && !isFinite(message.shiftX) ? String(message.shiftX) : message.shiftX;
      if (message.shiftZ != null && message.hasOwnProperty("shiftZ")) object.shiftZ = options.json && !isFinite(message.shiftZ) ? String(message.shiftZ) : message.shiftZ;
      return object;
    };
    /**
     * Converts this StitchParam to JSON.
     * @function toJSON
     * @memberof Peopletrack.StitchParam
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    StitchParam.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return StitchParam;
  }();

  Peopletrack.SetupInput = /*#__PURE__*/function () {
    /**
     * Properties of a SetupInput.
     * @memberof Peopletrack
     * @interface ISetupInput
     * @property {number|null} [stitchID] SetupInput stitchID
     * @property {number|null} [frameNumber] SetupInput frameNumber
     * @property {number|Long|null} [timestamp] SetupInput timestamp
     * @property {Uint8Array|null} [cloudpoint] SetupInput cloudpoint
     */

    /**
     * Constructs a new SetupInput.
     * @memberof Peopletrack
     * @classdesc Represents a SetupInput.
     * @implements ISetupInput
     * @constructor
     * @param {Peopletrack.ISetupInput=} [properties] Properties to set
     */
    function SetupInput(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * SetupInput stitchID.
     * @member {number} stitchID
     * @memberof Peopletrack.SetupInput
     * @instance
     */


    SetupInput.prototype.stitchID = 0;
    /**
     * SetupInput frameNumber.
     * @member {number} frameNumber
     * @memberof Peopletrack.SetupInput
     * @instance
     */

    SetupInput.prototype.frameNumber = 0;
    /**
     * SetupInput timestamp.
     * @member {number|Long} timestamp
     * @memberof Peopletrack.SetupInput
     * @instance
     */

    SetupInput.prototype.timestamp = $util$4.Long ? /*#__PURE__*/$util$4.Long.fromBits(0, 0, true) : 0;
    /**
     * SetupInput cloudpoint.
     * @member {Uint8Array} cloudpoint
     * @memberof Peopletrack.SetupInput
     * @instance
     */

    SetupInput.prototype.cloudpoint = /*#__PURE__*/$util$4.newBuffer([]);
    /**
     * Creates a new SetupInput instance using the specified properties.
     * @function create
     * @memberof Peopletrack.SetupInput
     * @static
     * @param {Peopletrack.ISetupInput=} [properties] Properties to set
     * @returns {Peopletrack.SetupInput} SetupInput instance
     */

    SetupInput.create = function create(properties) {
      return new SetupInput(properties);
    };
    /**
     * Encodes the specified SetupInput message. Does not implicitly {@link Peopletrack.SetupInput.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.SetupInput
     * @static
     * @param {Peopletrack.ISetupInput} message SetupInput message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    SetupInput.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.stitchID != null && Object.hasOwnProperty.call(message, "stitchID")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.stitchID);
      if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).uint32(message.frameNumber);
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 3, wireType 0 =*/
      24).uint64(message.timestamp);
      if (message.cloudpoint != null && Object.hasOwnProperty.call(message, "cloudpoint")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).bytes(message.cloudpoint);
      return writer;
    };
    /**
     * Encodes the specified SetupInput message, length delimited. Does not implicitly {@link Peopletrack.SetupInput.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.SetupInput
     * @static
     * @param {Peopletrack.ISetupInput} message SetupInput message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    SetupInput.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a SetupInput message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.SetupInput
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.SetupInput} SetupInput
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    SetupInput.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.SetupInput();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.stitchID = reader.uint32();
            break;

          case 2:
            message.frameNumber = reader.uint32();
            break;

          case 3:
            message.timestamp = reader.uint64();
            break;

          case 4:
            message.cloudpoint = reader.bytes();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a SetupInput message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.SetupInput
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.SetupInput} SetupInput
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    SetupInput.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a SetupInput message.
     * @function verify
     * @memberof Peopletrack.SetupInput
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    SetupInput.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.stitchID != null && message.hasOwnProperty("stitchID")) if (!$util$4.isInteger(message.stitchID)) return "stitchID: integer expected";
      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) if (!$util$4.isInteger(message.frameNumber)) return "frameNumber: integer expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$4.isInteger(message.timestamp) && !(message.timestamp && $util$4.isInteger(message.timestamp.low) && $util$4.isInteger(message.timestamp.high))) return "timestamp: integer|Long expected";
      if (message.cloudpoint != null && message.hasOwnProperty("cloudpoint")) if (!(message.cloudpoint && typeof message.cloudpoint.length === "number" || $util$4.isString(message.cloudpoint))) return "cloudpoint: buffer expected";
      return null;
    };
    /**
     * Creates a SetupInput message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.SetupInput
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.SetupInput} SetupInput
     */


    SetupInput.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.SetupInput) return object;
      let message = new $root$5.Peopletrack.SetupInput();
      if (object.stitchID != null) message.stitchID = object.stitchID >>> 0;
      if (object.frameNumber != null) message.frameNumber = object.frameNumber >>> 0;
      if (object.timestamp != null) if ($util$4.Long) (message.timestamp = $util$4.Long.fromValue(object.timestamp)).unsigned = true;else if (typeof object.timestamp === "string") message.timestamp = parseInt(object.timestamp, 10);else if (typeof object.timestamp === "number") message.timestamp = object.timestamp;else if (typeof object.timestamp === "object") message.timestamp = new $util$4.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);
      if (object.cloudpoint != null) if (typeof object.cloudpoint === "string") $util$4.base64.decode(object.cloudpoint, message.cloudpoint = $util$4.newBuffer($util$4.base64.length(object.cloudpoint)), 0);else if (object.cloudpoint.length) message.cloudpoint = object.cloudpoint;
      return message;
    };
    /**
     * Creates a plain object from a SetupInput message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.SetupInput
     * @static
     * @param {Peopletrack.SetupInput} message SetupInput
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    SetupInput.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.stitchID = 0;
        object.frameNumber = 0;

        if ($util$4.Long) {
          let long = new $util$4.Long(0, 0, true);
          object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
        } else object.timestamp = options.longs === String ? "0" : 0;

        if (options.bytes === String) object.cloudpoint = "";else {
          object.cloudpoint = [];
          if (options.bytes !== Array) object.cloudpoint = $util$4.newBuffer(object.cloudpoint);
        }
      }

      if (message.stitchID != null && message.hasOwnProperty("stitchID")) object.stitchID = message.stitchID;
      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) object.frameNumber = message.frameNumber;
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (typeof message.timestamp === "number") object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;else object.timestamp = options.longs === String ? $util$4.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util$4.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;
      if (message.cloudpoint != null && message.hasOwnProperty("cloudpoint")) object.cloudpoint = options.bytes === String ? $util$4.base64.encode(message.cloudpoint, 0, message.cloudpoint.length) : options.bytes === Array ? Array.prototype.slice.call(message.cloudpoint) : message.cloudpoint;
      return object;
    };
    /**
     * Converts this SetupInput to JSON.
     * @function toJSON
     * @memberof Peopletrack.SetupInput
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    SetupInput.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return SetupInput;
  }();

  Peopletrack.SetupOutput = /*#__PURE__*/function () {
    /**
     * Properties of a SetupOutput.
     * @memberof Peopletrack
     * @interface ISetupOutput
     * @property {number|null} [frameNumber] SetupOutput frameNumber
     * @property {number|Long|null} [timestamp] SetupOutput timestamp
     * @property {Array.<Peopletrack.IPeople>|null} [people] SetupOutput people
     * @property {Uint8Array|null} [backgroundChart] SetupOutput backgroundChart
     * @property {Peopletrack.IStitchParam|null} [stitchParam] SetupOutput stitchParam
     */

    /**
     * Constructs a new SetupOutput.
     * @memberof Peopletrack
     * @classdesc Represents a SetupOutput.
     * @implements ISetupOutput
     * @constructor
     * @param {Peopletrack.ISetupOutput=} [properties] Properties to set
     */
    function SetupOutput(properties) {
      this.people = [];
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * SetupOutput frameNumber.
     * @member {number} frameNumber
     * @memberof Peopletrack.SetupOutput
     * @instance
     */


    SetupOutput.prototype.frameNumber = 0;
    /**
     * SetupOutput timestamp.
     * @member {number|Long} timestamp
     * @memberof Peopletrack.SetupOutput
     * @instance
     */

    SetupOutput.prototype.timestamp = $util$4.Long ? /*#__PURE__*/$util$4.Long.fromBits(0, 0, true) : 0;
    /**
     * SetupOutput people.
     * @member {Array.<Peopletrack.IPeople>} people
     * @memberof Peopletrack.SetupOutput
     * @instance
     */

    SetupOutput.prototype.people = $util$4.emptyArray;
    /**
     * SetupOutput backgroundChart.
     * @member {Uint8Array} backgroundChart
     * @memberof Peopletrack.SetupOutput
     * @instance
     */

    SetupOutput.prototype.backgroundChart = /*#__PURE__*/$util$4.newBuffer([]);
    /**
     * SetupOutput stitchParam.
     * @member {Peopletrack.IStitchParam|null|undefined} stitchParam
     * @memberof Peopletrack.SetupOutput
     * @instance
     */

    SetupOutput.prototype.stitchParam = null;
    /**
     * Creates a new SetupOutput instance using the specified properties.
     * @function create
     * @memberof Peopletrack.SetupOutput
     * @static
     * @param {Peopletrack.ISetupOutput=} [properties] Properties to set
     * @returns {Peopletrack.SetupOutput} SetupOutput instance
     */

    SetupOutput.create = function create(properties) {
      return new SetupOutput(properties);
    };
    /**
     * Encodes the specified SetupOutput message. Does not implicitly {@link Peopletrack.SetupOutput.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.SetupOutput
     * @static
     * @param {Peopletrack.ISetupOutput} message SetupOutput message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    SetupOutput.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.frameNumber);
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).uint64(message.timestamp);
      if (message.people != null && message.people.length) for (let i = 0; i < message.people.length; ++i) $root$5.Peopletrack.People.encode(message.people[i], writer.uint32(
      /* id 3, wireType 2 =*/
      26).fork()).ldelim();
      if (message.backgroundChart != null && Object.hasOwnProperty.call(message, "backgroundChart")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).bytes(message.backgroundChart);
      if (message.stitchParam != null && Object.hasOwnProperty.call(message, "stitchParam")) $root$5.Peopletrack.StitchParam.encode(message.stitchParam, writer.uint32(
      /* id 5, wireType 2 =*/
      42).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified SetupOutput message, length delimited. Does not implicitly {@link Peopletrack.SetupOutput.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.SetupOutput
     * @static
     * @param {Peopletrack.ISetupOutput} message SetupOutput message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    SetupOutput.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a SetupOutput message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.SetupOutput
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.SetupOutput} SetupOutput
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    SetupOutput.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.SetupOutput();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.frameNumber = reader.uint32();
            break;

          case 2:
            message.timestamp = reader.uint64();
            break;

          case 3:
            if (!(message.people && message.people.length)) message.people = [];
            message.people.push($root$5.Peopletrack.People.decode(reader, reader.uint32()));
            break;

          case 4:
            message.backgroundChart = reader.bytes();
            break;

          case 5:
            message.stitchParam = $root$5.Peopletrack.StitchParam.decode(reader, reader.uint32());
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a SetupOutput message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.SetupOutput
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.SetupOutput} SetupOutput
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    SetupOutput.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a SetupOutput message.
     * @function verify
     * @memberof Peopletrack.SetupOutput
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    SetupOutput.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) if (!$util$4.isInteger(message.frameNumber)) return "frameNumber: integer expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$4.isInteger(message.timestamp) && !(message.timestamp && $util$4.isInteger(message.timestamp.low) && $util$4.isInteger(message.timestamp.high))) return "timestamp: integer|Long expected";

      if (message.people != null && message.hasOwnProperty("people")) {
        if (!Array.isArray(message.people)) return "people: array expected";

        for (let i = 0; i < message.people.length; ++i) {
          let error = $root$5.Peopletrack.People.verify(message.people[i]);
          if (error) return "people." + error;
        }
      }

      if (message.backgroundChart != null && message.hasOwnProperty("backgroundChart")) if (!(message.backgroundChart && typeof message.backgroundChart.length === "number" || $util$4.isString(message.backgroundChart))) return "backgroundChart: buffer expected";

      if (message.stitchParam != null && message.hasOwnProperty("stitchParam")) {
        let error = $root$5.Peopletrack.StitchParam.verify(message.stitchParam);
        if (error) return "stitchParam." + error;
      }

      return null;
    };
    /**
     * Creates a SetupOutput message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.SetupOutput
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.SetupOutput} SetupOutput
     */


    SetupOutput.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.SetupOutput) return object;
      let message = new $root$5.Peopletrack.SetupOutput();
      if (object.frameNumber != null) message.frameNumber = object.frameNumber >>> 0;
      if (object.timestamp != null) if ($util$4.Long) (message.timestamp = $util$4.Long.fromValue(object.timestamp)).unsigned = true;else if (typeof object.timestamp === "string") message.timestamp = parseInt(object.timestamp, 10);else if (typeof object.timestamp === "number") message.timestamp = object.timestamp;else if (typeof object.timestamp === "object") message.timestamp = new $util$4.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);

      if (object.people) {
        if (!Array.isArray(object.people)) throw TypeError(".Peopletrack.SetupOutput.people: array expected");
        message.people = [];

        for (let i = 0; i < object.people.length; ++i) {
          if (typeof object.people[i] !== "object") throw TypeError(".Peopletrack.SetupOutput.people: object expected");
          message.people[i] = $root$5.Peopletrack.People.fromObject(object.people[i]);
        }
      }

      if (object.backgroundChart != null) if (typeof object.backgroundChart === "string") $util$4.base64.decode(object.backgroundChart, message.backgroundChart = $util$4.newBuffer($util$4.base64.length(object.backgroundChart)), 0);else if (object.backgroundChart.length) message.backgroundChart = object.backgroundChart;

      if (object.stitchParam != null) {
        if (typeof object.stitchParam !== "object") throw TypeError(".Peopletrack.SetupOutput.stitchParam: object expected");
        message.stitchParam = $root$5.Peopletrack.StitchParam.fromObject(object.stitchParam);
      }

      return message;
    };
    /**
     * Creates a plain object from a SetupOutput message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.SetupOutput
     * @static
     * @param {Peopletrack.SetupOutput} message SetupOutput
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    SetupOutput.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.arrays || options.defaults) object.people = [];

      if (options.defaults) {
        object.frameNumber = 0;

        if ($util$4.Long) {
          let long = new $util$4.Long(0, 0, true);
          object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
        } else object.timestamp = options.longs === String ? "0" : 0;

        if (options.bytes === String) object.backgroundChart = "";else {
          object.backgroundChart = [];
          if (options.bytes !== Array) object.backgroundChart = $util$4.newBuffer(object.backgroundChart);
        }
        object.stitchParam = null;
      }

      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) object.frameNumber = message.frameNumber;
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (typeof message.timestamp === "number") object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;else object.timestamp = options.longs === String ? $util$4.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util$4.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;

      if (message.people && message.people.length) {
        object.people = [];

        for (let j = 0; j < message.people.length; ++j) object.people[j] = $root$5.Peopletrack.People.toObject(message.people[j], options);
      }

      if (message.backgroundChart != null && message.hasOwnProperty("backgroundChart")) object.backgroundChart = options.bytes === String ? $util$4.base64.encode(message.backgroundChart, 0, message.backgroundChart.length) : options.bytes === Array ? Array.prototype.slice.call(message.backgroundChart) : message.backgroundChart;
      if (message.stitchParam != null && message.hasOwnProperty("stitchParam")) object.stitchParam = $root$5.Peopletrack.StitchParam.toObject(message.stitchParam, options);
      return object;
    };
    /**
     * Converts this SetupOutput to JSON.
     * @function toJSON
     * @memberof Peopletrack.SetupOutput
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    SetupOutput.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return SetupOutput;
  }();

  Peopletrack.StitchInput = /*#__PURE__*/function () {
    /**
     * Properties of a StitchInput.
     * @memberof Peopletrack
     * @interface IStitchInput
     * @property {number|null} [stitchID] StitchInput stitchID
     * @property {number|null} [frameNumber] StitchInput frameNumber
     * @property {number|Long|null} [timestamp] StitchInput timestamp
     * @property {Array.<Peopletrack.IPeople>|null} [people] StitchInput people
     * @property {Peopletrack.IStitchParam|null} [stitchParam] StitchInput stitchParam
     */

    /**
     * Constructs a new StitchInput.
     * @memberof Peopletrack
     * @classdesc Represents a StitchInput.
     * @implements IStitchInput
     * @constructor
     * @param {Peopletrack.IStitchInput=} [properties] Properties to set
     */
    function StitchInput(properties) {
      this.people = [];
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * StitchInput stitchID.
     * @member {number} stitchID
     * @memberof Peopletrack.StitchInput
     * @instance
     */


    StitchInput.prototype.stitchID = 0;
    /**
     * StitchInput frameNumber.
     * @member {number} frameNumber
     * @memberof Peopletrack.StitchInput
     * @instance
     */

    StitchInput.prototype.frameNumber = 0;
    /**
     * StitchInput timestamp.
     * @member {number|Long} timestamp
     * @memberof Peopletrack.StitchInput
     * @instance
     */

    StitchInput.prototype.timestamp = $util$4.Long ? /*#__PURE__*/$util$4.Long.fromBits(0, 0, true) : 0;
    /**
     * StitchInput people.
     * @member {Array.<Peopletrack.IPeople>} people
     * @memberof Peopletrack.StitchInput
     * @instance
     */

    StitchInput.prototype.people = $util$4.emptyArray;
    /**
     * StitchInput stitchParam.
     * @member {Peopletrack.IStitchParam|null|undefined} stitchParam
     * @memberof Peopletrack.StitchInput
     * @instance
     */

    StitchInput.prototype.stitchParam = null;
    /**
     * Creates a new StitchInput instance using the specified properties.
     * @function create
     * @memberof Peopletrack.StitchInput
     * @static
     * @param {Peopletrack.IStitchInput=} [properties] Properties to set
     * @returns {Peopletrack.StitchInput} StitchInput instance
     */

    StitchInput.create = function create(properties) {
      return new StitchInput(properties);
    };
    /**
     * Encodes the specified StitchInput message. Does not implicitly {@link Peopletrack.StitchInput.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.StitchInput
     * @static
     * @param {Peopletrack.IStitchInput} message StitchInput message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    StitchInput.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.stitchID != null && Object.hasOwnProperty.call(message, "stitchID")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.stitchID);
      if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).uint32(message.frameNumber);
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 3, wireType 0 =*/
      24).uint64(message.timestamp);
      if (message.people != null && message.people.length) for (let i = 0; i < message.people.length; ++i) $root$5.Peopletrack.People.encode(message.people[i], writer.uint32(
      /* id 4, wireType 2 =*/
      34).fork()).ldelim();
      if (message.stitchParam != null && Object.hasOwnProperty.call(message, "stitchParam")) $root$5.Peopletrack.StitchParam.encode(message.stitchParam, writer.uint32(
      /* id 5, wireType 2 =*/
      42).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified StitchInput message, length delimited. Does not implicitly {@link Peopletrack.StitchInput.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.StitchInput
     * @static
     * @param {Peopletrack.IStitchInput} message StitchInput message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    StitchInput.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a StitchInput message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.StitchInput
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.StitchInput} StitchInput
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    StitchInput.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.StitchInput();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.stitchID = reader.uint32();
            break;

          case 2:
            message.frameNumber = reader.uint32();
            break;

          case 3:
            message.timestamp = reader.uint64();
            break;

          case 4:
            if (!(message.people && message.people.length)) message.people = [];
            message.people.push($root$5.Peopletrack.People.decode(reader, reader.uint32()));
            break;

          case 5:
            message.stitchParam = $root$5.Peopletrack.StitchParam.decode(reader, reader.uint32());
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a StitchInput message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.StitchInput
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.StitchInput} StitchInput
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    StitchInput.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a StitchInput message.
     * @function verify
     * @memberof Peopletrack.StitchInput
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    StitchInput.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.stitchID != null && message.hasOwnProperty("stitchID")) if (!$util$4.isInteger(message.stitchID)) return "stitchID: integer expected";
      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) if (!$util$4.isInteger(message.frameNumber)) return "frameNumber: integer expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$4.isInteger(message.timestamp) && !(message.timestamp && $util$4.isInteger(message.timestamp.low) && $util$4.isInteger(message.timestamp.high))) return "timestamp: integer|Long expected";

      if (message.people != null && message.hasOwnProperty("people")) {
        if (!Array.isArray(message.people)) return "people: array expected";

        for (let i = 0; i < message.people.length; ++i) {
          let error = $root$5.Peopletrack.People.verify(message.people[i]);
          if (error) return "people." + error;
        }
      }

      if (message.stitchParam != null && message.hasOwnProperty("stitchParam")) {
        let error = $root$5.Peopletrack.StitchParam.verify(message.stitchParam);
        if (error) return "stitchParam." + error;
      }

      return null;
    };
    /**
     * Creates a StitchInput message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.StitchInput
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.StitchInput} StitchInput
     */


    StitchInput.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.StitchInput) return object;
      let message = new $root$5.Peopletrack.StitchInput();
      if (object.stitchID != null) message.stitchID = object.stitchID >>> 0;
      if (object.frameNumber != null) message.frameNumber = object.frameNumber >>> 0;
      if (object.timestamp != null) if ($util$4.Long) (message.timestamp = $util$4.Long.fromValue(object.timestamp)).unsigned = true;else if (typeof object.timestamp === "string") message.timestamp = parseInt(object.timestamp, 10);else if (typeof object.timestamp === "number") message.timestamp = object.timestamp;else if (typeof object.timestamp === "object") message.timestamp = new $util$4.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);

      if (object.people) {
        if (!Array.isArray(object.people)) throw TypeError(".Peopletrack.StitchInput.people: array expected");
        message.people = [];

        for (let i = 0; i < object.people.length; ++i) {
          if (typeof object.people[i] !== "object") throw TypeError(".Peopletrack.StitchInput.people: object expected");
          message.people[i] = $root$5.Peopletrack.People.fromObject(object.people[i]);
        }
      }

      if (object.stitchParam != null) {
        if (typeof object.stitchParam !== "object") throw TypeError(".Peopletrack.StitchInput.stitchParam: object expected");
        message.stitchParam = $root$5.Peopletrack.StitchParam.fromObject(object.stitchParam);
      }

      return message;
    };
    /**
     * Creates a plain object from a StitchInput message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.StitchInput
     * @static
     * @param {Peopletrack.StitchInput} message StitchInput
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    StitchInput.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.arrays || options.defaults) object.people = [];

      if (options.defaults) {
        object.stitchID = 0;
        object.frameNumber = 0;

        if ($util$4.Long) {
          let long = new $util$4.Long(0, 0, true);
          object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
        } else object.timestamp = options.longs === String ? "0" : 0;

        object.stitchParam = null;
      }

      if (message.stitchID != null && message.hasOwnProperty("stitchID")) object.stitchID = message.stitchID;
      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) object.frameNumber = message.frameNumber;
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (typeof message.timestamp === "number") object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;else object.timestamp = options.longs === String ? $util$4.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util$4.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;

      if (message.people && message.people.length) {
        object.people = [];

        for (let j = 0; j < message.people.length; ++j) object.people[j] = $root$5.Peopletrack.People.toObject(message.people[j], options);
      }

      if (message.stitchParam != null && message.hasOwnProperty("stitchParam")) object.stitchParam = $root$5.Peopletrack.StitchParam.toObject(message.stitchParam, options);
      return object;
    };
    /**
     * Converts this StitchInput to JSON.
     * @function toJSON
     * @memberof Peopletrack.StitchInput
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    StitchInput.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return StitchInput;
  }();

  Peopletrack.StitchOutput = /*#__PURE__*/function () {
    /**
     * Properties of a StitchOutput.
     * @memberof Peopletrack
     * @interface IStitchOutput
     * @property {number|null} [frameNumber] StitchOutput frameNumber
     * @property {number|Long|null} [timestamp] StitchOutput timestamp
     * @property {Array.<Peopletrack.IPeople>|null} [people] StitchOutput people
     * @property {Uint8Array|null} [backgroundChart] StitchOutput backgroundChart
     * @property {Uint8Array|null} [parameters] StitchOutput parameters
     */

    /**
     * Constructs a new StitchOutput.
     * @memberof Peopletrack
     * @classdesc Represents a StitchOutput.
     * @implements IStitchOutput
     * @constructor
     * @param {Peopletrack.IStitchOutput=} [properties] Properties to set
     */
    function StitchOutput(properties) {
      this.people = [];
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * StitchOutput frameNumber.
     * @member {number} frameNumber
     * @memberof Peopletrack.StitchOutput
     * @instance
     */


    StitchOutput.prototype.frameNumber = 0;
    /**
     * StitchOutput timestamp.
     * @member {number|Long} timestamp
     * @memberof Peopletrack.StitchOutput
     * @instance
     */

    StitchOutput.prototype.timestamp = $util$4.Long ? /*#__PURE__*/$util$4.Long.fromBits(0, 0, true) : 0;
    /**
     * StitchOutput people.
     * @member {Array.<Peopletrack.IPeople>} people
     * @memberof Peopletrack.StitchOutput
     * @instance
     */

    StitchOutput.prototype.people = $util$4.emptyArray;
    /**
     * StitchOutput backgroundChart.
     * @member {Uint8Array} backgroundChart
     * @memberof Peopletrack.StitchOutput
     * @instance
     */

    StitchOutput.prototype.backgroundChart = /*#__PURE__*/$util$4.newBuffer([]);
    /**
     * StitchOutput parameters.
     * @member {Uint8Array} parameters
     * @memberof Peopletrack.StitchOutput
     * @instance
     */

    StitchOutput.prototype.parameters = /*#__PURE__*/$util$4.newBuffer([]);
    /**
     * Creates a new StitchOutput instance using the specified properties.
     * @function create
     * @memberof Peopletrack.StitchOutput
     * @static
     * @param {Peopletrack.IStitchOutput=} [properties] Properties to set
     * @returns {Peopletrack.StitchOutput} StitchOutput instance
     */

    StitchOutput.create = function create(properties) {
      return new StitchOutput(properties);
    };
    /**
     * Encodes the specified StitchOutput message. Does not implicitly {@link Peopletrack.StitchOutput.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.StitchOutput
     * @static
     * @param {Peopletrack.IStitchOutput} message StitchOutput message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    StitchOutput.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.frameNumber != null && Object.hasOwnProperty.call(message, "frameNumber")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.frameNumber);
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).uint64(message.timestamp);
      if (message.people != null && message.people.length) for (let i = 0; i < message.people.length; ++i) $root$5.Peopletrack.People.encode(message.people[i], writer.uint32(
      /* id 3, wireType 2 =*/
      26).fork()).ldelim();
      if (message.backgroundChart != null && Object.hasOwnProperty.call(message, "backgroundChart")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).bytes(message.backgroundChart);
      if (message.parameters != null && Object.hasOwnProperty.call(message, "parameters")) writer.uint32(
      /* id 5, wireType 2 =*/
      42).bytes(message.parameters);
      return writer;
    };
    /**
     * Encodes the specified StitchOutput message, length delimited. Does not implicitly {@link Peopletrack.StitchOutput.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.StitchOutput
     * @static
     * @param {Peopletrack.IStitchOutput} message StitchOutput message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    StitchOutput.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a StitchOutput message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.StitchOutput
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.StitchOutput} StitchOutput
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    StitchOutput.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.StitchOutput();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.frameNumber = reader.uint32();
            break;

          case 2:
            message.timestamp = reader.uint64();
            break;

          case 3:
            if (!(message.people && message.people.length)) message.people = [];
            message.people.push($root$5.Peopletrack.People.decode(reader, reader.uint32()));
            break;

          case 4:
            message.backgroundChart = reader.bytes();
            break;

          case 5:
            message.parameters = reader.bytes();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a StitchOutput message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.StitchOutput
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.StitchOutput} StitchOutput
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    StitchOutput.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a StitchOutput message.
     * @function verify
     * @memberof Peopletrack.StitchOutput
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    StitchOutput.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) if (!$util$4.isInteger(message.frameNumber)) return "frameNumber: integer expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$4.isInteger(message.timestamp) && !(message.timestamp && $util$4.isInteger(message.timestamp.low) && $util$4.isInteger(message.timestamp.high))) return "timestamp: integer|Long expected";

      if (message.people != null && message.hasOwnProperty("people")) {
        if (!Array.isArray(message.people)) return "people: array expected";

        for (let i = 0; i < message.people.length; ++i) {
          let error = $root$5.Peopletrack.People.verify(message.people[i]);
          if (error) return "people." + error;
        }
      }

      if (message.backgroundChart != null && message.hasOwnProperty("backgroundChart")) if (!(message.backgroundChart && typeof message.backgroundChart.length === "number" || $util$4.isString(message.backgroundChart))) return "backgroundChart: buffer expected";
      if (message.parameters != null && message.hasOwnProperty("parameters")) if (!(message.parameters && typeof message.parameters.length === "number" || $util$4.isString(message.parameters))) return "parameters: buffer expected";
      return null;
    };
    /**
     * Creates a StitchOutput message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.StitchOutput
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.StitchOutput} StitchOutput
     */


    StitchOutput.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.StitchOutput) return object;
      let message = new $root$5.Peopletrack.StitchOutput();
      if (object.frameNumber != null) message.frameNumber = object.frameNumber >>> 0;
      if (object.timestamp != null) if ($util$4.Long) (message.timestamp = $util$4.Long.fromValue(object.timestamp)).unsigned = true;else if (typeof object.timestamp === "string") message.timestamp = parseInt(object.timestamp, 10);else if (typeof object.timestamp === "number") message.timestamp = object.timestamp;else if (typeof object.timestamp === "object") message.timestamp = new $util$4.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber(true);

      if (object.people) {
        if (!Array.isArray(object.people)) throw TypeError(".Peopletrack.StitchOutput.people: array expected");
        message.people = [];

        for (let i = 0; i < object.people.length; ++i) {
          if (typeof object.people[i] !== "object") throw TypeError(".Peopletrack.StitchOutput.people: object expected");
          message.people[i] = $root$5.Peopletrack.People.fromObject(object.people[i]);
        }
      }

      if (object.backgroundChart != null) if (typeof object.backgroundChart === "string") $util$4.base64.decode(object.backgroundChart, message.backgroundChart = $util$4.newBuffer($util$4.base64.length(object.backgroundChart)), 0);else if (object.backgroundChart.length) message.backgroundChart = object.backgroundChart;
      if (object.parameters != null) if (typeof object.parameters === "string") $util$4.base64.decode(object.parameters, message.parameters = $util$4.newBuffer($util$4.base64.length(object.parameters)), 0);else if (object.parameters.length) message.parameters = object.parameters;
      return message;
    };
    /**
     * Creates a plain object from a StitchOutput message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.StitchOutput
     * @static
     * @param {Peopletrack.StitchOutput} message StitchOutput
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    StitchOutput.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.arrays || options.defaults) object.people = [];

      if (options.defaults) {
        object.frameNumber = 0;

        if ($util$4.Long) {
          let long = new $util$4.Long(0, 0, true);
          object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
        } else object.timestamp = options.longs === String ? "0" : 0;

        if (options.bytes === String) object.backgroundChart = "";else {
          object.backgroundChart = [];
          if (options.bytes !== Array) object.backgroundChart = $util$4.newBuffer(object.backgroundChart);
        }
        if (options.bytes === String) object.parameters = "";else {
          object.parameters = [];
          if (options.bytes !== Array) object.parameters = $util$4.newBuffer(object.parameters);
        }
      }

      if (message.frameNumber != null && message.hasOwnProperty("frameNumber")) object.frameNumber = message.frameNumber;
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (typeof message.timestamp === "number") object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;else object.timestamp = options.longs === String ? $util$4.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util$4.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber(true) : message.timestamp;

      if (message.people && message.people.length) {
        object.people = [];

        for (let j = 0; j < message.people.length; ++j) object.people[j] = $root$5.Peopletrack.People.toObject(message.people[j], options);
      }

      if (message.backgroundChart != null && message.hasOwnProperty("backgroundChart")) object.backgroundChart = options.bytes === String ? $util$4.base64.encode(message.backgroundChart, 0, message.backgroundChart.length) : options.bytes === Array ? Array.prototype.slice.call(message.backgroundChart) : message.backgroundChart;
      if (message.parameters != null && message.hasOwnProperty("parameters")) object.parameters = options.bytes === String ? $util$4.base64.encode(message.parameters, 0, message.parameters.length) : options.bytes === Array ? Array.prototype.slice.call(message.parameters) : message.parameters;
      return object;
    };
    /**
     * Converts this StitchOutput to JSON.
     * @function toJSON
     * @memberof Peopletrack.StitchOutput
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    StitchOutput.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return StitchOutput;
  }();

  Peopletrack.Device = /*#__PURE__*/function () {
    /**
     * Properties of a Device.
     * @memberof Peopletrack
     * @interface IDevice
     * @property {number|null} [stitchID] Device stitchID
     * @property {string|null} [token] Device token
     */

    /**
     * Constructs a new Device.
     * @memberof Peopletrack
     * @classdesc Represents a Device.
     * @implements IDevice
     * @constructor
     * @param {Peopletrack.IDevice=} [properties] Properties to set
     */
    function Device(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Device stitchID.
     * @member {number} stitchID
     * @memberof Peopletrack.Device
     * @instance
     */


    Device.prototype.stitchID = 0;
    /**
     * Device token.
     * @member {string} token
     * @memberof Peopletrack.Device
     * @instance
     */

    Device.prototype.token = "";
    /**
     * Creates a new Device instance using the specified properties.
     * @function create
     * @memberof Peopletrack.Device
     * @static
     * @param {Peopletrack.IDevice=} [properties] Properties to set
     * @returns {Peopletrack.Device} Device instance
     */

    Device.create = function create(properties) {
      return new Device(properties);
    };
    /**
     * Encodes the specified Device message. Does not implicitly {@link Peopletrack.Device.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.Device
     * @static
     * @param {Peopletrack.IDevice} message Device message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Device.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.stitchID != null && Object.hasOwnProperty.call(message, "stitchID")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).uint32(message.stitchID);
      if (message.token != null && Object.hasOwnProperty.call(message, "token")) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.token);
      return writer;
    };
    /**
     * Encodes the specified Device message, length delimited. Does not implicitly {@link Peopletrack.Device.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.Device
     * @static
     * @param {Peopletrack.IDevice} message Device message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Device.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Device message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.Device
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.Device} Device
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Device.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.Device();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.stitchID = reader.uint32();
            break;

          case 2:
            message.token = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Device message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.Device
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.Device} Device
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Device.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Device message.
     * @function verify
     * @memberof Peopletrack.Device
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Device.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.stitchID != null && message.hasOwnProperty("stitchID")) if (!$util$4.isInteger(message.stitchID)) return "stitchID: integer expected";
      if (message.token != null && message.hasOwnProperty("token")) if (!$util$4.isString(message.token)) return "token: string expected";
      return null;
    };
    /**
     * Creates a Device message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.Device
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.Device} Device
     */


    Device.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.Device) return object;
      let message = new $root$5.Peopletrack.Device();
      if (object.stitchID != null) message.stitchID = object.stitchID >>> 0;
      if (object.token != null) message.token = String(object.token);
      return message;
    };
    /**
     * Creates a plain object from a Device message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.Device
     * @static
     * @param {Peopletrack.Device} message Device
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Device.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.stitchID = 0;
        object.token = "";
      }

      if (message.stitchID != null && message.hasOwnProperty("stitchID")) object.stitchID = message.stitchID;
      if (message.token != null && message.hasOwnProperty("token")) object.token = message.token;
      return object;
    };
    /**
     * Converts this Device to JSON.
     * @function toJSON
     * @memberof Peopletrack.Device
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Device.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Device;
  }();

  Peopletrack.Devices = /*#__PURE__*/function () {
    /**
     * Properties of a Devices.
     * @memberof Peopletrack
     * @interface IDevices
     * @property {Array.<Peopletrack.IDevice>|null} [device] Devices device
     */

    /**
     * Constructs a new Devices.
     * @memberof Peopletrack
     * @classdesc Represents a Devices.
     * @implements IDevices
     * @constructor
     * @param {Peopletrack.IDevices=} [properties] Properties to set
     */
    function Devices(properties) {
      this.device = [];
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Devices device.
     * @member {Array.<Peopletrack.IDevice>} device
     * @memberof Peopletrack.Devices
     * @instance
     */


    Devices.prototype.device = $util$4.emptyArray;
    /**
     * Creates a new Devices instance using the specified properties.
     * @function create
     * @memberof Peopletrack.Devices
     * @static
     * @param {Peopletrack.IDevices=} [properties] Properties to set
     * @returns {Peopletrack.Devices} Devices instance
     */

    Devices.create = function create(properties) {
      return new Devices(properties);
    };
    /**
     * Encodes the specified Devices message. Does not implicitly {@link Peopletrack.Devices.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.Devices
     * @static
     * @param {Peopletrack.IDevices} message Devices message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Devices.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.device != null && message.device.length) for (let i = 0; i < message.device.length; ++i) $root$5.Peopletrack.Device.encode(message.device[i], writer.uint32(
      /* id 1, wireType 2 =*/
      10).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified Devices message, length delimited. Does not implicitly {@link Peopletrack.Devices.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.Devices
     * @static
     * @param {Peopletrack.IDevices} message Devices message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Devices.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Devices message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.Devices
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.Devices} Devices
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Devices.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.Devices();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            if (!(message.device && message.device.length)) message.device = [];
            message.device.push($root$5.Peopletrack.Device.decode(reader, reader.uint32()));
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Devices message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.Devices
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.Devices} Devices
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Devices.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Devices message.
     * @function verify
     * @memberof Peopletrack.Devices
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Devices.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";

      if (message.device != null && message.hasOwnProperty("device")) {
        if (!Array.isArray(message.device)) return "device: array expected";

        for (let i = 0; i < message.device.length; ++i) {
          let error = $root$5.Peopletrack.Device.verify(message.device[i]);
          if (error) return "device." + error;
        }
      }

      return null;
    };
    /**
     * Creates a Devices message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.Devices
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.Devices} Devices
     */


    Devices.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.Devices) return object;
      let message = new $root$5.Peopletrack.Devices();

      if (object.device) {
        if (!Array.isArray(object.device)) throw TypeError(".Peopletrack.Devices.device: array expected");
        message.device = [];

        for (let i = 0; i < object.device.length; ++i) {
          if (typeof object.device[i] !== "object") throw TypeError(".Peopletrack.Devices.device: object expected");
          message.device[i] = $root$5.Peopletrack.Device.fromObject(object.device[i]);
        }
      }

      return message;
    };
    /**
     * Creates a plain object from a Devices message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.Devices
     * @static
     * @param {Peopletrack.Devices} message Devices
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Devices.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.arrays || options.defaults) object.device = [];

      if (message.device && message.device.length) {
        object.device = [];

        for (let j = 0; j < message.device.length; ++j) object.device[j] = $root$5.Peopletrack.Device.toObject(message.device[j], options);
      }

      return object;
    };
    /**
     * Converts this Devices to JSON.
     * @function toJSON
     * @memberof Peopletrack.Devices
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Devices.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Devices;
  }();

  Peopletrack.DeviceReq = /*#__PURE__*/function () {
    /**
     * Properties of a DeviceReq.
     * @memberof Peopletrack
     * @interface IDeviceReq
     * @property {string|null} [token] DeviceReq token
     */

    /**
     * Constructs a new DeviceReq.
     * @memberof Peopletrack
     * @classdesc Represents a DeviceReq.
     * @implements IDeviceReq
     * @constructor
     * @param {Peopletrack.IDeviceReq=} [properties] Properties to set
     */
    function DeviceReq(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * DeviceReq token.
     * @member {string} token
     * @memberof Peopletrack.DeviceReq
     * @instance
     */


    DeviceReq.prototype.token = "";
    /**
     * Creates a new DeviceReq instance using the specified properties.
     * @function create
     * @memberof Peopletrack.DeviceReq
     * @static
     * @param {Peopletrack.IDeviceReq=} [properties] Properties to set
     * @returns {Peopletrack.DeviceReq} DeviceReq instance
     */

    DeviceReq.create = function create(properties) {
      return new DeviceReq(properties);
    };
    /**
     * Encodes the specified DeviceReq message. Does not implicitly {@link Peopletrack.DeviceReq.verify|verify} messages.
     * @function encode
     * @memberof Peopletrack.DeviceReq
     * @static
     * @param {Peopletrack.IDeviceReq} message DeviceReq message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    DeviceReq.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$5.create();
      if (message.token != null && Object.hasOwnProperty.call(message, "token")) writer.uint32(
      /* id 1, wireType 2 =*/
      10).string(message.token);
      return writer;
    };
    /**
     * Encodes the specified DeviceReq message, length delimited. Does not implicitly {@link Peopletrack.DeviceReq.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Peopletrack.DeviceReq
     * @static
     * @param {Peopletrack.IDeviceReq} message DeviceReq message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    DeviceReq.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a DeviceReq message from the specified reader or buffer.
     * @function decode
     * @memberof Peopletrack.DeviceReq
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Peopletrack.DeviceReq} DeviceReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    DeviceReq.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$5)) reader = $Reader$5.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$5.Peopletrack.DeviceReq();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.token = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a DeviceReq message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Peopletrack.DeviceReq
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Peopletrack.DeviceReq} DeviceReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    DeviceReq.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$5)) reader = new $Reader$5(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a DeviceReq message.
     * @function verify
     * @memberof Peopletrack.DeviceReq
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    DeviceReq.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.token != null && message.hasOwnProperty("token")) if (!$util$4.isString(message.token)) return "token: string expected";
      return null;
    };
    /**
     * Creates a DeviceReq message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Peopletrack.DeviceReq
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Peopletrack.DeviceReq} DeviceReq
     */


    DeviceReq.fromObject = function fromObject(object) {
      if (object instanceof $root$5.Peopletrack.DeviceReq) return object;
      let message = new $root$5.Peopletrack.DeviceReq();
      if (object.token != null) message.token = String(object.token);
      return message;
    };
    /**
     * Creates a plain object from a DeviceReq message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Peopletrack.DeviceReq
     * @static
     * @param {Peopletrack.DeviceReq} message DeviceReq
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    DeviceReq.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.defaults) object.token = "";
      if (message.token != null && message.hasOwnProperty("token")) object.token = message.token;
      return object;
    };
    /**
     * Converts this DeviceReq to JSON.
     * @function toJSON
     * @memberof Peopletrack.DeviceReq
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    DeviceReq.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return DeviceReq;
  }();

  Peopletrack.AI = /*#__PURE__*/function () {
    /**
     * Constructs a new AI service.
     * @memberof Peopletrack
     * @classdesc Represents a AI
     * @extends $protobuf.rpc.Service
     * @constructor
     * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
     * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
     * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
     */
    function AI(rpcImpl, requestDelimited, responseDelimited) {
      $protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
    }

    (AI.prototype = /*#__PURE__*/Object.create($protobuf.rpc.Service.prototype)).constructor = AI;
    /**
     * Creates new AI service using the specified rpc implementation.
     * @function create
     * @memberof Peopletrack.AI
     * @static
     * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
     * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
     * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
     * @returns {AI} RPC service. Useful where requests and/or responses are streamed.
     */

    AI.create = function create(rpcImpl, requestDelimited, responseDelimited) {
      return new this(rpcImpl, requestDelimited, responseDelimited);
    };
    /**
     * Callback as used by {@link Peopletrack.AI#echo}.
     * @memberof Peopletrack.AI
     * @typedef EchoCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Peopletrack.Msg} [response] Msg
     */

    /**
     * Calls Echo.
     * @function echo
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.IMsg} request Msg message or plain object
     * @param {Peopletrack.AI.EchoCallback} callback Node-style callback called with the error, if any, and Msg
     * @returns {undefined}
     * @variation 1
     */


    Object.defineProperty(AI.prototype.echo = function echo(request, callback) {
      return this.rpcCall(echo, $root$5.Peopletrack.Msg, $root$5.Peopletrack.Msg, request, callback);
    }, "name", {
      value: "Echo"
    });
    /**
     * Calls Echo.
     * @function echo
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.IMsg} request Msg message or plain object
     * @returns {Promise<Peopletrack.Msg>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link Peopletrack.AI#setup}.
     * @memberof Peopletrack.AI
     * @typedef SetupCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Peopletrack.SetupOutput} [response] SetupOutput
     */

    /**
     * Calls Setup.
     * @function setup
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.ISetupInput} request SetupInput message or plain object
     * @param {Peopletrack.AI.SetupCallback} callback Node-style callback called with the error, if any, and SetupOutput
     * @returns {undefined}
     * @variation 1
     */

    Object.defineProperty(AI.prototype.setup = function setup(request, callback) {
      return this.rpcCall(setup, $root$5.Peopletrack.SetupInput, $root$5.Peopletrack.SetupOutput, request, callback);
    }, "name", {
      value: "Setup"
    });
    /**
     * Calls Setup.
     * @function setup
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.ISetupInput} request SetupInput message or plain object
     * @returns {Promise<Peopletrack.SetupOutput>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link Peopletrack.AI#stitch}.
     * @memberof Peopletrack.AI
     * @typedef StitchCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Peopletrack.StitchOutput} [response] StitchOutput
     */

    /**
     * Calls Stitch.
     * @function stitch
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.IStitchInput} request StitchInput message or plain object
     * @param {Peopletrack.AI.StitchCallback} callback Node-style callback called with the error, if any, and StitchOutput
     * @returns {undefined}
     * @variation 1
     */

    Object.defineProperty(AI.prototype.stitch = function stitch(request, callback) {
      return this.rpcCall(stitch, $root$5.Peopletrack.StitchInput, $root$5.Peopletrack.StitchOutput, request, callback);
    }, "name", {
      value: "Stitch"
    });
    /**
     * Calls Stitch.
     * @function stitch
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.IStitchInput} request StitchInput message or plain object
     * @returns {Promise<Peopletrack.StitchOutput>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link Peopletrack.AI#stitchApp}.
     * @memberof Peopletrack.AI
     * @typedef StitchAppCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Peopletrack.StitchOutput} [response] StitchOutput
     */

    /**
     * Calls StitchApp.
     * @function stitchApp
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.IDevices} request Devices message or plain object
     * @param {Peopletrack.AI.StitchAppCallback} callback Node-style callback called with the error, if any, and StitchOutput
     * @returns {undefined}
     * @variation 1
     */

    Object.defineProperty(AI.prototype.stitchApp = function stitchApp(request, callback) {
      return this.rpcCall(stitchApp, $root$5.Peopletrack.Devices, $root$5.Peopletrack.StitchOutput, request, callback);
    }, "name", {
      value: "StitchApp"
    });
    /**
     * Calls StitchApp.
     * @function stitchApp
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.IDevices} request Devices message or plain object
     * @returns {Promise<Peopletrack.StitchOutput>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link Peopletrack.AI#getMsg}.
     * @memberof Peopletrack.AI
     * @typedef GetMsgCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Peopletrack.Msg} [response] Msg
     */

    /**
     * Calls GetMsg.
     * @function getMsg
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.IDeviceReq} request DeviceReq message or plain object
     * @param {Peopletrack.AI.GetMsgCallback} callback Node-style callback called with the error, if any, and Msg
     * @returns {undefined}
     * @variation 1
     */

    Object.defineProperty(AI.prototype.getMsg = function getMsg(request, callback) {
      return this.rpcCall(getMsg, $root$5.Peopletrack.DeviceReq, $root$5.Peopletrack.Msg, request, callback);
    }, "name", {
      value: "GetMsg"
    });
    /**
     * Calls GetMsg.
     * @function getMsg
     * @memberof Peopletrack.AI
     * @instance
     * @param {Peopletrack.IDeviceReq} request DeviceReq message or plain object
     * @returns {Promise<Peopletrack.Msg>} Promise
     * @variation 2
     */

    return AI;
  }();

  return Peopletrack;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$6 = $protobuf.Reader,
      $Writer$6 = $protobuf.Writer,
      $util$5 = $protobuf.util; // Exported root namespace

const $root$6 = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Playback = $root$6.Playback = /*#__PURE__*/(() => {
  /**
   * Namespace Playback.
   * @exports Playback
   * @namespace
   */
  const Playback = {};

  Playback.Msg = /*#__PURE__*/function () {
    /**
     * Properties of a Msg.
     * @memberof Playback
     * @interface IMsg
     * @property {Playback.Msg.Type|null} [type] Msg type
     * @property {string|null} [startTime] Msg startTime
     * @property {string|null} [stopTime] Msg stopTime
     */

    /**
     * Constructs a new Msg.
     * @memberof Playback
     * @classdesc Represents a Msg.
     * @implements IMsg
     * @constructor
     * @param {Playback.IMsg=} [properties] Properties to set
     */
    function Msg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Msg type.
     * @member {Playback.Msg.Type} type
     * @memberof Playback.Msg
     * @instance
     */


    Msg.prototype.type = 0;
    /**
     * Msg startTime.
     * @member {string} startTime
     * @memberof Playback.Msg
     * @instance
     */

    Msg.prototype.startTime = "";
    /**
     * Msg stopTime.
     * @member {string} stopTime
     * @memberof Playback.Msg
     * @instance
     */

    Msg.prototype.stopTime = "";
    /**
     * Creates a new Msg instance using the specified properties.
     * @function create
     * @memberof Playback.Msg
     * @static
     * @param {Playback.IMsg=} [properties] Properties to set
     * @returns {Playback.Msg} Msg instance
     */

    Msg.create = function create(properties) {
      return new Msg(properties);
    };
    /**
     * Encodes the specified Msg message. Does not implicitly {@link Playback.Msg.verify|verify} messages.
     * @function encode
     * @memberof Playback.Msg
     * @static
     * @param {Playback.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$6.create();
      if (message.type != null && Object.hasOwnProperty.call(message, "type")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).int32(message.type);
      if (message.startTime != null && Object.hasOwnProperty.call(message, "startTime")) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.startTime);
      if (message.stopTime != null && Object.hasOwnProperty.call(message, "stopTime")) writer.uint32(
      /* id 3, wireType 2 =*/
      26).string(message.stopTime);
      return writer;
    };
    /**
     * Encodes the specified Msg message, length delimited. Does not implicitly {@link Playback.Msg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Playback.Msg
     * @static
     * @param {Playback.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Msg message from the specified reader or buffer.
     * @function decode
     * @memberof Playback.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Playback.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$6)) reader = $Reader$6.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$6.Playback.Msg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.type = reader.int32();
            break;

          case 2:
            message.startTime = reader.string();
            break;

          case 3:
            message.stopTime = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Msg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Playback.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Playback.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$6)) reader = new $Reader$6(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Msg message.
     * @function verify
     * @memberof Playback.Msg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Msg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.type != null && message.hasOwnProperty("type")) switch (message.type) {
        default:
          return "type: enum value expected";

        case 0:
        case 1:
          break;
      }
      if (message.startTime != null && message.hasOwnProperty("startTime")) if (!$util$5.isString(message.startTime)) return "startTime: string expected";
      if (message.stopTime != null && message.hasOwnProperty("stopTime")) if (!$util$5.isString(message.stopTime)) return "stopTime: string expected";
      return null;
    };
    /**
     * Creates a Msg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Playback.Msg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Playback.Msg} Msg
     */


    Msg.fromObject = function fromObject(object) {
      if (object instanceof $root$6.Playback.Msg) return object;
      let message = new $root$6.Playback.Msg();

      switch (object.type) {
        case "PLAY":
        case 0:
          message.type = 0;
          break;

        case "KEEPALIVE":
        case 1:
          message.type = 1;
          break;
      }

      if (object.startTime != null) message.startTime = String(object.startTime);
      if (object.stopTime != null) message.stopTime = String(object.stopTime);
      return message;
    };
    /**
     * Creates a plain object from a Msg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Playback.Msg
     * @static
     * @param {Playback.Msg} message Msg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Msg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.type = options.enums === String ? "PLAY" : 0;
        object.startTime = "";
        object.stopTime = "";
      }

      if (message.type != null && message.hasOwnProperty("type")) object.type = options.enums === String ? $root$6.Playback.Msg.Type[message.type] : message.type;
      if (message.startTime != null && message.hasOwnProperty("startTime")) object.startTime = message.startTime;
      if (message.stopTime != null && message.hasOwnProperty("stopTime")) object.stopTime = message.stopTime;
      return object;
    };
    /**
     * Converts this Msg to JSON.
     * @function toJSON
     * @memberof Playback.Msg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Msg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * Type enum.
     * @name Playback.Msg.Type
     * @enum {number}
     * @property {number} PLAY=0 PLAY value
     * @property {number} KEEPALIVE=1 KEEPALIVE value
     */


    Msg.Type = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "PLAY"] = 0;
      values[valuesById[1] = "KEEPALIVE"] = 1;
      return values;
    }();

    return Msg;
  }();

  return Playback;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$7 = $protobuf.Reader,
      $Writer$7 = $protobuf.Writer,
      $util$6 = $protobuf.util; // Exported root namespace

const $root$7 = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Facedetection = $root$7.Facedetection = /*#__PURE__*/(() => {
  /**
   * Namespace Facedetection.
   * @exports Facedetection
   * @namespace
   */
  const Facedetection = {};

  Facedetection.Detection = /*#__PURE__*/function () {
    /**
     * Properties of a Detection.
     * @memberof Facedetection
     * @interface IDetection
     * @property {number|Long|null} [timestamp] Detection timestamp
     * @property {number|Long|null} [imageWidth] Detection imageWidth
     * @property {number|Long|null} [imageHeight] Detection imageHeight
     * @property {Uint8Array|null} [image] Detection image
     * @property {Array.<Facedetection.IRectangle>|null} [rects] Detection rects
     */

    /**
     * Constructs a new Detection.
     * @memberof Facedetection
     * @classdesc Represents a Detection.
     * @implements IDetection
     * @constructor
     * @param {Facedetection.IDetection=} [properties] Properties to set
     */
    function Detection(properties) {
      this.rects = [];
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Detection timestamp.
     * @member {number|Long} timestamp
     * @memberof Facedetection.Detection
     * @instance
     */


    Detection.prototype.timestamp = $util$6.Long ? /*#__PURE__*/$util$6.Long.fromBits(0, 0, false) : 0;
    /**
     * Detection imageWidth.
     * @member {number|Long} imageWidth
     * @memberof Facedetection.Detection
     * @instance
     */

    Detection.prototype.imageWidth = $util$6.Long ? /*#__PURE__*/$util$6.Long.fromBits(0, 0, false) : 0;
    /**
     * Detection imageHeight.
     * @member {number|Long} imageHeight
     * @memberof Facedetection.Detection
     * @instance
     */

    Detection.prototype.imageHeight = $util$6.Long ? /*#__PURE__*/$util$6.Long.fromBits(0, 0, false) : 0;
    /**
     * Detection image.
     * @member {Uint8Array} image
     * @memberof Facedetection.Detection
     * @instance
     */

    Detection.prototype.image = /*#__PURE__*/$util$6.newBuffer([]);
    /**
     * Detection rects.
     * @member {Array.<Facedetection.IRectangle>} rects
     * @memberof Facedetection.Detection
     * @instance
     */

    Detection.prototype.rects = $util$6.emptyArray;
    /**
     * Creates a new Detection instance using the specified properties.
     * @function create
     * @memberof Facedetection.Detection
     * @static
     * @param {Facedetection.IDetection=} [properties] Properties to set
     * @returns {Facedetection.Detection} Detection instance
     */

    Detection.create = function create(properties) {
      return new Detection(properties);
    };
    /**
     * Encodes the specified Detection message. Does not implicitly {@link Facedetection.Detection.verify|verify} messages.
     * @function encode
     * @memberof Facedetection.Detection
     * @static
     * @param {Facedetection.IDetection} message Detection message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Detection.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$7.create();
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).int64(message.timestamp);
      if (message.imageWidth != null && Object.hasOwnProperty.call(message, "imageWidth")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).int64(message.imageWidth);
      if (message.imageHeight != null && Object.hasOwnProperty.call(message, "imageHeight")) writer.uint32(
      /* id 3, wireType 0 =*/
      24).int64(message.imageHeight);
      if (message.image != null && Object.hasOwnProperty.call(message, "image")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).bytes(message.image);
      if (message.rects != null && message.rects.length) for (let i = 0; i < message.rects.length; ++i) $root$7.Facedetection.Rectangle.encode(message.rects[i], writer.uint32(
      /* id 5, wireType 2 =*/
      42).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified Detection message, length delimited. Does not implicitly {@link Facedetection.Detection.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Facedetection.Detection
     * @static
     * @param {Facedetection.IDetection} message Detection message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Detection.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Detection message from the specified reader or buffer.
     * @function decode
     * @memberof Facedetection.Detection
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Facedetection.Detection} Detection
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Detection.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$7)) reader = $Reader$7.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$7.Facedetection.Detection();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.timestamp = reader.int64();
            break;

          case 2:
            message.imageWidth = reader.int64();
            break;

          case 3:
            message.imageHeight = reader.int64();
            break;

          case 4:
            message.image = reader.bytes();
            break;

          case 5:
            if (!(message.rects && message.rects.length)) message.rects = [];
            message.rects.push($root$7.Facedetection.Rectangle.decode(reader, reader.uint32()));
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Detection message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Facedetection.Detection
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Facedetection.Detection} Detection
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Detection.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$7)) reader = new $Reader$7(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Detection message.
     * @function verify
     * @memberof Facedetection.Detection
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Detection.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$6.isInteger(message.timestamp) && !(message.timestamp && $util$6.isInteger(message.timestamp.low) && $util$6.isInteger(message.timestamp.high))) return "timestamp: integer|Long expected";
      if (message.imageWidth != null && message.hasOwnProperty("imageWidth")) if (!$util$6.isInteger(message.imageWidth) && !(message.imageWidth && $util$6.isInteger(message.imageWidth.low) && $util$6.isInteger(message.imageWidth.high))) return "imageWidth: integer|Long expected";
      if (message.imageHeight != null && message.hasOwnProperty("imageHeight")) if (!$util$6.isInteger(message.imageHeight) && !(message.imageHeight && $util$6.isInteger(message.imageHeight.low) && $util$6.isInteger(message.imageHeight.high))) return "imageHeight: integer|Long expected";
      if (message.image != null && message.hasOwnProperty("image")) if (!(message.image && typeof message.image.length === "number" || $util$6.isString(message.image))) return "image: buffer expected";

      if (message.rects != null && message.hasOwnProperty("rects")) {
        if (!Array.isArray(message.rects)) return "rects: array expected";

        for (let i = 0; i < message.rects.length; ++i) {
          let error = $root$7.Facedetection.Rectangle.verify(message.rects[i]);
          if (error) return "rects." + error;
        }
      }

      return null;
    };
    /**
     * Creates a Detection message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Facedetection.Detection
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Facedetection.Detection} Detection
     */


    Detection.fromObject = function fromObject(object) {
      if (object instanceof $root$7.Facedetection.Detection) return object;
      let message = new $root$7.Facedetection.Detection();
      if (object.timestamp != null) if ($util$6.Long) (message.timestamp = $util$6.Long.fromValue(object.timestamp)).unsigned = false;else if (typeof object.timestamp === "string") message.timestamp = parseInt(object.timestamp, 10);else if (typeof object.timestamp === "number") message.timestamp = object.timestamp;else if (typeof object.timestamp === "object") message.timestamp = new $util$6.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber();
      if (object.imageWidth != null) if ($util$6.Long) (message.imageWidth = $util$6.Long.fromValue(object.imageWidth)).unsigned = false;else if (typeof object.imageWidth === "string") message.imageWidth = parseInt(object.imageWidth, 10);else if (typeof object.imageWidth === "number") message.imageWidth = object.imageWidth;else if (typeof object.imageWidth === "object") message.imageWidth = new $util$6.LongBits(object.imageWidth.low >>> 0, object.imageWidth.high >>> 0).toNumber();
      if (object.imageHeight != null) if ($util$6.Long) (message.imageHeight = $util$6.Long.fromValue(object.imageHeight)).unsigned = false;else if (typeof object.imageHeight === "string") message.imageHeight = parseInt(object.imageHeight, 10);else if (typeof object.imageHeight === "number") message.imageHeight = object.imageHeight;else if (typeof object.imageHeight === "object") message.imageHeight = new $util$6.LongBits(object.imageHeight.low >>> 0, object.imageHeight.high >>> 0).toNumber();
      if (object.image != null) if (typeof object.image === "string") $util$6.base64.decode(object.image, message.image = $util$6.newBuffer($util$6.base64.length(object.image)), 0);else if (object.image.length) message.image = object.image;

      if (object.rects) {
        if (!Array.isArray(object.rects)) throw TypeError(".Facedetection.Detection.rects: array expected");
        message.rects = [];

        for (let i = 0; i < object.rects.length; ++i) {
          if (typeof object.rects[i] !== "object") throw TypeError(".Facedetection.Detection.rects: object expected");
          message.rects[i] = $root$7.Facedetection.Rectangle.fromObject(object.rects[i]);
        }
      }

      return message;
    };
    /**
     * Creates a plain object from a Detection message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Facedetection.Detection
     * @static
     * @param {Facedetection.Detection} message Detection
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Detection.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};
      if (options.arrays || options.defaults) object.rects = [];

      if (options.defaults) {
        if ($util$6.Long) {
          let long = new $util$6.Long(0, 0, false);
          object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
        } else object.timestamp = options.longs === String ? "0" : 0;

        if ($util$6.Long) {
          let long = new $util$6.Long(0, 0, false);
          object.imageWidth = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
        } else object.imageWidth = options.longs === String ? "0" : 0;

        if ($util$6.Long) {
          let long = new $util$6.Long(0, 0, false);
          object.imageHeight = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
        } else object.imageHeight = options.longs === String ? "0" : 0;

        if (options.bytes === String) object.image = "";else {
          object.image = [];
          if (options.bytes !== Array) object.image = $util$6.newBuffer(object.image);
        }
      }

      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (typeof message.timestamp === "number") object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;else object.timestamp = options.longs === String ? $util$6.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util$6.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber() : message.timestamp;
      if (message.imageWidth != null && message.hasOwnProperty("imageWidth")) if (typeof message.imageWidth === "number") object.imageWidth = options.longs === String ? String(message.imageWidth) : message.imageWidth;else object.imageWidth = options.longs === String ? $util$6.Long.prototype.toString.call(message.imageWidth) : options.longs === Number ? new $util$6.LongBits(message.imageWidth.low >>> 0, message.imageWidth.high >>> 0).toNumber() : message.imageWidth;
      if (message.imageHeight != null && message.hasOwnProperty("imageHeight")) if (typeof message.imageHeight === "number") object.imageHeight = options.longs === String ? String(message.imageHeight) : message.imageHeight;else object.imageHeight = options.longs === String ? $util$6.Long.prototype.toString.call(message.imageHeight) : options.longs === Number ? new $util$6.LongBits(message.imageHeight.low >>> 0, message.imageHeight.high >>> 0).toNumber() : message.imageHeight;
      if (message.image != null && message.hasOwnProperty("image")) object.image = options.bytes === String ? $util$6.base64.encode(message.image, 0, message.image.length) : options.bytes === Array ? Array.prototype.slice.call(message.image) : message.image;

      if (message.rects && message.rects.length) {
        object.rects = [];

        for (let j = 0; j < message.rects.length; ++j) object.rects[j] = $root$7.Facedetection.Rectangle.toObject(message.rects[j], options);
      }

      return object;
    };
    /**
     * Converts this Detection to JSON.
     * @function toJSON
     * @memberof Facedetection.Detection
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Detection.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Detection;
  }();

  Facedetection.Rectangle = /*#__PURE__*/function () {
    /**
     * Properties of a Rectangle.
     * @memberof Facedetection
     * @interface IRectangle
     * @property {number|null} [x] Rectangle x
     * @property {number|null} [y] Rectangle y
     * @property {number|null} [w] Rectangle w
     * @property {number|null} [h] Rectangle h
     */

    /**
     * Constructs a new Rectangle.
     * @memberof Facedetection
     * @classdesc Represents a Rectangle.
     * @implements IRectangle
     * @constructor
     * @param {Facedetection.IRectangle=} [properties] Properties to set
     */
    function Rectangle(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Rectangle x.
     * @member {number} x
     * @memberof Facedetection.Rectangle
     * @instance
     */


    Rectangle.prototype.x = 0;
    /**
     * Rectangle y.
     * @member {number} y
     * @memberof Facedetection.Rectangle
     * @instance
     */

    Rectangle.prototype.y = 0;
    /**
     * Rectangle w.
     * @member {number} w
     * @memberof Facedetection.Rectangle
     * @instance
     */

    Rectangle.prototype.w = 0;
    /**
     * Rectangle h.
     * @member {number} h
     * @memberof Facedetection.Rectangle
     * @instance
     */

    Rectangle.prototype.h = 0;
    /**
     * Creates a new Rectangle instance using the specified properties.
     * @function create
     * @memberof Facedetection.Rectangle
     * @static
     * @param {Facedetection.IRectangle=} [properties] Properties to set
     * @returns {Facedetection.Rectangle} Rectangle instance
     */

    Rectangle.create = function create(properties) {
      return new Rectangle(properties);
    };
    /**
     * Encodes the specified Rectangle message. Does not implicitly {@link Facedetection.Rectangle.verify|verify} messages.
     * @function encode
     * @memberof Facedetection.Rectangle
     * @static
     * @param {Facedetection.IRectangle} message Rectangle message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Rectangle.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$7.create();
      if (message.x != null && Object.hasOwnProperty.call(message, "x")) writer.uint32(
      /* id 1, wireType 1 =*/
      9).double(message.x);
      if (message.y != null && Object.hasOwnProperty.call(message, "y")) writer.uint32(
      /* id 2, wireType 1 =*/
      17).double(message.y);
      if (message.w != null && Object.hasOwnProperty.call(message, "w")) writer.uint32(
      /* id 3, wireType 1 =*/
      25).double(message.w);
      if (message.h != null && Object.hasOwnProperty.call(message, "h")) writer.uint32(
      /* id 4, wireType 1 =*/
      33).double(message.h);
      return writer;
    };
    /**
     * Encodes the specified Rectangle message, length delimited. Does not implicitly {@link Facedetection.Rectangle.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Facedetection.Rectangle
     * @static
     * @param {Facedetection.IRectangle} message Rectangle message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Rectangle.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Rectangle message from the specified reader or buffer.
     * @function decode
     * @memberof Facedetection.Rectangle
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Facedetection.Rectangle} Rectangle
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Rectangle.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$7)) reader = $Reader$7.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$7.Facedetection.Rectangle();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.x = reader.double();
            break;

          case 2:
            message.y = reader.double();
            break;

          case 3:
            message.w = reader.double();
            break;

          case 4:
            message.h = reader.double();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Rectangle message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Facedetection.Rectangle
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Facedetection.Rectangle} Rectangle
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Rectangle.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$7)) reader = new $Reader$7(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Rectangle message.
     * @function verify
     * @memberof Facedetection.Rectangle
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Rectangle.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.x != null && message.hasOwnProperty("x")) if (typeof message.x !== "number") return "x: number expected";
      if (message.y != null && message.hasOwnProperty("y")) if (typeof message.y !== "number") return "y: number expected";
      if (message.w != null && message.hasOwnProperty("w")) if (typeof message.w !== "number") return "w: number expected";
      if (message.h != null && message.hasOwnProperty("h")) if (typeof message.h !== "number") return "h: number expected";
      return null;
    };
    /**
     * Creates a Rectangle message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Facedetection.Rectangle
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Facedetection.Rectangle} Rectangle
     */


    Rectangle.fromObject = function fromObject(object) {
      if (object instanceof $root$7.Facedetection.Rectangle) return object;
      let message = new $root$7.Facedetection.Rectangle();
      if (object.x != null) message.x = Number(object.x);
      if (object.y != null) message.y = Number(object.y);
      if (object.w != null) message.w = Number(object.w);
      if (object.h != null) message.h = Number(object.h);
      return message;
    };
    /**
     * Creates a plain object from a Rectangle message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Facedetection.Rectangle
     * @static
     * @param {Facedetection.Rectangle} message Rectangle
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Rectangle.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.x = 0;
        object.y = 0;
        object.w = 0;
        object.h = 0;
      }

      if (message.x != null && message.hasOwnProperty("x")) object.x = options.json && !isFinite(message.x) ? String(message.x) : message.x;
      if (message.y != null && message.hasOwnProperty("y")) object.y = options.json && !isFinite(message.y) ? String(message.y) : message.y;
      if (message.w != null && message.hasOwnProperty("w")) object.w = options.json && !isFinite(message.w) ? String(message.w) : message.w;
      if (message.h != null && message.hasOwnProperty("h")) object.h = options.json && !isFinite(message.h) ? String(message.h) : message.h;
      return object;
    };
    /**
     * Converts this Rectangle to JSON.
     * @function toJSON
     * @memberof Facedetection.Rectangle
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Rectangle.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Rectangle;
  }();

  return Facedetection;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$8 = $protobuf.Reader,
      $Writer$8 = $protobuf.Writer,
      $util$7 = $protobuf.util; // Exported root namespace

const $root$8 = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Recognition = $root$8.Recognition = /*#__PURE__*/(() => {
  /**
   * Namespace Recognition.
   * @exports Recognition
   * @namespace
   */
  const Recognition = {};

  Recognition.Result = /*#__PURE__*/function () {
    /**
     * Properties of a Result.
     * @memberof Recognition
     * @interface IResult
     * @property {number|Long|null} [timestamp] Result timestamp
     * @property {Array.<string>|null} [names] Result names
     * @property {Array.<Recognition.IRectangle>|null} [rects] Result rects
     */

    /**
     * Constructs a new Result.
     * @memberof Recognition
     * @classdesc Represents a Result.
     * @implements IResult
     * @constructor
     * @param {Recognition.IResult=} [properties] Properties to set
     */
    function Result(properties) {
      this.names = [];
      this.rects = [];
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Result timestamp.
     * @member {number|Long} timestamp
     * @memberof Recognition.Result
     * @instance
     */


    Result.prototype.timestamp = $util$7.Long ? /*#__PURE__*/$util$7.Long.fromBits(0, 0, false) : 0;
    /**
     * Result names.
     * @member {Array.<string>} names
     * @memberof Recognition.Result
     * @instance
     */

    Result.prototype.names = $util$7.emptyArray;
    /**
     * Result rects.
     * @member {Array.<Recognition.IRectangle>} rects
     * @memberof Recognition.Result
     * @instance
     */

    Result.prototype.rects = $util$7.emptyArray;
    /**
     * Creates a new Result instance using the specified properties.
     * @function create
     * @memberof Recognition.Result
     * @static
     * @param {Recognition.IResult=} [properties] Properties to set
     * @returns {Recognition.Result} Result instance
     */

    Result.create = function create(properties) {
      return new Result(properties);
    };
    /**
     * Encodes the specified Result message. Does not implicitly {@link Recognition.Result.verify|verify} messages.
     * @function encode
     * @memberof Recognition.Result
     * @static
     * @param {Recognition.IResult} message Result message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Result.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$8.create();
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).int64(message.timestamp);
      if (message.names != null && message.names.length) for (let i = 0; i < message.names.length; ++i) writer.uint32(
      /* id 2, wireType 2 =*/
      18).string(message.names[i]);
      if (message.rects != null && message.rects.length) for (let i = 0; i < message.rects.length; ++i) $root$8.Recognition.Rectangle.encode(message.rects[i], writer.uint32(
      /* id 3, wireType 2 =*/
      26).fork()).ldelim();
      return writer;
    };
    /**
     * Encodes the specified Result message, length delimited. Does not implicitly {@link Recognition.Result.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Recognition.Result
     * @static
     * @param {Recognition.IResult} message Result message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Result.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Result message from the specified reader or buffer.
     * @function decode
     * @memberof Recognition.Result
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Recognition.Result} Result
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Result.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$8)) reader = $Reader$8.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$8.Recognition.Result();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.timestamp = reader.int64();
            break;

          case 2:
            if (!(message.names && message.names.length)) message.names = [];
            message.names.push(reader.string());
            break;

          case 3:
            if (!(message.rects && message.rects.length)) message.rects = [];
            message.rects.push($root$8.Recognition.Rectangle.decode(reader, reader.uint32()));
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Result message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Recognition.Result
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Recognition.Result} Result
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Result.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$8)) reader = new $Reader$8(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Result message.
     * @function verify
     * @memberof Recognition.Result
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Result.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$7.isInteger(message.timestamp) && !(message.timestamp && $util$7.isInteger(message.timestamp.low) && $util$7.isInteger(message.timestamp.high))) return "timestamp: integer|Long expected";

      if (message.names != null && message.hasOwnProperty("names")) {
        if (!Array.isArray(message.names)) return "names: array expected";

        for (let i = 0; i < message.names.length; ++i) if (!$util$7.isString(message.names[i])) return "names: string[] expected";
      }

      if (message.rects != null && message.hasOwnProperty("rects")) {
        if (!Array.isArray(message.rects)) return "rects: array expected";

        for (let i = 0; i < message.rects.length; ++i) {
          let error = $root$8.Recognition.Rectangle.verify(message.rects[i]);
          if (error) return "rects." + error;
        }
      }

      return null;
    };
    /**
     * Creates a Result message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Recognition.Result
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Recognition.Result} Result
     */


    Result.fromObject = function fromObject(object) {
      if (object instanceof $root$8.Recognition.Result) return object;
      let message = new $root$8.Recognition.Result();
      if (object.timestamp != null) if ($util$7.Long) (message.timestamp = $util$7.Long.fromValue(object.timestamp)).unsigned = false;else if (typeof object.timestamp === "string") message.timestamp = parseInt(object.timestamp, 10);else if (typeof object.timestamp === "number") message.timestamp = object.timestamp;else if (typeof object.timestamp === "object") message.timestamp = new $util$7.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber();

      if (object.names) {
        if (!Array.isArray(object.names)) throw TypeError(".Recognition.Result.names: array expected");
        message.names = [];

        for (let i = 0; i < object.names.length; ++i) message.names[i] = String(object.names[i]);
      }

      if (object.rects) {
        if (!Array.isArray(object.rects)) throw TypeError(".Recognition.Result.rects: array expected");
        message.rects = [];

        for (let i = 0; i < object.rects.length; ++i) {
          if (typeof object.rects[i] !== "object") throw TypeError(".Recognition.Result.rects: object expected");
          message.rects[i] = $root$8.Recognition.Rectangle.fromObject(object.rects[i]);
        }
      }

      return message;
    };
    /**
     * Creates a plain object from a Result message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Recognition.Result
     * @static
     * @param {Recognition.Result} message Result
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Result.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.arrays || options.defaults) {
        object.names = [];
        object.rects = [];
      }

      if (options.defaults) if ($util$7.Long) {
        let long = new $util$7.Long(0, 0, false);
        object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
      } else object.timestamp = options.longs === String ? "0" : 0;
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (typeof message.timestamp === "number") object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;else object.timestamp = options.longs === String ? $util$7.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util$7.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber() : message.timestamp;

      if (message.names && message.names.length) {
        object.names = [];

        for (let j = 0; j < message.names.length; ++j) object.names[j] = message.names[j];
      }

      if (message.rects && message.rects.length) {
        object.rects = [];

        for (let j = 0; j < message.rects.length; ++j) object.rects[j] = $root$8.Recognition.Rectangle.toObject(message.rects[j], options);
      }

      return object;
    };
    /**
     * Converts this Result to JSON.
     * @function toJSON
     * @memberof Recognition.Result
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Result.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Result;
  }();

  Recognition.Rectangle = /*#__PURE__*/function () {
    /**
     * Properties of a Rectangle.
     * @memberof Recognition
     * @interface IRectangle
     * @property {number|null} [x] Rectangle x
     * @property {number|null} [y] Rectangle y
     * @property {number|null} [w] Rectangle w
     * @property {number|null} [h] Rectangle h
     */

    /**
     * Constructs a new Rectangle.
     * @memberof Recognition
     * @classdesc Represents a Rectangle.
     * @implements IRectangle
     * @constructor
     * @param {Recognition.IRectangle=} [properties] Properties to set
     */
    function Rectangle(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Rectangle x.
     * @member {number} x
     * @memberof Recognition.Rectangle
     * @instance
     */


    Rectangle.prototype.x = 0;
    /**
     * Rectangle y.
     * @member {number} y
     * @memberof Recognition.Rectangle
     * @instance
     */

    Rectangle.prototype.y = 0;
    /**
     * Rectangle w.
     * @member {number} w
     * @memberof Recognition.Rectangle
     * @instance
     */

    Rectangle.prototype.w = 0;
    /**
     * Rectangle h.
     * @member {number} h
     * @memberof Recognition.Rectangle
     * @instance
     */

    Rectangle.prototype.h = 0;
    /**
     * Creates a new Rectangle instance using the specified properties.
     * @function create
     * @memberof Recognition.Rectangle
     * @static
     * @param {Recognition.IRectangle=} [properties] Properties to set
     * @returns {Recognition.Rectangle} Rectangle instance
     */

    Rectangle.create = function create(properties) {
      return new Rectangle(properties);
    };
    /**
     * Encodes the specified Rectangle message. Does not implicitly {@link Recognition.Rectangle.verify|verify} messages.
     * @function encode
     * @memberof Recognition.Rectangle
     * @static
     * @param {Recognition.IRectangle} message Rectangle message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Rectangle.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$8.create();
      if (message.x != null && Object.hasOwnProperty.call(message, "x")) writer.uint32(
      /* id 1, wireType 1 =*/
      9).double(message.x);
      if (message.y != null && Object.hasOwnProperty.call(message, "y")) writer.uint32(
      /* id 2, wireType 1 =*/
      17).double(message.y);
      if (message.w != null && Object.hasOwnProperty.call(message, "w")) writer.uint32(
      /* id 3, wireType 1 =*/
      25).double(message.w);
      if (message.h != null && Object.hasOwnProperty.call(message, "h")) writer.uint32(
      /* id 4, wireType 1 =*/
      33).double(message.h);
      return writer;
    };
    /**
     * Encodes the specified Rectangle message, length delimited. Does not implicitly {@link Recognition.Rectangle.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Recognition.Rectangle
     * @static
     * @param {Recognition.IRectangle} message Rectangle message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Rectangle.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Rectangle message from the specified reader or buffer.
     * @function decode
     * @memberof Recognition.Rectangle
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Recognition.Rectangle} Rectangle
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Rectangle.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$8)) reader = $Reader$8.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$8.Recognition.Rectangle();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.x = reader.double();
            break;

          case 2:
            message.y = reader.double();
            break;

          case 3:
            message.w = reader.double();
            break;

          case 4:
            message.h = reader.double();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Rectangle message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Recognition.Rectangle
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Recognition.Rectangle} Rectangle
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Rectangle.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$8)) reader = new $Reader$8(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Rectangle message.
     * @function verify
     * @memberof Recognition.Rectangle
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Rectangle.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.x != null && message.hasOwnProperty("x")) if (typeof message.x !== "number") return "x: number expected";
      if (message.y != null && message.hasOwnProperty("y")) if (typeof message.y !== "number") return "y: number expected";
      if (message.w != null && message.hasOwnProperty("w")) if (typeof message.w !== "number") return "w: number expected";
      if (message.h != null && message.hasOwnProperty("h")) if (typeof message.h !== "number") return "h: number expected";
      return null;
    };
    /**
     * Creates a Rectangle message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Recognition.Rectangle
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Recognition.Rectangle} Rectangle
     */


    Rectangle.fromObject = function fromObject(object) {
      if (object instanceof $root$8.Recognition.Rectangle) return object;
      let message = new $root$8.Recognition.Rectangle();
      if (object.x != null) message.x = Number(object.x);
      if (object.y != null) message.y = Number(object.y);
      if (object.w != null) message.w = Number(object.w);
      if (object.h != null) message.h = Number(object.h);
      return message;
    };
    /**
     * Creates a plain object from a Rectangle message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Recognition.Rectangle
     * @static
     * @param {Recognition.Rectangle} message Rectangle
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Rectangle.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.x = 0;
        object.y = 0;
        object.w = 0;
        object.h = 0;
      }

      if (message.x != null && message.hasOwnProperty("x")) object.x = options.json && !isFinite(message.x) ? String(message.x) : message.x;
      if (message.y != null && message.hasOwnProperty("y")) object.y = options.json && !isFinite(message.y) ? String(message.y) : message.y;
      if (message.w != null && message.hasOwnProperty("w")) object.w = options.json && !isFinite(message.w) ? String(message.w) : message.w;
      if (message.h != null && message.hasOwnProperty("h")) object.h = options.json && !isFinite(message.h) ? String(message.h) : message.h;
      return object;
    };
    /**
     * Converts this Rectangle to JSON.
     * @function toJSON
     * @memberof Recognition.Rectangle
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Rectangle.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Rectangle;
  }();

  return Recognition;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$9 = $protobuf.Reader,
      $Writer$9 = $protobuf.Writer,
      $util$8 = $protobuf.util; // Exported root namespace

const $root$9 = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Websocket = $root$9.Websocket = /*#__PURE__*/(() => {
  /**
   * Namespace Websocket.
   * @exports Websocket
   * @namespace
   */
  const Websocket = {};

  Websocket.Msg = /*#__PURE__*/function () {
    /**
     * Properties of a Msg.
     * @memberof Websocket
     * @interface IMsg
     * @property {number|Long|null} [timestamp] Msg timestamp
     * @property {Websocket.Msg.Status|null} [status] Msg status
     */

    /**
     * Constructs a new Msg.
     * @memberof Websocket
     * @classdesc Represents a Msg.
     * @implements IMsg
     * @constructor
     * @param {Websocket.IMsg=} [properties] Properties to set
     */
    function Msg(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Msg timestamp.
     * @member {number|Long} timestamp
     * @memberof Websocket.Msg
     * @instance
     */


    Msg.prototype.timestamp = $util$8.Long ? /*#__PURE__*/$util$8.Long.fromBits(0, 0, false) : 0;
    /**
     * Msg status.
     * @member {Websocket.Msg.Status} status
     * @memberof Websocket.Msg
     * @instance
     */

    Msg.prototype.status = 0;
    /**
     * Creates a new Msg instance using the specified properties.
     * @function create
     * @memberof Websocket.Msg
     * @static
     * @param {Websocket.IMsg=} [properties] Properties to set
     * @returns {Websocket.Msg} Msg instance
     */

    Msg.create = function create(properties) {
      return new Msg(properties);
    };
    /**
     * Encodes the specified Msg message. Does not implicitly {@link Websocket.Msg.verify|verify} messages.
     * @function encode
     * @memberof Websocket.Msg
     * @static
     * @param {Websocket.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$9.create();
      if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp")) writer.uint32(
      /* id 1, wireType 0 =*/
      8).int64(message.timestamp);
      if (message.status != null && Object.hasOwnProperty.call(message, "status")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).int32(message.status);
      return writer;
    };
    /**
     * Encodes the specified Msg message, length delimited. Does not implicitly {@link Websocket.Msg.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Websocket.Msg
     * @static
     * @param {Websocket.IMsg} message Msg message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Msg.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Msg message from the specified reader or buffer.
     * @function decode
     * @memberof Websocket.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Websocket.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$9)) reader = $Reader$9.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$9.Websocket.Msg();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.timestamp = reader.int64();
            break;

          case 2:
            message.status = reader.int32();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Msg message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Websocket.Msg
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Websocket.Msg} Msg
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Msg.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$9)) reader = new $Reader$9(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Msg message.
     * @function verify
     * @memberof Websocket.Msg
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Msg.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (!$util$8.isInteger(message.timestamp) && !(message.timestamp && $util$8.isInteger(message.timestamp.low) && $util$8.isInteger(message.timestamp.high))) return "timestamp: integer|Long expected";
      if (message.status != null && message.hasOwnProperty("status")) switch (message.status) {
        default:
          return "status: enum value expected";

        case 0:
        case 1:
          break;
      }
      return null;
    };
    /**
     * Creates a Msg message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Websocket.Msg
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Websocket.Msg} Msg
     */


    Msg.fromObject = function fromObject(object) {
      if (object instanceof $root$9.Websocket.Msg) return object;
      let message = new $root$9.Websocket.Msg();
      if (object.timestamp != null) if ($util$8.Long) (message.timestamp = $util$8.Long.fromValue(object.timestamp)).unsigned = false;else if (typeof object.timestamp === "string") message.timestamp = parseInt(object.timestamp, 10);else if (typeof object.timestamp === "number") message.timestamp = object.timestamp;else if (typeof object.timestamp === "object") message.timestamp = new $util$8.LongBits(object.timestamp.low >>> 0, object.timestamp.high >>> 0).toNumber();

      switch (object.status) {
        case "ONLINE":
        case 0:
          message.status = 0;
          break;

        case "OFFLINE":
        case 1:
          message.status = 1;
          break;
      }

      return message;
    };
    /**
     * Creates a plain object from a Msg message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Websocket.Msg
     * @static
     * @param {Websocket.Msg} message Msg
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Msg.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        if ($util$8.Long) {
          let long = new $util$8.Long(0, 0, false);
          object.timestamp = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
        } else object.timestamp = options.longs === String ? "0" : 0;

        object.status = options.enums === String ? "ONLINE" : 0;
      }

      if (message.timestamp != null && message.hasOwnProperty("timestamp")) if (typeof message.timestamp === "number") object.timestamp = options.longs === String ? String(message.timestamp) : message.timestamp;else object.timestamp = options.longs === String ? $util$8.Long.prototype.toString.call(message.timestamp) : options.longs === Number ? new $util$8.LongBits(message.timestamp.low >>> 0, message.timestamp.high >>> 0).toNumber() : message.timestamp;
      if (message.status != null && message.hasOwnProperty("status")) object.status = options.enums === String ? $root$9.Websocket.Msg.Status[message.status] : message.status;
      return object;
    };
    /**
     * Converts this Msg to JSON.
     * @function toJSON
     * @memberof Websocket.Msg
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Msg.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * Status enum.
     * @name Websocket.Msg.Status
     * @enum {number}
     * @property {number} ONLINE=0 ONLINE value
     * @property {number} OFFLINE=1 OFFLINE value
     */


    Msg.Status = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "ONLINE"] = 0;
      values[valuesById[1] = "OFFLINE"] = 1;
      return values;
    }();

    return Msg;
  }();

  return Websocket;
})();

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

const $Reader$a = $protobuf.Reader,
      $Writer$a = $protobuf.Writer,
      $util$9 = $protobuf.util; // Exported root namespace

const $root$a = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
const Log = $root$a.Log = /*#__PURE__*/(() => {
  /**
   * Namespace Log.
   * @exports Log
   * @namespace
   */
  const Log = {};

  Log.Data = /*#__PURE__*/function () {
    /**
     * Properties of a Data.
     * @memberof Log
     * @interface IData
     * @property {string|null} [source] Data source
     * @property {Log.Data.Level|null} [level] Data level
     * @property {string|null} [user] Data user
     * @property {string|null} [caller] Data caller
     * @property {string|null} [time] Data time
     * @property {string|null} [msg] Data msg
     * @property {string|null} [deviceToken] Data deviceToken
     * @property {string|null} [websocketID] Data websocketID
     * @property {string|null} [webrtcID] Data webrtcID
     * @property {number|null} [count] Data count
     * @property {string|null} [hostname] Data hostname
     */

    /**
     * Constructs a new Data.
     * @memberof Log
     * @classdesc Represents a Data.
     * @implements IData
     * @constructor
     * @param {Log.IData=} [properties] Properties to set
     */
    function Data(properties) {
      if (properties) for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i) if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
    /**
     * Data source.
     * @member {string} source
     * @memberof Log.Data
     * @instance
     */


    Data.prototype.source = "";
    /**
     * Data level.
     * @member {Log.Data.Level} level
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.level = 0;
    /**
     * Data user.
     * @member {string} user
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.user = "";
    /**
     * Data caller.
     * @member {string} caller
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.caller = "";
    /**
     * Data time.
     * @member {string} time
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.time = "";
    /**
     * Data msg.
     * @member {string} msg
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.msg = "";
    /**
     * Data deviceToken.
     * @member {string} deviceToken
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.deviceToken = "";
    /**
     * Data websocketID.
     * @member {string} websocketID
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.websocketID = "";
    /**
     * Data webrtcID.
     * @member {string} webrtcID
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.webrtcID = "";
    /**
     * Data count.
     * @member {number} count
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.count = 0;
    /**
     * Data hostname.
     * @member {string} hostname
     * @memberof Log.Data
     * @instance
     */

    Data.prototype.hostname = "";
    /**
     * Creates a new Data instance using the specified properties.
     * @function create
     * @memberof Log.Data
     * @static
     * @param {Log.IData=} [properties] Properties to set
     * @returns {Log.Data} Data instance
     */

    Data.create = function create(properties) {
      return new Data(properties);
    };
    /**
     * Encodes the specified Data message. Does not implicitly {@link Log.Data.verify|verify} messages.
     * @function encode
     * @memberof Log.Data
     * @static
     * @param {Log.IData} message Data message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Data.encode = function encode(message, writer) {
      if (!writer) writer = $Writer$a.create();
      if (message.source != null && Object.hasOwnProperty.call(message, "source")) writer.uint32(
      /* id 1, wireType 2 =*/
      10).string(message.source);
      if (message.level != null && Object.hasOwnProperty.call(message, "level")) writer.uint32(
      /* id 2, wireType 0 =*/
      16).int32(message.level);
      if (message.user != null && Object.hasOwnProperty.call(message, "user")) writer.uint32(
      /* id 3, wireType 2 =*/
      26).string(message.user);
      if (message.caller != null && Object.hasOwnProperty.call(message, "caller")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).string(message.caller);
      if (message.time != null && Object.hasOwnProperty.call(message, "time")) writer.uint32(
      /* id 5, wireType 2 =*/
      42).string(message.time);
      if (message.msg != null && Object.hasOwnProperty.call(message, "msg")) writer.uint32(
      /* id 6, wireType 2 =*/
      50).string(message.msg);
      if (message.deviceToken != null && Object.hasOwnProperty.call(message, "deviceToken")) writer.uint32(
      /* id 7, wireType 2 =*/
      58).string(message.deviceToken);
      if (message.websocketID != null && Object.hasOwnProperty.call(message, "websocketID")) writer.uint32(
      /* id 8, wireType 2 =*/
      66).string(message.websocketID);
      if (message.webrtcID != null && Object.hasOwnProperty.call(message, "webrtcID")) writer.uint32(
      /* id 9, wireType 2 =*/
      74).string(message.webrtcID);
      if (message.count != null && Object.hasOwnProperty.call(message, "count")) writer.uint32(
      /* id 10, wireType 0 =*/
      80).uint32(message.count);
      if (message.hostname != null && Object.hasOwnProperty.call(message, "hostname")) writer.uint32(
      /* id 11, wireType 2 =*/
      90).string(message.hostname);
      return writer;
    };
    /**
     * Encodes the specified Data message, length delimited. Does not implicitly {@link Log.Data.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Log.Data
     * @static
     * @param {Log.IData} message Data message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Data.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Data message from the specified reader or buffer.
     * @function decode
     * @memberof Log.Data
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Log.Data} Data
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Data.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader$a)) reader = $Reader$a.create(reader);
      let end = length === undefined ? reader.len : reader.pos + length,
          message = new $root$a.Log.Data();

      while (reader.pos < end) {
        let tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.source = reader.string();
            break;

          case 2:
            message.level = reader.int32();
            break;

          case 3:
            message.user = reader.string();
            break;

          case 4:
            message.caller = reader.string();
            break;

          case 5:
            message.time = reader.string();
            break;

          case 6:
            message.msg = reader.string();
            break;

          case 7:
            message.deviceToken = reader.string();
            break;

          case 8:
            message.websocketID = reader.string();
            break;

          case 9:
            message.webrtcID = reader.string();
            break;

          case 10:
            message.count = reader.uint32();
            break;

          case 11:
            message.hostname = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Data message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Log.Data
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Log.Data} Data
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Data.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader$a)) reader = new $Reader$a(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Data message.
     * @function verify
     * @memberof Log.Data
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Data.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      if (message.source != null && message.hasOwnProperty("source")) if (!$util$9.isString(message.source)) return "source: string expected";
      if (message.level != null && message.hasOwnProperty("level")) switch (message.level) {
        default:
          return "level: enum value expected";

        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
          break;
      }
      if (message.user != null && message.hasOwnProperty("user")) if (!$util$9.isString(message.user)) return "user: string expected";
      if (message.caller != null && message.hasOwnProperty("caller")) if (!$util$9.isString(message.caller)) return "caller: string expected";
      if (message.time != null && message.hasOwnProperty("time")) if (!$util$9.isString(message.time)) return "time: string expected";
      if (message.msg != null && message.hasOwnProperty("msg")) if (!$util$9.isString(message.msg)) return "msg: string expected";
      if (message.deviceToken != null && message.hasOwnProperty("deviceToken")) if (!$util$9.isString(message.deviceToken)) return "deviceToken: string expected";
      if (message.websocketID != null && message.hasOwnProperty("websocketID")) if (!$util$9.isString(message.websocketID)) return "websocketID: string expected";
      if (message.webrtcID != null && message.hasOwnProperty("webrtcID")) if (!$util$9.isString(message.webrtcID)) return "webrtcID: string expected";
      if (message.count != null && message.hasOwnProperty("count")) if (!$util$9.isInteger(message.count)) return "count: integer expected";
      if (message.hostname != null && message.hasOwnProperty("hostname")) if (!$util$9.isString(message.hostname)) return "hostname: string expected";
      return null;
    };
    /**
     * Creates a Data message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Log.Data
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Log.Data} Data
     */


    Data.fromObject = function fromObject(object) {
      if (object instanceof $root$a.Log.Data) return object;
      let message = new $root$a.Log.Data();
      if (object.source != null) message.source = String(object.source);

      switch (object.level) {
        case "Debug":
        case 0:
          message.level = 0;
          break;

        case "Info":
        case 1:
          message.level = 1;
          break;

        case "Warn":
        case 2:
          message.level = 2;
          break;

        case "Error":
        case 3:
          message.level = 3;
          break;

        case "Fatal":
        case 4:
          message.level = 4;
          break;
      }

      if (object.user != null) message.user = String(object.user);
      if (object.caller != null) message.caller = String(object.caller);
      if (object.time != null) message.time = String(object.time);
      if (object.msg != null) message.msg = String(object.msg);
      if (object.deviceToken != null) message.deviceToken = String(object.deviceToken);
      if (object.websocketID != null) message.websocketID = String(object.websocketID);
      if (object.webrtcID != null) message.webrtcID = String(object.webrtcID);
      if (object.count != null) message.count = object.count >>> 0;
      if (object.hostname != null) message.hostname = String(object.hostname);
      return message;
    };
    /**
     * Creates a plain object from a Data message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Log.Data
     * @static
     * @param {Log.Data} message Data
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Data.toObject = function toObject(message, options) {
      if (!options) options = {};
      let object = {};

      if (options.defaults) {
        object.source = "";
        object.level = options.enums === String ? "Debug" : 0;
        object.user = "";
        object.caller = "";
        object.time = "";
        object.msg = "";
        object.deviceToken = "";
        object.websocketID = "";
        object.webrtcID = "";
        object.count = 0;
        object.hostname = "";
      }

      if (message.source != null && message.hasOwnProperty("source")) object.source = message.source;
      if (message.level != null && message.hasOwnProperty("level")) object.level = options.enums === String ? $root$a.Log.Data.Level[message.level] : message.level;
      if (message.user != null && message.hasOwnProperty("user")) object.user = message.user;
      if (message.caller != null && message.hasOwnProperty("caller")) object.caller = message.caller;
      if (message.time != null && message.hasOwnProperty("time")) object.time = message.time;
      if (message.msg != null && message.hasOwnProperty("msg")) object.msg = message.msg;
      if (message.deviceToken != null && message.hasOwnProperty("deviceToken")) object.deviceToken = message.deviceToken;
      if (message.websocketID != null && message.hasOwnProperty("websocketID")) object.websocketID = message.websocketID;
      if (message.webrtcID != null && message.hasOwnProperty("webrtcID")) object.webrtcID = message.webrtcID;
      if (message.count != null && message.hasOwnProperty("count")) object.count = message.count;
      if (message.hostname != null && message.hasOwnProperty("hostname")) object.hostname = message.hostname;
      return object;
    };
    /**
     * Converts this Data to JSON.
     * @function toJSON
     * @memberof Log.Data
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Data.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };
    /**
     * Level enum.
     * @name Log.Data.Level
     * @enum {number}
     * @property {number} Debug=0 Debug value
     * @property {number} Info=1 Info value
     * @property {number} Warn=2 Warn value
     * @property {number} Error=3 Error value
     * @property {number} Fatal=4 Fatal value
     */


    Data.Level = /*#__PURE__*/function () {
      const valuesById = {},
            values = /*#__PURE__*/Object.create(valuesById);
      values[valuesById[0] = "Debug"] = 0;
      values[valuesById[1] = "Info"] = 1;
      values[valuesById[2] = "Warn"] = 2;
      values[valuesById[3] = "Error"] = 3;
      values[valuesById[4] = "Fatal"] = 4;
      return values;
    }();

    return Data;
  }();

  return Log;
})();

exports.Event = Event;
exports.Facedetection = Facedetection;
exports.Livestream = Livestream;
exports.Log = Log;
exports.Peopletrack = Peopletrack;
exports.Playback = Playback;
exports.Recognition = Recognition;
exports.Rule = Rule;
exports.Shadow = Shadow;
exports.Webrtc = Webrtc;
exports.Websocket = Websocket;
//# sourceMappingURL=vvtk-protobuf.cjs.development.js.map
