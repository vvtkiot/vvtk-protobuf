
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./vvtk-protobuf.cjs.production.min.js')
} else {
  module.exports = require('./vvtk-protobuf.cjs.development.js')
}
